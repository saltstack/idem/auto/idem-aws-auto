"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    creator_request_id: str = None,
    description: str = None,
    tags: List = None,
    properties: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a public namespace based on DNS, which is visible on the internet. The namespace defines your service naming scheme. For example, if you name your namespace example.com and name your service backend, the resulting DNS name for the service is backend.example.com. You can discover instances that were registered with a public DNS namespace by using either a DiscoverInstances request or using DNS. For the current quota on the number of namespaces that you can create using the same account, see Cloud Map quotas in the Cloud Map Developer Guide.

    Args:
        name(str): The name that you want to assign to this namespace.
        creator_request_id(str, optional): A unique string that identifies the request and that allows failed CreatePublicDnsNamespace requests to be retried without the risk of running the operation twice. CreatorRequestId can be any unique string (for example, a date/timestamp). Defaults to None.
        description(str, optional): A description for the namespace. Defaults to None.
        tags(List, optional): The tags to add to the namespace. Each tag consists of a key and an optional value that you define. Tags keys can be up to 128 characters in length, and tag values can be up to 256 characters in length. Defaults to None.
        properties(Dict, optional): Properties for the public DNS namespace. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicediscovery.public_dns_namespace.create(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicediscovery.public_dns_namespace.create name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicediscovery",
        operation="create_public_dns_namespace",
        op_kwargs={
            "Name": name,
            "CreatorRequestId": creator_request_id,
            "Description": description,
            "Tags": tags,
            "Properties": properties,
        },
    )


async def update(
    hub, ctx, id_: str, namespace: Dict, updater_request_id: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a public DNS namespace.

    Args:
        id_(str): The ID of the namespace being updated.
        updater_request_id(str, optional): A unique string that identifies the request and that allows failed UpdatePublicDnsNamespace requests to be retried without the risk of running the operation twice. UpdaterRequestId can be any unique string (for example, a date/timestamp). Defaults to None.
        namespace(Dict): Updated properties for the public DNS namespace.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicediscovery.public_dns_namespace.update(
                ctx, id_=value, namespace=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicediscovery.public_dns_namespace.update id_=value, namespace=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicediscovery",
        operation="update_public_dns_namespace",
        op_kwargs={
            "Id": id_,
            "UpdaterRequestId": updater_request_id,
            "Namespace": namespace,
        },
    )
