"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def restore(hub, ctx, snapshot_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Restores a directory using an existing directory snapshot. When you restore a directory from a snapshot, any changes made to the directory after the snapshot date are overwritten. This action returns as soon as the restore operation is initiated. You can monitor the progress of the restore operation by calling the DescribeDirectories operation with the directory identifier. When the DirectoryDescription.Stage value changes to Active, the restore operation is complete.

    Args:
        snapshot_id(str): The identifier of the snapshot to restore from.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ds.from_snapshot.restore(ctx, snapshot_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ds.from_snapshot.restore snapshot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ds",
        operation="restore_from_snapshot",
        op_kwargs={"SnapshotId": snapshot_id},
    )
