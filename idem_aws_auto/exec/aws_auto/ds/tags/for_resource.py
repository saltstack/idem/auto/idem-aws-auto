"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, resource_id: str, next_token: str = None, limit: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all tags on a directory.

    Args:
        resource_id(str): Identifier (ID) of the directory for which you want to retrieve tags.
        next_token(str, optional): Reserved for future use. Defaults to None.
        limit(int, optional): Reserved for future use. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ds.tags.for_resource.list(ctx, resource_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ds.tags.for_resource.list resource_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ds",
        operation="list_tags_for_resource",
        op_kwargs={"ResourceId": resource_id, "NextToken": next_token, "Limit": limit},
    )
