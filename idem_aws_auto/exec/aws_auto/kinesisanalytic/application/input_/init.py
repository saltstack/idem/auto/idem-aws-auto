"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def add(
    hub, ctx, application_name: str, current_application_version_id: int, input_: Dict
) -> Dict:
    r"""
    **Autogenerated function**

     This documentation is for version 1 of the Amazon Kinesis Data Analytics API, which only supports SQL applications. Version 2 of the API supports SQL and Java applications. For more information about version 2, see Amazon Kinesis Data Analytics API V2 Documentation.   Adds a streaming source to your Amazon Kinesis application. For conceptual information, see Configuring Application Input.  You can add a streaming source either when you create an application or you can use this operation to add a streaming source after you create an application. For more information, see CreateApplication. Any configuration update, including adding a streaming source using this operation, results in a new version of the application. You can use the DescribeApplication operation to find the current application version.  This operation requires permissions to perform the kinesisanalytics:AddApplicationInput action.

    Args:
        application_name(str): Name of your existing Amazon Kinesis Analytics application to which you want to add the streaming source.
        current_application_version_id(int): Current version of your Amazon Kinesis Analytics application. You can use the DescribeApplication operation to find the current application version.
        input_(Dict): The Input to add.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kinesisanalytic.application.input_.init.add(
                ctx, application_name=value, current_application_version_id=value, input_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kinesisanalytic.application.input_.init.add application_name=value, current_application_version_id=value, input_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kinesisanalytics",
        operation="add_application_input",
        op_kwargs={
            "ApplicationName": application_name,
            "CurrentApplicationVersionId": current_application_version_id,
            "Input": input_,
        },
    )
