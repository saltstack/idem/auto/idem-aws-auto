"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub, ctx, event_arn: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of accounts in the organization from AWS Organizations that are affected by the provided event. For more information about the different types of AWS Health events, see Event.  Before you can call this operation, you must first enable AWS Health to work with AWS Organizations. To do this, call the EnableHealthServiceAccessForOrganization operation from your organization's management account.  This API operation uses pagination. Specify the nextToken parameter in the next request to return more results.

    Args:
        event_arn(str): The unique identifier for the event. The event ARN has the arn:aws:health:event-region::event/SERVICE/EVENT_TYPE_CODE/EVENT_TYPE_PLUS_ID  format. For example, an event ARN might look like the following:  arn:aws:health:us-east-1::event/EC2/EC2_INSTANCE_RETIREMENT_SCHEDULED/EC2_INSTANCE_RETIREMENT_SCHEDULED_ABC123-DEF456 .
        next_token(str, optional): If the results of a search are large, only a portion of the results are returned, and a nextToken pagination token is returned in the response. To retrieve the next batch of results, reissue the search request and include the returned token. When all results have been returned, the response does not contain a pagination token value. Defaults to None.
        max_results(int, optional): The maximum number of items to return in one batch, between 10 and 100, inclusive. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.health.affected.account_for_organization.describe(
                ctx, event_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.health.affected.account_for_organization.describe event_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="health",
        operation="describe_affected_accounts_for_organization",
        op_kwargs={
            "eventArn": event_arn,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
