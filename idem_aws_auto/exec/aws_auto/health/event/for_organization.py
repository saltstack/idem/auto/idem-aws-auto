"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub,
    ctx,
    filter_: Dict = None,
    next_token: str = None,
    max_results: int = None,
    locale: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about events across your organization in AWS Organizations. You can use thefilters parameter to specify the events that you want to return. Events are returned in a summary form and don't include the affected accounts, detailed description, any additional metadata that depends on the event type, or any affected resources. To retrieve that information, use the following operations:    DescribeAffectedAccountsForOrganization     DescribeEventDetailsForOrganization     DescribeAffectedEntitiesForOrganization    If you don't specify a filter, the DescribeEventsForOrganizations returns all events across your organization. Results are sorted by lastModifiedTime, starting with the most recent event.  For more information about the different types of AWS Health events, see Event. Before you can call this operation, you must first enable AWS Health to work with AWS Organizations. To do this, call the EnableHealthServiceAccessForOrganization operation from your organization's management account.  This API operation uses pagination. Specify the nextToken parameter in the next request to return more results.

    Args:
        filter_(Dict, optional): Values to narrow the results returned. Defaults to None.
        next_token(str, optional): If the results of a search are large, only a portion of the results are returned, and a nextToken pagination token is returned in the response. To retrieve the next batch of results, reissue the search request and include the returned token. When all results have been returned, the response does not contain a pagination token value. Defaults to None.
        max_results(int, optional): The maximum number of items to return in one batch, between 10 and 100, inclusive. Defaults to None.
        locale(str, optional): The locale (language) to return information in. English (en) is the default and the only supported value at this time. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.health.event.for_organization.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.health.event.for_organization.describe
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="health",
        operation="describe_events_for_organization",
        op_kwargs={
            "filter": filter_,
            "nextToken": next_token,
            "maxResults": max_results,
            "locale": locale,
        },
    )
