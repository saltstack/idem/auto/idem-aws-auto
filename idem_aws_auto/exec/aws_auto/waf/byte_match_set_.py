"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__virtualname__ = "byte_match_set"


async def create(hub, ctx, name: str, change_token: str) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Creates a ByteMatchSet. You then use UpdateByteMatchSet to identify the part of a web request that you want AWS WAF to inspect, such as the values of the User-Agent header or the query string. For example, you can create a ByteMatchSet that matches any requests with User-Agent headers that contain the string BadBot. You can then configure AWS WAF to reject those requests. To create and configure a ByteMatchSet, perform the following steps:   Use GetChangeToken to get the change token that you provide in the ChangeToken parameter of a CreateByteMatchSet request.   Submit a CreateByteMatchSet request.   Use GetChangeToken to get the change token that you provide in the ChangeToken parameter of an UpdateByteMatchSet request.   Submit an UpdateByteMatchSet request to specify the part of the request that you want AWS WAF to inspect (for example, the header or the URI) and the value that you want AWS WAF to watch for.   For more information about how to use the AWS WAF API to allow or block HTTP requests, see the AWS WAF Developer Guide.

    Args:
        name(str): A friendly name or description of the ByteMatchSet. You can't change Name after you create a ByteMatchSet.
        change_token(str): The value returned by the most recent call to GetChangeToken.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.byte_match_set_.create(ctx, name=value, change_token=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.byte_match_set.create name=value, change_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="create_byte_match_set",
        op_kwargs={"Name": name, "ChangeToken": change_token},
    )


async def delete(hub, ctx, byte_match_set_id: str, change_token: str) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Permanently deletes a ByteMatchSet. You can't delete a ByteMatchSet if it's still used in any Rules or if it still includes any ByteMatchTuple objects (any filters). If you just want to remove a ByteMatchSet from a Rule, use UpdateRule. To permanently delete a ByteMatchSet, perform the following steps:   Update the ByteMatchSet to remove filters, if any. For more information, see UpdateByteMatchSet.   Use GetChangeToken to get the change token that you provide in the ChangeToken parameter of a DeleteByteMatchSet request.   Submit a DeleteByteMatchSet request.

    Args:
        byte_match_set_id(str): The ByteMatchSetId of the ByteMatchSet that you want to delete. ByteMatchSetId is returned by CreateByteMatchSet and by ListByteMatchSets.
        change_token(str): The value returned by the most recent call to GetChangeToken.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.byte_match_set_.delete(
                ctx, byte_match_set_id=value, change_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.byte_match_set.delete byte_match_set_id=value, change_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="delete_byte_match_set",
        op_kwargs={"ByteMatchSetId": byte_match_set_id, "ChangeToken": change_token},
    )


async def get(hub, ctx, byte_match_set_id: str) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Returns the ByteMatchSet specified by ByteMatchSetId.

    Args:
        byte_match_set_id(str): The ByteMatchSetId of the ByteMatchSet that you want to get. ByteMatchSetId is returned by CreateByteMatchSet and by ListByteMatchSets.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.byte_match_set_.get(ctx, byte_match_set_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.byte_match_set.get byte_match_set_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="get_byte_match_set",
        op_kwargs={"ByteMatchSetId": byte_match_set_id},
    )


async def list_all(hub, ctx, next_marker: str = None, limit: int = None) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Returns an array of ByteMatchSetSummary objects.

    Args:
        next_marker(str, optional): If you specify a value for Limit and you have more ByteMatchSets than the value of Limit, AWS WAF returns a NextMarker value in the response that allows you to list another group of ByteMatchSets. For the second and subsequent ListByteMatchSets requests, specify the value of NextMarker from the previous response to get information about another batch of ByteMatchSets. Defaults to None.
        limit(int, optional): Specifies the number of ByteMatchSet objects that you want AWS WAF to return for this request. If you have more ByteMatchSets objects than the number you specify for Limit, the response includes a NextMarker value that you can use to get another batch of ByteMatchSet objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.byte_match_set_.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.byte_match_set.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="list_byte_match_sets",
        op_kwargs={"NextMarker": next_marker, "Limit": limit},
    )


async def update(
    hub, ctx, byte_match_set_id: str, change_token: str, updates: List
) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Inserts or deletes ByteMatchTuple objects (filters) in a ByteMatchSet. For each ByteMatchTuple object, you specify the following values:    Whether to insert or delete the object from the array. If you want to change a ByteMatchSetUpdate object, you delete the existing object and add a new one.   The part of a web request that you want AWS WAF to inspect, such as a query string or the value of the User-Agent header.    The bytes (typically a string that corresponds with ASCII characters) that you want AWS WAF to look for. For more information, including how you specify the values for the AWS WAF API and the AWS CLI or SDKs, see TargetString in the ByteMatchTuple data type.    Where to look, such as at the beginning or the end of a query string.   Whether to perform any conversions on the request, such as converting it to lowercase, before inspecting it for the specified string.   For example, you can add a ByteMatchSetUpdate object that matches web requests in which User-Agent headers contain the string BadBot. You can then configure AWS WAF to block those requests. To create and configure a ByteMatchSet, perform the following steps:   Create a ByteMatchSet. For more information, see CreateByteMatchSet.   Use GetChangeToken to get the change token that you provide in the ChangeToken parameter of an UpdateByteMatchSet request.   Submit an UpdateByteMatchSet request to specify the part of the request that you want AWS WAF to inspect (for example, the header or the URI) and the value that you want AWS WAF to watch for.   For more information about how to use the AWS WAF API to allow or block HTTP requests, see the AWS WAF Developer Guide.

    Args:
        byte_match_set_id(str): The ByteMatchSetId of the ByteMatchSet that you want to update. ByteMatchSetId is returned by CreateByteMatchSet and by ListByteMatchSets.
        change_token(str): The value returned by the most recent call to GetChangeToken.
        updates(List): An array of ByteMatchSetUpdate objects that you want to insert into or delete from a ByteMatchSet. For more information, see the applicable data types:    ByteMatchSetUpdate: Contains Action and ByteMatchTuple     ByteMatchTuple: Contains FieldToMatch, PositionalConstraint, TargetString, and TextTransformation     FieldToMatch: Contains Data and Type   .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.byte_match_set_.update(
                ctx, byte_match_set_id=value, change_token=value, updates=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.byte_match_set.update byte_match_set_id=value, change_token=value, updates=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="update_byte_match_set",
        op_kwargs={
            "ByteMatchSetId": byte_match_set_id,
            "ChangeToken": change_token,
            "Updates": updates,
        },
    )
