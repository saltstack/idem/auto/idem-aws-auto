"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub, ctx, web_acl_id: str, s3_bucket_name: str, ignore_unsupported_type: bool
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an AWS CloudFormation WAFV2 template for the specified web ACL in the specified Amazon S3 bucket. Then, in CloudFormation, you create a stack from the template, to create the web ACL and its resources in AWS WAFV2. Use this to migrate your AWS WAF Classic web ACL to the latest version of AWS WAF. This is part of a larger migration procedure for web ACLs from AWS WAF Classic to the latest version of AWS WAF. For the full procedure, including caveats and manual steps to complete the migration and switch over to the new web ACL, see Migrating your AWS WAF Classic resources to AWS WAF in the AWS WAF Developer Guide.

    Args:
        web_acl_id(str): The UUID of the WAF Classic web ACL that you want to migrate to WAF v2.
        s3_bucket_name(str): The name of the Amazon S3 bucket to store the CloudFormation template in. The S3 bucket must be configured as follows for the migration:    The bucket name must start with aws-waf-migration-. For example, aws-waf-migration-my-web-acl.   The bucket must be in the Region where you are deploying the template. For example, for a web ACL in us-west-2, you must use an Amazon S3 bucket in us-west-2 and you must deploy the template stack to us-west-2.    The bucket policies must permit the migration process to write data. For listings of the bucket policies, see the Examples section.   .
        ignore_unsupported_type(bool): Indicates whether to exclude entities that can't be migrated or to stop the migration. Set this to true to ignore unsupported entities in the web ACL during the migration. Otherwise, if AWS WAF encounters unsupported entities, it stops the process and throws an exception. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf.web_acl.migration_stack.create(
                ctx, web_acl_id=value, s3_bucket_name=value, ignore_unsupported_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf.web_acl.migration_stack.create web_acl_id=value, s3_bucket_name=value, ignore_unsupported_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf",
        operation="create_web_acl_migration_stack",
        op_kwargs={
            "WebACLId": web_acl_id,
            "S3BucketName": s3_bucket_name,
            "IgnoreUnsupportedType": ignore_unsupported_type,
        },
    )
