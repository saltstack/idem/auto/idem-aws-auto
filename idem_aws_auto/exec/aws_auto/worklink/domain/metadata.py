"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(
    hub, ctx, fleet_arn: str, domain_name: str, display_name: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates domain metadata, such as DisplayName.

    Args:
        fleet_arn(str): The ARN of the fleet.
        domain_name(str): The name of the domain.
        display_name(str, optional): The name to display. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.worklink.domain.metadata.update(
                ctx, fleet_arn=value, domain_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.worklink.domain.metadata.update fleet_arn=value, domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="worklink",
        operation="update_domain_metadata",
        op_kwargs={
            "FleetArn": fleet_arn,
            "DomainName": domain_name,
            "DisplayName": display_name,
        },
    )
