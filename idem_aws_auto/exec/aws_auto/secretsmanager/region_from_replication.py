"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def remove(hub, ctx, secret_id: str, remove_replica_regions: List) -> Dict:
    r"""
    **Autogenerated function**

    Remove regions from replication.

    Args:
        secret_id(str): Remove a secret by SecretId from replica Regions.
        remove_replica_regions(List): Remove replication from specific Regions.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.secretsmanager.region_from_replication.remove(
                ctx, secret_id=value, remove_replica_regions=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.secretsmanager.region_from_replication.remove secret_id=value, remove_replica_regions=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="secretsmanager",
        operation="remove_regions_from_replication",
        op_kwargs={
            "SecretId": secret_id,
            "RemoveReplicaRegions": remove_replica_regions,
        },
    )
