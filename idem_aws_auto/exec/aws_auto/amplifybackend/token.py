"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx, app_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Generates a one-time challenge code to authenticate a user into your Amplify Admin UI.

    Args:
        app_id(str): The app ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplifybackend.token.create(ctx, app_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplifybackend.token.create app_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplifybackend",
        operation="create_token",
        op_kwargs={"AppId": app_id},
    )


async def delete(hub, ctx, app_id: str, session_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the challenge token based on the given appId and sessionId.

    Args:
        app_id(str): The app ID.
        session_id(str): The session ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplifybackend.token.delete(ctx, app_id=value, session_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplifybackend.token.delete app_id=value, session_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplifybackend",
        operation="delete_token",
        op_kwargs={"AppId": app_id, "SessionId": session_id},
    )


async def get(hub, ctx, app_id: str, session_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets the challenge token based on the given appId and sessionId.

    Args:
        app_id(str): The app ID.
        session_id(str): The session ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplifybackend.token.get(ctx, app_id=value, session_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplifybackend.token.get app_id=value, session_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplifybackend",
        operation="get_token",
        op_kwargs={"AppId": app_id, "SessionId": session_id},
    )
