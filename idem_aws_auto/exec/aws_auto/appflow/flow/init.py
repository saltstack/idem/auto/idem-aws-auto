"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    flow_name: str,
    trigger_config: Dict,
    source_flow_config: Dict,
    destination_flow_config_list: List,
    tasks: List,
    description: str = None,
    kms_arn: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Enables your application to create a new flow using Amazon AppFlow. You must create a connector profile before calling this API. Please note that the Request Syntax below shows syntax for multiple destinations, however, you can only transfer data to one item in this list at a time. Amazon AppFlow does not currently support flows to multiple destinations at once.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .
        description(str, optional):  A description of the flow you want to create. . Defaults to None.
        kms_arn(str, optional):  The ARN (Amazon Resource Name) of the Key Management Service (KMS) key you provide for encryption. This is required if you do not want to use the Amazon AppFlow-managed KMS key. If you don't provide anything here, Amazon AppFlow uses the Amazon AppFlow-managed KMS key. . Defaults to None.
        trigger_config(Dict):  The trigger settings that determine how and when the flow runs. .
        source_flow_config(Dict):  The configuration that controls how Amazon AppFlow retrieves data from the source connector. .
        destination_flow_config_list(List):  The configuration that controls how Amazon AppFlow places data in the destination connector. .
        tasks(List):  A list of tasks that Amazon AppFlow performs while transferring the data in the flow run. .
        tags(Dict, optional):  The tags used to organize, track, or control access for your flow. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.create(
                ctx,
                flow_name=value,
                trigger_config=value,
                source_flow_config=value,
                destination_flow_config_list=value,
                tasks=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.create flow_name=value, trigger_config=value, source_flow_config=value, destination_flow_config_list=value, tasks=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="create_flow",
        op_kwargs={
            "flowName": flow_name,
            "description": description,
            "kmsArn": kms_arn,
            "triggerConfig": trigger_config,
            "sourceFlowConfig": source_flow_config,
            "destinationFlowConfigList": destination_flow_config_list,
            "tasks": tasks,
            "tags": tags,
        },
    )


async def delete(hub, ctx, flow_name: str, force_delete: bool = None) -> Dict:
    r"""
    **Autogenerated function**

     Enables your application to delete an existing flow. Before deleting the flow, Amazon AppFlow validates the request by checking the flow configuration and status. You can delete flows one at a time.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .
        force_delete(bool, optional):  Indicates whether Amazon AppFlow should delete the flow, even if it is currently in use. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.delete(ctx, flow_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.delete flow_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="delete_flow",
        op_kwargs={"flowName": flow_name, "forceDelete": force_delete},
    )


async def describe(hub, ctx, flow_name: str) -> Dict:
    r"""
    **Autogenerated function**

     Provides a description of the specified flow.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.describe(ctx, flow_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.describe flow_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="describe_flow",
        op_kwargs={"flowName": flow_name},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

     Lists all of the flows associated with your account.

    Args:
        max_results(int, optional):  Specifies the maximum number of items that should be returned in the result set. . Defaults to None.
        next_token(str, optional):  The pagination token for next page of data. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="list_flows",
        op_kwargs={"maxResults": max_results, "nextToken": next_token},
    )


async def start(hub, ctx, flow_name: str) -> Dict:
    r"""
    **Autogenerated function**

     Activates an existing flow. For on-demand flows, this operation runs the flow immediately. For schedule and event-triggered flows, this operation activates the flow.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.start(ctx, flow_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.start flow_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="start_flow",
        op_kwargs={"flowName": flow_name},
    )


async def stop(hub, ctx, flow_name: str) -> Dict:
    r"""
    **Autogenerated function**

     Deactivates the existing flow. For on-demand flows, this operation returns an unsupportedOperationException error message. For schedule and event-triggered flows, this operation deactivates the flow.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.stop(ctx, flow_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.stop flow_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="stop_flow",
        op_kwargs={"flowName": flow_name},
    )


async def update(
    hub,
    ctx,
    flow_name: str,
    trigger_config: Dict,
    destination_flow_config_list: List,
    tasks: List,
    description: str = None,
    source_flow_config: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Updates an existing flow.

    Args:
        flow_name(str):  The specified name of the flow. Spaces are not allowed. Use underscores (_) or hyphens (-) only. .
        description(str, optional):  A description of the flow. . Defaults to None.
        trigger_config(Dict):  The trigger settings that determine how and when the flow runs. .
        source_flow_config(Dict, optional):  Contains information about the configuration of the source connector used in the flow. . Defaults to None.
        destination_flow_config_list(List):  The configuration that controls how Amazon AppFlow transfers data to the destination connector. .
        tasks(List):  A list of tasks that Amazon AppFlow performs while transferring the data in the flow run. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appflow.flow.init.update(
                ctx,
                flow_name=value,
                trigger_config=value,
                destination_flow_config_list=value,
                tasks=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appflow.flow.init.update flow_name=value, trigger_config=value, destination_flow_config_list=value, tasks=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appflow",
        operation="update_flow",
        op_kwargs={
            "flowName": flow_name,
            "description": description,
            "triggerConfig": trigger_config,
            "sourceFlowConfig": source_flow_config,
            "destinationFlowConfigList": destination_flow_config_list,
            "tasks": tasks,
        },
    )
