"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    rest_api_id: str,
    stage_name: str = None,
    stage_description: str = None,
    description: str = None,
    cache_cluster_enabled: bool = None,
    cache_cluster_size: str = None,
    variables: Dict = None,
    canary_settings: Dict = None,
    tracing_enabled: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Deployment resource, which makes a specified RestApi callable over the internet.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        stage_name(str, optional): The name of the Stage resource for the Deployment resource to create. Defaults to None.
        stage_description(str, optional): The description of the Stage resource for the Deployment resource to create. Defaults to None.
        description(str, optional): The description for the Deployment resource to create. Defaults to None.
        cache_cluster_enabled(bool, optional): Enables a cache cluster for the Stage resource specified in the input. Defaults to None.
        cache_cluster_size(str, optional): Specifies the cache cluster size for the Stage resource specified in the input, if a cache cluster is enabled. Defaults to None.
        variables(Dict, optional): A map that defines the stage variables for the Stage resource that is associated with the new deployment. Variable names can have alphanumeric and underscore characters, and the values must match [A-Za-z0-9-._~:/?#&=,]+. Defaults to None.
        canary_settings(Dict, optional): The input configuration for the canary deployment when the deployment is a canary release deployment. . Defaults to None.
        tracing_enabled(bool, optional): Specifies whether active tracing with X-ray is enabled for the Stage. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.deployment.create(ctx, rest_api_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.deployment.create rest_api_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="create_deployment",
        op_kwargs={
            "restApiId": rest_api_id,
            "stageName": stage_name,
            "stageDescription": stage_description,
            "description": description,
            "cacheClusterEnabled": cache_cluster_enabled,
            "cacheClusterSize": cache_cluster_size,
            "variables": variables,
            "canarySettings": canary_settings,
            "tracingEnabled": tracing_enabled,
        },
    )


async def delete(hub, ctx, rest_api_id: str, deployment_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a Deployment resource. Deleting a deployment will only succeed if there are no Stage resources associated with it.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        deployment_id(str): [Required] The identifier of the Deployment resource to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.deployment.delete(
                ctx, rest_api_id=value, deployment_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.deployment.delete rest_api_id=value, deployment_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="delete_deployment",
        op_kwargs={"restApiId": rest_api_id, "deploymentId": deployment_id},
    )


async def get(
    hub, ctx, rest_api_id: str, deployment_id: str, embed: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a Deployment resource.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        deployment_id(str): [Required] The identifier of the Deployment resource to get information about.
        embed(List, optional): A query parameter to retrieve the specified embedded resources of the returned Deployment resource in the response. In a REST API call, this embed parameter value is a list of comma-separated strings, as in GET /restapis/{restapi_id}/deployments/{deployment_id}?embed=var1,var2. The SDK and other platform-dependent libraries might use a different format for the list. Currently, this request supports only retrieval of the embedded API summary this way. Hence, the parameter value must be a single-valued list containing only the "apisummary" string. For example, GET /restapis/{restapi_id}/deployments/{deployment_id}?embed=apisummary. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.deployment.get(
                ctx, rest_api_id=value, deployment_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.deployment.get rest_api_id=value, deployment_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="get_deployment",
        op_kwargs={
            "restApiId": rest_api_id,
            "deploymentId": deployment_id,
            "embed": embed,
        },
    )


async def get_all(
    hub, ctx, rest_api_id: str, position: str = None, limit: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a Deployments collection.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        position(str, optional): The current pagination position in the paged result set. Defaults to None.
        limit(int, optional): The maximum number of returned results per page. The default value is 25 and the maximum value is 500. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.deployment.get_all(ctx, rest_api_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.deployment.get_all rest_api_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="get_deployments",
        op_kwargs={"restApiId": rest_api_id, "position": position, "limit": limit},
    )


async def update(
    hub, ctx, rest_api_id: str, deployment_id: str, patch_operations: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Changes information about a Deployment resource.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        deployment_id(str): The replacement identifier for the Deployment resource to change information about.
        patch_operations(List, optional): A list of update operations to be applied to the specified resource and in the order specified in this list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.deployment.update(
                ctx, rest_api_id=value, deployment_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.deployment.update rest_api_id=value, deployment_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="update_deployment",
        op_kwargs={
            "restApiId": rest_api_id,
            "deploymentId": deployment_id,
            "patchOperations": patch_operations,
        },
    )
