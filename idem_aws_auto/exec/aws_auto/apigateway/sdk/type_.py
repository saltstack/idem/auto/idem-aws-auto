"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "type"


async def get(hub, ctx, id_: str) -> Dict:
    r"""
    **Autogenerated function**

    Args:
        id_(str): [Required] The identifier of the queried SdkType instance.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.sdk.type_.get(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.sdk.type.get id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="apigateway", operation="get_sdk_type", op_kwargs={"id": id_}
    )


async def get_all(hub, ctx, position: str = None, limit: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Args:
        position(str, optional): The current pagination position in the paged result set. Defaults to None.
        limit(int, optional): The maximum number of returned results per page. The default value is 25 and the maximum value is 500. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.sdk.type_.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.sdk.type.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="get_sdk_types",
        op_kwargs={"position": position, "limit": limit},
    )
