"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub, ctx, rest_api_id: str, stage_name: str, sdk_type: str, parameters: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Generates a client SDK for a RestApi and Stage.

    Args:
        rest_api_id(str): [Required] The string identifier of the associated RestApi.
        stage_name(str): [Required] The name of the Stage that the SDK will use.
        sdk_type(str): [Required] The language for the generated SDK. Currently java, javascript, android, objectivec (for iOS), swift (for iOS), and ruby are supported.
        parameters(Dict, optional): A string-to-string key-value map of query parameters sdkType-dependent properties of the SDK. For sdkType of objectivec or swift, a parameter named classPrefix is required. For sdkType of android, parameters named groupId, artifactId, artifactVersion, and invokerPackage are required. For sdkType of java, parameters named serviceName and javaPackageName are required. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigateway.sdk.init.get(
                ctx, rest_api_id=value, stage_name=value, sdk_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigateway.sdk.init.get rest_api_id=value, stage_name=value, sdk_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigateway",
        operation="get_sdk",
        op_kwargs={
            "restApiId": rest_api_id,
            "stageName": stage_name,
            "sdkType": sdk_type,
            "parameters": parameters,
        },
    )
