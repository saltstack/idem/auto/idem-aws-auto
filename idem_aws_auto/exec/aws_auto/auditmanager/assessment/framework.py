"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    control_sets: List,
    description: str = None,
    compliance_type: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a custom framework in Audit Manager.

    Args:
        name(str):  The name of the new custom framework. .
        description(str, optional):  An optional description for the new custom framework. . Defaults to None.
        compliance_type(str, optional):  The compliance type that the new custom framework supports, such as CIS or HIPAA. . Defaults to None.
        control_sets(List):  The control sets to be associated with the framework. .
        tags(Dict, optional):  The tags associated with the framework. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.framework.create(
                ctx, name=value, control_sets=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.framework.create name=value, control_sets=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="create_assessment_framework",
        op_kwargs={
            "name": name,
            "description": description,
            "complianceType": compliance_type,
            "controlSets": control_sets,
            "tags": tags,
        },
    )


async def delete(hub, ctx, framework_id: str) -> Dict:
    r"""
    **Autogenerated function**

     Deletes a custom framework in Audit Manager.

    Args:
        framework_id(str):  The identifier for the specified framework. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.framework.delete(
                ctx, framework_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.framework.delete framework_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="delete_assessment_framework",
        op_kwargs={"frameworkId": framework_id},
    )


async def get(hub, ctx, framework_id: str) -> Dict:
    r"""
    **Autogenerated function**

     Returns a framework from Audit Manager.

    Args:
        framework_id(str):  The identifier for the specified framework. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.framework.get(ctx, framework_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.framework.get framework_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="get_assessment_framework",
        op_kwargs={"frameworkId": framework_id},
    )


async def list_all(
    hub, ctx, framework_type: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of the frameworks available in the Audit Manager framework library.

    Args:
        framework_type(str):  The type of framework, such as standard or custom. .
        next_token(str, optional):  The pagination token used to fetch the next set of results. . Defaults to None.
        max_results(int, optional):  Represents the maximum number of results per page, or per API request call. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.framework.list_all(
                ctx, framework_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.framework.list_all framework_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="list_assessment_frameworks",
        op_kwargs={
            "frameworkType": framework_type,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    framework_id: str,
    name: str,
    control_sets: List,
    description: str = None,
    compliance_type: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Updates a custom framework in Audit Manager.

    Args:
        framework_id(str):  The identifier for the specified framework. .
        name(str):  The name of the framework to be updated. .
        description(str, optional):  The description of the framework that is to be updated. . Defaults to None.
        compliance_type(str, optional):  The compliance type that the new custom framework supports, such as CIS or HIPAA. . Defaults to None.
        control_sets(List):  The control sets associated with the framework. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.framework.update(
                ctx, framework_id=value, name=value, control_sets=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.framework.update framework_id=value, name=value, control_sets=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="update_assessment_framework",
        op_kwargs={
            "frameworkId": framework_id,
            "name": name,
            "description": description,
            "complianceType": compliance_type,
            "controlSets": control_sets,
        },
    )
