"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, assessment_report_id: str, assessment_id: str) -> Dict:
    r"""
    **Autogenerated function**

     Returns the URL of a specified assessment report in Audit Manager.

    Args:
        assessment_report_id(str):  The identifier for the assessment report. .
        assessment_id(str):  The identifier for the specified assessment. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.assessment.report.url.get(
                ctx, assessment_report_id=value, assessment_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.assessment.report.url.get assessment_report_id=value, assessment_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="get_assessment_report_url",
        op_kwargs={
            "assessmentReportId": assessment_report_id,
            "assessmentId": assessment_id,
        },
    )
