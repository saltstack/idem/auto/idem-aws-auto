"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of tags for the specified resource in Audit Manager.

    Args:
        resource_arn(str):  The Amazon Resource Name (ARN) of the specified resource. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.tags_for_resource.list(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="list_tags_for_resource",
        op_kwargs={"resourceArn": resource_arn},
    )
