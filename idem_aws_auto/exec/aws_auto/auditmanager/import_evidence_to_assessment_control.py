"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(
    hub,
    ctx,
    assessment_id: str,
    control_set_id: str,
    control_id: str,
    manual_evidence: List,
) -> Dict:
    r"""
    **Autogenerated function**

     Uploads one or more pieces of evidence to the specified control in the assessment in Audit Manager.

    Args:
        assessment_id(str):  The identifier for the specified assessment. .
        control_set_id(str):  The identifier for the specified control set. .
        control_id(str):  The identifier for the specified control. .
        manual_evidence(List):  The list of manual evidence objects. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.auditmanager.import_evidence_to_assessment_control.batch(
                ctx,
                assessment_id=value,
                control_set_id=value,
                control_id=value,
                manual_evidence=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.auditmanager.import_evidence_to_assessment_control.batch assessment_id=value, control_set_id=value, control_id=value, manual_evidence=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="auditmanager",
        operation="batch_import_evidence_to_assessment_control",
        op_kwargs={
            "assessmentId": assessment_id,
            "controlSetId": control_set_id,
            "controlId": control_id,
            "manualEvidence": manual_evidence,
        },
    )
