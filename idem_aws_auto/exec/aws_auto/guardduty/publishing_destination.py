"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    detector_id: str,
    destination_type: str,
    destination_properties: Dict,
    client_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a publishing destination to export findings to. The resource to export findings to must exist before you use this operation.

    Args:
        detector_id(str): The ID of the GuardDuty detector associated with the publishing destination.
        destination_type(str): The type of resource for the publishing destination. Currently only Amazon S3 buckets are supported.
        destination_properties(Dict): The properties of the publishing destination, including the ARNs for the destination and the KMS key used for encryption.
        client_token(str, optional): The idempotency token for the request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.guardduty.publishing_destination.create(
                ctx, detector_id=value, destination_type=value, destination_properties=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.guardduty.publishing_destination.create detector_id=value, destination_type=value, destination_properties=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="guardduty",
        operation="create_publishing_destination",
        op_kwargs={
            "DetectorId": detector_id,
            "DestinationType": destination_type,
            "DestinationProperties": destination_properties,
            "ClientToken": client_token,
        },
    )


async def delete(hub, ctx, detector_id: str, destination_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the publishing definition with the specified destinationId.

    Args:
        detector_id(str): The unique ID of the detector associated with the publishing destination to delete.
        destination_id(str): The ID of the publishing destination to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.guardduty.publishing_destination.delete(
                ctx, detector_id=value, destination_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.guardduty.publishing_destination.delete detector_id=value, destination_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="guardduty",
        operation="delete_publishing_destination",
        op_kwargs={"DetectorId": detector_id, "DestinationId": destination_id},
    )


async def describe(hub, ctx, detector_id: str, destination_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the publishing destination specified by the provided destinationId.

    Args:
        detector_id(str): The unique ID of the detector associated with the publishing destination to retrieve.
        destination_id(str): The ID of the publishing destination to retrieve.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.guardduty.publishing_destination.describe(
                ctx, detector_id=value, destination_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.guardduty.publishing_destination.describe detector_id=value, destination_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="guardduty",
        operation="describe_publishing_destination",
        op_kwargs={"DetectorId": detector_id, "DestinationId": destination_id},
    )


async def list_all(
    hub, ctx, detector_id: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of publishing destinations associated with the specified dectectorId.

    Args:
        detector_id(str): The ID of the detector to retrieve publishing destinations for.
        max_results(int, optional): The maximum number of results to return in the response. Defaults to None.
        next_token(str, optional): A token to use for paginating results that are returned in the response. Set the value of this parameter to null for the first request to a list action. For subsequent calls, use the NextToken value returned from the previous request to continue listing results after the first page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.guardduty.publishing_destination.list_all(
                ctx, detector_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.guardduty.publishing_destination.list_all detector_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="guardduty",
        operation="list_publishing_destinations",
        op_kwargs={
            "DetectorId": detector_id,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )


async def update(
    hub, ctx, detector_id: str, destination_id: str, destination_properties: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates information about the publishing destination specified by the destinationId.

    Args:
        detector_id(str): The ID of the detector associated with the publishing destinations to update.
        destination_id(str): The ID of the publishing destination to update.
        destination_properties(Dict, optional): A DestinationProperties object that includes the DestinationArn and KmsKeyArn of the publishing destination. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.guardduty.publishing_destination.update(
                ctx, detector_id=value, destination_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.guardduty.publishing_destination.update detector_id=value, destination_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="guardduty",
        operation="update_publishing_destination",
        op_kwargs={
            "DetectorId": detector_id,
            "DestinationId": destination_id,
            "DestinationProperties": destination_properties,
        },
    )
