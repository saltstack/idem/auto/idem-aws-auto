"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, cluster_arn: str, secret_arn_list: List) -> Dict:
    r"""
        **Autogenerated function**


        Disassociates one or more Scram Secrets from an Amazon MSK cluster.


        Args:
            cluster_arn(str):
    The Amazon Resource Name (ARN) of the cluster to be updated.
    .
            secret_arn_list(List):
    List of AWS Secrets Manager secret ARNs.
    .

        Returns:
            Dict

        Examples:
            Call from code:

            .. code-block:: python

                await hub.exec.aws_auto.kafka.disassociate_scram_secret.batch(
                    ctx, cluster_arn=value, secret_arn_list=value
                )

            Call from CLI:

            .. code-block:: bash

                $ idem exec aws_auto.kafka.disassociate_scram_secret.batch cluster_arn=value, secret_arn_list=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kafka",
        operation="batch_disassociate_scram_secret",
        op_kwargs={"ClusterArn": cluster_arn, "SecretArnList": secret_arn_list},
    )
