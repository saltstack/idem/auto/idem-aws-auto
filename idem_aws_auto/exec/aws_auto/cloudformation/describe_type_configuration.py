"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, type_configuration_identifiers: List) -> Dict:
    r"""
    **Autogenerated function**

    Returns configuration data for the specified CloudFormation extensions, from the CloudFormation registry for the account and region. For more information, see Configuring extensions at the account level in the CloudFormation User Guide.

    Args:
        type_configuration_identifiers(List): The list of identifiers for the desired extension configurations.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.describe_type_configuration.batch(
                ctx, type_configuration_identifiers=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.describe_type_configuration.batch type_configuration_identifiers=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="batch_describe_type_configurations",
        op_kwargs={"TypeConfigurationIdentifiers": type_configuration_identifiers},
    )
