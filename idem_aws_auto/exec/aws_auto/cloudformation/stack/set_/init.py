"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    stack_set_name: str,
    description: str = None,
    template_body: str = None,
    template_url: str = None,
    stack_id: str = None,
    parameters: List = None,
    capabilities: List = None,
    tags: List = None,
    administration_role_arn: str = None,
    execution_role_name: str = None,
    permission_model: str = None,
    auto_deployment: Dict = None,
    call_as: str = None,
    client_request_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a stack set.

    Args:
        stack_set_name(str): The name to associate with the stack set. The name must be unique in the Region where you create your stack set.  A stack name can contain only alphanumeric characters (case-sensitive) and hyphens. It must start with an alphabetic character and can't be longer than 128 characters. .
        description(str, optional): A description of the stack set. You can use the description to identify the stack set's purpose or other important information. Defaults to None.
        template_body(str, optional): The structure that contains the template body, with a minimum length of 1 byte and a maximum length of 51,200 bytes. For more information, see Template Anatomy in the CloudFormation User Guide. Conditional: You must specify either the TemplateBody or the TemplateURL parameter, but not both. Defaults to None.
        template_url(str, optional): The location of the file that contains the template body. The URL must point to a template (maximum size: 460,800 bytes) that's located in an Amazon S3 bucket or a Systems Manager document. For more information, see Template Anatomy in the CloudFormation User Guide. Conditional: You must specify either the TemplateBody or the TemplateURL parameter, but not both. Defaults to None.
        stack_id(str, optional): The stack ID you are importing into a new stack set. Specify the Amazon Resource Number (ARN) of the stack. Defaults to None.
        parameters(List, optional): The input parameters for the stack set template. . Defaults to None.
        capabilities(List, optional): In some cases, you must explicitly acknowledge that your stack set template contains certain capabilities in order for CloudFormation to create the stack set and related stack instances.    CAPABILITY_IAM and CAPABILITY_NAMED_IAM  Some stack templates might include resources that can affect permissions in your account; for example, by creating new Identity and Access Management (IAM) users. For those stack sets, you must explicitly acknowledge this by specifying one of these capabilities. The following IAM resources require you to specify either the CAPABILITY_IAM or CAPABILITY_NAMED_IAM capability.   If you have IAM resources, you can specify either capability.    If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM.    If you don't specify either of these capabilities, CloudFormation returns an InsufficientCapabilities error.   If your stack template contains these resources, we recommend that you review all permissions associated with them and edit their permissions if necessary.     AWS::IAM::AccessKey      AWS::IAM::Group      AWS::IAM::InstanceProfile      AWS::IAM::Policy      AWS::IAM::Role      AWS::IAM::User      AWS::IAM::UserToGroupAddition    For more information, see Acknowledging IAM Resources in CloudFormation Templates.    CAPABILITY_AUTO_EXPAND  Some templates reference macros. If your stack set template references one or more macros, you must create the stack set directly from the processed template, without first reviewing the resulting changes in a change set. To create the stack set directly, you must acknowledge this capability. For more information, see Using CloudFormation Macros to Perform Custom Processing on Templates.  Stack sets with service-managed permissions do not currently support the use of macros in templates. (This includes the AWS::Include and AWS::Serverless transforms, which are macros hosted by CloudFormation.) Even if you specify this capability for a stack set with service-managed permissions, if you reference a macro in your template the stack set operation will fail.   . Defaults to None.
        tags(List, optional): The key-value pairs to associate with this stack set and the stacks created from it. CloudFormation also propagates these tags to supported resources that are created in the stacks. A maximum number of 50 tags can be specified. If you specify tags as part of a CreateStackSet action, CloudFormation checks to see if you have the required IAM permission to tag resources. If you don't, the entire CreateStackSet action fails with an access denied error, and the stack set is not created. Defaults to None.
        administration_role_arn(str, optional): The Amazon Resource Number (ARN) of the IAM role to use to create this stack set.  Specify an IAM role only if you are using customized administrator roles to control which users or groups can manage specific stack sets within the same administrator account. For more information, see Prerequisites: Granting Permissions for Stack Set Operations in the CloudFormation User Guide. Defaults to None.
        execution_role_name(str, optional): The name of the IAM execution role to use to create the stack set. If you do not specify an execution role, CloudFormation uses the AWSCloudFormationStackSetExecutionRole role for the stack set operation. Specify an IAM role only if you are using customized execution roles to control which stack resources users and groups can include in their stack sets. . Defaults to None.
        permission_model(str, optional): Describes how the IAM roles required for stack set operations are created. By default, SELF-MANAGED is specified.   With self-managed permissions, you must create the administrator and execution roles required to deploy to target accounts. For more information, see Grant Self-Managed Stack Set Permissions.   With service-managed permissions, StackSets automatically creates the IAM roles required to deploy to accounts managed by Organizations. For more information, see Grant Service-Managed Stack Set Permissions.  . Defaults to None.
        auto_deployment(Dict, optional): Describes whether StackSets automatically deploys to Organizations accounts that are added to the target organization or organizational unit (OU). Specify only if PermissionModel is SERVICE_MANAGED. Defaults to None.
        call_as(str, optional): [Service-managed permissions] Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account. By default, SELF is specified. Use SELF for stack sets with self-managed permissions.   To create a stack set with service-managed permissions while signed in to the management account, specify SELF.   To create a stack set with service-managed permissions while signed in to a delegated administrator account, specify DELEGATED_ADMIN. Your account must be registered as a delegated admin in the management account. For more information, see Register a delegated administrator in the CloudFormation User Guide.   Stack sets with service-managed permissions are created in the management account, including stack sets that are created by delegated administrators. Defaults to None.
        client_request_token(str, optional): A unique identifier for this CreateStackSet request. Specify this token if you plan to retry requests so that CloudFormation knows that you're not attempting to create another stack set with the same name. You might retry CreateStackSet requests to ensure that CloudFormation successfully received them. If you don't specify an operation ID, the SDK generates one automatically. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.set_.init.create(ctx, stack_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.set_.init.create stack_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="create_stack_set",
        op_kwargs={
            "StackSetName": stack_set_name,
            "Description": description,
            "TemplateBody": template_body,
            "TemplateURL": template_url,
            "StackId": stack_id,
            "Parameters": parameters,
            "Capabilities": capabilities,
            "Tags": tags,
            "AdministrationRoleARN": administration_role_arn,
            "ExecutionRoleName": execution_role_name,
            "PermissionModel": permission_model,
            "AutoDeployment": auto_deployment,
            "CallAs": call_as,
            "ClientRequestToken": client_request_token,
        },
    )


async def delete(hub, ctx, stack_set_name: str, call_as: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a stack set. Before you can delete a stack set, all of its member stack instances must be deleted. For more information about how to do this, see DeleteStackInstances.

    Args:
        stack_set_name(str): The name or unique ID of the stack set that you're deleting. You can obtain this value by running ListStackSets.
        call_as(str, optional): [Service-managed permissions] Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account. By default, SELF is specified. Use SELF for stack sets with self-managed permissions.   If you are signed in to the management account, specify SELF.   If you are signed in to a delegated administrator account, specify DELEGATED_ADMIN. Your account must be registered as a delegated administrator in the management account. For more information, see Register a delegated administrator in the CloudFormation User Guide.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.set_.init.delete(ctx, stack_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.set_.init.delete stack_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="delete_stack_set",
        op_kwargs={"StackSetName": stack_set_name, "CallAs": call_as},
    )


async def describe(hub, ctx, stack_set_name: str, call_as: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns the description of the specified stack set.

    Args:
        stack_set_name(str): The name or unique ID of the stack set whose description you want.
        call_as(str, optional): [Service-managed permissions] Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account. By default, SELF is specified. Use SELF for stack sets with self-managed permissions.   If you are signed in to the management account, specify SELF.   If you are signed in to a delegated administrator account, specify DELEGATED_ADMIN. Your account must be registered as a delegated administrator in the management account. For more information, see Register a delegated administrator in the CloudFormation User Guide.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.set_.init.describe(
                ctx, stack_set_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.set_.init.describe stack_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="describe_stack_set",
        op_kwargs={"StackSetName": stack_set_name, "CallAs": call_as},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    status: str = None,
    call_as: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns summary information about stack sets that are associated with the user.   [Self-managed permissions] If you set the CallAs parameter to SELF while signed in to your account, ListStackSets returns all self-managed stack sets in your account.   [Service-managed permissions] If you set the CallAs parameter to SELF while signed in to the organization's management account, ListStackSets returns all stack sets in the management account.   [Service-managed permissions] If you set the CallAs parameter to DELEGATED_ADMIN while signed in to your member account, ListStackSets returns all stack sets with service-managed permissions in the management account.

    Args:
        next_token(str, optional): If the previous paginated request didn't return all of the remaining results, the response object's NextToken parameter value is set to a token. To retrieve the next set of results, call ListStackSets again and assign that token to the request object's NextToken parameter. If there are no remaining results, the previous response object's NextToken parameter is set to null. Defaults to None.
        max_results(int, optional): The maximum number of results to be returned with a single call. If the number of available results exceeds this maximum, the response includes a NextToken value that you can assign to the NextToken request parameter to get the next set of results. Defaults to None.
        status(str, optional): The status of the stack sets that you want to get summary information about. Defaults to None.
        call_as(str, optional): [Service-managed permissions] Specifies whether you are acting as an account administrator in the management account or as a delegated administrator in a member account. By default, SELF is specified. Use SELF for stack sets with self-managed permissions.   If you are signed in to the management account, specify SELF.   If you are signed in to a delegated administrator account, specify DELEGATED_ADMIN. Your account must be registered as a delegated administrator in the management account. For more information, see Register a delegated administrator in the CloudFormation User Guide.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.set_.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.set_.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="list_stack_sets",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "Status": status,
            "CallAs": call_as,
        },
    )


async def update(
    hub,
    ctx,
    stack_set_name: str,
    description: str = None,
    template_body: str = None,
    template_url: str = None,
    use_previous_template: bool = None,
    parameters: List = None,
    capabilities: List = None,
    tags: List = None,
    operation_preferences: Dict = None,
    administration_role_arn: str = None,
    execution_role_name: str = None,
    deployment_targets: Dict = None,
    permission_model: str = None,
    auto_deployment: Dict = None,
    operation_id: str = None,
    accounts: List = None,
    regions: List = None,
    call_as: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the stack set, and associated stack instances in the specified accounts and Regions. Even if the stack set operation created by updating the stack set fails (completely or partially, below or above a specified failure tolerance), the stack set is updated with your changes. Subsequent CreateStackInstances calls on the specified stack set use the updated stack set.

    Args:
        stack_set_name(str): The name or unique ID of the stack set that you want to update.
        description(str, optional): A brief description of updates that you are making. Defaults to None.
        template_body(str, optional): The structure that contains the template body, with a minimum length of 1 byte and a maximum length of 51,200 bytes. For more information, see Template Anatomy in the CloudFormation User Guide. Conditional: You must specify only one of the following parameters: TemplateBody or TemplateURL—or set UsePreviousTemplate to true. Defaults to None.
        template_url(str, optional): The location of the file that contains the template body. The URL must point to a template (maximum size: 460,800 bytes) that is located in an Amazon S3 bucket or a Systems Manager document. For more information, see Template Anatomy in the CloudFormation User Guide. Conditional: You must specify only one of the following parameters: TemplateBody or TemplateURL—or set UsePreviousTemplate to true. . Defaults to None.
        use_previous_template(bool, optional): Use the existing template that's associated with the stack set that you're updating. Conditional: You must specify only one of the following parameters: TemplateBody or TemplateURL—or set UsePreviousTemplate to true. . Defaults to None.
        parameters(List, optional): A list of input parameters for the stack set template. . Defaults to None.
        capabilities(List, optional): In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for CloudFormation to update the stack set and its associated stack instances.    CAPABILITY_IAM and CAPABILITY_NAMED_IAM  Some stack templates might include resources that can affect permissions in your account; for example, by creating new Identity and Access Management (IAM) users. For those stacks sets, you must explicitly acknowledge this by specifying one of these capabilities. The following IAM resources require you to specify either the CAPABILITY_IAM or CAPABILITY_NAMED_IAM capability.   If you have IAM resources, you can specify either capability.    If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM.    If you don't specify either of these capabilities, CloudFormation returns an InsufficientCapabilities error.   If your stack template contains these resources, we recommend that you review all permissions associated with them and edit their permissions if necessary.     AWS::IAM::AccessKey      AWS::IAM::Group      AWS::IAM::InstanceProfile      AWS::IAM::Policy      AWS::IAM::Role      AWS::IAM::User      AWS::IAM::UserToGroupAddition    For more information, see Acknowledging IAM Resources in CloudFormation Templates.    CAPABILITY_AUTO_EXPAND  Some templates reference macros. If your stack set template references one or more macros, you must update the stack set directly from the processed template, without first reviewing the resulting changes in a change set. To update the stack set directly, you must acknowledge this capability. For more information, see Using CloudFormation Macros to Perform Custom Processing on Templates.  Stack sets with service-managed permissions do not currently support the use of macros in templates. (This includes the AWS::Include and AWS::Serverless transforms, which are macros hosted by CloudFormation.) Even if you specify this capability for a stack set with service-managed permissions, if you reference a macro in your template the stack set operation will fail.   . Defaults to None.
        tags(List, optional): The key-value pairs to associate with this stack set and the stacks created from it. CloudFormation also propagates these tags to supported resources that are created in the stacks. You can specify a maximum number of 50 tags. If you specify tags for this parameter, those tags replace any list of tags that are currently associated with this stack set. This means:   If you don't specify this parameter, CloudFormation doesn't modify the stack's tags.    If you specify any tags using this parameter, you must specify all the tags that you want associated with this stack set, even tags you've specifed before (for example, when creating the stack set or during a previous update of the stack set.). Any tags that you don't include in the updated list of tags are removed from the stack set, and therefore from the stacks and resources as well.    If you specify an empty value, CloudFormation removes all currently associated tags.   If you specify new tags as part of an UpdateStackSet action, CloudFormation checks to see if you have the required IAM permission to tag resources. If you omit tags that are currently associated with the stack set from the list of tags you specify, CloudFormation assumes that you want to remove those tags from the stack set, and checks to see if you have permission to untag resources. If you don't have the necessary permission(s), the entire UpdateStackSet action fails with an access denied error, and the stack set is not updated. Defaults to None.
        operation_preferences(Dict, optional): Preferences for how CloudFormation performs this stack set operation. Defaults to None.
        administration_role_arn(str, optional): The Amazon Resource Number (ARN) of the IAM role to use to update this stack set. Specify an IAM role only if you are using customized administrator roles to control which users or groups can manage specific stack sets within the same administrator account. For more information, see Granting Permissions for Stack Set Operations in the CloudFormation User Guide. If you specified a customized administrator role when you created the stack set, you must specify a customized administrator role, even if it is the same customized administrator role used with this stack set previously. Defaults to None.
        execution_role_name(str, optional): The name of the IAM execution role to use to update the stack set. If you do not specify an execution role, CloudFormation uses the AWSCloudFormationStackSetExecutionRole role for the stack set operation. Specify an IAM role only if you are using customized execution roles to control which stack resources users and groups can include in their stack sets.   If you specify a customized execution role, CloudFormation uses that role to update the stack. If you do not specify a customized execution role, CloudFormation performs the update using the role previously associated with the stack set, so long as you have permissions to perform operations on the stack set. Defaults to None.
        deployment_targets(Dict, optional): [Service-managed permissions] The Organizations accounts in which to update associated stack instances. To update all the stack instances associated with this stack set, do not specify DeploymentTargets or Regions. If the stack set update includes changes to the template (that is, if TemplateBody or TemplateURL is specified), or the Parameters, CloudFormation marks all stack instances with a status of OUTDATED prior to updating the stack instances in the specified accounts and Regions. If the stack set update does not include changes to the template or parameters, CloudFormation updates the stack instances in the specified accounts and Regions, while leaving all other stack instances with their existing stack instance status. Defaults to None.
        permission_model(str, optional): Describes how the IAM roles required for stack set operations are created. You cannot modify PermissionModel if there are stack instances associated with your stack set.   With self-managed permissions, you must create the administrator and execution roles required to deploy to target accounts. For more information, see Grant Self-Managed Stack Set Permissions.   With service-managed permissions, StackSets automatically creates the IAM roles required to deploy to accounts managed by Organizations. For more information, see Grant Service-Managed Stack Set Permissions.  . Defaults to None.
        auto_deployment(Dict, optional): [Service-managed permissions] Describes whether StackSets automatically deploys to Organizations accounts that are added to a target organization or organizational unit (OU). If you specify AutoDeployment, do not specify DeploymentTargets or Regions. Defaults to None.
        operation_id(str, optional): The unique ID for this stack set operation.  The operation ID also functions as an idempotency token, to ensure that CloudFormation performs the stack set operation only once, even if you retry the request multiple times. You might retry stack set operation requests to ensure that CloudFormation successfully received them. If you don't specify an operation ID, CloudFormation generates one automatically. Repeating this stack set operation with a new operation ID retries all stack instances whose status is OUTDATED. . Defaults to None.
        accounts(List, optional): [Self-managed permissions] The accounts in which to update associated stack instances. If you specify accounts, you must also specify the Regions in which to update stack set instances. To update all the stack instances associated with this stack set, do not specify the Accounts or Regions properties. If the stack set update includes changes to the template (that is, if the TemplateBody or TemplateURL properties are specified), or the Parameters property, CloudFormation marks all stack instances with a status of OUTDATED prior to updating the stack instances in the specified accounts and Regions. If the stack set update does not include changes to the template or parameters, CloudFormation updates the stack instances in the specified accounts and Regions, while leaving all other stack instances with their existing stack instance status. . Defaults to None.
        regions(List, optional): The Regions in which to update associated stack instances. If you specify Regions, you must also specify accounts in which to update stack set instances. To update all the stack instances associated with this stack set, do not specify the Accounts or Regions properties. If the stack set update includes changes to the template (that is, if the TemplateBody or TemplateURL properties are specified), or the Parameters property, CloudFormation marks all stack instances with a status of OUTDATED prior to updating the stack instances in the specified accounts and Regions. If the stack set update does not include changes to the template or parameters, CloudFormation updates the stack instances in the specified accounts and Regions, while leaving all other stack instances with their existing stack instance status. . Defaults to None.
        call_as(str, optional): [Service-managed permissions] Specifies whether you are acting as an account administrator in the organization's management account or as a delegated administrator in a member account. By default, SELF is specified. Use SELF for stack sets with self-managed permissions.   If you are signed in to the management account, specify SELF.   If you are signed in to a delegated administrator account, specify DELEGATED_ADMIN. Your account must be registered as a delegated administrator in the management account. For more information, see Register a delegated administrator in the CloudFormation User Guide.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.set_.init.update(ctx, stack_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.set_.init.update stack_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="update_stack_set",
        op_kwargs={
            "StackSetName": stack_set_name,
            "Description": description,
            "TemplateBody": template_body,
            "TemplateURL": template_url,
            "UsePreviousTemplate": use_previous_template,
            "Parameters": parameters,
            "Capabilities": capabilities,
            "Tags": tags,
            "OperationPreferences": operation_preferences,
            "AdministrationRoleARN": administration_role_arn,
            "ExecutionRoleName": execution_role_name,
            "DeploymentTargets": deployment_targets,
            "PermissionModel": permission_model,
            "AutoDeployment": auto_deployment,
            "OperationId": operation_id,
            "Accounts": accounts,
            "Regions": regions,
            "CallAs": call_as,
        },
    )
