"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    stack_name: str,
    template_body: str = None,
    template_url: str = None,
    parameters: List = None,
    disable_rollback: bool = None,
    rollback_configuration: Dict = None,
    timeout_in_minutes: int = None,
    notification_ar_ns: List = None,
    capabilities: List = None,
    resource_types: List = None,
    role_arn: str = None,
    on_failure: str = None,
    stack_policy_body: str = None,
    stack_policy_url: str = None,
    tags: List = None,
    client_request_token: str = None,
    enable_termination_protection: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a stack as specified in the template. After the call completes successfully, the stack creation starts. You can check the status of the stack via the DescribeStacks API.

    Args:
        stack_name(str): The name that is associated with the stack. The name must be unique in the Region in which you are creating the stack.  A stack name can contain only alphanumeric characters (case sensitive) and hyphens. It must start with an alphabetic character and cannot be longer than 128 characters. .
        template_body(str, optional): Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes. For more information, go to Template Anatomy in the CloudFormation User Guide. Conditional: You must specify either the TemplateBody or the TemplateURL parameter, but not both. Defaults to None.
        template_url(str, optional): Location of file containing the template body. The URL must point to a template (max size: 460,800 bytes) that is located in an Amazon S3 bucket or a Systems Manager document. For more information, go to the Template Anatomy in the CloudFormation User Guide. Conditional: You must specify either the TemplateBody or the TemplateURL parameter, but not both. Defaults to None.
        parameters(List, optional): A list of Parameter structures that specify input parameters for the stack. For more information, see the Parameter data type. Defaults to None.
        disable_rollback(bool, optional): Set to true to disable rollback of the stack if stack creation failed. You can specify either DisableRollback or OnFailure, but not both. Default: false . Defaults to None.
        rollback_configuration(Dict, optional): The rollback triggers for CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards. Defaults to None.
        timeout_in_minutes(int, optional): The amount of time that can pass before the stack status becomes CREATE_FAILED; if DisableRollback is not set or is set to false, the stack will be rolled back. Defaults to None.
        notification_ar_ns(List, optional): The Simple Notification Service (SNS) topic ARNs to publish stack related events. You can find your SNS topic ARNs using the SNS console or your Command Line Interface (CLI). Defaults to None.
        capabilities(List, optional): In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for CloudFormation to create the stack.    CAPABILITY_IAM and CAPABILITY_NAMED_IAM  Some stack templates might include resources that can affect permissions in your account; for example, by creating new Identity and Access Management (IAM) users. For those stacks, you must explicitly acknowledge this by specifying one of these capabilities. The following IAM resources require you to specify either the CAPABILITY_IAM or CAPABILITY_NAMED_IAM capability.   If you have IAM resources, you can specify either capability.    If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM.    If you don't specify either of these capabilities, CloudFormation returns an InsufficientCapabilities error.   If your stack template contains these resources, we recommend that you review all permissions associated with them and edit their permissions if necessary.     AWS::IAM::AccessKey      AWS::IAM::Group      AWS::IAM::InstanceProfile      AWS::IAM::Policy      AWS::IAM::Role      AWS::IAM::User      AWS::IAM::UserToGroupAddition    For more information, see Acknowledging IAM Resources in CloudFormation Templates.    CAPABILITY_AUTO_EXPAND  Some template contain macros. Macros perform custom processing on templates; this can include simple actions like find-and-replace operations, all the way to extensive transformations of entire templates. Because of this, users typically create a change set from the processed template, so that they can review the changes resulting from the macros before actually creating the stack. If your stack template contains one or more macros, and you choose to create a stack directly from the processed template, without first reviewing the resulting changes in a change set, you must acknowledge this capability. This includes the AWS::Include and AWS::Serverless transforms, which are macros hosted by CloudFormation. If you want to create a stack from a stack template that contains macros and nested stacks, you must create the stack directly from the template using this capability.  You should only create stacks directly from a stack template that contains macros if you know what processing the macro performs. Each macro relies on an underlying Lambda service function for processing stack templates. Be aware that the Lambda function owner can update the function operation without CloudFormation being notified.  For more information, see Using CloudFormation Macros to Perform Custom Processing on Templates.  . Defaults to None.
        resource_types(List, optional): The template resource types that you have permissions to work with for this create stack action, such as AWS::EC2::Instance, AWS::EC2::*, or Custom::MyCustomInstance. Use the following syntax to describe template resource types: AWS::* (for all Amazon Web Services resources), Custom::* (for all custom resources), Custom::logical_ID  (for a specific custom resource), AWS::service_name::* (for all resources of a particular Amazon Web Services service), and AWS::service_name::resource_logical_ID  (for a specific Amazon Web Services resource). If the list of resource types doesn't include a resource that you're creating, the stack creation fails. By default, CloudFormation grants permissions to all resource types. Identity and Access Management (IAM) uses this parameter for CloudFormation-specific condition keys in IAM policies. For more information, see Controlling Access with Identity and Access Management. Defaults to None.
        role_arn(str, optional): The Amazon Resource Name (ARN) of an Identity and Access Management (IAM) role that CloudFormation assumes to create the stack. CloudFormation uses the role's credentials to make calls on your behalf. CloudFormation always uses this role for all future operations on the stack. As long as users have permission to operate on the stack, CloudFormation uses this role even if the users don't have permission to pass it. Ensure that the role grants least privilege. If you don't specify a value, CloudFormation uses the role that was previously associated with the stack. If no role is available, CloudFormation uses a temporary session that is generated from your user credentials. Defaults to None.
        on_failure(str, optional): Determines what action will be taken if stack creation fails. This must be one of: DO_NOTHING, ROLLBACK, or DELETE. You can specify either OnFailure or DisableRollback, but not both. Default: ROLLBACK . Defaults to None.
        stack_policy_body(str, optional): Structure containing the stack policy body. For more information, go to  Prevent Updates to Stack Resources in the CloudFormation User Guide. You can specify either the StackPolicyBody or the StackPolicyURL parameter, but not both. Defaults to None.
        stack_policy_url(str, optional): Location of a file containing the stack policy. The URL must point to a policy (maximum size: 16 KB) located in an S3 bucket in the same Region as the stack. You can specify either the StackPolicyBody or the StackPolicyURL parameter, but not both. Defaults to None.
        tags(List, optional): Key-value pairs to associate with this stack. CloudFormation also propagates these tags to the resources created in the stack. A maximum number of 50 tags can be specified. Defaults to None.
        client_request_token(str, optional): A unique identifier for this CreateStack request. Specify this token if you plan to retry requests so that CloudFormation knows that you're not attempting to create a stack with the same name. You might retry CreateStack requests to ensure that CloudFormation successfully received them. All events triggered by a given stack operation are assigned the same client request token, which you can use to track operations. For example, if you execute a CreateStack operation with the token token1, then all the StackEvents generated by that operation will have ClientRequestToken set as token1. In the console, stack operations display the client request token on the Events tab. Stack operations that are initiated from the console use the token format Console-StackOperation-ID, which helps you easily identify the stack operation . For example, if you create a stack using the console, each stack event would be assigned the same token in the following format: Console-CreateStack-7f59c3cf-00d2-40c7-b2ff-e75db0987002. . Defaults to None.
        enable_termination_protection(bool, optional): Whether to enable termination protection on the specified stack. If a user attempts to delete a stack with termination protection enabled, the operation fails and the stack remains unchanged. For more information, see Protecting a Stack From Being Deleted in the CloudFormation User Guide. Termination protection is disabled on stacks by default.   For nested stacks, termination protection is set on the root stack and cannot be changed directly on the nested stack. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.init.create(ctx, stack_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.init.create stack_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="create_stack",
        op_kwargs={
            "StackName": stack_name,
            "TemplateBody": template_body,
            "TemplateURL": template_url,
            "Parameters": parameters,
            "DisableRollback": disable_rollback,
            "RollbackConfiguration": rollback_configuration,
            "TimeoutInMinutes": timeout_in_minutes,
            "NotificationARNs": notification_ar_ns,
            "Capabilities": capabilities,
            "ResourceTypes": resource_types,
            "RoleARN": role_arn,
            "OnFailure": on_failure,
            "StackPolicyBody": stack_policy_body,
            "StackPolicyURL": stack_policy_url,
            "Tags": tags,
            "ClientRequestToken": client_request_token,
            "EnableTerminationProtection": enable_termination_protection,
        },
    )


async def delete(
    hub,
    ctx,
    stack_name: str,
    retain_resources: List = None,
    role_arn: str = None,
    client_request_token: str = None,
) -> None:
    r"""
    **Autogenerated function**

    Deletes a specified stack. Once the call completes successfully, stack deletion starts. Deleted stacks do not show up in the DescribeStacks API if the deletion has been completed successfully.

    Args:
        stack_name(str): The name or the unique stack ID that is associated with the stack.
        retain_resources(List, optional): For stacks in the DELETE_FAILED state, a list of resource logical IDs that are associated with the resources you want to retain. During deletion, CloudFormation deletes the stack but does not delete the retained resources. Retaining resources is useful when you cannot delete a resource, such as a non-empty S3 bucket, but you want to delete the stack. Defaults to None.
        role_arn(str, optional): The Amazon Resource Name (ARN) of an Identity and Access Management (IAM) role that CloudFormation assumes to delete the stack. CloudFormation uses the role's credentials to make calls on your behalf. If you don't specify a value, CloudFormation uses the role that was previously associated with the stack. If no role is available, CloudFormation uses a temporary session that is generated from your user credentials. Defaults to None.
        client_request_token(str, optional): A unique identifier for this DeleteStack request. Specify this token if you plan to retry requests so that CloudFormation knows that you're not attempting to delete a stack with the same name. You might retry DeleteStack requests to ensure that CloudFormation successfully received them. All events triggered by a given stack operation are assigned the same client request token, which you can use to track operations. For example, if you execute a CreateStack operation with the token token1, then all the StackEvents generated by that operation will have ClientRequestToken set as token1. In the console, stack operations display the client request token on the Events tab. Stack operations that are initiated from the console use the token format Console-StackOperation-ID, which helps you easily identify the stack operation . For example, if you create a stack using the console, each stack event would be assigned the same token in the following format: Console-CreateStack-7f59c3cf-00d2-40c7-b2ff-e75db0987002. . Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.init.delete(ctx, stack_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.init.delete stack_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="delete_stack",
        op_kwargs={
            "StackName": stack_name,
            "RetainResources": retain_resources,
            "RoleARN": role_arn,
            "ClientRequestToken": client_request_token,
        },
    )


async def describe_all(
    hub, ctx, stack_name: str = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the description for the specified stack; if no stack name was specified, then it returns the description for all the stacks created.  If the stack does not exist, an AmazonCloudFormationException is returned.

    Args:
        stack_name(str, optional): The name or the unique stack ID that is associated with the stack, which are not always interchangeable:   Running stacks: You can specify either the stack's name or its unique stack ID.   Deleted stacks: You must specify the unique stack ID.   Default: There is no default value. Defaults to None.
        next_token(str, optional): A string that identifies the next page of stacks that you want to retrieve. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="describe_stacks",
        op_kwargs={"StackName": stack_name, "NextToken": next_token},
    )


async def list_all(
    hub, ctx, next_token: str = None, stack_status_filter: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the summary information for stacks whose status matches the specified StackStatusFilter. Summary information for stacks that have been deleted is kept for 90 days after the stack is deleted. If no StackStatusFilter is specified, summary information for all stacks is returned (including existing stacks and stacks that have been deleted).

    Args:
        next_token(str, optional): A string that identifies the next page of stacks that you want to retrieve. Defaults to None.
        stack_status_filter(List, optional): Stack status to use as a filter. Specify one or more stack status codes to list only stacks with the specified status codes. For a complete list of stack status codes, see the StackStatus parameter of the Stack data type. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="list_stacks",
        op_kwargs={"NextToken": next_token, "StackStatusFilter": stack_status_filter},
    )


async def update(
    hub,
    ctx,
    stack_name: str,
    template_body: str = None,
    template_url: str = None,
    use_previous_template: bool = None,
    stack_policy_during_update_body: str = None,
    stack_policy_during_update_url: str = None,
    parameters: List = None,
    capabilities: List = None,
    resource_types: List = None,
    role_arn: str = None,
    rollback_configuration: Dict = None,
    stack_policy_body: str = None,
    stack_policy_url: str = None,
    notification_ar_ns: List = None,
    tags: List = None,
    client_request_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a stack as specified in the template. After the call completes successfully, the stack update starts. You can check the status of the stack via the DescribeStacks action. To get a copy of the template for an existing stack, you can use the GetTemplate action. For more information about creating an update template, updating a stack, and monitoring the progress of the update, see Updating a Stack.

    Args:
        stack_name(str): The name or unique stack ID of the stack to update.
        template_body(str, optional): Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes. (For more information, go to Template Anatomy in the CloudFormation User Guide.) Conditional: You must specify only one of the following parameters: TemplateBody, TemplateURL, or set the UsePreviousTemplate to true. Defaults to None.
        template_url(str, optional): Location of file containing the template body. The URL must point to a template that is located in an Amazon S3 bucket or a Systems Manager document. For more information, go to Template Anatomy in the CloudFormation User Guide. Conditional: You must specify only one of the following parameters: TemplateBody, TemplateURL, or set the UsePreviousTemplate to true. Defaults to None.
        use_previous_template(bool, optional): Reuse the existing template that is associated with the stack that you are updating. Conditional: You must specify only one of the following parameters: TemplateBody, TemplateURL, or set the UsePreviousTemplate to true. Defaults to None.
        stack_policy_during_update_body(str, optional): Structure containing the temporary overriding stack policy body. You can specify either the StackPolicyDuringUpdateBody or the StackPolicyDuringUpdateURL parameter, but not both. If you want to update protected resources, specify a temporary overriding stack policy during this update. If you do not specify a stack policy, the current policy that is associated with the stack will be used. Defaults to None.
        stack_policy_during_update_url(str, optional): Location of a file containing the temporary overriding stack policy. The URL must point to a policy (max size: 16KB) located in an S3 bucket in the same Region as the stack. You can specify either the StackPolicyDuringUpdateBody or the StackPolicyDuringUpdateURL parameter, but not both. If you want to update protected resources, specify a temporary overriding stack policy during this update. If you do not specify a stack policy, the current policy that is associated with the stack will be used. Defaults to None.
        parameters(List, optional): A list of Parameter structures that specify input parameters for the stack. For more information, see the Parameter data type. Defaults to None.
        capabilities(List, optional): In some cases, you must explicitly acknowledge that your stack template contains certain capabilities in order for CloudFormation to update the stack.    CAPABILITY_IAM and CAPABILITY_NAMED_IAM  Some stack templates might include resources that can affect permissions in your account; for example, by creating new Identity and Access Management (IAM) users. For those stacks, you must explicitly acknowledge this by specifying one of these capabilities. The following IAM resources require you to specify either the CAPABILITY_IAM or CAPABILITY_NAMED_IAM capability.   If you have IAM resources, you can specify either capability.    If you have IAM resources with custom names, you must specify CAPABILITY_NAMED_IAM.    If you don't specify either of these capabilities, CloudFormation returns an InsufficientCapabilities error.   If your stack template contains these resources, we recommend that you review all permissions associated with them and edit their permissions if necessary.     AWS::IAM::AccessKey      AWS::IAM::Group      AWS::IAM::InstanceProfile      AWS::IAM::Policy      AWS::IAM::Role      AWS::IAM::User      AWS::IAM::UserToGroupAddition    For more information, see Acknowledging IAM Resources in CloudFormation Templates.    CAPABILITY_AUTO_EXPAND  Some template contain macros. Macros perform custom processing on templates; this can include simple actions like find-and-replace operations, all the way to extensive transformations of entire templates. Because of this, users typically create a change set from the processed template, so that they can review the changes resulting from the macros before actually updating the stack. If your stack template contains one or more macros, and you choose to update a stack directly from the processed template, without first reviewing the resulting changes in a change set, you must acknowledge this capability. This includes the AWS::Include and AWS::Serverless transforms, which are macros hosted by CloudFormation. If you want to update a stack from a stack template that contains macros and nested stacks, you must update the stack directly from the template using this capability.  You should only update stacks directly from a stack template that contains macros if you know what processing the macro performs. Each macro relies on an underlying Lambda service function for processing stack templates. Be aware that the Lambda function owner can update the function operation without CloudFormation being notified.  For more information, see Using CloudFormation Macros to Perform Custom Processing on Templates.  . Defaults to None.
        resource_types(List, optional): The template resource types that you have permissions to work with for this update stack action, such as AWS::EC2::Instance, AWS::EC2::*, or Custom::MyCustomInstance. If the list of resource types doesn't include a resource that you're updating, the stack update fails. By default, CloudFormation grants permissions to all resource types. Identity and Access Management (IAM) uses this parameter for CloudFormation-specific condition keys in IAM policies. For more information, see Controlling Access with Identity and Access Management. Defaults to None.
        role_arn(str, optional): The Amazon Resource Name (ARN) of an Identity and Access Management (IAM) role that CloudFormation assumes to update the stack. CloudFormation uses the role's credentials to make calls on your behalf. CloudFormation always uses this role for all future operations on the stack. As long as users have permission to operate on the stack, CloudFormation uses this role even if the users don't have permission to pass it. Ensure that the role grants least privilege. If you don't specify a value, CloudFormation uses the role that was previously associated with the stack. If no role is available, CloudFormation uses a temporary session that is generated from your user credentials. Defaults to None.
        rollback_configuration(Dict, optional): The rollback triggers for CloudFormation to monitor during stack creation and updating operations, and for the specified monitoring period afterwards. Defaults to None.
        stack_policy_body(str, optional): Structure containing a new stack policy body. You can specify either the StackPolicyBody or the StackPolicyURL parameter, but not both. You might update the stack policy, for example, in order to protect a new resource that you created during a stack update. If you do not specify a stack policy, the current policy that is associated with the stack is unchanged. Defaults to None.
        stack_policy_url(str, optional): Location of a file containing the updated stack policy. The URL must point to a policy (max size: 16KB) located in an S3 bucket in the same Region as the stack. You can specify either the StackPolicyBody or the StackPolicyURL parameter, but not both. You might update the stack policy, for example, in order to protect a new resource that you created during a stack update. If you do not specify a stack policy, the current policy that is associated with the stack is unchanged. Defaults to None.
        notification_ar_ns(List, optional): Amazon Simple Notification Service topic Amazon Resource Names (ARNs) that CloudFormation associates with the stack. Specify an empty list to remove all notification topics. Defaults to None.
        tags(List, optional): Key-value pairs to associate with this stack. CloudFormation also propagates these tags to supported resources in the stack. You can specify a maximum number of 50 tags. If you don't specify this parameter, CloudFormation doesn't modify the stack's tags. If you specify an empty value, CloudFormation removes all associated tags. Defaults to None.
        client_request_token(str, optional): A unique identifier for this UpdateStack request. Specify this token if you plan to retry requests so that CloudFormation knows that you're not attempting to update a stack with the same name. You might retry UpdateStack requests to ensure that CloudFormation successfully received them. All events triggered by a given stack operation are assigned the same client request token, which you can use to track operations. For example, if you execute a CreateStack operation with the token token1, then all the StackEvents generated by that operation will have ClientRequestToken set as token1. In the console, stack operations display the client request token on the Events tab. Stack operations that are initiated from the console use the token format Console-StackOperation-ID, which helps you easily identify the stack operation . For example, if you create a stack using the console, each stack event would be assigned the same token in the following format: Console-CreateStack-7f59c3cf-00d2-40c7-b2ff-e75db0987002. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.stack.init.update(ctx, stack_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.stack.init.update stack_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="update_stack",
        op_kwargs={
            "StackName": stack_name,
            "TemplateBody": template_body,
            "TemplateURL": template_url,
            "UsePreviousTemplate": use_previous_template,
            "StackPolicyDuringUpdateBody": stack_policy_during_update_body,
            "StackPolicyDuringUpdateURL": stack_policy_during_update_url,
            "Parameters": parameters,
            "Capabilities": capabilities,
            "ResourceTypes": resource_types,
            "RoleARN": role_arn,
            "RollbackConfiguration": rollback_configuration,
            "StackPolicyBody": stack_policy_body,
            "StackPolicyURL": stack_policy_url,
            "NotificationARNs": notification_ar_ns,
            "Tags": tags,
            "ClientRequestToken": client_request_token,
        },
    )
