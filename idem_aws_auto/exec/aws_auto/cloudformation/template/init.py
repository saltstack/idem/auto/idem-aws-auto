"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub,
    ctx,
    stack_name: str = None,
    change_set_name: str = None,
    template_stage: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the template body for a specified stack. You can get the template for running or deleted stacks. For deleted stacks, GetTemplate returns the template for up to 90 days after the stack has been deleted.   If the template does not exist, a ValidationError is returned.

    Args:
        stack_name(str, optional): The name or the unique stack ID that is associated with the stack, which are not always interchangeable:   Running stacks: You can specify either the stack's name or its unique stack ID.   Deleted stacks: You must specify the unique stack ID.   Default: There is no default value. Defaults to None.
        change_set_name(str, optional): The name or Amazon Resource Name (ARN) of a change set for which CloudFormation returns the associated template. If you specify a name, you must also specify the StackName. Defaults to None.
        template_stage(str, optional): For templates that include transforms, the stage of the template that CloudFormation returns. To get the user-submitted template, specify Original. To get the template after CloudFormation has processed all transforms, specify Processed.  If the template doesn't include transforms, Original and Processed return the same template. By default, CloudFormation specifies Processed. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.template.init.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.template.init.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="get_template",
        op_kwargs={
            "StackName": stack_name,
            "ChangeSetName": change_set_name,
            "TemplateStage": template_stage,
        },
    )


async def validate(
    hub, ctx, template_body: str = None, template_url: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Validates a specified template. CloudFormation first checks if the template is valid JSON. If it isn't, CloudFormation checks if the template is valid YAML. If both these checks fail, CloudFormation returns a template validation error.

    Args:
        template_body(str, optional): Structure containing the template body with a minimum length of 1 byte and a maximum length of 51,200 bytes. For more information, go to Template Anatomy in the CloudFormation User Guide. Conditional: You must pass TemplateURL or TemplateBody. If both are passed, only TemplateBody is used. Defaults to None.
        template_url(str, optional): Location of file containing the template body. The URL must point to a template (max size: 460,800 bytes) that is located in an Amazon S3 bucket or a Systems Manager document. For more information, go to Template Anatomy in the CloudFormation User Guide. Conditional: You must pass TemplateURL or TemplateBody. If both are passed, only TemplateBody is used. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudformation.template.init.validate(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudformation.template.init.validate
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudformation",
        operation="validate_template",
        op_kwargs={"TemplateBody": template_body, "TemplateURL": template_url},
    )
