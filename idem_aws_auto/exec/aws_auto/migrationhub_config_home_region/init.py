"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Returns the calling account’s home region, if configured. This API is used by other AWS services to determine the regional endpoint for calling AWS Application Discovery Service and Migration Hub. You must call GetHomeRegion at least once before you call any other AWS Application Discovery Service and AWS Migration Hub APIs, to obtain the account's Migration Hub home region.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.migrationhub_config_home_region.init.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.migrationhub_config_home_region.init.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="migrationhub-config",
        operation="get_home_region",
        op_kwargs={},
    )
