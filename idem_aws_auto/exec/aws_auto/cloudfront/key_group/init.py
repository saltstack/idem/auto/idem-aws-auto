"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx, key_group_config: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Creates a key group that you can use with CloudFront signed URLs and signed cookies. To create a key group, you must specify at least one public key for the key group. After you create a key group, you can reference it from one or more cache behaviors. When you reference a key group in a cache behavior, CloudFront requires signed URLs or signed cookies for all requests that match the cache behavior. The URLs or cookies must be signed with a private key whose corresponding public key is in the key group. The signed URL or cookie contains information about which public key CloudFront should use to verify the signature. For more information, see Serving private content in the Amazon CloudFront Developer Guide.

    Args:
        key_group_config(Dict): A key group configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.key_group.init.create(ctx, key_group_config=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.key_group.init.create key_group_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="create_key_group",
        op_kwargs={"KeyGroupConfig": key_group_config},
    )


async def delete(hub, ctx, id_: str, if_match: str = None) -> None:
    r"""
    **Autogenerated function**

    Deletes a key group. You cannot delete a key group that is referenced in a cache behavior. First update your distributions to remove the key group from all cache behaviors, then delete the key group. To delete a key group, you must provide the key group’s identifier and version. To get these values, use ListKeyGroups followed by GetKeyGroup or GetKeyGroupConfig.

    Args:
        id_(str): The identifier of the key group that you are deleting. To get the identifier, use ListKeyGroups.
        if_match(str, optional): The version of the key group that you are deleting. The version is the key group’s ETag value. To get the ETag, use GetKeyGroup or GetKeyGroupConfig. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.key_group.init.delete(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.key_group.init.delete id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="delete_key_group",
        op_kwargs={"Id": id_, "IfMatch": if_match},
    )


async def get(hub, ctx, id_: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets a key group, including the date and time when the key group was last modified. To get a key group, you must provide the key group’s identifier. If the key group is referenced in a distribution’s cache behavior, you can get the key group’s identifier using ListDistributions or GetDistribution. If the key group is not referenced in a cache behavior, you can get the identifier using ListKeyGroups.

    Args:
        id_(str): The identifier of the key group that you are getting. To get the identifier, use ListKeyGroups.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.key_group.init.get(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.key_group.init.get id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="cloudfront", operation="get_key_group", op_kwargs={"Id": id_}
    )


async def list_all(hub, ctx, marker: str = None, max_items: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of key groups. You can optionally specify the maximum number of items to receive in the response. If the total number of items in the list exceeds the maximum that you specify, or the default maximum, the response is paginated. To get the next page of items, send a subsequent request that specifies the NextMarker value from the current response as the Marker value in the subsequent request.

    Args:
        marker(str, optional): Use this field when paginating results to indicate where to begin in your list of key groups. The response includes key groups in the list that occur after the marker. To get the next page of the list, set this field’s value to the value of NextMarker from the current page’s response. Defaults to None.
        max_items(str, optional): The maximum number of key groups that you want in the response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.key_group.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.key_group.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="list_key_groups",
        op_kwargs={"Marker": marker, "MaxItems": max_items},
    )


async def update(
    hub, ctx, key_group_config: Dict, id_: str, if_match: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a key group. When you update a key group, all the fields are updated with the values provided in the request. You cannot update some fields independent of others. To update a key group:   Get the current key group with GetKeyGroup or GetKeyGroupConfig.   Locally modify the fields in the key group that you want to update. For example, add or remove public key IDs.   Call UpdateKeyGroup with the entire key group object, including the fields that you modified and those that you didn’t.

    Args:
        key_group_config(Dict): The key group configuration.
        id_(str): The identifier of the key group that you are updating.
        if_match(str, optional): The version of the key group that you are updating. The version is the key group’s ETag value. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.key_group.init.update(
                ctx, key_group_config=value, id_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.key_group.init.update key_group_config=value, id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="update_key_group",
        op_kwargs={"KeyGroupConfig": key_group_config, "Id": id_, "IfMatch": if_match},
    )
