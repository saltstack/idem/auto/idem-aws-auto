"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "cache_policy_id"


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, cache_policy_id: str, marker: str = None, max_items: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of distribution IDs for distributions that have a cache behavior that’s associated with the specified cache policy. You can optionally specify the maximum number of items to receive in the response. If the total number of items in the list exceeds the maximum that you specify, or the default maximum, the response is paginated. To get the next page of items, send a subsequent request that specifies the NextMarker value from the current response as the Marker value in the subsequent request.

    Args:
        marker(str, optional): Use this field when paginating results to indicate where to begin in your list of distribution IDs. The response includes distribution IDs in the list that occur after the marker. To get the next page of the list, set this field’s value to the value of NextMarker from the current page’s response. Defaults to None.
        max_items(str, optional): The maximum number of distribution IDs that you want in the response. Defaults to None.
        cache_policy_id(str): The ID of the cache policy whose associated distribution IDs you want to list.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.distribution.by.cache_policy_id_.list(
                ctx, cache_policy_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.distribution.by.cache_policy_id.list cache_policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="list_distributions_by_cache_policy_id",
        op_kwargs={
            "Marker": marker,
            "MaxItems": max_items,
            "CachePolicyId": cache_policy_id,
        },
    )
