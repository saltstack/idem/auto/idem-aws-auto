"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, id_: str) -> Dict:
    r"""
    **Autogenerated function**

    Get the configuration information about a distribution.

    Args:
        id_(str): The distribution's ID. If the ID is empty, an empty distribution configuration is returned.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudfront.distribution.config.get(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudfront.distribution.config.get id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudfront",
        operation="get_distribution_config",
        op_kwargs={"Id": id_},
    )
