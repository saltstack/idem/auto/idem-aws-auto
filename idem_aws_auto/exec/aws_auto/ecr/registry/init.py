"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Describes the settings for a registry. The replication configuration for a repository can be created or updated with the PutReplicationConfiguration API action.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ecr.registry.init.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ecr.registry.init.describe
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="ecr", operation="describe_registry", op_kwargs={}
    )
