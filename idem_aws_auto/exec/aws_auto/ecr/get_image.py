"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(
    hub,
    ctx,
    repository_name: str,
    image_ids: List,
    registry_id: str = None,
    accepted_media_types: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets detailed information for an image. Images are specified with either an imageTag or imageDigest. When an image is pulled, the BatchGetImage API is called once to retrieve the image manifest.

    Args:
        registry_id(str, optional): The AWS account ID associated with the registry that contains the images to describe. If you do not specify a registry, the default registry is assumed. Defaults to None.
        repository_name(str): The repository that contains the images to describe.
        image_ids(List): A list of image ID references that correspond to images to describe. The format of the imageIds reference is imageTag=tag or imageDigest=digest.
        accepted_media_types(List, optional): The accepted media types for the request. Valid values: application/vnd.docker.distribution.manifest.v1+json | application/vnd.docker.distribution.manifest.v2+json | application/vnd.oci.image.manifest.v1+json . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ecr.get_image.batch(ctx, repository_name=value, image_ids=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ecr.get_image.batch repository_name=value, image_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ecr",
        operation="batch_get_image",
        op_kwargs={
            "registryId": registry_id,
            "repositoryName": repository_name,
            "imageIds": image_ids,
            "acceptedMediaTypes": accepted_media_types,
        },
    )
