"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    alert_name: str,
    alert_sensitivity_threshold: int,
    anomaly_detector_arn: str,
    action: Dict,
    alert_description: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an alert for an anomaly detector.

    Args:
        alert_name(str): The name of the alert.
        alert_sensitivity_threshold(int): An integer from 0 to 100 specifying the alert sensitivity threshold.
        alert_description(str, optional): A description of the alert. Defaults to None.
        anomaly_detector_arn(str): The ARN of the detector to which the alert is attached.
        action(Dict): Action that will be triggered when there is an alert.
        tags(Dict, optional): A list of tags to apply to the alert. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.alert.create(
                ctx,
                alert_name=value,
                alert_sensitivity_threshold=value,
                anomaly_detector_arn=value,
                action=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.alert.create alert_name=value, alert_sensitivity_threshold=value, anomaly_detector_arn=value, action=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="create_alert",
        op_kwargs={
            "AlertName": alert_name,
            "AlertSensitivityThreshold": alert_sensitivity_threshold,
            "AlertDescription": alert_description,
            "AnomalyDetectorArn": anomaly_detector_arn,
            "Action": action,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, alert_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an alert.

    Args:
        alert_arn(str): The ARN of the alert to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.alert.delete(ctx, alert_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.alert.delete alert_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="delete_alert",
        op_kwargs={"AlertArn": alert_arn},
    )


async def describe(hub, ctx, alert_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes an alert. Amazon Lookout for Metrics API actions are eventually consistent. If you do a read operation on a resource immediately after creating or modifying it, use retries to allow time for the write operation to complete.

    Args:
        alert_arn(str): The ARN of the alert to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.alert.describe(ctx, alert_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.alert.describe alert_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="describe_alert",
        op_kwargs={"AlertArn": alert_arn},
    )


async def list_all(
    hub,
    ctx,
    anomaly_detector_arn: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the alerts attached to a detector. Amazon Lookout for Metrics API actions are eventually consistent. If you do a read operation on a resource immediately after creating or modifying it, use retries to allow time for the write operation to complete.

    Args:
        anomaly_detector_arn(str, optional): The ARN of the alert's detector. Defaults to None.
        next_token(str, optional): If the result of the previous request is truncated, the response includes a NextToken. To retrieve the next set of results, use the token in the next request. Tokens expire after 24 hours. Defaults to None.
        max_results(int, optional): The maximum number of results that will be displayed by the request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.alert.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.alert.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="list_alerts",
        op_kwargs={
            "AnomalyDetectorArn": anomaly_detector_arn,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
