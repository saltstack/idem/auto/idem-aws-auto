"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def activate(hub, ctx, anomaly_detector_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Activates an anomaly detector.

    Args:
        anomaly_detector_arn(str): The ARN of the anomaly detector.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.activate(
                ctx, anomaly_detector_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.activate anomaly_detector_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="activate_anomaly_detector",
        op_kwargs={"AnomalyDetectorArn": anomaly_detector_arn},
    )


async def create(
    hub,
    ctx,
    anomaly_detector_name: str,
    anomaly_detector_config: Dict,
    anomaly_detector_description: str = None,
    kms_key_arn: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an anomaly detector.

    Args:
        anomaly_detector_name(str): The name of the detector.
        anomaly_detector_description(str, optional): A description of the detector. Defaults to None.
        anomaly_detector_config(Dict): Contains information about the configuration of the anomaly detector.
        kms_key_arn(str, optional): The ARN of the KMS key to use to encrypt your data. Defaults to None.
        tags(Dict, optional): A list of tags to apply to the anomaly detector. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.create(
                ctx, anomaly_detector_name=value, anomaly_detector_config=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.create anomaly_detector_name=value, anomaly_detector_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="create_anomaly_detector",
        op_kwargs={
            "AnomalyDetectorName": anomaly_detector_name,
            "AnomalyDetectorDescription": anomaly_detector_description,
            "AnomalyDetectorConfig": anomaly_detector_config,
            "KmsKeyArn": kms_key_arn,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, anomaly_detector_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a detector. Deleting an anomaly detector will delete all of its corresponding resources including any configured datasets and alerts.

    Args:
        anomaly_detector_arn(str): The ARN of the detector to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.delete(
                ctx, anomaly_detector_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.delete anomaly_detector_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="delete_anomaly_detector",
        op_kwargs={"AnomalyDetectorArn": anomaly_detector_arn},
    )


async def describe(hub, ctx, anomaly_detector_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes a detector. Amazon Lookout for Metrics API actions are eventually consistent. If you do a read operation on a resource immediately after creating or modifying it, use retries to allow time for the write operation to complete.

    Args:
        anomaly_detector_arn(str): The ARN of the detector to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.describe(
                ctx, anomaly_detector_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.describe anomaly_detector_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="describe_anomaly_detector",
        op_kwargs={"AnomalyDetectorArn": anomaly_detector_arn},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the detectors in the current AWS Region. Amazon Lookout for Metrics API actions are eventually consistent. If you do a read operation on a resource immediately after creating or modifying it, use retries to allow time for the write operation to complete.

    Args:
        max_results(int, optional): The maximum number of results to return. Defaults to None.
        next_token(str, optional): If the result of the previous request was truncated, the response includes a NextToken. To retrieve the next set of results, use the token in the next request. Tokens expire after 24 hours. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="list_anomaly_detectors",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(
    hub,
    ctx,
    anomaly_detector_arn: str,
    kms_key_arn: str = None,
    anomaly_detector_description: str = None,
    anomaly_detector_config: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a detector. After activation, you can only change a detector's ingestion delay and description.

    Args:
        anomaly_detector_arn(str): The ARN of the detector to update.
        kms_key_arn(str, optional): The Amazon Resource Name (ARN) of an AWS KMS encryption key. Defaults to None.
        anomaly_detector_description(str, optional): The updated detector description. Defaults to None.
        anomaly_detector_config(Dict, optional): Contains information about the configuration to which the detector will be updated. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutmetric.anomaly.detector.update(
                ctx, anomaly_detector_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutmetric.anomaly.detector.update anomaly_detector_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutmetrics",
        operation="update_anomaly_detector",
        op_kwargs={
            "AnomalyDetectorArn": anomaly_detector_arn,
            "KmsKeyArn": kms_key_arn,
            "AnomalyDetectorDescription": anomaly_detector_description,
            "AnomalyDetectorConfig": anomaly_detector_config,
        },
    )
