"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, campaign_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieve all the deliverability data for a specific campaign. This data is available for a campaign only if the campaign sent email by using a domain that the Deliverability dashboard is enabled for.

    Args:
        campaign_id(str): The unique identifier for the campaign. The Deliverability dashboard automatically generates and assigns this identifier to a campaign.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.domain.deliverability_campaign.get(ctx, campaign_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.domain.deliverability_campaign.get campaign_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="get_domain_deliverability_campaign",
        op_kwargs={"CampaignId": campaign_id},
    )


async def list_all(
    hub,
    ctx,
    start_date: str,
    end_date: str,
    subscribed_domain: str,
    next_token: str = None,
    page_size: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieve deliverability data for all the campaigns that used a specific domain to send email during a specified time range. This data is available for a domain only if you enabled the Deliverability dashboard for the domain.

    Args:
        start_date(str): The first day, in Unix time format, that you want to obtain deliverability data for.
        end_date(str): The last day, in Unix time format, that you want to obtain deliverability data for. This value has to be less than or equal to 30 days after the value of the StartDate parameter.
        subscribed_domain(str): The domain to obtain deliverability data for.
        next_token(str, optional): A token that’s returned from a previous call to the ListDomainDeliverabilityCampaigns operation. This token indicates the position of a campaign in the list of campaigns. Defaults to None.
        page_size(int, optional): The maximum number of results to include in response to a single call to the ListDomainDeliverabilityCampaigns operation. If the number of results is larger than the number that you specify in this parameter, the response includes a NextToken element, which you can use to obtain additional results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.domain.deliverability_campaign.list_all(
                ctx, start_date=value, end_date=value, subscribed_domain=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.domain.deliverability_campaign.list_all start_date=value, end_date=value, subscribed_domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="list_domain_deliverability_campaigns",
        op_kwargs={
            "StartDate": start_date,
            "EndDate": end_date,
            "SubscribedDomain": subscribed_domain,
            "NextToken": next_token,
            "PageSize": page_size,
        },
    )
