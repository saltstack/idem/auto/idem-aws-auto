"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def delete(hub, ctx, email_address: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes an email address from the suppression list for your account.

    Args:
        email_address(str): The suppressed email destination to remove from the account suppression list.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.suppressed_destination.delete(ctx, email_address=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.suppressed_destination.delete email_address=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="delete_suppressed_destination",
        op_kwargs={"EmailAddress": email_address},
    )


async def get(hub, ctx, email_address: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about a specific email address that's on the suppression list for your account.

    Args:
        email_address(str): The email address that's on the account suppression list.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.suppressed_destination.get(ctx, email_address=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.suppressed_destination.get email_address=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="get_suppressed_destination",
        op_kwargs={"EmailAddress": email_address},
    )


async def list_all(
    hub,
    ctx,
    reasons: List = None,
    start_date: str = None,
    end_date: str = None,
    next_token: str = None,
    page_size: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of email addresses that are on the suppression list for your account.

    Args:
        reasons(List, optional): The factors that caused the email address to be added to . Defaults to None.
        start_date(str, optional): Used to filter the list of suppressed email destinations so that it only includes addresses that were added to the list after a specific date. The date that you specify should be in Unix time format. Defaults to None.
        end_date(str, optional): Used to filter the list of suppressed email destinations so that it only includes addresses that were added to the list before a specific date. The date that you specify should be in Unix time format. Defaults to None.
        next_token(str, optional): A token returned from a previous call to ListSuppressedDestinations to indicate the position in the list of suppressed email addresses. Defaults to None.
        page_size(int, optional): The number of results to show in a single call to ListSuppressedDestinations. If the number of results is larger than the number you specified in this parameter, then the response includes a NextToken element, which you can use to obtain additional results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.suppressed_destination.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.suppressed_destination.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="list_suppressed_destinations",
        op_kwargs={
            "Reasons": reasons,
            "StartDate": start_date,
            "EndDate": end_date,
            "NextToken": next_token,
            "PageSize": page_size,
        },
    )


async def put(hub, ctx, email_address: str, reason: str) -> Dict:
    r"""
    **Autogenerated function**

    Adds an email address to the suppression list for your account.

    Args:
        email_address(str): The email address that should be added to the suppression list for your account.
        reason(str): The factors that should cause the email address to be added to the suppression list for your account.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sesv2.suppressed_destination.put(
                ctx, email_address=value, reason=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sesv2.suppressed_destination.put email_address=value, reason=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sesv2",
        operation="put_suppressed_destination",
        op_kwargs={"EmailAddress": email_address, "Reason": reason},
    )
