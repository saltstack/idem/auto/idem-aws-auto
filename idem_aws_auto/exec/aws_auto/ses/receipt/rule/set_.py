"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__virtualname__ = "set"


async def clone(hub, ctx, rule_set_name: str, original_rule_set_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Creates a receipt rule set by cloning an existing one. All receipt rules and configurations are copied to the new receipt rule set and are completely independent of the source rule set. For information about setting up rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        rule_set_name(str): The name of the rule set to create. The name must:   This value can only contain ASCII letters (a-z, A-Z), numbers (0-9), underscores (_), or dashes (-).   Start and end with a letter or number.   Contain less than 64 characters.  .
        original_rule_set_name(str): The name of the rule set to clone.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.clone(
                ctx, rule_set_name=value, original_rule_set_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.clone rule_set_name=value, original_rule_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="clone_receipt_rule_set",
        op_kwargs={
            "RuleSetName": rule_set_name,
            "OriginalRuleSetName": original_rule_set_name,
        },
    )


async def create(hub, ctx, rule_set_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Creates an empty receipt rule set. For information about setting up receipt rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        rule_set_name(str): The name of the rule set to create. The name must:   This value can only contain ASCII letters (a-z, A-Z), numbers (0-9), underscores (_), or dashes (-).   Start and end with a letter or number.   Contain less than 64 characters.  .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.create(ctx, rule_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.create rule_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="create_receipt_rule_set",
        op_kwargs={"RuleSetName": rule_set_name},
    )


async def delete(hub, ctx, rule_set_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified receipt rule set and all of the receipt rules it contains.  The currently active rule set cannot be deleted.  For information about managing receipt rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        rule_set_name(str): The name of the receipt rule set to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.delete(ctx, rule_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.delete rule_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="delete_receipt_rule_set",
        op_kwargs={"RuleSetName": rule_set_name},
    )


async def describe(hub, ctx, rule_set_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the details of the specified receipt rule set. For information about managing receipt rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        rule_set_name(str): The name of the receipt rule set to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.describe(ctx, rule_set_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.describe rule_set_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="describe_receipt_rule_set",
        op_kwargs={"RuleSetName": rule_set_name},
    )


async def list_all(hub, ctx, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the receipt rule sets that exist under your AWS account in the current AWS Region. If there are additional receipt rule sets to be retrieved, you will receive a NextToken that you can provide to the next call to ListReceiptRuleSets to retrieve the additional entries. For information about managing receipt rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        next_token(str, optional): A token returned from a previous call to ListReceiptRuleSets to indicate the position in the receipt rule set list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="list_receipt_rule_sets",
        op_kwargs={"NextToken": next_token},
    )


async def reorder(hub, ctx, rule_set_name: str, rule_names: List) -> Dict:
    r"""
    **Autogenerated function**

    Reorders the receipt rules within a receipt rule set.  All of the rules in the rule set must be represented in this request. That is, this API will return an error if the reorder request doesn't explicitly position all of the rules.  For information about managing receipt rule sets, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        rule_set_name(str): The name of the receipt rule set to reorder.
        rule_names(List): A list of the specified receipt rule set's receipt rules in the order that you want to put them.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.receipt.rule.set_.reorder(
                ctx, rule_set_name=value, rule_names=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.receipt.rule.set.reorder rule_set_name=value, rule_names=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="reorder_receipt_rule_set",
        op_kwargs={"RuleSetName": rule_set_name, "RuleNames": rule_names},
    )
