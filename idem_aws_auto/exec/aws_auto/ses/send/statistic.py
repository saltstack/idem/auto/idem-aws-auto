"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Provides sending statistics for the current AWS Region. The result is a list of data points, representing the last two weeks of sending activity. Each data point in the list contains statistics for a 15-minute period of time. You can execute this operation no more than once per second.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.send.statistic.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.send.statistic.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="ses", operation="get_send_statistics", op_kwargs={}
    )
