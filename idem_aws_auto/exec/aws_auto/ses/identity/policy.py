"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def delete(hub, ctx, identity: str, policy_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified sending authorization policy for the given identity (an email address or a domain). This API returns successfully even if a policy with the specified name does not exist.  This API is for the identity owner only. If you have not verified the identity, this API will return an error.  Sending authorization is a feature that enables an identity owner to authorize other senders to use its identities. For information about using sending authorization, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        identity(str): The identity that is associated with the policy that you want to delete. You can specify the identity by using its name or by using its Amazon Resource Name (ARN). Examples: user@example.com, example.com, arn:aws:ses:us-east-1:123456789012:identity/example.com. To successfully call this API, you must own the identity.
        policy_name(str): The name of the policy to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.identity.policy.delete(
                ctx, identity=value, policy_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.identity.policy.delete identity=value, policy_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="delete_identity_policy",
        op_kwargs={"Identity": identity, "PolicyName": policy_name},
    )


async def get_all(hub, ctx, identity: str, policy_names: List) -> Dict:
    r"""
    **Autogenerated function**

    Returns the requested sending authorization policies for the given identity (an email address or a domain). The policies are returned as a map of policy names to policy contents. You can retrieve a maximum of 20 policies at a time.  This API is for the identity owner only. If you have not verified the identity, this API will return an error.  Sending authorization is a feature that enables an identity owner to authorize other senders to use its identities. For information about using sending authorization, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        identity(str): The identity for which the policies will be retrieved. You can specify an identity by using its name or by using its Amazon Resource Name (ARN). Examples: user@example.com, example.com, arn:aws:ses:us-east-1:123456789012:identity/example.com. To successfully call this API, you must own the identity.
        policy_names(List): A list of the names of policies to be retrieved. You can retrieve a maximum of 20 policies at a time. If you do not know the names of the policies that are attached to the identity, you can use ListIdentityPolicies.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.identity.policy.get_all(
                ctx, identity=value, policy_names=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.identity.policy.get_all identity=value, policy_names=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="get_identity_policies",
        op_kwargs={"Identity": identity, "PolicyNames": policy_names},
    )


async def list_all(hub, ctx, identity: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of sending authorization policies that are attached to the given identity (an email address or a domain). This API returns only a list. If you want the actual policy content, you can use GetIdentityPolicies.  This API is for the identity owner only. If you have not verified the identity, this API will return an error.  Sending authorization is a feature that enables an identity owner to authorize other senders to use its identities. For information about using sending authorization, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        identity(str): The identity that is associated with the policy for which the policies will be listed. You can specify an identity by using its name or by using its Amazon Resource Name (ARN). Examples: user@example.com, example.com, arn:aws:ses:us-east-1:123456789012:identity/example.com. To successfully call this API, you must own the identity.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.identity.policy.list_all(ctx, identity=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.identity.policy.list_all identity=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="list_identity_policies",
        op_kwargs={"Identity": identity},
    )


async def put(hub, ctx, identity: str, policy_name: str, policy: str) -> Dict:
    r"""
    **Autogenerated function**

    Adds or updates a sending authorization policy for the specified identity (an email address or a domain).  This API is for the identity owner only. If you have not verified the identity, this API will return an error.  Sending authorization is a feature that enables an identity owner to authorize other senders to use its identities. For information about using sending authorization, see the Amazon SES Developer Guide. You can execute this operation no more than once per second.

    Args:
        identity(str): The identity that the policy will apply to. You can specify an identity by using its name or by using its Amazon Resource Name (ARN). Examples: user@example.com, example.com, arn:aws:ses:us-east-1:123456789012:identity/example.com. To successfully call this API, you must own the identity.
        policy_name(str): The name of the policy. The policy name cannot exceed 64 characters and can only include alphanumeric characters, dashes, and underscores.
        policy(str): The text of the policy in JSON format. The policy cannot exceed 4 KB. For information about the syntax of sending authorization policies, see the Amazon SES Developer Guide. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.identity.policy.put(
                ctx, identity=value, policy_name=value, policy=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.identity.policy.put identity=value, policy_name=value, policy=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="put_identity_policy",
        op_kwargs={"Identity": identity, "PolicyName": policy_name, "Policy": policy},
    )
