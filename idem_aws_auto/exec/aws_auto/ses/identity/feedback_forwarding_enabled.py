"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"set_": "set"}


async def set_(hub, ctx, identity: str, forwarding_enabled: bool) -> Dict:
    r"""
    **Autogenerated function**

    Given an identity (an email address or a domain), enables or disables whether Amazon SES forwards bounce and complaint notifications as email. Feedback forwarding can only be disabled when Amazon Simple Notification Service (Amazon SNS) topics are specified for both bounces and complaints.  Feedback forwarding does not apply to delivery notifications. Delivery notifications are only available through Amazon SNS.  You can execute this operation no more than once per second. For more information about using notifications with Amazon SES, see the Amazon SES Developer Guide.

    Args:
        identity(str): The identity for which to set bounce and complaint notification forwarding. Examples: user@example.com, example.com.
        forwarding_enabled(bool): Sets whether Amazon SES will forward bounce and complaint notifications as email. true specifies that Amazon SES will forward bounce and complaint notifications as email, in addition to any Amazon SNS topic publishing otherwise specified. false specifies that Amazon SES will publish bounce and complaint notifications only through Amazon SNS. This value can only be set to false when Amazon SNS topics are set for both Bounce and Complaint notification types.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ses.identity.feedback_forwarding_enabled.set(
                ctx, identity=value, forwarding_enabled=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ses.identity.feedback_forwarding_enabled.set identity=value, forwarding_enabled=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ses",
        operation="set_identity_feedback_forwarding_enabled",
        op_kwargs={"Identity": identity, "ForwardingEnabled": forwarding_enabled},
    )
