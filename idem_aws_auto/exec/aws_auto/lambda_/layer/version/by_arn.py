"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a version of an Lambda layer, with a link to download the layer archive that's valid for 10 minutes.

    Args:
        arn(str): The ARN of the layer version.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.layer.version.by_arn.get(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.layer.version.by_arn.get arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="get_layer_version_by_arn",
        op_kwargs={"Arn": arn},
    )
