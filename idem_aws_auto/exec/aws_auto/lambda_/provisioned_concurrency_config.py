"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, function_name: str, qualifier: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the provisioned concurrency configuration for a function.

    Args:
        function_name(str): The name of the Lambda function.  Name formats     Function name - my-function.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:my-function.    Partial ARN - 123456789012:function:my-function.   The length constraint applies only to the full ARN. If you specify only the function name, it is limited to 64 characters in length.
        qualifier(str): The version number or alias name.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.provisioned_concurrency_config.delete(
                ctx, function_name=value, qualifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.provisioned_concurrency_config.delete function_name=value, qualifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="delete_provisioned_concurrency_config",
        op_kwargs={"FunctionName": function_name, "Qualifier": qualifier},
    )


async def get(hub, ctx, function_name: str, qualifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the provisioned concurrency configuration for a function's alias or version.

    Args:
        function_name(str): The name of the Lambda function.  Name formats     Function name - my-function.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:my-function.    Partial ARN - 123456789012:function:my-function.   The length constraint applies only to the full ARN. If you specify only the function name, it is limited to 64 characters in length.
        qualifier(str): The version number or alias name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.provisioned_concurrency_config.get(
                ctx, function_name=value, qualifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.provisioned_concurrency_config.get function_name=value, qualifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="get_provisioned_concurrency_config",
        op_kwargs={"FunctionName": function_name, "Qualifier": qualifier},
    )


async def list_all(
    hub, ctx, function_name: str, marker: str = None, max_items: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of provisioned concurrency configurations for a function.

    Args:
        function_name(str): The name of the Lambda function.  Name formats     Function name - my-function.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:my-function.    Partial ARN - 123456789012:function:my-function.   The length constraint applies only to the full ARN. If you specify only the function name, it is limited to 64 characters in length.
        marker(str, optional): Specify the pagination token that's returned by a previous request to retrieve the next page of results. Defaults to None.
        max_items(int, optional): Specify a number to limit the number of configurations returned. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.provisioned_concurrency_config.list_all(
                ctx, function_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.provisioned_concurrency_config.list_all function_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="list_provisioned_concurrency_configs",
        op_kwargs={
            "FunctionName": function_name,
            "Marker": marker,
            "MaxItems": max_items,
        },
    )


async def put(
    hub, ctx, function_name: str, qualifier: str, provisioned_concurrent_executions: int
) -> Dict:
    r"""
    **Autogenerated function**

    Adds a provisioned concurrency configuration to a function's alias or version.

    Args:
        function_name(str): The name of the Lambda function.  Name formats     Function name - my-function.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:my-function.    Partial ARN - 123456789012:function:my-function.   The length constraint applies only to the full ARN. If you specify only the function name, it is limited to 64 characters in length.
        qualifier(str): The version number or alias name.
        provisioned_concurrent_executions(int): The amount of provisioned concurrency to allocate for the version or alias.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.provisioned_concurrency_config.put(
                ctx, function_name=value, qualifier=value, provisioned_concurrent_executions=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.provisioned_concurrency_config.put function_name=value, qualifier=value, provisioned_concurrent_executions=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="put_provisioned_concurrency_config",
        op_kwargs={
            "FunctionName": function_name,
            "Qualifier": qualifier,
            "ProvisionedConcurrentExecutions": provisioned_concurrent_executions,
        },
    )
