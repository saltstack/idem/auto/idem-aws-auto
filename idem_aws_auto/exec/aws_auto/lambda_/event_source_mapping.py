"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    function_name: str,
    event_source_arn: str = None,
    enabled: bool = None,
    batch_size: int = None,
    maximum_batching_window_in_seconds: int = None,
    parallelization_factor: int = None,
    starting_position: str = None,
    starting_position_timestamp: str = None,
    destination_config: Dict = None,
    maximum_record_age_in_seconds: int = None,
    bisect_batch_on_function_error: bool = None,
    maximum_retry_attempts: int = None,
    tumbling_window_in_seconds: int = None,
    topics: List = None,
    queues: List = None,
    source_access_configurations: List = None,
    self_managed_event_source: Dict = None,
    function_response_types: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a mapping between an event source and an Lambda function. Lambda reads items from the event source and triggers the function. For details about each event source type, see the following topics. In particular, each of the topics describes the required and optional parameters for the specific event source.      Configuring a Dynamo DB stream as an event source      Configuring a Kinesis stream as an event source      Configuring an SQS queue as an event source      Configuring an MQ broker as an event source      Configuring MSK as an event source      Configuring Self-Managed Apache Kafka as an event source    The following error handling options are only available for stream sources (DynamoDB and Kinesis):    BisectBatchOnFunctionError - If the function returns an error, split the batch in two and retry.    DestinationConfig - Send discarded records to an Amazon SQS queue or Amazon SNS topic.    MaximumRecordAgeInSeconds - Discard records older than the specified age. The default value is infinite (-1). When set to infinite (-1), failed records are retried until the record expires    MaximumRetryAttempts - Discard records after the specified number of retries. The default value is infinite (-1). When set to infinite (-1), failed records are retried until the record expires.    ParallelizationFactor - Process multiple batches from each shard concurrently.

    Args:
        event_source_arn(str, optional): The Amazon Resource Name (ARN) of the event source.    Amazon Kinesis - The ARN of the data stream or a stream consumer.    Amazon DynamoDB Streams - The ARN of the stream.    Amazon Simple Queue Service - The ARN of the queue.    Amazon Managed Streaming for Apache Kafka - The ARN of the cluster.  . Defaults to None.
        function_name(str): The name of the Lambda function.  Name formats     Function name - MyFunction.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction.    Version or Alias ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction:PROD.    Partial ARN - 123456789012:function:MyFunction.   The length constraint applies only to the full ARN. If you specify only the function name, it's limited to 64 characters in length.
        enabled(bool, optional): If true, the event source mapping is active. Set to false to pause polling and invocation. Defaults to None.
        batch_size(int, optional): The maximum number of items to retrieve in a single batch.    Amazon Kinesis - Default 100. Max 10,000.    Amazon DynamoDB Streams - Default 100. Max 1,000.    Amazon Simple Queue Service - Default 10. For standard queues the max is 10,000. For FIFO queues the max is 10.    Amazon Managed Streaming for Apache Kafka - Default 100. Max 10,000.    Self-Managed Apache Kafka - Default 100. Max 10,000.  . Defaults to None.
        maximum_batching_window_in_seconds(int, optional): (Streams and SQS standard queues) The maximum amount of time to gather records before invoking the function, in seconds. Defaults to None.
        parallelization_factor(int, optional): (Streams only) The number of batches to process from each shard concurrently. Defaults to None.
        starting_position(str, optional): The position in a stream from which to start reading. Required for Amazon Kinesis, Amazon DynamoDB, and Amazon MSK Streams sources. AT_TIMESTAMP is only supported for Amazon Kinesis streams. Defaults to None.
        starting_position_timestamp(str, optional): With StartingPosition set to AT_TIMESTAMP, the time from which to start reading. Defaults to None.
        destination_config(Dict, optional): (Streams only) An Amazon SQS queue or Amazon SNS topic destination for discarded records. Defaults to None.
        maximum_record_age_in_seconds(int, optional): (Streams only) Discard records older than the specified age. The default value is infinite (-1). Defaults to None.
        bisect_batch_on_function_error(bool, optional): (Streams only) If the function returns an error, split the batch in two and retry. Defaults to None.
        maximum_retry_attempts(int, optional): (Streams only) Discard records after the specified number of retries. The default value is infinite (-1). When set to infinite (-1), failed records will be retried until the record expires. Defaults to None.
        tumbling_window_in_seconds(int, optional): (Streams only) The duration in seconds of a processing window. The range is between 1 second up to 900 seconds. Defaults to None.
        topics(List, optional): The name of the Kafka topic. Defaults to None.
        queues(List, optional):  (MQ) The name of the Amazon MQ broker destination queue to consume. . Defaults to None.
        source_access_configurations(List, optional): An array of authentication protocols or VPC components required to secure your event source. Defaults to None.
        self_managed_event_source(Dict, optional): The Self-Managed Apache Kafka cluster to send records. Defaults to None.
        function_response_types(List, optional): (Streams only) A list of current response type enums applied to the event source mapping. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.event_source_mapping.create(ctx, function_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.event_source_mapping.create function_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="create_event_source_mapping",
        op_kwargs={
            "EventSourceArn": event_source_arn,
            "FunctionName": function_name,
            "Enabled": enabled,
            "BatchSize": batch_size,
            "MaximumBatchingWindowInSeconds": maximum_batching_window_in_seconds,
            "ParallelizationFactor": parallelization_factor,
            "StartingPosition": starting_position,
            "StartingPositionTimestamp": starting_position_timestamp,
            "DestinationConfig": destination_config,
            "MaximumRecordAgeInSeconds": maximum_record_age_in_seconds,
            "BisectBatchOnFunctionError": bisect_batch_on_function_error,
            "MaximumRetryAttempts": maximum_retry_attempts,
            "TumblingWindowInSeconds": tumbling_window_in_seconds,
            "Topics": topics,
            "Queues": queues,
            "SourceAccessConfigurations": source_access_configurations,
            "SelfManagedEventSource": self_managed_event_source,
            "FunctionResponseTypes": function_response_types,
        },
    )


async def delete(hub, ctx, uuid: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an event source mapping. You can get the identifier of a mapping from the output of ListEventSourceMappings. When you delete an event source mapping, it enters a Deleting state and might not be completely deleted for several seconds.

    Args:
        uuid(str): The identifier of the event source mapping.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.event_source_mapping.delete(ctx, uuid=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.event_source_mapping.delete uuid=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="delete_event_source_mapping",
        op_kwargs={"UUID": uuid},
    )


async def get(hub, ctx, uuid: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns details about an event source mapping. You can get the identifier of a mapping from the output of ListEventSourceMappings.

    Args:
        uuid(str): The identifier of the event source mapping.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.event_source_mapping.get(ctx, uuid=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.event_source_mapping.get uuid=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="get_event_source_mapping",
        op_kwargs={"UUID": uuid},
    )


async def list_all(
    hub,
    ctx,
    event_source_arn: str = None,
    function_name: str = None,
    marker: str = None,
    max_items: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists event source mappings. Specify an EventSourceArn to only show event source mappings for a single event source.

    Args:
        event_source_arn(str, optional): The Amazon Resource Name (ARN) of the event source.    Amazon Kinesis - The ARN of the data stream or a stream consumer.    Amazon DynamoDB Streams - The ARN of the stream.    Amazon Simple Queue Service - The ARN of the queue.    Amazon Managed Streaming for Apache Kafka - The ARN of the cluster.  . Defaults to None.
        function_name(str, optional): The name of the Lambda function.  Name formats     Function name - MyFunction.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction.    Version or Alias ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction:PROD.    Partial ARN - 123456789012:function:MyFunction.   The length constraint applies only to the full ARN. If you specify only the function name, it's limited to 64 characters in length. Defaults to None.
        marker(str, optional): A pagination token returned by a previous call. Defaults to None.
        max_items(int, optional): The maximum number of event source mappings to return. Note that ListEventSourceMappings returns a maximum of 100 items in each response, even if you set the number higher. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.event_source_mapping.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.event_source_mapping.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="list_event_source_mappings",
        op_kwargs={
            "EventSourceArn": event_source_arn,
            "FunctionName": function_name,
            "Marker": marker,
            "MaxItems": max_items,
        },
    )


async def update(
    hub,
    ctx,
    uuid: str,
    function_name: str = None,
    enabled: bool = None,
    batch_size: int = None,
    maximum_batching_window_in_seconds: int = None,
    destination_config: Dict = None,
    maximum_record_age_in_seconds: int = None,
    bisect_batch_on_function_error: bool = None,
    maximum_retry_attempts: int = None,
    parallelization_factor: int = None,
    source_access_configurations: List = None,
    tumbling_window_in_seconds: int = None,
    function_response_types: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an event source mapping. You can change the function that Lambda invokes, or pause invocation and resume later from the same location. The following error handling options are only available for stream sources (DynamoDB and Kinesis):    BisectBatchOnFunctionError - If the function returns an error, split the batch in two and retry.    DestinationConfig - Send discarded records to an Amazon SQS queue or Amazon SNS topic.    MaximumRecordAgeInSeconds - Discard records older than the specified age. The default value is infinite (-1). When set to infinite (-1), failed records are retried until the record expires    MaximumRetryAttempts - Discard records after the specified number of retries. The default value is infinite (-1). When set to infinite (-1), failed records are retried until the record expires.    ParallelizationFactor - Process multiple batches from each shard concurrently.

    Args:
        uuid(str): The identifier of the event source mapping.
        function_name(str, optional): The name of the Lambda function.  Name formats     Function name - MyFunction.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction.    Version or Alias ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction:PROD.    Partial ARN - 123456789012:function:MyFunction.   The length constraint applies only to the full ARN. If you specify only the function name, it's limited to 64 characters in length. Defaults to None.
        enabled(bool, optional): If true, the event source mapping is active. Set to false to pause polling and invocation. Defaults to None.
        batch_size(int, optional): The maximum number of items to retrieve in a single batch.    Amazon Kinesis - Default 100. Max 10,000.    Amazon DynamoDB Streams - Default 100. Max 1,000.    Amazon Simple Queue Service - Default 10. For standard queues the max is 10,000. For FIFO queues the max is 10.    Amazon Managed Streaming for Apache Kafka - Default 100. Max 10,000.    Self-Managed Apache Kafka - Default 100. Max 10,000.  . Defaults to None.
        maximum_batching_window_in_seconds(int, optional): (Streams and SQS standard queues) The maximum amount of time to gather records before invoking the function, in seconds. Defaults to None.
        destination_config(Dict, optional): (Streams only) An Amazon SQS queue or Amazon SNS topic destination for discarded records. Defaults to None.
        maximum_record_age_in_seconds(int, optional): (Streams only) Discard records older than the specified age. The default value is infinite (-1). Defaults to None.
        bisect_batch_on_function_error(bool, optional): (Streams only) If the function returns an error, split the batch in two and retry. Defaults to None.
        maximum_retry_attempts(int, optional): (Streams only) Discard records after the specified number of retries. The default value is infinite (-1). When set to infinite (-1), failed records will be retried until the record expires. Defaults to None.
        parallelization_factor(int, optional): (Streams only) The number of batches to process from each shard concurrently. Defaults to None.
        source_access_configurations(List, optional): An array of authentication protocols or VPC components required to secure your event source. Defaults to None.
        tumbling_window_in_seconds(int, optional): (Streams only) The duration in seconds of a processing window. The range is between 1 second up to 900 seconds. Defaults to None.
        function_response_types(List, optional): (Streams only) A list of current response type enums applied to the event source mapping. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.event_source_mapping.update(ctx, uuid=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.event_source_mapping.update uuid=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="update_event_source_mapping",
        op_kwargs={
            "UUID": uuid,
            "FunctionName": function_name,
            "Enabled": enabled,
            "BatchSize": batch_size,
            "MaximumBatchingWindowInSeconds": maximum_batching_window_in_seconds,
            "DestinationConfig": destination_config,
            "MaximumRecordAgeInSeconds": maximum_record_age_in_seconds,
            "BisectBatchOnFunctionError": bisect_batch_on_function_error,
            "MaximumRetryAttempts": maximum_retry_attempts,
            "ParallelizationFactor": parallelization_factor,
            "SourceAccessConfigurations": source_access_configurations,
            "TumblingWindowInSeconds": tumbling_window_in_seconds,
            "FunctionResponseTypes": function_response_types,
        },
    )
