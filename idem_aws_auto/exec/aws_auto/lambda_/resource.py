"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource: str, tags: Dict) -> None:
    r"""
    **Autogenerated function**

    Adds tags to a function.

    Args:
        resource(str): The function's Amazon Resource Name (ARN).
        tags(Dict): A list of tags to apply to the function.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.resource.tag(ctx, resource=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.resource.tag resource=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="tag_resource",
        op_kwargs={"Resource": resource, "Tags": tags},
    )


async def untag(hub, ctx, resource: str, tag_keys: List) -> None:
    r"""
    **Autogenerated function**

    Removes tags from a function.

    Args:
        resource(str): The function's Amazon Resource Name (ARN).
        tag_keys(List): A list of tag keys to remove from the function.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.resource.untag(ctx, resource=value, tag_keys=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.resource.untag resource=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="untag_resource",
        op_kwargs={"Resource": resource, "TagKeys": tag_keys},
    )
