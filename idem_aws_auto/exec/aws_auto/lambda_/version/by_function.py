"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, function_name: str, marker: str = None, max_items: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of versions, with the version-specific configuration of each. Lambda returns up to 50 versions per call.

    Args:
        function_name(str): The name of the Lambda function.  Name formats     Function name - MyFunction.    Function ARN - arn:aws:lambda:us-west-2:123456789012:function:MyFunction.    Partial ARN - 123456789012:function:MyFunction.   The length constraint applies only to the full ARN. If you specify only the function name, it is limited to 64 characters in length.
        marker(str, optional): Specify the pagination token that's returned by a previous request to retrieve the next page of results. Defaults to None.
        max_items(int, optional): The maximum number of versions to return. Note that ListVersionsByFunction returns a maximum of 50 items in each response, even if you set the number higher. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lambda_.version.by_function.list(ctx, function_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lambda_.version.by_function.list function_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lambda",
        operation="list_versions_by_function",
        op_kwargs={
            "FunctionName": function_name,
            "Marker": marker,
            "MaxItems": max_items,
        },
    )
