"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, filters: Dict, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of the events emitted by the resources that are evaluated by DevOps Guru. You can use filters to specify which events are returned.

    Args:
        filters(Dict):  A ListEventsFilters object used to specify which events to return. .
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.
        next_token(str, optional): The pagination token to use to retrieve the next page of results for this operation. If this value is null, it retrieves the first page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.devop_guru.event.list_all(ctx, filters=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.devop_guru.event.list_all filters=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="devops-guru",
        operation="list_events",
        op_kwargs={
            "Filters": filters,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
