"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, pipeline_name: str, pipeline_activities: List, tags: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a pipeline. A pipeline consumes messages from a channel and allows you to process the messages before storing them in a data store. You must specify both a channel and a datastore activity and, optionally, as many as 23 additional activities in the pipelineActivities array.

    Args:
        pipeline_name(str): The name of the pipeline.
        pipeline_activities(List): A list of PipelineActivity objects. Activities perform transformations on your messages, such as removing, renaming or adding message attributes; filtering messages based on attribute values; invoking your Lambda unctions on messages for advanced processing; or performing mathematical transformations to normalize device data. The list can be 2-25 PipelineActivity objects and must contain both a channel and a datastore activity. Each entry in the list must contain only one activity. For example:  pipelineActivities = [ { "channel": { ... } }, { "lambda": { ... } }, ... ] .
        tags(List, optional): Metadata which can be used to manage the pipeline. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotanalytic.pipeline.init.create(
                ctx, pipeline_name=value, pipeline_activities=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotanalytic.pipeline.init.create pipeline_name=value, pipeline_activities=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotanalytics",
        operation="create_pipeline",
        op_kwargs={
            "pipelineName": pipeline_name,
            "pipelineActivities": pipeline_activities,
            "tags": tags,
        },
    )


async def delete(hub, ctx, pipeline_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified pipeline.

    Args:
        pipeline_name(str): The name of the pipeline to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotanalytic.pipeline.init.delete(ctx, pipeline_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotanalytic.pipeline.init.delete pipeline_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotanalytics",
        operation="delete_pipeline",
        op_kwargs={"pipelineName": pipeline_name},
    )


async def describe(hub, ctx, pipeline_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about a pipeline.

    Args:
        pipeline_name(str): The name of the pipeline whose information is retrieved.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotanalytic.pipeline.init.describe(ctx, pipeline_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotanalytic.pipeline.init.describe pipeline_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotanalytics",
        operation="describe_pipeline",
        op_kwargs={"pipelineName": pipeline_name},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of pipelines.

    Args:
        next_token(str, optional): The token for the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return in this request. The default value is 100. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotanalytic.pipeline.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotanalytic.pipeline.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotanalytics",
        operation="list_pipelines",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )


async def update(hub, ctx, pipeline_name: str, pipeline_activities: List) -> None:
    r"""
    **Autogenerated function**

    Updates the settings of a pipeline. You must specify both a channel and a datastore activity and, optionally, as many as 23 additional activities in the pipelineActivities array.

    Args:
        pipeline_name(str): The name of the pipeline to update.
        pipeline_activities(List): A list of PipelineActivity objects. Activities perform transformations on your messages, such as removing, renaming or adding message attributes; filtering messages based on attribute values; invoking your Lambda functions on messages for advanced processing; or performing mathematical transformations to normalize device data. The list can be 2-25 PipelineActivity objects and must contain both a channel and a datastore activity. Each entry in the list must contain only one activity. For example:  pipelineActivities = [ { "channel": { ... } }, { "lambda": { ... } }, ... ] .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotanalytic.pipeline.init.update(
                ctx, pipeline_name=value, pipeline_activities=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotanalytic.pipeline.init.update pipeline_name=value, pipeline_activities=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotanalytics",
        operation="update_pipeline",
        op_kwargs={
            "pipelineName": pipeline_name,
            "pipelineActivities": pipeline_activities,
        },
    )
