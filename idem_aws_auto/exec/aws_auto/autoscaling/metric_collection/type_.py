"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "type"


async def describe_all(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Describes the available CloudWatch metrics for Amazon EC2 Auto Scaling. The GroupStandbyInstances metric is not returned by default. You must explicitly request this metric when calling the EnableMetricsCollection API.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.autoscaling.metric_collection.type_.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.autoscaling.metric_collection.type.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="autoscaling",
        operation="describe_metric_collection_types",
        op_kwargs={},
    )
