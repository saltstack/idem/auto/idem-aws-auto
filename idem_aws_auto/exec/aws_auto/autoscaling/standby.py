"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__func_alias__ = {"exit_": "exit"}


async def enter(
    hub,
    ctx,
    auto_scaling_group_name: str,
    should_decrement_desired_capacity: bool,
    instance_ids: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Moves the specified instances into the standby state. If you choose to decrement the desired capacity of the Auto Scaling group, the instances can enter standby as long as the desired capacity of the Auto Scaling group after the instances are placed into standby is equal to or greater than the minimum capacity of the group. If you choose not to decrement the desired capacity of the Auto Scaling group, the Auto Scaling group launches new instances to replace the instances on standby. For more information, see Temporarily removing instances from your Auto Scaling group in the Amazon EC2 Auto Scaling User Guide.

    Args:
        instance_ids(List, optional): The IDs of the instances. You can specify up to 20 instances. Defaults to None.
        auto_scaling_group_name(str): The name of the Auto Scaling group.
        should_decrement_desired_capacity(bool): Indicates whether to decrement the desired capacity of the Auto Scaling group by the number of instances moved to Standby mode.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.autoscaling.standby.enter(
                ctx, auto_scaling_group_name=value, should_decrement_desired_capacity=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.autoscaling.standby.enter auto_scaling_group_name=value, should_decrement_desired_capacity=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="autoscaling",
        operation="enter_standby",
        op_kwargs={
            "InstanceIds": instance_ids,
            "AutoScalingGroupName": auto_scaling_group_name,
            "ShouldDecrementDesiredCapacity": should_decrement_desired_capacity,
        },
    )


async def exit_(
    hub, ctx, auto_scaling_group_name: str, instance_ids: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Moves the specified instances out of the standby state. After you put the instances back in service, the desired capacity is incremented. For more information, see Temporarily removing instances from your Auto Scaling group in the Amazon EC2 Auto Scaling User Guide.

    Args:
        instance_ids(List, optional): The IDs of the instances. You can specify up to 20 instances. Defaults to None.
        auto_scaling_group_name(str): The name of the Auto Scaling group.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.autoscaling.standby.exit(ctx, auto_scaling_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.autoscaling.standby.exit auto_scaling_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="autoscaling",
        operation="exit_standby",
        op_kwargs={
            "InstanceIds": instance_ids,
            "AutoScalingGroupName": auto_scaling_group_name,
        },
    )
