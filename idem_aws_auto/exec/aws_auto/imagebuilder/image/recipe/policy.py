"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, image_recipe_arn: str) -> Dict:
    r"""
    **Autogenerated function**

     Gets an image recipe policy.

    Args:
        image_recipe_arn(str): The Amazon Resource Name (ARN) of the image recipe whose policy you want to retrieve.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.image.recipe.policy.get(
                ctx, image_recipe_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.image.recipe.policy.get image_recipe_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="get_image_recipe_policy",
        op_kwargs={"imageRecipeArn": image_recipe_arn},
    )


async def put(hub, ctx, image_recipe_arn: str, policy: str) -> Dict:
    r"""
    **Autogenerated function**

     Applies a policy to an image recipe. We recommend that you call the RAM API CreateResourceShare to share resources. If you call the Image Builder API PutImageRecipePolicy, you must also call the RAM API PromoteResourceShareCreatedFromPolicy in order for the resource to be visible to all principals with whom the resource is shared.

    Args:
        image_recipe_arn(str): The Amazon Resource Name (ARN) of the image recipe that this policy should be applied to.
        policy(str): The policy to apply.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.image.recipe.policy.put(
                ctx, image_recipe_arn=value, policy=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.image.recipe.policy.put image_recipe_arn=value, policy=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="put_image_recipe_policy",
        op_kwargs={"imageRecipeArn": image_recipe_arn, "policy": policy},
    )
