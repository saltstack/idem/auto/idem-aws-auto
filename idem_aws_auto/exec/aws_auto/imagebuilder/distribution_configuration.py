"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    distributions: List,
    client_token: str,
    description: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new distribution configuration. Distribution configurations define and configure the outputs of your pipeline.

    Args:
        name(str):  The name of the distribution configuration.
        description(str, optional):  The description of the distribution configuration. Defaults to None.
        distributions(List):  The distributions of the distribution configuration.
        tags(Dict, optional):  The tags of the distribution configuration. Defaults to None.
        client_token(str):  The idempotency token of the distribution configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.distribution_configuration.create(
                ctx, name=value, distributions=value, client_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.distribution_configuration.create name=value, distributions=value, client_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="create_distribution_configuration",
        op_kwargs={
            "name": name,
            "description": description,
            "distributions": distributions,
            "tags": tags,
            "clientToken": client_token,
        },
    )


async def delete(hub, ctx, distribution_configuration_arn: str) -> Dict:
    r"""
    **Autogenerated function**

     Deletes a distribution configuration.

    Args:
        distribution_configuration_arn(str): The Amazon Resource Name (ARN) of the distribution configuration to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.distribution_configuration.delete(
                ctx, distribution_configuration_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.distribution_configuration.delete distribution_configuration_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="delete_distribution_configuration",
        op_kwargs={"distributionConfigurationArn": distribution_configuration_arn},
    )


async def get(hub, ctx, distribution_configuration_arn: str) -> Dict:
    r"""
    **Autogenerated function**

     Gets a distribution configuration.

    Args:
        distribution_configuration_arn(str): The Amazon Resource Name (ARN) of the distribution configuration that you want to retrieve.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.distribution_configuration.get(
                ctx, distribution_configuration_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.distribution_configuration.get distribution_configuration_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="get_distribution_configuration",
        op_kwargs={"distributionConfigurationArn": distribution_configuration_arn},
    )


async def list_all(
    hub, ctx, filters: List = None, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of distribution configurations.

    Args:
        filters(List, optional): The filters.    name - The name of this distribution configuration.  . Defaults to None.
        max_results(int, optional): The maximum items to return in a request. Defaults to None.
        next_token(str, optional): A token to specify where to start paginating. This is the NextToken from a previously truncated response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.distribution_configuration.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.distribution_configuration.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="list_distribution_configurations",
        op_kwargs={
            "filters": filters,
            "maxResults": max_results,
            "nextToken": next_token,
        },
    )


async def update(
    hub,
    ctx,
    distribution_configuration_arn: str,
    distributions: List,
    client_token: str,
    description: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Updates a new distribution configuration. Distribution configurations define and configure the outputs of your pipeline.

    Args:
        distribution_configuration_arn(str): The Amazon Resource Name (ARN) of the distribution configuration that you want to update.
        description(str, optional): The description of the distribution configuration. Defaults to None.
        distributions(List): The distributions of the distribution configuration.
        client_token(str): The idempotency token of the distribution configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.imagebuilder.distribution_configuration.update(
                ctx, distribution_configuration_arn=value, distributions=value, client_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.imagebuilder.distribution_configuration.update distribution_configuration_arn=value, distributions=value, client_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="imagebuilder",
        operation="update_distribution_configuration",
        op_kwargs={
            "distributionConfigurationArn": distribution_configuration_arn,
            "description": description,
            "distributions": distributions,
            "clientToken": client_token,
        },
    )
