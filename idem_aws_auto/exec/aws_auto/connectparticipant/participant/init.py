"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def disconnect(hub, ctx, connection_token: str, client_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Disconnects a participant. Note that ConnectionToken is used for invoking this API instead of ParticipantToken. The Amazon Connect Participant Service APIs do not use Signature Version 4 authentication.

    Args:
        client_token(str, optional): A unique, case-sensitive identifier that you provide to ensure the idempotency of the request. Defaults to None.
        connection_token(str): The authentication token associated with the participant's connection.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connectparticipant.participant.init.disconnect(
                ctx, connection_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connectparticipant.participant.init.disconnect connection_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connectparticipant",
        operation="disconnect_participant",
        op_kwargs={"ClientToken": client_token, "ConnectionToken": connection_token},
    )
