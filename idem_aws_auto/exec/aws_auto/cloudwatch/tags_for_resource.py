"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Displays the tags associated with a CloudWatch resource. Currently, alarms and Contributor Insights rules support tagging.

    Args:
        resource_arn(str): The ARN of the CloudWatch resource that you want to view tags for. The ARN format of an alarm is arn:aws:cloudwatch:Region:account-id:alarm:alarm-name   The ARN format of a Contributor Insights rule is arn:aws:cloudwatch:Region:account-id:insight-rule:insight-rule-name   For more information about ARN format, see  Resource Types Defined by Amazon CloudWatch in the Amazon Web Services General Reference.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudwatch.tags_for_resource.list(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudwatch.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudwatch",
        operation="list_tags_for_resource",
        op_kwargs={"ResourceARN": resource_arn},
    )
