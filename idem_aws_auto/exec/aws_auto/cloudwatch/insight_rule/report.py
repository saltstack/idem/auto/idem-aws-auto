"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(
    hub,
    ctx,
    rule_name: str,
    start_time: str,
    end_time: str,
    period: int,
    max_contributor_count: int = None,
    metrics: List = None,
    order_by: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    This operation returns the time series data collected by a Contributor Insights rule. The data includes the identity and number of contributors to the log group. You can also optionally return one or more statistics about each data point in the time series. These statistics can include the following:    UniqueContributors -- the number of unique contributors for each data point.    MaxContributorValue -- the value of the top contributor for each data point. The identity of the contributor might change for each data point in the graph. If this rule aggregates by COUNT, the top contributor for each data point is the contributor with the most occurrences in that period. If the rule aggregates by SUM, the top contributor is the contributor with the highest sum in the log field specified by the rule's Value, during that period.    SampleCount -- the number of data points matched by the rule.    Sum -- the sum of the values from all contributors during the time period represented by that data point.    Minimum -- the minimum value from a single observation during the time period represented by that data point.    Maximum -- the maximum value from a single observation during the time period represented by that data point.    Average -- the average value from all contributors during the time period represented by that data point.

    Args:
        rule_name(str): The name of the rule that you want to see data from.
        start_time(str): The start time of the data to use in the report. When used in a raw HTTP Query API, it is formatted as yyyy-MM-dd'T'HH:mm:ss. For example, 2019-07-01T23:59:59.
        end_time(str): The end time of the data to use in the report. When used in a raw HTTP Query API, it is formatted as yyyy-MM-dd'T'HH:mm:ss. For example, 2019-07-01T23:59:59.
        period(int): The period, in seconds, to use for the statistics in the InsightRuleMetricDatapoint results.
        max_contributor_count(int, optional): The maximum number of contributors to include in the report. The range is 1 to 100. If you omit this, the default of 10 is used. Defaults to None.
        metrics(List, optional): Specifies which metrics to use for aggregation of contributor values for the report. You can specify one or more of the following metrics:    UniqueContributors -- the number of unique contributors for each data point.    MaxContributorValue -- the value of the top contributor for each data point. The identity of the contributor might change for each data point in the graph. If this rule aggregates by COUNT, the top contributor for each data point is the contributor with the most occurrences in that period. If the rule aggregates by SUM, the top contributor is the contributor with the highest sum in the log field specified by the rule's Value, during that period.    SampleCount -- the number of data points matched by the rule.    Sum -- the sum of the values from all contributors during the time period represented by that data point.    Minimum -- the minimum value from a single observation during the time period represented by that data point.    Maximum -- the maximum value from a single observation during the time period represented by that data point.    Average -- the average value from all contributors during the time period represented by that data point.  . Defaults to None.
        order_by(str, optional): Determines what statistic to use to rank the contributors. Valid values are SUM and MAXIMUM. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudwatch.insight_rule.report.get(
                ctx, rule_name=value, start_time=value, end_time=value, period=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudwatch.insight_rule.report.get rule_name=value, start_time=value, end_time=value, period=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudwatch",
        operation="get_insight_rule_report",
        op_kwargs={
            "RuleName": rule_name,
            "StartTime": start_time,
            "EndTime": end_time,
            "Period": period,
            "MaxContributorCount": max_contributor_count,
            "Metrics": metrics,
            "OrderBy": order_by,
        },
    )
