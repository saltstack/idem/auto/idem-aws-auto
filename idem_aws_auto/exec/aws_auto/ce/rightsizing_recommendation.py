"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub,
    ctx,
    service: str,
    filter_: Dict = None,
    configuration: Dict = None,
    page_size: int = None,
    next_page_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates recommendations that help you save cost by identifying idle and underutilized Amazon EC2 instances. Recommendations are generated to either downsize or terminate instances, along with providing savings detail and metrics. For details on calculation and function, see Optimizing Your Cost with Rightsizing Recommendations in the AWS Billing and Cost Management User Guide.

    Args:
        filter_(Dict, optional): Use Expression to filter by cost or by usage. There are two patterns:    Simple dimension values - You can set the dimension name and values for the filters that you plan to use. For example, you can filter for REGION==us-east-1 OR REGION==us-west-1. For GetRightsizingRecommendation, the Region is a full name (for example, REGION==US East (N. Virginia). The Expression example looks like:  { "Dimensions": { "Key": "REGION", "Values": [ "us-east-1", “us-west-1” ] } }  The list of dimension values are OR'd together to retrieve cost or usage data. You can create Expression and DimensionValues objects using either with* methods or set* methods in multiple lines.    Compound dimension values with logical operations - You can use multiple Expression types and the logical operators AND/OR/NOT to create a list of one or more Expression objects. This allows you to filter on more advanced options. For example, you can filter on ((REGION == us-east-1 OR REGION == us-west-1) OR (TAG.Type == Type1)) AND (USAGE_TYPE != DataTransfer). The Expression for that looks like this:  { "And": [ {"Or": [ {"Dimensions": { "Key": "REGION", "Values": [ "us-east-1", "us-west-1" ] }}, {"Tags": { "Key": "TagName", "Values": ["Value1"] } } ]}, {"Not": {"Dimensions": { "Key": "USAGE_TYPE", "Values": ["DataTransfer"] }}} ] }    Because each Expression can have only one operator, the service returns an error if more than one is specified. The following example shows an Expression object that creates an error.    { "And": [ ... ], "DimensionValues": { "Dimension": "USAGE_TYPE", "Values": [ "DataTransfer" ] } }      For the GetRightsizingRecommendation action, a combination of OR and NOT is not supported. OR is not supported between different dimensions, or dimensions and tags. NOT operators aren't supported. Dimensions are also limited to LINKED_ACCOUNT, REGION, or RIGHTSIZING_TYPE. For the GetReservationPurchaseRecommendation action, only NOT is supported. AND and OR are not supported. Dimensions are limited to LINKED_ACCOUNT. . Defaults to None.
        configuration(Dict, optional):  Enables you to customize recommendations across two attributes. You can choose to view recommendations for instances within the same instance families or across different instance families. You can also choose to view your estimated savings associated with recommendations with consideration of existing Savings Plans or RI benefits, or neither. . Defaults to None.
        service(str): The specific service that you want recommendations for. The only valid value for GetRightsizingRecommendation is "AmazonEC2".
        page_size(int, optional): The number of recommendations that you want returned in a single response object. Defaults to None.
        next_page_token(str, optional): The pagination token that indicates the next set of results that you want to retrieve. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.rightsizing_recommendation.get(ctx, service=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.rightsizing_recommendation.get service=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="get_rightsizing_recommendation",
        op_kwargs={
            "Filter": filter_,
            "Configuration": configuration,
            "Service": service,
            "PageSize": page_size,
            "NextPageToken": next_page_token,
        },
    )
