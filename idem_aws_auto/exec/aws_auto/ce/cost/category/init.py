"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get_all(
    hub,
    ctx,
    time_period: Dict,
    search_string: str = None,
    cost_category_name: str = None,
    filter_: Dict = None,
    sort_by: List = None,
    max_results: int = None,
    next_page_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves an array of Cost Category names and values incurred cost.  If some Cost Category names and values are not associated with any cost, they will not be returned by this API.

    Args:
        search_string(str, optional): The value that you want to search the filter values for. If you do not specify a CostCategoryName, SearchString will be used to filter Cost Category names that match the SearchString pattern. If you do specifiy a CostCategoryName, SearchString will be used to filter Cost Category values that match the SearchString pattern. Defaults to None.
        time_period(Dict): The time period of the request. .
        cost_category_name(str, optional): The unique name of the Cost Category. Defaults to None.
        filter_(Dict, optional): Use Expression to filter by cost or by usage. There are two patterns:    Simple dimension values - You can set the dimension name and values for the filters that you plan to use. For example, you can filter for REGION==us-east-1 OR REGION==us-west-1. For GetRightsizingRecommendation, the Region is a full name (for example, REGION==US East (N. Virginia). The Expression example looks like:  { "Dimensions": { "Key": "REGION", "Values": [ "us-east-1", “us-west-1” ] } }  The list of dimension values are OR'd together to retrieve cost or usage data. You can create Expression and DimensionValues objects using either with* methods or set* methods in multiple lines.    Compound dimension values with logical operations - You can use multiple Expression types and the logical operators AND/OR/NOT to create a list of one or more Expression objects. This allows you to filter on more advanced options. For example, you can filter on ((REGION == us-east-1 OR REGION == us-west-1) OR (TAG.Type == Type1)) AND (USAGE_TYPE != DataTransfer). The Expression for that looks like this:  { "And": [ {"Or": [ {"Dimensions": { "Key": "REGION", "Values": [ "us-east-1", "us-west-1" ] }}, {"Tags": { "Key": "TagName", "Values": ["Value1"] } } ]}, {"Not": {"Dimensions": { "Key": "USAGE_TYPE", "Values": ["DataTransfer"] }}} ] }    Because each Expression can have only one operator, the service returns an error if more than one is specified. The following example shows an Expression object that creates an error.    { "And": [ ... ], "DimensionValues": { "Dimension": "USAGE_TYPE", "Values": [ "DataTransfer" ] } }      For the GetRightsizingRecommendation action, a combination of OR and NOT is not supported. OR is not supported between different dimensions, or dimensions and tags. NOT operators aren't supported. Dimensions are also limited to LINKED_ACCOUNT, REGION, or RIGHTSIZING_TYPE. For the GetReservationPurchaseRecommendation action, only NOT is supported. AND and OR are not supported. Dimensions are limited to LINKED_ACCOUNT. . Defaults to None.
        sort_by(List, optional): The value by which you want to sort the data. The key represents cost and usage metrics. The following values are supported:    BlendedCost     UnblendedCost     AmortizedCost     NetAmortizedCost     NetUnblendedCost     UsageQuantity     NormalizedUsageAmount    Supported values for SortOrder are ASCENDING or DESCENDING. When using SortBy, NextPageToken and SearchString are not supported. Defaults to None.
        max_results(int, optional): This field is only used when SortBy is provided in the request. The maximum number of objects that to be returned for this request. If MaxResults is not specified with SortBy, the request will return 1000 results as the default value for this parameter. For GetCostCategories, MaxResults has an upper limit of 1000. Defaults to None.
        next_page_token(str, optional): If the number of objects that are still available for retrieval exceeds the limit, AWS returns a NextPageToken value in the response. To retrieve the next batch of objects, provide the NextPageToken from the prior call in your next request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.cost.category.init.get_all(ctx, time_period=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.cost.category.init.get_all time_period=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="get_cost_categories",
        op_kwargs={
            "SearchString": search_string,
            "TimePeriod": time_period,
            "CostCategoryName": cost_category_name,
            "Filter": filter_,
            "SortBy": sort_by,
            "MaxResults": max_results,
            "NextPageToken": next_page_token,
        },
    )
