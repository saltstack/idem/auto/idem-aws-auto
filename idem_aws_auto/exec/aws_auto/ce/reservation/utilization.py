"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(
    hub,
    ctx,
    time_period: Dict,
    group_by: List = None,
    granularity: str = None,
    filter_: Dict = None,
    sort_by: Dict = None,
    next_page_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the reservation utilization for your account. Management account in an organization have access to member accounts. You can filter data by dimensions in a time period. You can use GetDimensionValues to determine the possible dimension values. Currently, you can group only by SUBSCRIPTION_ID.

    Args:
        time_period(Dict): Sets the start and end dates for retrieving RI utilization. The start date is inclusive, but the end date is exclusive. For example, if start is 2017-01-01 and end is 2017-05-01, then the cost and usage data is retrieved from 2017-01-01 up to and including 2017-04-30 but not including 2017-05-01. .
        group_by(List, optional): Groups only by SUBSCRIPTION_ID. Metadata is included. Defaults to None.
        granularity(str, optional): If GroupBy is set, Granularity can't be set. If Granularity isn't set, the response object doesn't include Granularity, either MONTHLY or DAILY. If both GroupBy and Granularity aren't set, GetReservationUtilization defaults to DAILY. The GetReservationUtilization operation supports only DAILY and MONTHLY granularities. Defaults to None.
        filter_(Dict, optional): Filters utilization data by dimensions. You can filter by the following dimensions:   AZ   CACHE_ENGINE   DEPLOYMENT_OPTION   INSTANCE_TYPE   LINKED_ACCOUNT   OPERATING_SYSTEM   PLATFORM   REGION   SERVICE   SCOPE   TENANCY    GetReservationUtilization uses the same Expression object as the other operations, but only AND is supported among each dimension, and nesting is supported up to only one level deep. If there are multiple values for a dimension, they are OR'd together. Defaults to None.
        sort_by(Dict, optional): The value by which you want to sort the data. The following values are supported for Key:    UtilizationPercentage     UtilizationPercentageInUnits     PurchasedHours     PurchasedUnits     TotalActualHours     TotalActualUnits     UnusedHours     UnusedUnits     OnDemandCostOfRIHoursUsed     NetRISavings     TotalPotentialRISavings     AmortizedUpfrontFee     AmortizedRecurringFee     TotalAmortizedFee     RICostForUnusedHours     RealizedSavings     UnrealizedSavings    Supported values for SortOrder are ASCENDING or DESCENDING. Defaults to None.
        next_page_token(str, optional): The token to retrieve the next set of results. AWS provides the token when the response from a previous call has more results than the maximum page size. Defaults to None.
        max_results(int, optional): The maximum number of objects that you returned for this request. If more objects are available, in the response, AWS provides a NextPageToken value that you can use in a subsequent call to get the next batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.reservation.utilization.get(ctx, time_period=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.reservation.utilization.get time_period=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="get_reservation_utilization",
        op_kwargs={
            "TimePeriod": time_period,
            "GroupBy": group_by,
            "Granularity": granularity,
            "Filter": filter_,
            "SortBy": sort_by,
            "NextPageToken": next_page_token,
            "MaxResults": max_results,
        },
    )
