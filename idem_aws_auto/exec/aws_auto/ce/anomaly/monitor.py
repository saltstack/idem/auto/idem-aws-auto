"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(hub, ctx, anomaly_monitor: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new cost anomaly detection monitor with the requested type and monitor specification.

    Args:
        anomaly_monitor(Dict):  The cost anomaly detection monitor object that you want to create.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.anomaly.monitor.create(ctx, anomaly_monitor=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.anomaly.monitor.create anomaly_monitor=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="create_anomaly_monitor",
        op_kwargs={"AnomalyMonitor": anomaly_monitor},
    )


async def delete(hub, ctx, monitor_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a cost anomaly monitor.

    Args:
        monitor_arn(str):  The unique identifier of the cost anomaly monitor that you want to delete. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.anomaly.monitor.delete(ctx, monitor_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.anomaly.monitor.delete monitor_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="delete_anomaly_monitor",
        op_kwargs={"MonitorArn": monitor_arn},
    )


async def get_all(
    hub,
    ctx,
    monitor_arn_list: List = None,
    next_page_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the cost anomaly monitor definitions for your account. You can filter using a list of cost anomaly monitor Amazon Resource Names (ARNs).

    Args:
        monitor_arn_list(List, optional):  A list of cost anomaly monitor ARNs. . Defaults to None.
        next_page_token(str, optional):  The token to retrieve the next set of results. AWS provides the token when the response from a previous call has more results than the maximum page size. . Defaults to None.
        max_results(int, optional):  The number of entries a paginated response contains. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.anomaly.monitor.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.anomaly.monitor.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="get_anomaly_monitors",
        op_kwargs={
            "MonitorArnList": monitor_arn_list,
            "NextPageToken": next_page_token,
            "MaxResults": max_results,
        },
    )


async def update(hub, ctx, monitor_arn: str, monitor_name: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates an existing cost anomaly monitor. The changes made are applied going forward, and does not change anomalies detected in the past.

    Args:
        monitor_arn(str):  Cost anomaly monitor Amazon Resource Names (ARNs). .
        monitor_name(str, optional):  The new name for the cost anomaly monitor. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ce.anomaly.monitor.update(ctx, monitor_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ce.anomaly.monitor.update monitor_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ce",
        operation="update_anomaly_monitor",
        op_kwargs={"MonitorArn": monitor_arn, "MonitorName": monitor_name},
    )
