"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub, ctx, report_group_arn: str, trend_field: str, num_of_reports: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Analyzes and accumulates test report values for the specified test reports.

    Args:
        report_group_arn(str): The ARN of the report group that contains the reports to analyze.
        num_of_reports(int, optional): The number of reports to analyze. This operation always retrieves the most recent reports. If this parameter is omitted, the most recent 100 reports are analyzed. Defaults to None.
        trend_field(str): The test report value to accumulate. This must be one of the following values:  Test reports:   DURATION  Accumulate the test run times for the specified reports.  PASS_RATE  Accumulate the percentage of tests that passed for the specified test reports.  TOTAL  Accumulate the total number of tests for the specified test reports.      Code coverage reports:   BRANCH_COVERAGE  Accumulate the branch coverage percentages for the specified test reports.  BRANCHES_COVERED  Accumulate the branches covered values for the specified test reports.  BRANCHES_MISSED  Accumulate the branches missed values for the specified test reports.  LINE_COVERAGE  Accumulate the line coverage percentages for the specified test reports.  LINES_COVERED  Accumulate the lines covered values for the specified test reports.  LINES_MISSED  Accumulate the lines not covered values for the specified test reports.    .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codebuild.report.group.trend.get(
                ctx, report_group_arn=value, trend_field=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codebuild.report.group.trend.get report_group_arn=value, trend_field=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codebuild",
        operation="get_report_group_trend",
        op_kwargs={
            "reportGroupArn": report_group_arn,
            "numOfReports": num_of_reports,
            "trendField": trend_field,
        },
    )
