"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def delete(hub, ctx, bot_name: str, bot_alias: str, user_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes session information for a specified bot, alias, and user ID.

    Args:
        bot_name(str): The name of the bot that contains the session data.
        bot_alias(str): The alias in use for the bot that contains the session data.
        user_id(str): The identifier of the user associated with the session data.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_runtime.session.delete(
                ctx, bot_name=value, bot_alias=value, user_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_runtime.session.delete bot_name=value, bot_alias=value, user_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-runtime",
        operation="delete_session",
        op_kwargs={"botName": bot_name, "botAlias": bot_alias, "userId": user_id},
    )


async def get(
    hub,
    ctx,
    bot_name: str,
    bot_alias: str,
    user_id: str,
    checkpoint_label_filter: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns session information for a specified bot, alias, and user ID.

    Args:
        bot_name(str): The name of the bot that contains the session data.
        bot_alias(str): The alias in use for the bot that contains the session data.
        user_id(str): The ID of the client application user. Amazon Lex uses this to identify a user's conversation with your bot. .
        checkpoint_label_filter(str, optional): A string used to filter the intents returned in the recentIntentSummaryView structure.  When you specify a filter, only intents with their checkpointLabel field set to that string are returned. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_runtime.session.get(
                ctx, bot_name=value, bot_alias=value, user_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_runtime.session.get bot_name=value, bot_alias=value, user_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-runtime",
        operation="get_session",
        op_kwargs={
            "botName": bot_name,
            "botAlias": bot_alias,
            "userId": user_id,
            "checkpointLabelFilter": checkpoint_label_filter,
        },
    )


async def put(
    hub,
    ctx,
    bot_name: str,
    bot_alias: str,
    user_id: str,
    session_attributes: Dict = None,
    dialog_action: Dict = None,
    recent_intent_summary_view: List = None,
    accept: str = None,
    active_contexts: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new session or modifies an existing session with an Amazon Lex bot. Use this operation to enable your application to set the state of the bot. For more information, see Managing Sessions.

    Args:
        bot_name(str): The name of the bot that contains the session data.
        bot_alias(str): The alias in use for the bot that contains the session data.
        user_id(str): The ID of the client application user. Amazon Lex uses this to identify a user's conversation with your bot. .
        session_attributes(Dict, optional): Map of key/value pairs representing the session-specific context information. It contains application information passed between Amazon Lex and a client application. Defaults to None.
        dialog_action(Dict, optional): Sets the next action that the bot should take to fulfill the conversation. Defaults to None.
        recent_intent_summary_view(List, optional): A summary of the recent intents for the bot. You can use the intent summary view to set a checkpoint label on an intent and modify attributes of intents. You can also use it to remove or add intent summary objects to the list. An intent that you modify or add to the list must make sense for the bot. For example, the intent name must be valid for the bot. You must provide valid values for:    intentName    slot names    slotToElict    If you send the recentIntentSummaryView parameter in a PutSession request, the contents of the new summary view replaces the old summary view. For example, if a GetSession request returns three intents in the summary view and you call PutSession with one intent in the summary view, the next call to GetSession will only return one intent. Defaults to None.
        accept(str, optional): The message that Amazon Lex returns in the response can be either text or speech based depending on the value of this field.   If the value is text/plain; charset=utf-8, Amazon Lex returns text in the response.   If the value begins with audio/, Amazon Lex returns speech in the response. Amazon Lex uses Amazon Polly to generate the speech in the configuration that you specify. For example, if you specify audio/mpeg as the value, Amazon Lex returns speech in the MPEG format.   If the value is audio/pcm, the speech is returned as audio/pcm in 16-bit, little endian format.   The following are the accepted values:    audio/mpeg     audio/ogg     audio/pcm     audio/* (defaults to mpeg)    text/plain; charset=utf-8     . Defaults to None.
        active_contexts(List, optional): A list of contexts active for the request. A context can be activated when a previous intent is fulfilled, or by including the context in the request, If you don't specify a list of contexts, Amazon Lex will use the current list of contexts for the session. If you specify an empty list, all contexts for the session are cleared. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_runtime.session.put(
                ctx, bot_name=value, bot_alias=value, user_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_runtime.session.put bot_name=value, bot_alias=value, user_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-runtime",
        operation="put_session",
        op_kwargs={
            "botName": bot_name,
            "botAlias": bot_alias,
            "userId": user_id,
            "sessionAttributes": session_attributes,
            "dialogAction": dialog_action,
            "recentIntentSummaryView": recent_intent_summary_view,
            "accept": accept,
            "activeContexts": active_contexts,
        },
    )
