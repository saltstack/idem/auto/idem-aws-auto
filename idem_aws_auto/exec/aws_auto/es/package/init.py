"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def associate(hub, ctx, package_id: str, domain_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Associates a package with an Amazon ES domain.

    Args:
        package_id(str): Internal ID of the package that you want to associate with a domain. Use DescribePackages to find this value.
        domain_name(str): Name of the domain that you want to associate the package with.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.associate(
                ctx, package_id=value, domain_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.associate package_id=value, domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="associate_package",
        op_kwargs={"PackageID": package_id, "DomainName": domain_name},
    )


async def create(
    hub,
    ctx,
    package_name: str,
    package_type: str,
    package_source: Dict,
    package_description: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Create a package for use with Amazon ES domains.

    Args:
        package_name(str): Unique identifier for the package.
        package_type(str): Type of package. Currently supports only TXT-DICTIONARY.
        package_description(str, optional): Description of the package. Defaults to None.
        package_source(Dict): The customer S3 location PackageSource for importing the package.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.create(
                ctx, package_name=value, package_type=value, package_source=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.create package_name=value, package_type=value, package_source=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="create_package",
        op_kwargs={
            "PackageName": package_name,
            "PackageType": package_type,
            "PackageDescription": package_description,
            "PackageSource": package_source,
        },
    )


async def delete(hub, ctx, package_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Delete the package.

    Args:
        package_id(str): Internal ID of the package that you want to delete. Use DescribePackages to find this value.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.delete(ctx, package_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.delete package_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="delete_package",
        op_kwargs={"PackageID": package_id},
    )


async def describe_all(
    hub, ctx, filters: List = None, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes all packages available to Amazon ES. Includes options for filtering, limiting the number of results, and pagination.

    Args:
        filters(List, optional): Only returns packages that match the DescribePackagesFilterList values. Defaults to None.
        max_results(int, optional): Limits results to a maximum number of packages. Defaults to None.
        next_token(str, optional): Used for pagination. Only necessary if a previous API call includes a non-null NextToken value. If provided, returns results for the next page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="describe_packages",
        op_kwargs={
            "Filters": filters,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )


async def dissociate(hub, ctx, package_id: str, domain_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Dissociates a package from the Amazon ES domain.

    Args:
        package_id(str): Internal ID of the package that you want to associate with a domain. Use DescribePackages to find this value.
        domain_name(str): Name of the domain that you want to associate the package with.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.dissociate(
                ctx, package_id=value, domain_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.dissociate package_id=value, domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="dissociate_package",
        op_kwargs={"PackageID": package_id, "DomainName": domain_name},
    )


async def update(
    hub,
    ctx,
    package_id: str,
    package_source: Dict,
    package_description: str = None,
    commit_message: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a package for use with Amazon ES domains.

    Args:
        package_id(str): Unique identifier for the package.
        package_source(Dict): The S3 location for importing the package specified as S3BucketName and S3Key.
        package_description(str, optional): New description of the package. Defaults to None.
        commit_message(str, optional): An info message for the new version which will be shown as part of GetPackageVersionHistoryResponse. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.init.update(
                ctx, package_id=value, package_source=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.init.update package_id=value, package_source=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="update_package",
        op_kwargs={
            "PackageID": package_id,
            "PackageSource": package_source,
            "PackageDescription": package_description,
            "CommitMessage": commit_message,
        },
    )
