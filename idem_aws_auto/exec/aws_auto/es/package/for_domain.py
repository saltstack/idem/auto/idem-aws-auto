"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, domain_name: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all packages associated with the Amazon ES domain.

    Args:
        domain_name(str): The name of the domain for which you want to list associated packages.
        max_results(int, optional): Limits results to a maximum number of packages. Defaults to None.
        next_token(str, optional): Used for pagination. Only necessary if a previous API call includes a non-null NextToken value. If provided, returns results for the next page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.package.for_domain.list(ctx, domain_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.package.for_domain.list domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="list_packages_for_domain",
        op_kwargs={
            "DomainName": domain_name,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
