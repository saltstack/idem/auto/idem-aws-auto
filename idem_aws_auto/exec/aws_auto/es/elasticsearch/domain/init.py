"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    domain_name: str,
    elasticsearch_version: str = None,
    elasticsearch_cluster_config: Dict = None,
    ebs_options: Dict = None,
    access_policies: str = None,
    snapshot_options: Dict = None,
    vpc_options: Dict = None,
    cognito_options: Dict = None,
    encryption_at_rest_options: Dict = None,
    node_to_node_encryption_options: Dict = None,
    advanced_options: Dict = None,
    log_publishing_options: Dict = None,
    domain_endpoint_options: Dict = None,
    advanced_security_options: Dict = None,
    auto_tune_options: Dict = None,
    tag_list: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new Elasticsearch domain. For more information, see Creating Elasticsearch Domains in the Amazon Elasticsearch Service Developer Guide.

    Args:
        domain_name(str): The name of the Elasticsearch domain that you are creating. Domain names are unique across the domains owned by an account within an AWS region. Domain names must start with a lowercase letter and can contain the following characters: a-z (lowercase), 0-9, and - (hyphen).
        elasticsearch_version(str, optional): String of format X.Y to specify version for the Elasticsearch domain eg. "1.5" or "2.3". For more information, see Creating Elasticsearch Domains in the Amazon Elasticsearch Service Developer Guide. Defaults to None.
        elasticsearch_cluster_config(Dict, optional): Configuration options for an Elasticsearch domain. Specifies the instance type and number of instances in the domain cluster. . Defaults to None.
        ebs_options(Dict, optional): Options to enable, disable and specify the type and size of EBS storage volumes. . Defaults to None.
        access_policies(str, optional):  IAM access policy as a JSON-formatted string. Defaults to None.
        snapshot_options(Dict, optional): Option to set time, in UTC format, of the daily automated snapshot. Default value is 0 hours. . Defaults to None.
        vpc_options(Dict, optional): Options to specify the subnets and security groups for VPC endpoint. For more information, see Creating a VPC in VPC Endpoints for Amazon Elasticsearch Service Domains. Defaults to None.
        cognito_options(Dict, optional): Options to specify the Cognito user and identity pools for Kibana authentication. For more information, see Amazon Cognito Authentication for Kibana. Defaults to None.
        encryption_at_rest_options(Dict, optional): Specifies the Encryption At Rest Options. Defaults to None.
        node_to_node_encryption_options(Dict, optional): Specifies the NodeToNodeEncryptionOptions. Defaults to None.
        advanced_options(Dict, optional):  Option to allow references to indices in an HTTP request body. Must be false when configuring access to individual sub-resources. By default, the value is true. See Configuration Advanced Options for more information. Defaults to None.
        log_publishing_options(Dict, optional): Map of LogType and LogPublishingOption, each containing options to publish a given type of Elasticsearch log. Defaults to None.
        domain_endpoint_options(Dict, optional): Options to specify configuration that will be applied to the domain endpoint. Defaults to None.
        advanced_security_options(Dict, optional): Specifies advanced security options. Defaults to None.
        auto_tune_options(Dict, optional): Specifies Auto-Tune options. Defaults to None.
        tag_list(List, optional): A list of Tag added during domain creation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.elasticsearch.domain.init.create(ctx, domain_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.elasticsearch.domain.init.create domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="create_elasticsearch_domain",
        op_kwargs={
            "DomainName": domain_name,
            "ElasticsearchVersion": elasticsearch_version,
            "ElasticsearchClusterConfig": elasticsearch_cluster_config,
            "EBSOptions": ebs_options,
            "AccessPolicies": access_policies,
            "SnapshotOptions": snapshot_options,
            "VPCOptions": vpc_options,
            "CognitoOptions": cognito_options,
            "EncryptionAtRestOptions": encryption_at_rest_options,
            "NodeToNodeEncryptionOptions": node_to_node_encryption_options,
            "AdvancedOptions": advanced_options,
            "LogPublishingOptions": log_publishing_options,
            "DomainEndpointOptions": domain_endpoint_options,
            "AdvancedSecurityOptions": advanced_security_options,
            "AutoTuneOptions": auto_tune_options,
            "TagList": tag_list,
        },
    )


async def delete(hub, ctx, domain_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Permanently deletes the specified Elasticsearch domain and all of its data. Once a domain is deleted, it cannot be recovered.

    Args:
        domain_name(str): The name of the Elasticsearch domain that you want to permanently delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.elasticsearch.domain.init.delete(ctx, domain_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.elasticsearch.domain.init.delete domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="delete_elasticsearch_domain",
        op_kwargs={"DomainName": domain_name},
    )


async def describe(hub, ctx, domain_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns domain configuration information about the specified Elasticsearch domain, including the domain ID, domain endpoint, and domain ARN.

    Args:
        domain_name(str): The name of the Elasticsearch domain for which you want information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.elasticsearch.domain.init.describe(ctx, domain_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.elasticsearch.domain.init.describe domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="describe_elasticsearch_domain",
        op_kwargs={"DomainName": domain_name},
    )


async def describe_all(hub, ctx, domain_names: List) -> Dict:
    r"""
    **Autogenerated function**

    Returns domain configuration information about the specified Elasticsearch domains, including the domain ID, domain endpoint, and domain ARN.

    Args:
        domain_names(List): The Elasticsearch domains for which you want information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.elasticsearch.domain.init.describe_all(
                ctx, domain_names=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.elasticsearch.domain.init.describe_all domain_names=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="describe_elasticsearch_domains",
        op_kwargs={"DomainNames": domain_names},
    )


async def upgrade(
    hub, ctx, domain_name: str, target_version: str, perform_check_only: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Allows you to either upgrade your domain or perform an Upgrade eligibility check to a compatible Elasticsearch version.

    Args:
        domain_name(str): The name of an Elasticsearch domain. Domain names are unique across the domains owned by an account within an AWS region. Domain names start with a letter or number and can contain the following characters: a-z (lowercase), 0-9, and - (hyphen).
        target_version(str): The version of Elasticsearch that you intend to upgrade the domain to.
        perform_check_only(bool, optional):  This flag, when set to True, indicates that an Upgrade Eligibility Check needs to be performed. This will not actually perform the Upgrade. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.es.elasticsearch.domain.init.upgrade(
                ctx, domain_name=value, target_version=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.es.elasticsearch.domain.init.upgrade domain_name=value, target_version=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="es",
        operation="upgrade_elasticsearch_domain",
        op_kwargs={
            "DomainName": domain_name,
            "TargetVersion": target_version,
            "PerformCheckOnly": perform_check_only,
        },
    )
