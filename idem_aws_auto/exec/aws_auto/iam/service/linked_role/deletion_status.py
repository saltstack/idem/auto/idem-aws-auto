"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, deletion_task_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the status of your service-linked role deletion. After you use DeleteServiceLinkedRole to submit a service-linked role for deletion, you can use the DeletionTaskId parameter in GetServiceLinkedRoleDeletionStatus to check the status of the deletion. If the deletion fails, this operation returns the reason that it failed, if that information is returned by the service.

    Args:
        deletion_task_id(str): The deletion task identifier. This identifier is returned by the DeleteServiceLinkedRole operation in the format task/aws-service-role/<service-principal-name>/<role-name>/<task-uuid>.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.service.linked_role.deletion_status.get(
                ctx, deletion_task_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.service.linked_role.deletion_status.get deletion_task_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="get_service_linked_role_deletion_status",
        op_kwargs={"DeletionTaskId": deletion_task_id},
    )
