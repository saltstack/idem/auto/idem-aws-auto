"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, role_name: str, marker: str = None, max_items: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the tags that are attached to the specified role. The returned list of tags is sorted by tag key. For more information about tagging, see Tagging IAM resources in the IAM User Guide.

    Args:
        role_name(str): The name of the IAM role for which you want to see the list of tags. This parameter accepts (through its regex pattern) a string of characters that consist of upper and lowercase alphanumeric characters with no spaces. You can also include any of the following characters: _+=,.@-.
        marker(str, optional): Use this parameter only when paginating results and only after you receive a response indicating that the results are truncated. Set it to the value of the Marker element in the response that you received to indicate where the next call should start. Defaults to None.
        max_items(int, optional): Use this only when paginating results to indicate the maximum number of items you want in the response. If additional items exist beyond the maximum you specify, the IsTruncated response element is true. If you do not include this parameter, the number of items defaults to 100. Note that IAM might return fewer results, even when there are more results available. In that case, the IsTruncated response element returns true, and Marker contains a value to include in the subsequent call that tells the service where to continue from. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.role.tag.list_all(ctx, role_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.role.tag.list_all role_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="list_role_tags",
        op_kwargs={"RoleName": role_name, "Marker": marker, "MaxItems": max_items},
    )
