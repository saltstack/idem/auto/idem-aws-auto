"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, arn: str, service_namespaces: List, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of policies that the IAM identity (user, group, or role) can use to access each specified service.  This operation does not use other policy types when determining whether a resource could access a service. These other policy types include resource-based policies, access control lists, Organizations policies, IAM permissions boundaries, and STS assume role policies. It only applies permissions policy logic. For more about the evaluation of policy types, see Evaluating policies in the IAM User Guide.  The list of policies returned by the operation depends on the ARN of the identity that you provide.    User – The list of policies includes the managed and inline policies that are attached to the user directly. The list also includes any additional managed and inline policies that are attached to the group to which the user belongs.     Group – The list of policies includes only the managed and inline policies that are attached to the group directly. Policies that are attached to the group’s user are not included.    Role – The list of policies includes only the managed and inline policies that are attached to the role.   For each managed policy, this operation returns the ARN and policy name. For each inline policy, it returns the policy name and the entity to which it is attached. Inline policies do not have an ARN. For more information about these policy types, see Managed policies and inline policies in the IAM User Guide. Policies that are attached to users and roles as permissions boundaries are not returned. To view which managed policy is currently used to set the permissions boundary for a user or role, use the GetUser or GetRole operations.

    Args:
        marker(str, optional): Use this parameter only when paginating results and only after you receive a response indicating that the results are truncated. Set it to the value of the Marker element in the response that you received to indicate where the next call should start. Defaults to None.
        arn(str): The ARN of the IAM identity (user, group, or role) whose policies you want to list.
        service_namespaces(List): The service namespace for the Amazon Web Services services whose policies you want to list. To learn the service namespace for a service, see Actions, resources, and condition keys for Amazon Web Services services in the IAM User Guide. Choose the name of the service to view details for that service. In the first paragraph, find the service prefix. For example, (service prefix: a4b). For more information about service namespaces, see Amazon Web Services service namespaces in the Amazon Web Services General Reference.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.policy.granting_service_access.list(
                ctx, arn=value, service_namespaces=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.policy.granting_service_access.list arn=value, service_namespaces=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="list_policies_granting_service_access",
        op_kwargs={
            "Marker": marker,
            "Arn": arn,
            "ServiceNamespaces": service_namespaces,
        },
    )
