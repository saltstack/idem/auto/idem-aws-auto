"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub, ctx, policy_arn: str, policy_document: str, set_as_default: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new version of the specified managed policy. To update a managed policy, you create a new policy version. A managed policy can have up to five versions. If the policy has five versions, you must delete an existing version using DeletePolicyVersion before you create a new version. Optionally, you can set the new version as the policy's default version. The default version is the version that is in effect for the IAM users, groups, and roles to which the policy is attached. For more information about managed policy versions, see Versioning for managed policies in the IAM User Guide.

    Args:
        policy_arn(str): The Amazon Resource Name (ARN) of the IAM policy to which you want to add a new version. For more information about ARNs, see Amazon Resource Names (ARNs) in the Amazon Web Services General Reference.
        policy_document(str): The JSON policy document that you want to use as the content for this new version of the policy. You must provide policies in JSON format in IAM. However, for CloudFormation templates formatted in YAML, you can provide the policy in JSON or YAML format. CloudFormation always converts a YAML policy to JSON format before submitting it to IAM. The maximum length of the policy document that you can pass in this operation, including whitespace, is listed below. To view the maximum character counts of a managed policy with no whitespaces, see IAM and STS character quotas. The regex pattern used to validate this parameter is a string of characters consisting of the following:   Any printable ASCII character ranging from the space character (\u0020) through the end of the ASCII character range   The printable characters in the Basic Latin and Latin-1 Supplement character set (through \u00FF)   The special characters tab (\u0009), line feed (\u000A), and carriage return (\u000D)  .
        set_as_default(bool, optional): Specifies whether to set this version as the policy's default version. When this parameter is true, the new policy version becomes the operative version. That is, it becomes the version that is in effect for the IAM users, groups, and roles that the policy is attached to. For more information about managed policy versions, see Versioning for managed policies in the IAM User Guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.policy.version.create(
                ctx, policy_arn=value, policy_document=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.policy.version.create policy_arn=value, policy_document=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="create_policy_version",
        op_kwargs={
            "PolicyArn": policy_arn,
            "PolicyDocument": policy_document,
            "SetAsDefault": set_as_default,
        },
    )


async def delete(hub, ctx, policy_arn: str, version_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified version from the specified managed policy. You cannot delete the default version from a policy using this operation. To delete the default version from a policy, use DeletePolicy. To find out which version of a policy is marked as the default version, use ListPolicyVersions. For information about versions for managed policies, see Versioning for managed policies in the IAM User Guide.

    Args:
        policy_arn(str): The Amazon Resource Name (ARN) of the IAM policy from which you want to delete a version. For more information about ARNs, see Amazon Resource Names (ARNs) in the Amazon Web Services General Reference.
        version_id(str): The policy version to delete. This parameter allows (through its regex pattern) a string of characters that consists of the lowercase letter 'v' followed by one or two digits, and optionally followed by a period '.' and a string of letters and digits. For more information about managed policy versions, see Versioning for managed policies in the IAM User Guide.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.policy.version.delete(
                ctx, policy_arn=value, version_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.policy.version.delete policy_arn=value, version_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="delete_policy_version",
        op_kwargs={"PolicyArn": policy_arn, "VersionId": version_id},
    )


async def get(hub, ctx, policy_arn: str, version_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about the specified version of the specified managed policy, including the policy document.  Policies returned by this operation are URL-encoded compliant with RFC 3986. You can use a URL decoding method to convert the policy back to plain JSON text. For example, if you use Java, you can use the decode method of the java.net.URLDecoder utility class in the Java SDK. Other languages and SDKs provide similar functionality.  To list the available versions for a policy, use ListPolicyVersions. This operation retrieves information about managed policies. To retrieve information about an inline policy that is embedded in a user, group, or role, use GetUserPolicy, GetGroupPolicy, or GetRolePolicy. For more information about the types of policies, see Managed policies and inline policies in the IAM User Guide. For more information about managed policy versions, see Versioning for managed policies in the IAM User Guide.

    Args:
        policy_arn(str): The Amazon Resource Name (ARN) of the managed policy that you want information about. For more information about ARNs, see Amazon Resource Names (ARNs) in the Amazon Web Services General Reference.
        version_id(str): Identifies the policy version to retrieve. This parameter allows (through its regex pattern) a string of characters that consists of the lowercase letter 'v' followed by one or two digits, and optionally followed by a period '.' and a string of letters and digits.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.policy.version.get(ctx, policy_arn=value, version_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.policy.version.get policy_arn=value, version_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="get_policy_version",
        op_kwargs={"PolicyArn": policy_arn, "VersionId": version_id},
    )


async def list_all(
    hub, ctx, policy_arn: str, marker: str = None, max_items: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists information about the versions of the specified managed policy, including the version that is currently set as the policy's default version. For more information about managed policies, see Managed policies and inline policies in the IAM User Guide.

    Args:
        policy_arn(str): The Amazon Resource Name (ARN) of the IAM policy for which you want the versions. For more information about ARNs, see Amazon Resource Names (ARNs) in the Amazon Web Services General Reference.
        marker(str, optional): Use this parameter only when paginating results and only after you receive a response indicating that the results are truncated. Set it to the value of the Marker element in the response that you received to indicate where the next call should start. Defaults to None.
        max_items(int, optional): Use this only when paginating results to indicate the maximum number of items you want in the response. If additional items exist beyond the maximum you specify, the IsTruncated response element is true. If you do not include this parameter, the number of items defaults to 100. Note that IAM might return fewer results, even when there are more results available. In that case, the IsTruncated response element returns true, and Marker contains a value to include in the subsequent call that tells the service where to continue from. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iam.policy.version.list_all(ctx, policy_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iam.policy.version.list_all policy_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iam",
        operation="list_policy_versions",
        op_kwargs={"PolicyArn": policy_arn, "Marker": marker, "MaxItems": max_items},
    )
