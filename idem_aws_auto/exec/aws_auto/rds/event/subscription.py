"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str,
    source_type: str = None,
    event_categories: List = None,
    source_ids: List = None,
    enabled: bool = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an RDS event notification subscription. This action requires a topic Amazon Resource Name (ARN) created by either the RDS console, the SNS console, or the SNS API. To obtain an ARN with SNS, you must create a topic in Amazon SNS and subscribe to the topic. The ARN is displayed in the SNS console. You can specify the type of source (SourceType) that you want to be notified of and provide a list of RDS sources (SourceIds) that triggers the events. You can also provide a list of event categories (EventCategories) for events that you want to be notified of. For example, you can specify SourceType = db-instance, SourceIds = mydbinstance1, mydbinstance2 and EventCategories = Availability, Backup. If you specify both the SourceType and SourceIds, such as SourceType = db-instance and SourceIdentifier = myDBInstance1, you are notified of all the db-instance events for the specified source. If you specify a SourceType but do not specify a SourceIdentifier, you receive notice of the events for that source type for all your RDS sources. If you don't specify either the SourceType or the SourceIdentifier, you are notified of events generated from all RDS sources belonging to your customer account.  RDS event notification is only available for unencrypted SNS topics. If you specify an encrypted SNS topic, event notifications aren't sent for the topic.

    Args:
        subscription_name(str): The name of the subscription. Constraints: The name must be less than 255 characters.
        sns_topic_arn(str): The Amazon Resource Name (ARN) of the SNS topic created for event notification. The ARN is created by Amazon SNS when you create a topic and subscribe to it.
        source_type(str, optional): The type of source that is generating the events. For example, if you want to be notified of events generated by a DB instance, you set this parameter to db-instance. If this value isn't specified, all events are returned. Valid values: db-instance | db-cluster | db-parameter-group | db-security-group | db-snapshot | db-cluster-snapshot . Defaults to None.
        event_categories(List, optional):  A list of event categories for a particular source type (SourceType) that you want to subscribe to. You can see a list of the categories for a given source type in Events in the Amazon RDS User Guide or by using the DescribeEventCategories operation. . Defaults to None.
        source_ids(List, optional): The list of identifiers of the event sources for which events are returned. If not specified, then all sources are included in the response. An identifier must begin with a letter and must contain only ASCII letters, digits, and hyphens. It can't end with a hyphen or contain two consecutive hyphens. Constraints:   If SourceIds are supplied, SourceType must also be provided.   If the source type is a DB instance, a DBInstanceIdentifier value must be supplied.   If the source type is a DB cluster, a DBClusterIdentifier value must be supplied.   If the source type is a DB parameter group, a DBParameterGroupName value must be supplied.   If the source type is a DB security group, a DBSecurityGroupName value must be supplied.   If the source type is a DB snapshot, a DBSnapshotIdentifier value must be supplied.   If the source type is a DB cluster snapshot, a DBClusterSnapshotIdentifier value must be supplied.  . Defaults to None.
        enabled(bool, optional):  A value that indicates whether to activate the subscription. If the event notification subscription isn't activated, the subscription is created but not active. . Defaults to None.
        tags(List, optional): A list of tags. For more information, see Tagging Amazon RDS Resources in the Amazon RDS User Guide. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.event.subscription.create(
                ctx, subscription_name=value, sns_topic_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.event.subscription.create subscription_name=value, sns_topic_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="create_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "SourceIds": source_ids,
            "Enabled": enabled,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, subscription_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an RDS event notification subscription.

    Args:
        subscription_name(str): The name of the RDS event notification subscription you want to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.event.subscription.delete(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.event.subscription.delete subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="delete_event_subscription",
        op_kwargs={"SubscriptionName": subscription_name},
    )


async def describe_all(
    hub,
    ctx,
    subscription_name: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the subscription descriptions for a customer account. The description for a subscription includes SubscriptionName, SNSTopicARN, CustomerID, SourceType, SourceID, CreationTime, and Status. If you specify a SubscriptionName, lists the description for that subscription.

    Args:
        subscription_name(str, optional): The name of the RDS event notification subscription you want to describe. Defaults to None.
        filters(List, optional): This parameter isn't currently supported. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that you can retrieve the remaining results.  Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional):  An optional pagination token provided by a previous DescribeOrderableDBInstanceOptions request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords . . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.event.subscription.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.event.subscription.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="describe_event_subscriptions",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str = None,
    source_type: str = None,
    event_categories: List = None,
    enabled: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies an existing RDS event notification subscription. You can't modify the source identifiers using this call. To change source identifiers for a subscription, use the AddSourceIdentifierToSubscription and RemoveSourceIdentifierFromSubscription calls. You can see a list of the event categories for a given source type (SourceType) in Events in the Amazon RDS User Guide or by using the DescribeEventCategories operation.

    Args:
        subscription_name(str): The name of the RDS event notification subscription.
        sns_topic_arn(str, optional): The Amazon Resource Name (ARN) of the SNS topic created for event notification. The ARN is created by Amazon SNS when you create a topic and subscribe to it. Defaults to None.
        source_type(str, optional): The type of source that is generating the events. For example, if you want to be notified of events generated by a DB instance, you would set this parameter to db-instance. If this value isn't specified, all events are returned. Valid values: db-instance | db-cluster | db-parameter-group | db-security-group | db-snapshot | db-cluster-snapshot . Defaults to None.
        event_categories(List, optional):  A list of event categories for a source type (SourceType) that you want to subscribe to. You can see a list of the categories for a given source type in Events in the Amazon RDS User Guide or by using the DescribeEventCategories operation. . Defaults to None.
        enabled(bool, optional):  A value that indicates whether to activate the subscription. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.event.subscription.modify(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.event.subscription.modify subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="modify_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "Enabled": enabled,
        },
    )
