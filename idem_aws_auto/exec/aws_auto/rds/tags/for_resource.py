"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_name: str, filters: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all tags on an Amazon RDS resource. For an overview on tagging an Amazon RDS resource, see Tagging Amazon RDS Resources in the Amazon RDS User Guide.

    Args:
        resource_name(str): The Amazon RDS resource with tags to be listed. This value is an Amazon Resource Name (ARN). For information about creating an ARN, see  Constructing an ARN for Amazon RDS in the Amazon RDS User Guide.
        filters(List, optional): This parameter isn't currently supported. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.tags.for_resource.list(ctx, resource_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.tags.for_resource.list resource_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="list_tags_for_resource",
        op_kwargs={"ResourceName": resource_name, "Filters": filters},
    )
