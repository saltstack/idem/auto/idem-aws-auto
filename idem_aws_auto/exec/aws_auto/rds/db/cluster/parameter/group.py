"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def copy(
    hub,
    ctx,
    source_db_cluster_parameter_group_identifier: str,
    target_db_cluster_parameter_group_identifier: str,
    target_db_cluster_parameter_group_description: str,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Copies the specified DB cluster parameter group.  This action only applies to Aurora DB clusters.

    Args:
        source_db_cluster_parameter_group_identifier(str): The identifier or Amazon Resource Name (ARN) for the source DB cluster parameter group. For information about creating an ARN, see  Constructing an ARN for Amazon RDS in the Amazon Aurora User Guide.  Constraints:   Must specify a valid DB cluster parameter group.  .
        target_db_cluster_parameter_group_identifier(str): The identifier for the copied DB cluster parameter group. Constraints:   Can't be null, empty, or blank   Must contain from 1 to 255 letters, numbers, or hyphens   First character must be a letter   Can't end with a hyphen or contain two consecutive hyphens   Example: my-cluster-param-group1 .
        target_db_cluster_parameter_group_description(str): A description for the copied DB cluster parameter group.
        tags(List, optional): A list of tags. For more information, see Tagging Amazon RDS Resources in the Amazon RDS User Guide. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.copy(
                ctx,
                source_db_cluster_parameter_group_identifier=value,
                target_db_cluster_parameter_group_identifier=value,
                target_db_cluster_parameter_group_description=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.copy source_db_cluster_parameter_group_identifier=value, target_db_cluster_parameter_group_identifier=value, target_db_cluster_parameter_group_description=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="copy_db_cluster_parameter_group",
        op_kwargs={
            "SourceDBClusterParameterGroupIdentifier": source_db_cluster_parameter_group_identifier,
            "TargetDBClusterParameterGroupIdentifier": target_db_cluster_parameter_group_identifier,
            "TargetDBClusterParameterGroupDescription": target_db_cluster_parameter_group_description,
            "Tags": tags,
        },
    )


async def create(
    hub,
    ctx,
    db_cluster_parameter_group_name: str,
    db_parameter_group_family: str,
    description: str,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new DB cluster parameter group. Parameters in a DB cluster parameter group apply to all of the instances in a DB cluster.  A DB cluster parameter group is initially created with the default parameters for the database engine used by instances in the DB cluster. To provide custom values for any of the parameters, you must modify the group after creating it using ModifyDBClusterParameterGroup. Once you've created a DB cluster parameter group, you need to associate it with your DB cluster using ModifyDBCluster. When you associate a new DB cluster parameter group with a running DB cluster, you need to reboot the DB instances in the DB cluster without failover for the new DB cluster parameter group and associated settings to take effect.   After you create a DB cluster parameter group, you should wait at least 5 minutes before creating your first DB cluster that uses that DB cluster parameter group as the default parameter group. This allows Amazon RDS to fully complete the create action before the DB cluster parameter group is used as the default for a new DB cluster. This is especially important for parameters that are critical when creating the default database for a DB cluster, such as the character set for the default database defined by the character_set_database parameter. You can use the Parameter Groups option of the Amazon RDS console or the DescribeDBClusterParameters action to verify that your DB cluster parameter group has been created or modified.  For more information on Amazon Aurora, see  What Is Amazon Aurora? in the Amazon Aurora User Guide.   This action only applies to Aurora DB clusters.

    Args:
        db_cluster_parameter_group_name(str): The name of the DB cluster parameter group. Constraints:   Must match the name of an existing DB cluster parameter group.    This value is stored as a lowercase string. .
        db_parameter_group_family(str): The DB cluster parameter group family name. A DB cluster parameter group can be associated with one and only one DB cluster parameter group family, and can be applied only to a DB cluster running a database engine and engine version compatible with that DB cluster parameter group family.  Aurora MySQL  Example: aurora5.6, aurora-mysql5.7   Aurora PostgreSQL  Example: aurora-postgresql9.6  To list all of the available parameter group families for a DB engine, use the following command:  aws rds describe-db-engine-versions --query "DBEngineVersions[].DBParameterGroupFamily" --engine <engine>  For example, to list all of the available parameter group families for the Aurora PostgreSQL DB engine, use the following command:  aws rds describe-db-engine-versions --query "DBEngineVersions[].DBParameterGroupFamily" --engine aurora-postgresql   The output contains duplicates.  The following are the valid DB engine values:    aurora (for MySQL 5.6-compatible Aurora)    aurora-mysql (for MySQL 5.7-compatible Aurora)    aurora-postgresql   .
        description(str): The description for the DB cluster parameter group.
        tags(List, optional): Tags to assign to the DB cluster parameter group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.create(
                ctx,
                db_cluster_parameter_group_name=value,
                db_parameter_group_family=value,
                description=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.create db_cluster_parameter_group_name=value, db_parameter_group_family=value, description=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="create_db_cluster_parameter_group",
        op_kwargs={
            "DBClusterParameterGroupName": db_cluster_parameter_group_name,
            "DBParameterGroupFamily": db_parameter_group_family,
            "Description": description,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, db_cluster_parameter_group_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a specified DB cluster parameter group. The DB cluster parameter group to be deleted can't be associated with any DB clusters. For more information on Amazon Aurora, see  What Is Amazon Aurora? in the Amazon Aurora User Guide.   This action only applies to Aurora DB clusters.

    Args:
        db_cluster_parameter_group_name(str): The name of the DB cluster parameter group. Constraints:   Must be the name of an existing DB cluster parameter group.   You can't delete a default DB cluster parameter group.   Can't be associated with any DB clusters.  .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.delete(
                ctx, db_cluster_parameter_group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.delete db_cluster_parameter_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="delete_db_cluster_parameter_group",
        op_kwargs={"DBClusterParameterGroupName": db_cluster_parameter_group_name},
    )


async def describe_all(
    hub,
    ctx,
    db_cluster_parameter_group_name: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of DBClusterParameterGroup descriptions. If a DBClusterParameterGroupName parameter is specified, the list will contain only the description of the specified DB cluster parameter group.  For more information on Amazon Aurora, see  What Is Amazon Aurora? in the Amazon Aurora User Guide.   This action only applies to Aurora DB clusters.

    Args:
        db_cluster_parameter_group_name(str, optional): The name of a specific DB cluster parameter group to return details for. Constraints:   If supplied, must match the name of an existing DBClusterParameterGroup.  . Defaults to None.
        filters(List, optional): This parameter isn't currently supported. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so you can retrieve the remaining results.  Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional):  An optional pagination token provided by a previous DescribeDBClusterParameterGroups request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="describe_db_cluster_parameter_groups",
        op_kwargs={
            "DBClusterParameterGroupName": db_cluster_parameter_group_name,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub, ctx, db_cluster_parameter_group_name: str, parameters: List
) -> Dict:
    r"""
    **Autogenerated function**

     Modifies the parameters of a DB cluster parameter group. To modify more than one parameter, submit a list of the following: ParameterName, ParameterValue, and ApplyMethod. A maximum of 20 parameters can be modified in a single request.  For more information on Amazon Aurora, see  What Is Amazon Aurora? in the Amazon Aurora User Guide.   After you create a DB cluster parameter group, you should wait at least 5 minutes before creating your first DB cluster that uses that DB cluster parameter group as the default parameter group. This allows Amazon RDS to fully complete the create action before the parameter group is used as the default for a new DB cluster. This is especially important for parameters that are critical when creating the default database for a DB cluster, such as the character set for the default database defined by the character_set_database parameter. You can use the Parameter Groups option of the Amazon RDS console or the DescribeDBClusterParameters action to verify that your DB cluster parameter group has been created or modified. If the modified DB cluster parameter group is used by an Aurora Serverless cluster, Aurora applies the update immediately. The cluster restart might interrupt your workload. In that case, your application must reopen any connections and retry any transactions that were active when the parameter changes took effect.   This action only applies to Aurora DB clusters.

    Args:
        db_cluster_parameter_group_name(str): The name of the DB cluster parameter group to modify.
        parameters(List): A list of parameters in the DB cluster parameter group to modify. Valid Values (for the application method): immediate | pending-reboot   You can use the immediate value with dynamic parameters only. You can use the pending-reboot value for both dynamic and static parameters. When the application method is immediate, changes to dynamic parameters are applied immediately to the DB clusters associated with the parameter group. When the application method is pending-reboot, changes to dynamic and static parameters are applied after a reboot without failover to the DB clusters associated with the parameter group. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.modify(
                ctx, db_cluster_parameter_group_name=value, parameters=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.modify db_cluster_parameter_group_name=value, parameters=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="modify_db_cluster_parameter_group",
        op_kwargs={
            "DBClusterParameterGroupName": db_cluster_parameter_group_name,
            "Parameters": parameters,
        },
    )


async def reset(
    hub,
    ctx,
    db_cluster_parameter_group_name: str,
    reset_all_parameters: bool = None,
    parameters: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Modifies the parameters of a DB cluster parameter group to the default value. To reset specific parameters submit a list of the following: ParameterName and ApplyMethod. To reset the entire DB cluster parameter group, specify the DBClusterParameterGroupName and ResetAllParameters parameters.   When resetting the entire group, dynamic parameters are updated immediately and static parameters are set to pending-reboot to take effect on the next DB instance restart or RebootDBInstance request. You must call RebootDBInstance for every DB instance in your DB cluster that you want the updated static parameter to apply to. For more information on Amazon Aurora, see  What Is Amazon Aurora? in the Amazon Aurora User Guide.   This action only applies to Aurora DB clusters.

    Args:
        db_cluster_parameter_group_name(str): The name of the DB cluster parameter group to reset.
        reset_all_parameters(bool, optional): A value that indicates whether to reset all parameters in the DB cluster parameter group to their default values. You can't use this parameter if there is a list of parameter names specified for the Parameters parameter. Defaults to None.
        parameters(List, optional): A list of parameter names in the DB cluster parameter group to reset to the default values. You can't use this parameter if the ResetAllParameters parameter is enabled. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.db.cluster.parameter.group.reset(
                ctx, db_cluster_parameter_group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.db.cluster.parameter.group.reset db_cluster_parameter_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="reset_db_cluster_parameter_group",
        op_kwargs={
            "DBClusterParameterGroupName": db_cluster_parameter_group_name,
            "ResetAllParameters": reset_all_parameters,
            "Parameters": parameters,
        },
    )
