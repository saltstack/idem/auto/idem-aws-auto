"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(hub, ctx, db_instance_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    You can call DescribeValidDBInstanceModifications to learn what modifications you can make to your DB instance. You can use this information when you call ModifyDBInstance.

    Args:
        db_instance_identifier(str): The customer identifier or the ARN of your DB instance. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rds.valid_db_instance_modification.describe_all(
                ctx, db_instance_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rds.valid_db_instance_modification.describe_all db_instance_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rds",
        operation="describe_valid_db_instance_modifications",
        op_kwargs={"DBInstanceIdentifier": db_instance_identifier},
    )
