"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(hub, ctx, phone_config: Dict, user_id: str, instance_id: str) -> None:
    r"""
    **Autogenerated function**

    Updates the phone configuration settings for the specified user.

    Args:
        phone_config(Dict): Information about phone configuration settings for the user.
        user_id(str): The identifier of the user account.
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.user.phone_config.update(
                ctx, phone_config=value, user_id=value, instance_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.user.phone_config.update phone_config=value, user_id=value, instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="update_user_phone_config",
        op_kwargs={
            "PhoneConfig": phone_config,
            "UserId": user_id,
            "InstanceId": instance_id,
        },
    )
