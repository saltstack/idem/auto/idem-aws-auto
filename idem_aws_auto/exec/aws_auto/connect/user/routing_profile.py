"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def update(
    hub, ctx, routing_profile_id: str, user_id: str, instance_id: str
) -> None:
    r"""
    **Autogenerated function**

    Assigns the specified routing profile to the specified user.

    Args:
        routing_profile_id(str): The identifier of the routing profile for the user.
        user_id(str): The identifier of the user account.
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.user.routing_profile.update(
                ctx, routing_profile_id=value, user_id=value, instance_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.user.routing_profile.update routing_profile_id=value, user_id=value, instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="update_user_routing_profile",
        op_kwargs={
            "RoutingProfileId": routing_profile_id,
            "UserId": user_id,
            "InstanceId": instance_id,
        },
    )
