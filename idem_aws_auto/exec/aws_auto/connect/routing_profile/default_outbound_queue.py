"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def update(
    hub, ctx, instance_id: str, routing_profile_id: str, default_outbound_queue_id: str
) -> None:
    r"""
    **Autogenerated function**

    Updates the default outbound queue of a routing profile.

    Args:
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
        routing_profile_id(str): The identifier of the routing profile.
        default_outbound_queue_id(str): The identifier for the default outbound queue.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.routing_profile.default_outbound_queue.update(
                ctx, instance_id=value, routing_profile_id=value, default_outbound_queue_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.routing_profile.default_outbound_queue.update instance_id=value, routing_profile_id=value, default_outbound_queue_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="update_routing_profile_default_outbound_queue",
        op_kwargs={
            "InstanceId": instance_id,
            "RoutingProfileId": routing_profile_id,
            "DefaultOutboundQueueId": default_outbound_queue_id,
        },
    )
