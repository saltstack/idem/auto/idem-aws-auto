"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(hub, ctx, instance_id: str, lex_bot: Dict) -> None:
    r"""
    **Autogenerated function**

    This API is in preview release for Amazon Connect and is subject to change. Allows the specified Amazon Connect instance to access the specified Amazon Lex bot.

    Args:
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
        lex_bot(Dict): The Amazon Lex bot to associate with the instance.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.lex_bot.associate(ctx, instance_id=value, lex_bot=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.lex_bot.associate instance_id=value, lex_bot=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="associate_lex_bot",
        op_kwargs={"InstanceId": instance_id, "LexBot": lex_bot},
    )


async def disassociate(
    hub, ctx, instance_id: str, bot_name: str, lex_region: str
) -> None:
    r"""
    **Autogenerated function**

    This API is in preview release for Amazon Connect and is subject to change. Revokes authorization from the specified instance to access the specified Amazon Lex bot.

    Args:
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
        bot_name(str): The name of the Amazon Lex bot. Maximum character limit of 50.
        lex_region(str): The Region in which the Amazon Lex bot has been created.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.lex_bot.disassociate(
                ctx, instance_id=value, bot_name=value, lex_region=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.lex_bot.disassociate instance_id=value, bot_name=value, lex_region=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="disassociate_lex_bot",
        op_kwargs={
            "InstanceId": instance_id,
            "BotName": bot_name,
            "LexRegion": lex_region,
        },
    )


async def list_all(
    hub, ctx, instance_id: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    This API is in preview release for Amazon Connect and is subject to change. Returns a paginated list of all the Amazon Lex bots currently associated with the instance.

    Args:
        instance_id(str): The identifier of the Amazon Connect instance. You can find the instanceId in the ARN of the instance.
        next_token(str, optional): The token for the next set of results. Use the value returned in the previous response in the next request to retrieve the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return per page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.connect.lex_bot.list_all(ctx, instance_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.connect.lex_bot.list_all instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="connect",
        operation="list_lex_bots",
        op_kwargs={
            "InstanceId": instance_id,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
