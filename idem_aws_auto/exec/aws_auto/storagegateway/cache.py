"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def add(hub, ctx, gateway_arn: str, disk_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Configures one or more gateway local disks as cache for a gateway. This operation is only supported in the cached volume, tape, and file gateway type (see How Storage Gateway works (architecture). In the request, you specify the gateway Amazon Resource Name (ARN) to which you want to add cache, and one or more disk IDs that you want to configure as cache.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.
        disk_ids(List): An array of strings that identify disks that are to be configured as working storage. Each string has a minimum length of 1 and maximum length of 300. You can get the disk IDs from the ListLocalDisks API.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.cache.add(ctx, gateway_arn=value, disk_ids=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.cache.add gateway_arn=value, disk_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="add_cache",
        op_kwargs={"GatewayARN": gateway_arn, "DiskIds": disk_ids},
    )


async def describe(hub, ctx, gateway_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the cache of a gateway. This operation is only supported in the cached volume, tape, and file gateway types. The response includes disk IDs that are configured as cache, and it includes the amount of cache allocated and used.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.cache.describe(ctx, gateway_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.cache.describe gateway_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="describe_cache",
        op_kwargs={"GatewayARN": gateway_arn},
    )


async def refresh(
    hub, ctx, file_share_arn: str, folder_list: List = None, recursive: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Refreshes the cached inventory of objects for the specified file share. This operation finds objects in the Amazon S3 bucket that were added, removed, or replaced since the gateway last listed the bucket's contents and cached the results. This operation does not import files into the S3 File Gateway cache storage. It only updates the cached inventory to reflect changes in the inventory of the objects in the S3 bucket. This operation is only supported in the S3 File Gateway types. You can subscribe to be notified through an Amazon CloudWatch event when your RefreshCache operation completes. For more information, see Getting notified about file operations in the Storage Gateway User Guide. This operation is Only supported for S3 File Gateways. When this API is called, it only initiates the refresh operation. When the API call completes and returns a success code, it doesn't necessarily mean that the file refresh has completed. You should use the refresh-complete notification to determine that the operation has completed before you check for new files on the gateway file share. You can subscribe to be notified through a CloudWatch event when your RefreshCache operation completes. Throttle limit: This API is asynchronous, so the gateway will accept no more than two refreshes at any time. We recommend using the refresh-complete CloudWatch event notification before issuing additional requests. For more information, see Getting notified about file operations in the Storage Gateway User Guide. If you invoke the RefreshCache API when two requests are already being processed, any new request will cause an InvalidGatewayRequestException error because too many requests were sent to the server. For more information, see Getting notified about file operations in the Storage Gateway User Guide.

    Args:
        file_share_arn(str): The Amazon Resource Name (ARN) of the file share you want to refresh.
        folder_list(List, optional): A comma-separated list of the paths of folders to refresh in the cache. The default is ["/"]. The default refreshes objects and folders at the root of the Amazon S3 bucket. If Recursive is set to true, the entire S3 bucket that the file share has access to is refreshed. Defaults to None.
        recursive(bool, optional): A value that specifies whether to recursively refresh folders in the cache. The refresh includes folders that were in the cache the last time the gateway listed the folder's contents. If this value set to true, each folder that is listed in FolderList is recursively updated. Otherwise, subfolders listed in FolderList are not refreshed. Only objects that are in folders listed directly under FolderList are found and used for the update. The default is true. Valid Values: true | false . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.cache.refresh(ctx, file_share_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.cache.refresh file_share_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="refresh_cache",
        op_kwargs={
            "FileShareARN": file_share_arn,
            "FolderList": folder_list,
            "Recursive": recursive,
        },
    )


async def reset(hub, ctx, gateway_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Resets all cache disks that have encountered an error and makes the disks available for reconfiguration as cache storage. If your cache disk encounters an error, the gateway prevents read and write operations on virtual tapes in the gateway. For example, an error can occur when a disk is corrupted or removed from the gateway. When a cache is reset, the gateway loses its cache storage. At this point, you can reconfigure the disks as cache disks. This operation is only supported in the cached volume and tape types.  If the cache disk you are resetting contains data that has not been uploaded to Amazon S3 yet, that data can be lost. After you reset cache disks, there will be no configured cache disks left in the gateway, so you must configure at least one new cache disk for your gateway to function properly.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.cache.reset(ctx, gateway_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.cache.reset gateway_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="reset_cache",
        op_kwargs={"GatewayARN": gateway_arn},
    )
