"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, gateway_arn: str, bandwidth_type: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the bandwidth rate limits of a gateway. You can delete either the upload and download bandwidth rate limit, or you can delete both. If you delete only one of the limits, the other limit remains unchanged. To specify which gateway to work with, use the Amazon Resource Name (ARN) of the gateway in your request. This operation is supported for the stored volume, cached volume and tape gateway types.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.
        bandwidth_type(str): One of the BandwidthType values that indicates the gateway bandwidth rate limit to delete. Valid Values: UPLOAD | DOWNLOAD | ALL .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.bandwidth_rate_limit.init.delete(
                ctx, gateway_arn=value, bandwidth_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.bandwidth_rate_limit.init.delete gateway_arn=value, bandwidth_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="delete_bandwidth_rate_limit",
        op_kwargs={"GatewayARN": gateway_arn, "BandwidthType": bandwidth_type},
    )


async def describe(hub, ctx, gateway_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the bandwidth rate limits of a gateway. By default, these limits are not set, which means no bandwidth rate limiting is in effect. This operation is supported for the stored volume, cached volume, and tape gateway types. This operation only returns a value for a bandwidth rate limit only if the limit is set. If no limits are set for the gateway, then this operation returns only the gateway ARN in the response body. To specify which gateway to describe, use the Amazon Resource Name (ARN) of the gateway in your request.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.bandwidth_rate_limit.init.describe(
                ctx, gateway_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.bandwidth_rate_limit.init.describe gateway_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="describe_bandwidth_rate_limit",
        op_kwargs={"GatewayARN": gateway_arn},
    )


async def update(
    hub,
    ctx,
    gateway_arn: str,
    average_upload_rate_limit_in_bits_per_sec: int = None,
    average_download_rate_limit_in_bits_per_sec: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the bandwidth rate limits of a gateway. You can update both the upload and download bandwidth rate limit or specify only one of the two. If you don't set a bandwidth rate limit, the existing rate limit remains. This operation is supported for the stored volume, cached volume, and tape gateway types. By default, a gateway's bandwidth rate limits are not set. If you don't set any limit, the gateway does not have any limitations on its bandwidth usage and could potentially use the maximum available bandwidth. To specify which gateway to update, use the Amazon Resource Name (ARN) of the gateway in your request.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.
        average_upload_rate_limit_in_bits_per_sec(int, optional): The average upload bandwidth rate limit in bits per second. Defaults to None.
        average_download_rate_limit_in_bits_per_sec(int, optional): The average download bandwidth rate limit in bits per second. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.bandwidth_rate_limit.init.update(
                ctx, gateway_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.bandwidth_rate_limit.init.update gateway_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="update_bandwidth_rate_limit",
        op_kwargs={
            "GatewayARN": gateway_arn,
            "AverageUploadRateLimitInBitsPerSec": average_upload_rate_limit_in_bits_per_sec,
            "AverageDownloadRateLimitInBitsPerSec": average_download_rate_limit_in_bits_per_sec,
        },
    )
