"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, file_share_arn: str, force_delete: bool = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a file share from an S3 File Gateway. This operation is only supported for S3 File Gateways.

    Args:
        file_share_arn(str): The Amazon Resource Name (ARN) of the file share to be deleted.
        force_delete(bool, optional): If this value is set to true, the operation deletes a file share immediately and aborts all data uploads to Amazon Web Services. Otherwise, the file share is not deleted until all data is uploaded to Amazon Web Services. This process aborts the data upload process, and the file share enters the FORCE_DELETING status. Valid Values: true | false . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.file.share.delete(ctx, file_share_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.file.share.delete file_share_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="delete_file_share",
        op_kwargs={"FileShareARN": file_share_arn, "ForceDelete": force_delete},
    )


async def list_all(
    hub, ctx, gateway_arn: str = None, limit: int = None, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of the file shares for a specific S3 File Gateway, or the list of file shares that belong to the calling user account. This operation is only supported for S3 File Gateways.

    Args:
        gateway_arn(str, optional): The Amazon Resource Name (ARN) of the gateway whose file shares you want to list. If this field is not present, all file shares under your account are listed. Defaults to None.
        limit(int, optional): The maximum number of file shares to return in the response. The value must be an integer with a value greater than zero. Optional. Defaults to None.
        marker(str, optional): Opaque pagination token returned from a previous ListFileShares operation. If present, Marker specifies where to continue the list from after a previous call to ListFileShares. Optional. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.file.share.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.file.share.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="list_file_shares",
        op_kwargs={"GatewayARN": gateway_arn, "Limit": limit, "Marker": marker},
    )
