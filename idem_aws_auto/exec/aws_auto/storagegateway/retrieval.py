"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def cancel(hub, ctx, gateway_arn: str, tape_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Cancels retrieval of a virtual tape from the virtual tape shelf (VTS) to a gateway after the retrieval process is initiated. The virtual tape is returned to the VTS. This operation is only supported in the tape gateway type.

    Args:
        gateway_arn(str): The Amazon Resource Name (ARN) of the gateway. Use the ListGateways operation to return a list of gateways for your account and Region.
        tape_arn(str): The Amazon Resource Name (ARN) of the virtual tape you want to cancel retrieval for.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.storagegateway.retrieval.cancel(
                ctx, gateway_arn=value, tape_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.storagegateway.retrieval.cancel gateway_arn=value, tape_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="storagegateway",
        operation="cancel_retrieval",
        op_kwargs={"GatewayARN": gateway_arn, "TapeARN": tape_arn},
    )
