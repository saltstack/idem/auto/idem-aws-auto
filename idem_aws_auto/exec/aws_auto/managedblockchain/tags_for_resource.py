"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of tags for the specified resource. Each tag consists of a key and optional value. For more information about tags, see Tagging Resources in the Amazon Managed Blockchain Ethereum Developer Guide, or Tagging Resources in the Amazon Managed Blockchain Hyperledger Fabric Developer Guide.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource. For more information about ARNs and their format, see Amazon Resource Names (ARNs) in the AWS General Reference.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.managedblockchain.tags_for_resource.list(
                ctx, resource_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.managedblockchain.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="managedblockchain",
        operation="list_tags_for_resource",
        op_kwargs={"ResourceArn": resource_arn},
    )
