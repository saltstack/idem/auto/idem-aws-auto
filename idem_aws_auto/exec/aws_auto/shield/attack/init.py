"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe(hub, ctx, attack_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes the details of a DDoS attack.

    Args:
        attack_id(str): The unique identifier (ID) for the attack that to be described.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.shield.attack.init.describe(ctx, attack_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.shield.attack.init.describe attack_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="shield",
        operation="describe_attack",
        op_kwargs={"AttackId": attack_id},
    )


async def list_all(
    hub,
    ctx,
    resource_arns: List = None,
    start_time: Dict = None,
    end_time: Dict = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns all ongoing DDoS attacks or all DDoS attacks during a specified time period.

    Args:
        resource_arns(List, optional): The ARN (Amazon Resource Name) of the resource that was attacked. If this is left blank, all applicable resources for this account will be included. Defaults to None.
        start_time(Dict, optional): The start of the time period for the attacks. This is a timestamp type. The sample request above indicates a number type because the default used by WAF is Unix time in seconds. However any valid timestamp format is allowed. . Defaults to None.
        end_time(Dict, optional): The end of the time period for the attacks. This is a timestamp type. The sample request above indicates a number type because the default used by WAF is Unix time in seconds. However any valid timestamp format is allowed. . Defaults to None.
        next_token(str, optional): The ListAttacksRequest.NextMarker value from a previous call to ListAttacksRequest. Pass null if this is the first call. Defaults to None.
        max_results(int, optional): The maximum number of AttackSummary objects to return. If you leave this blank, Shield Advanced returns the first 20 results. This is a maximum value. Shield Advanced might return the results in smaller batches. That is, the number of objects returned could be less than MaxResults, even if there are still more objects yet to return. If there are more objects to return, Shield Advanced returns a value in NextToken that you can use in your next request, to get the next batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.shield.attack.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.shield.attack.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="shield",
        operation="list_attacks",
        op_kwargs={
            "ResourceArns": resource_arns,
            "StartTime": start_time,
            "EndTime": end_time,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
