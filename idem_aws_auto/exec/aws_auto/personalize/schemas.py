"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns the list of schemas associated with the account. The response provides the properties for each schema, including the Amazon Resource Name (ARN). For more information on schemas, see CreateSchema.

    Args:
        next_token(str, optional): A token returned from the previous call to ListSchemas for getting the next set of schemas (if they exist). Defaults to None.
        max_results(int, optional): The maximum number of schemas to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.personalize.schemas.list(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.personalize.schemas.list
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="personalize",
        operation="list_schemas",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )
