"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    job_name: str,
    dataset_arn: str,
    role_arn: str,
    job_output: Dict,
    ingestion_mode: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a job that exports data from your dataset to an Amazon S3 bucket. To allow Amazon Personalize to export the training data, you must specify an service-linked IAM role that gives Amazon Personalize PutObject permissions for your Amazon S3 bucket. For information, see Exporting a dataset in the Amazon Personalize developer guide.   Status  A dataset export job can be in one of the following states:   CREATE PENDING > CREATE IN_PROGRESS > ACTIVE -or- CREATE FAILED    To get the status of the export job, call DescribeDatasetExportJob, and specify the Amazon Resource Name (ARN) of the dataset export job. The dataset export is complete when the status shows as ACTIVE. If the status shows as CREATE FAILED, the response includes a failureReason key, which describes why the job failed.

    Args:
        job_name(str): The name for the dataset export job.
        dataset_arn(str): The Amazon Resource Name (ARN) of the dataset that contains the data to export.
        ingestion_mode(str, optional): The data to export, based on how you imported the data. You can choose to export only BULK data that you imported using a dataset import job, only PUT data that you imported incrementally (using the console, PutEvents, PutUsers and PutItems operations), or ALL for both types. The default value is PUT. . Defaults to None.
        role_arn(str): The Amazon Resource Name (ARN) of the IAM service role that has permissions to add data to your output Amazon S3 bucket.
        job_output(Dict): The path to the Amazon S3 bucket where the job's output is stored.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.personalize.dataset.export_job.create(
                ctx, job_name=value, dataset_arn=value, role_arn=value, job_output=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.personalize.dataset.export_job.create job_name=value, dataset_arn=value, role_arn=value, job_output=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="personalize",
        operation="create_dataset_export_job",
        op_kwargs={
            "jobName": job_name,
            "datasetArn": dataset_arn,
            "ingestionMode": ingestion_mode,
            "roleArn": role_arn,
            "jobOutput": job_output,
        },
    )


async def describe(hub, ctx, dataset_export_job_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes the dataset export job created by CreateDatasetExportJob, including the export job status.

    Args:
        dataset_export_job_arn(str): The Amazon Resource Name (ARN) of the dataset export job to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.personalize.dataset.export_job.describe(
                ctx, dataset_export_job_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.personalize.dataset.export_job.describe dataset_export_job_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="personalize",
        operation="describe_dataset_export_job",
        op_kwargs={"datasetExportJobArn": dataset_export_job_arn},
    )


async def list_all(
    hub, ctx, dataset_arn: str = None, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of dataset export jobs that use the given dataset. When a dataset is not specified, all the dataset export jobs associated with the account are listed. The response provides the properties for each dataset export job, including the Amazon Resource Name (ARN). For more information on dataset export jobs, see CreateDatasetExportJob. For more information on datasets, see CreateDataset.

    Args:
        dataset_arn(str, optional): The Amazon Resource Name (ARN) of the dataset to list the dataset export jobs for. Defaults to None.
        next_token(str, optional): A token returned from the previous call to ListDatasetExportJobs for getting the next set of dataset export jobs (if they exist). Defaults to None.
        max_results(int, optional): The maximum number of dataset export jobs to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.personalize.dataset.export_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.personalize.dataset.export_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="personalize",
        operation="list_dataset_export_jobs",
        op_kwargs={
            "datasetArn": dataset_arn,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
