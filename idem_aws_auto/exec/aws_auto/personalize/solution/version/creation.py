"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def stop(hub, ctx, solution_version_arn: str) -> None:
    r"""
    **Autogenerated function**

    Stops creating a solution version that is in a state of CREATE_PENDING or CREATE IN_PROGRESS.  Depending on the current state of the solution version, the solution version state changes as follows:   CREATE_PENDING > CREATE_STOPPED or   CREATE_IN_PROGRESS > CREATE_STOPPING > CREATE_STOPPED   You are billed for all of the training completed up until you stop the solution version creation. You cannot resume creating a solution version once it has been stopped.

    Args:
        solution_version_arn(str): The Amazon Resource Name (ARN) of the solution version you want to stop creating.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.personalize.solution.version.creation.stop(
                ctx, solution_version_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.personalize.solution.version.creation.stop solution_version_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="personalize",
        operation="stop_solution_version_creation",
        op_kwargs={"solutionVersionArn": solution_version_arn},
    )
