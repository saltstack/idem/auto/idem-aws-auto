"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def request(
    hub, ctx, service_code: str, quota_code: str, desired_value: float
) -> Dict:
    r"""
    **Autogenerated function**

    Submits a quota increase request for the specified quota.

    Args:
        service_code(str): The service identifier.
        quota_code(str): The quota identifier.
        desired_value(float): The new, increased value for the quota.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.service_quotas.service.quota.increase.init.request(
                ctx, service_code=value, quota_code=value, desired_value=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.service_quotas.service.quota.increase.init.request service_code=value, quota_code=value, desired_value=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="service-quotas",
        operation="request_service_quota_increase",
        op_kwargs={
            "ServiceCode": service_code,
            "QuotaCode": quota_code,
            "DesiredValue": desired_value,
        },
    )
