"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx, alias_name: str, target_key_id: str) -> None:
    r"""
    **Autogenerated function**

    Creates a friendly name for a customer master key (CMK).   Adding, deleting, or updating an alias can allow or deny permission to the CMK. For details, see Using ABAC in AWS KMS in the AWS Key Management Service Developer Guide.  You can use an alias to identify a CMK in the AWS KMS console, in the DescribeKey operation and in cryptographic operations, such as Encrypt and GenerateDataKey. You can also change the CMK that's associated with the alias (UpdateAlias) or delete the alias (DeleteAlias) at any time. These operations don't affect the underlying CMK.  You can associate the alias with any customer managed CMK in the same AWS Region. Each alias is associated with only one CMK at a time, but a CMK can have multiple aliases. A valid CMK is required. You can't create an alias without a CMK. The alias must be unique in the account and Region, but you can have aliases with the same name in different Regions. For detailed information about aliases, see Using aliases in the AWS Key Management Service Developer Guide. This operation does not return a response. To get the alias that you created, use the ListAliases operation. The CMK that you use for this operation must be in a compatible key state. For details, see Key state: Effect on your CMK in the AWS Key Management Service Developer Guide.  Cross-account use: No. You cannot perform this operation on an alias in a different AWS account.  Required permissions     kms:CreateAlias on the alias (IAM policy).    kms:CreateAlias on the CMK (key policy).   For details, see Controlling access to aliases in the AWS Key Management Service Developer Guide.  Related operations:     DeleteAlias     ListAliases     UpdateAlias

    Args:
        alias_name(str): Specifies the alias name. This value must begin with alias/ followed by a name, such as alias/ExampleAlias.  The AliasName value must be string of 1-256 characters. It can contain only alphanumeric characters, forward slashes (/), underscores (_), and dashes (-). The alias name cannot begin with alias/aws/. The alias/aws/ prefix is reserved for AWS managed CMKs.
        target_key_id(str): Associates the alias with the specified customer managed CMK. The CMK must be in the same AWS Region.  A valid CMK ID is required. If you supply a null or empty string value, this operation returns an error. For help finding the key ID and ARN, see Finding the Key ID and ARN in the AWS Key Management Service Developer Guide. Specify the key ID or key ARN of the CMK. For example:   Key ID: 1234abcd-12ab-34cd-56ef-1234567890ab    Key ARN: arn:aws:kms:us-east-2:111122223333:key/1234abcd-12ab-34cd-56ef-1234567890ab    To get the key ID and key ARN for a CMK, use ListKeys or DescribeKey.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kms.alias.create(ctx, alias_name=value, target_key_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kms.alias.create alias_name=value, target_key_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kms",
        operation="create_alias",
        op_kwargs={"AliasName": alias_name, "TargetKeyId": target_key_id},
    )


async def delete(hub, ctx, alias_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified alias.   Adding, deleting, or updating an alias can allow or deny permission to the CMK. For details, see Using ABAC in AWS KMS in the AWS Key Management Service Developer Guide.  Because an alias is not a property of a CMK, you can delete and change the aliases of a CMK without affecting the CMK. Also, aliases do not appear in the response from the DescribeKey operation. To get the aliases of all CMKs, use the ListAliases operation.  Each CMK can have multiple aliases. To change the alias of a CMK, use DeleteAlias to delete the current alias and CreateAlias to create a new alias. To associate an existing alias with a different customer master key (CMK), call UpdateAlias.  Cross-account use: No. You cannot perform this operation on an alias in a different AWS account.  Required permissions     kms:DeleteAlias on the alias (IAM policy).    kms:DeleteAlias on the CMK (key policy).   For details, see Controlling access to aliases in the AWS Key Management Service Developer Guide.  Related operations:     CreateAlias     ListAliases     UpdateAlias

    Args:
        alias_name(str): The alias to be deleted. The alias name must begin with alias/ followed by the alias name, such as alias/ExampleAlias.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kms.alias.delete(ctx, alias_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kms.alias.delete alias_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kms",
        operation="delete_alias",
        op_kwargs={"AliasName": alias_name},
    )


async def list_all(
    hub, ctx, key_id: str = None, limit: int = None, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of aliases in the caller's AWS account and region. For more information about aliases, see CreateAlias. By default, the ListAliases operation returns all aliases in the account and region. To get only the aliases associated with a particular customer master key (CMK), use the KeyId parameter. The ListAliases response can include aliases that you created and associated with your customer managed CMKs, and aliases that AWS created and associated with AWS managed CMKs in your account. You can recognize AWS aliases because their names have the format aws/<service-name>, such as aws/dynamodb. The response might also include aliases that have no TargetKeyId field. These are predefined aliases that AWS has created but has not yet associated with a CMK. Aliases that AWS creates in your account, including predefined aliases, do not count against your AWS KMS aliases quota.  Cross-account use: No. ListAliases does not return aliases in other AWS accounts.  Required permissions: kms:ListAliases (IAM policy) For details, see Controlling access to aliases in the AWS Key Management Service Developer Guide.  Related operations:     CreateAlias     DeleteAlias     UpdateAlias

    Args:
        key_id(str, optional): Lists only aliases that are associated with the specified CMK. Enter a CMK in your AWS account.  This parameter is optional. If you omit it, ListAliases returns all aliases in the account and Region. Specify the key ID or key ARN of the CMK. For example:   Key ID: 1234abcd-12ab-34cd-56ef-1234567890ab    Key ARN: arn:aws:kms:us-east-2:111122223333:key/1234abcd-12ab-34cd-56ef-1234567890ab    To get the key ID and key ARN for a CMK, use ListKeys or DescribeKey. Defaults to None.
        limit(int, optional): Use this parameter to specify the maximum number of items to return. When this value is present, AWS KMS does not return more than the specified number of items, but it might return fewer. This value is optional. If you include a value, it must be between 1 and 100, inclusive. If you do not include a value, it defaults to 50. Defaults to None.
        marker(str, optional): Use this parameter in a subsequent request after you receive a response with truncated results. Set it to the value of NextMarker from the truncated response you just received. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kms.alias.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kms.alias.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kms",
        operation="list_aliases",
        op_kwargs={"KeyId": key_id, "Limit": limit, "Marker": marker},
    )


async def update(hub, ctx, alias_name: str, target_key_id: str) -> None:
    r"""
    **Autogenerated function**

    Associates an existing AWS KMS alias with a different customer master key (CMK). Each alias is associated with only one CMK at a time, although a CMK can have multiple aliases. The alias and the CMK must be in the same AWS account and Region.  Adding, deleting, or updating an alias can allow or deny permission to the CMK. For details, see Using ABAC in AWS KMS in the AWS Key Management Service Developer Guide.  The current and new CMK must be the same type (both symmetric or both asymmetric), and they must have the same key usage (ENCRYPT_DECRYPT or SIGN_VERIFY). This restriction prevents errors in code that uses aliases. If you must assign an alias to a different type of CMK, use DeleteAlias to delete the old alias and CreateAlias to create a new alias. You cannot use UpdateAlias to change an alias name. To change an alias name, use DeleteAlias to delete the old alias and CreateAlias to create a new alias. Because an alias is not a property of a CMK, you can create, update, and delete the aliases of a CMK without affecting the CMK. Also, aliases do not appear in the response from the DescribeKey operation. To get the aliases of all CMKs in the account, use the ListAliases operation.  The CMK that you use for this operation must be in a compatible key state. For details, see Key state: Effect on your CMK in the AWS Key Management Service Developer Guide.  Cross-account use: No. You cannot perform this operation on a CMK in a different AWS account.   Required permissions     kms:UpdateAlias on the alias (IAM policy).    kms:UpdateAlias on the current CMK (key policy).    kms:UpdateAlias on the new CMK (key policy).   For details, see Controlling access to aliases in the AWS Key Management Service Developer Guide.  Related operations:     CreateAlias     DeleteAlias     ListAliases

    Args:
        alias_name(str): Identifies the alias that is changing its CMK. This value must begin with alias/ followed by the alias name, such as alias/ExampleAlias. You cannot use UpdateAlias to change the alias name.
        target_key_id(str): Identifies the customer managed CMK to associate with the alias. You don't have permission to associate an alias with an AWS managed CMK. The CMK must be in the same AWS account and Region as the alias. Also, the new target CMK must be the same type as the current target CMK (both symmetric or both asymmetric) and they must have the same key usage.  Specify the key ID or key ARN of the CMK. For example:   Key ID: 1234abcd-12ab-34cd-56ef-1234567890ab    Key ARN: arn:aws:kms:us-east-2:111122223333:key/1234abcd-12ab-34cd-56ef-1234567890ab    To get the key ID and key ARN for a CMK, use ListKeys or DescribeKey. To verify that the alias is mapped to the correct CMK, use ListAliases.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kms.alias.update(ctx, alias_name=value, target_key_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kms.alias.update alias_name=value, target_key_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kms",
        operation="update_alias",
        op_kwargs={"AliasName": alias_name, "TargetKeyId": target_key_id},
    )
