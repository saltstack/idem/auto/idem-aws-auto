"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "parameter_for_import"


async def get(
    hub, ctx, key_id: str, wrapping_algorithm: str, wrapping_key_spec: str
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the items you need to import key material into a symmetric, customer managed customer master key (CMK). For more information about importing key material into AWS KMS, see Importing Key Material in the AWS Key Management Service Developer Guide. This operation returns a public key and an import token. Use the public key to encrypt the symmetric key material. Store the import token to send with a subsequent ImportKeyMaterial request. You must specify the key ID of the symmetric CMK into which you will import key material. This CMK's Origin must be EXTERNAL. You must also specify the wrapping algorithm and type of wrapping key (public key) that you will use to encrypt the key material. You cannot perform this operation on an asymmetric CMK or on any CMK in a different AWS account. To import key material, you must use the public key and import token from the same response. These items are valid for 24 hours. The expiration date and time appear in the GetParametersForImport response. You cannot use an expired token in an ImportKeyMaterial request. If your key and token expire, send another GetParametersForImport request. The CMK that you use for this operation must be in a compatible key state. For details, see Key state: Effect on your CMK in the AWS Key Management Service Developer Guide.  Cross-account use: No. You cannot perform this operation on a CMK in a different AWS account.  Required permissions: kms:GetParametersForImport (key policy)  Related operations:     ImportKeyMaterial     DeleteImportedKeyMaterial

    Args:
        key_id(str): The identifier of the symmetric CMK into which you will import key material. The Origin of the CMK must be EXTERNAL. Specify the key ID or key ARN of the CMK. For example:   Key ID: 1234abcd-12ab-34cd-56ef-1234567890ab    Key ARN: arn:aws:kms:us-east-2:111122223333:key/1234abcd-12ab-34cd-56ef-1234567890ab    To get the key ID and key ARN for a CMK, use ListKeys or DescribeKey.
        wrapping_algorithm(str): The algorithm you will use to encrypt the key material before importing it with ImportKeyMaterial. For more information, see Encrypt the Key Material in the AWS Key Management Service Developer Guide.
        wrapping_key_spec(str): The type of wrapping key (public key) to return in the response. Only 2048-bit RSA public keys are supported.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kms.parameter_for_import_.get(
                ctx, key_id=value, wrapping_algorithm=value, wrapping_key_spec=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kms.parameter_for_import.get key_id=value, wrapping_algorithm=value, wrapping_key_spec=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kms",
        operation="get_parameters_for_import",
        op_kwargs={
            "KeyId": key_id,
            "WrappingAlgorithm": wrapping_algorithm,
            "WrappingKeySpec": wrapping_key_spec,
        },
    )
