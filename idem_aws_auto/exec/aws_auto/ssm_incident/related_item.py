"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, incident_record_arn: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    List all related items for an incident record.

    Args:
        incident_record_arn(str): The Amazon Resource Name (ARN) of the incident record that you are listing related items for.
        max_results(int, optional): The maximum number of related items per page. Defaults to None.
        next_token(str, optional): The pagination token to continue to the next page of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm_incident.related_item.list_all(
                ctx, incident_record_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm_incident.related_item.list_all incident_record_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm-incidents",
        operation="list_related_items",
        op_kwargs={
            "incidentRecordArn": incident_record_arn,
            "maxResults": max_results,
            "nextToken": next_token,
        },
    )


async def update_multiple(
    hub,
    ctx,
    incident_record_arn: str,
    related_items_update: Dict,
    client_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Add or remove related items from the related items tab of an incident record.

    Args:
        client_token(str, optional): A token ensuring that the action is called only once with the specified details. Defaults to None.
        incident_record_arn(str): The Amazon Resource Name (ARN) of the incident record you are updating related items in.
        related_items_update(Dict): Details about the item you are adding or deleting.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm_incident.related_item.update_multiple(
                ctx, incident_record_arn=value, related_items_update=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm_incident.related_item.update_multiple incident_record_arn=value, related_items_update=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm-incidents",
        operation="update_related_items",
        op_kwargs={
            "clientToken": client_token,
            "incidentRecordArn": incident_record_arn,
            "relatedItemsUpdate": related_items_update,
        },
    )
