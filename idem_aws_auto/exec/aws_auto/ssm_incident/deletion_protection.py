"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(
    hub, ctx, arn: str, deletion_protected: bool, client_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Update deletion protection to either allow or deny deletion of the final Region in a replication set.

    Args:
        arn(str): The Amazon Resource Name (ARN) of the replication set you're updating.
        client_token(str, optional): A token ensuring that the action is called only once with the specified details. Defaults to None.
        deletion_protected(bool): Details if deletion protection is enabled or disabled in your account.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm_incident.deletion_protection.update(
                ctx, arn=value, deletion_protected=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm_incident.deletion_protection.update arn=value, deletion_protected=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm-incidents",
        operation="update_deletion_protection",
        op_kwargs={
            "arn": arn,
            "clientToken": client_token,
            "deletionProtected": deletion_protected,
        },
    )
