"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def write(
    hub,
    ctx,
    database_name: str,
    table_name: str,
    records: List,
    common_attributes: Dict = None,
) -> None:
    r"""
    **Autogenerated function**

    The WriteRecords operation enables you to write your time series data into Timestream. You can specify a single data point or a batch of data points to be inserted into the system. Timestream offers you with a flexible schema that auto detects the column names and data types for your Timestream tables based on the dimension names and data types of the data points you specify when invoking writes into the database. Timestream support eventual consistency read semantics. This means that when you query data immediately after writing a batch of data into Timestream, the query results might not reflect the results of a recently completed write operation. The results may also include some stale data. If you repeat the query request after a short time, the results should return the latest data. Service quotas apply. For more information, see Access Management in the Timestream Developer Guide.

    Args:
        database_name(str): The name of the Timestream database.
        table_name(str): The name of the Timesream table.
        common_attributes(Dict, optional): A record containing the common measure and dimension attributes shared across all the records in the request. The measure and dimension attributes specified in here will be merged with the measure and dimension attributes in the records object when the data is written into Timestream. . Defaults to None.
        records(List): An array of records containing the unique dimension and measure attributes for each time series data point. .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.timestream_write.record.write(
                ctx, database_name=value, table_name=value, records=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.timestream_write.record.write database_name=value, table_name=value, records=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="timestream-write",
        operation="write_records",
        op_kwargs={
            "DatabaseName": database_name,
            "TableName": table_name,
            "CommonAttributes": common_attributes,
            "Records": records,
        },
    )
