"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def abort(hub, ctx, account_id: str, vault_name: str) -> None:
    r"""
    **Autogenerated function**

    This operation aborts the vault locking process if the vault lock is not in the Locked state. If the vault lock is in the Locked state when this operation is requested, the operation returns an AccessDeniedException error. Aborting the vault locking process removes the vault lock policy from the specified vault.  A vault lock is put into the InProgress state by calling InitiateVaultLock. A vault lock is put into the Locked state by calling CompleteVaultLock. You can get the state of a vault lock by calling GetVaultLock. For more information about the vault locking process, see Amazon Glacier Vault Lock. For more information about vault lock policies, see Amazon Glacier Access Control with Vault Lock Policies.  This operation is idempotent. You can successfully invoke this operation multiple times, if the vault lock is in the InProgress state or if there is no policy associated with the vault.

    Args:
        account_id(str): The AccountId value is the AWS account ID. This value must match the AWS account ID associated with the credentials used to sign the request. You can either specify an AWS account ID or optionally a single '-' (hyphen), in which case Amazon Glacier uses the AWS account ID associated with the credentials used to sign the request. If you specify your account ID, do not include any hyphens ('-') in the ID.
        vault_name(str): The name of the vault.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glacier.vault.lock.abort(
                ctx, account_id=value, vault_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glacier.vault.lock.abort account_id=value, vault_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glacier",
        operation="abort_vault_lock",
        op_kwargs={"accountId": account_id, "vaultName": vault_name},
    )


async def complete(hub, ctx, account_id: str, vault_name: str, lock_id: str) -> None:
    r"""
    **Autogenerated function**

    This operation completes the vault locking process by transitioning the vault lock from the InProgress state to the Locked state, which causes the vault lock policy to become unchangeable. A vault lock is put into the InProgress state by calling InitiateVaultLock. You can obtain the state of the vault lock by calling GetVaultLock. For more information about the vault locking process, Amazon Glacier Vault Lock.  This operation is idempotent. This request is always successful if the vault lock is in the Locked state and the provided lock ID matches the lock ID originally used to lock the vault. If an invalid lock ID is passed in the request when the vault lock is in the Locked state, the operation returns an AccessDeniedException error. If an invalid lock ID is passed in the request when the vault lock is in the InProgress state, the operation throws an InvalidParameter error.

    Args:
        account_id(str): The AccountId value is the AWS account ID. This value must match the AWS account ID associated with the credentials used to sign the request. You can either specify an AWS account ID or optionally a single '-' (hyphen), in which case Amazon Glacier uses the AWS account ID associated with the credentials used to sign the request. If you specify your account ID, do not include any hyphens ('-') in the ID.
        vault_name(str): The name of the vault.
        lock_id(str): The lockId value is the lock ID obtained from a InitiateVaultLock request.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glacier.vault.lock.complete(
                ctx, account_id=value, vault_name=value, lock_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glacier.vault.lock.complete account_id=value, vault_name=value, lock_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glacier",
        operation="complete_vault_lock",
        op_kwargs={"accountId": account_id, "vaultName": vault_name, "lockId": lock_id},
    )


async def get(hub, ctx, account_id: str, vault_name: str) -> Dict:
    r"""
    **Autogenerated function**

    This operation retrieves the following attributes from the lock-policy subresource set on the specified vault:    The vault lock policy set on the vault.   The state of the vault lock, which is either InProgess or Locked.   When the lock ID expires. The lock ID is used to complete the vault locking process.   When the vault lock was initiated and put into the InProgress state.   A vault lock is put into the InProgress state by calling InitiateVaultLock. A vault lock is put into the Locked state by calling CompleteVaultLock. You can abort the vault locking process by calling AbortVaultLock. For more information about the vault locking process, Amazon Glacier Vault Lock.  If there is no vault lock policy set on the vault, the operation returns a 404 Not found error. For more information about vault lock policies, Amazon Glacier Access Control with Vault Lock Policies.

    Args:
        account_id(str): The AccountId value is the AWS account ID of the account that owns the vault. You can either specify an AWS account ID or optionally a single '-' (hyphen), in which case Amazon S3 Glacier uses the AWS account ID associated with the credentials used to sign the request. If you use an account ID, do not include any hyphens ('-') in the ID.
        vault_name(str): The name of the vault.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glacier.vault.lock.get(ctx, account_id=value, vault_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glacier.vault.lock.get account_id=value, vault_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glacier",
        operation="get_vault_lock",
        op_kwargs={"accountId": account_id, "vaultName": vault_name},
    )


async def initiate(
    hub, ctx, account_id: str, vault_name: str, policy: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    This operation initiates the vault locking process by doing the following:   Installing a vault lock policy on the specified vault.   Setting the lock state of vault lock to InProgress.   Returning a lock ID, which is used to complete the vault locking process.   You can set one vault lock policy for each vault and this policy can be up to 20 KB in size. For more information about vault lock policies, see Amazon Glacier Access Control with Vault Lock Policies.  You must complete the vault locking process within 24 hours after the vault lock enters the InProgress state. After the 24 hour window ends, the lock ID expires, the vault automatically exits the InProgress state, and the vault lock policy is removed from the vault. You call CompleteVaultLock to complete the vault locking process by setting the state of the vault lock to Locked.  After a vault lock is in the Locked state, you cannot initiate a new vault lock for the vault. You can abort the vault locking process by calling AbortVaultLock. You can get the state of the vault lock by calling GetVaultLock. For more information about the vault locking process, Amazon Glacier Vault Lock. If this operation is called when the vault lock is in the InProgress state, the operation returns an AccessDeniedException error. When the vault lock is in the InProgress state you must call AbortVaultLock before you can initiate a new vault lock policy.

    Args:
        account_id(str): The AccountId value is the AWS account ID. This value must match the AWS account ID associated with the credentials used to sign the request. You can either specify an AWS account ID or optionally a single '-' (hyphen), in which case Amazon Glacier uses the AWS account ID associated with the credentials used to sign the request. If you specify your account ID, do not include any hyphens ('-') in the ID.
        vault_name(str): The name of the vault.
        policy(Dict, optional): The vault lock policy as a JSON string, which uses "\" as an escape character. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glacier.vault.lock.initiate(
                ctx, account_id=value, vault_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glacier.vault.lock.initiate account_id=value, vault_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glacier",
        operation="initiate_vault_lock",
        op_kwargs={"accountId": account_id, "vaultName": vault_name, "policy": policy},
    )
