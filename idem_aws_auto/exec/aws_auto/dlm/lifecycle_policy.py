"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    execution_role_arn: str,
    description: str,
    state: str,
    policy_details: Dict,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a policy to manage the lifecycle of the specified AWS resources. You can create up to 100 lifecycle policies.

    Args:
        execution_role_arn(str): The Amazon Resource Name (ARN) of the IAM role used to run the operations specified by the lifecycle policy.
        description(str): A description of the lifecycle policy. The characters ^[0-9A-Za-z _-]+$ are supported.
        state(str): The desired activation state of the lifecycle policy after creation.
        policy_details(Dict): The configuration details of the lifecycle policy.
        tags(Dict, optional): The tags to apply to the lifecycle policy during creation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dlm.lifecycle_policy.create(
                ctx, execution_role_arn=value, description=value, state=value, policy_details=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dlm.lifecycle_policy.create execution_role_arn=value, description=value, state=value, policy_details=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dlm",
        operation="create_lifecycle_policy",
        op_kwargs={
            "ExecutionRoleArn": execution_role_arn,
            "Description": description,
            "State": state,
            "PolicyDetails": policy_details,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, policy_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified lifecycle policy and halts the automated operations that the policy specified.

    Args:
        policy_id(str): The identifier of the lifecycle policy.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dlm.lifecycle_policy.delete(ctx, policy_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dlm.lifecycle_policy.delete policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dlm",
        operation="delete_lifecycle_policy",
        op_kwargs={"PolicyId": policy_id},
    )


async def get_all(
    hub,
    ctx,
    policy_ids: List = None,
    state: str = None,
    resource_types: List = None,
    target_tags: List = None,
    tags_to_add: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets summary information about all or the specified data lifecycle policies. To get complete information about a policy, use GetLifecyclePolicy.

    Args:
        policy_ids(List, optional): The identifiers of the data lifecycle policies. Defaults to None.
        state(str, optional): The activation state. Defaults to None.
        resource_types(List, optional): The resource type. Defaults to None.
        target_tags(List, optional): The target tag for a policy. Tags are strings in the format key=value. Defaults to None.
        tags_to_add(List, optional): The tags to add to objects created by the policy. Tags are strings in the format key=value. These user-defined tags are added in addition to the AWS-added lifecycle tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dlm.lifecycle_policy.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dlm.lifecycle_policy.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dlm",
        operation="get_lifecycle_policies",
        op_kwargs={
            "PolicyIds": policy_ids,
            "State": state,
            "ResourceTypes": resource_types,
            "TargetTags": target_tags,
            "TagsToAdd": tags_to_add,
        },
    )


async def get(hub, ctx, policy_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets detailed information about the specified lifecycle policy.

    Args:
        policy_id(str): The identifier of the lifecycle policy.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dlm.lifecycle_policy.get(ctx, policy_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dlm.lifecycle_policy.get policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dlm",
        operation="get_lifecycle_policy",
        op_kwargs={"PolicyId": policy_id},
    )


async def update(
    hub,
    ctx,
    policy_id: str,
    execution_role_arn: str = None,
    state: str = None,
    description: str = None,
    policy_details: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified lifecycle policy.

    Args:
        policy_id(str): The identifier of the lifecycle policy.
        execution_role_arn(str, optional): The Amazon Resource Name (ARN) of the IAM role used to run the operations specified by the lifecycle policy. Defaults to None.
        state(str, optional): The desired activation state of the lifecycle policy after creation. Defaults to None.
        description(str, optional): A description of the lifecycle policy. Defaults to None.
        policy_details(Dict, optional): The configuration of the lifecycle policy. You cannot update the policy type or the resource type. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dlm.lifecycle_policy.update(ctx, policy_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dlm.lifecycle_policy.update policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dlm",
        operation="update_lifecycle_policy",
        op_kwargs={
            "PolicyId": policy_id,
            "ExecutionRoleArn": execution_role_arn,
            "State": state,
            "Description": description,
            "PolicyDetails": policy_details,
        },
    )
