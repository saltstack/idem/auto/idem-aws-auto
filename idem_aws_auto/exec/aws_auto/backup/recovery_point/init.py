"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, backup_vault_name: str, recovery_point_arn: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the recovery point specified by a recovery point ID. If the recovery point ID belongs to a continuous backup, calling this endpoint deletes the existing continuous backup and stops future continuous backup.

    Args:
        backup_vault_name(str): The name of a logical container where backups are stored. Backup vaults are identified by names that are unique to the account used to create them and the AWS Region where they are created. They consist of lowercase letters, numbers, and hyphens.
        recovery_point_arn(str): An Amazon Resource Name (ARN) that uniquely identifies a recovery point; for example, arn:aws:backup:us-east-1:123456789012:recovery-point:1EB3B5E7-9EB0-435A-A80B-108B488B0D45.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.recovery_point.init.delete(
                ctx, backup_vault_name=value, recovery_point_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.recovery_point.init.delete backup_vault_name=value, recovery_point_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="delete_recovery_point",
        op_kwargs={
            "BackupVaultName": backup_vault_name,
            "RecoveryPointArn": recovery_point_arn,
        },
    )


async def describe(hub, ctx, backup_vault_name: str, recovery_point_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns metadata associated with a recovery point, including ID, status, encryption, and lifecycle.

    Args:
        backup_vault_name(str): The name of a logical container where backups are stored. Backup vaults are identified by names that are unique to the account used to create them and the AWS Region where they are created. They consist of lowercase letters, numbers, and hyphens.
        recovery_point_arn(str): An Amazon Resource Name (ARN) that uniquely identifies a recovery point; for example, arn:aws:backup:us-east-1:123456789012:recovery-point:1EB3B5E7-9EB0-435A-A80B-108B488B0D45.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.recovery_point.init.describe(
                ctx, backup_vault_name=value, recovery_point_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.recovery_point.init.describe backup_vault_name=value, recovery_point_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="describe_recovery_point",
        op_kwargs={
            "BackupVaultName": backup_vault_name,
            "RecoveryPointArn": recovery_point_arn,
        },
    )


async def disassociate(
    hub, ctx, backup_vault_name: str, recovery_point_arn: str
) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified continuous backup recovery point from AWS Backup and releases control of that continuous backup to the source service, such as Amazon RDS. The source service will continue to create and retain continuous backups using the lifecycle that you specified in your original backup plan. Does not support snapshot backup recovery points.

    Args:
        backup_vault_name(str): The unique name of an AWS Backup vault. Required.
        recovery_point_arn(str): An Amazon Resource Name (ARN) that uniquely identifies an AWS Backup recovery point. Required.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.recovery_point.init.disassociate(
                ctx, backup_vault_name=value, recovery_point_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.recovery_point.init.disassociate backup_vault_name=value, recovery_point_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="disassociate_recovery_point",
        op_kwargs={
            "BackupVaultName": backup_vault_name,
            "RecoveryPointArn": recovery_point_arn,
        },
    )
