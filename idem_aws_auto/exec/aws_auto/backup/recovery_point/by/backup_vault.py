"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    backup_vault_name: str,
    next_token: str = None,
    max_results: int = None,
    by_resource_arn: str = None,
    by_resource_type: str = None,
    by_backup_plan_id: str = None,
    by_created_before: str = None,
    by_created_after: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns detailed information about the recovery points stored in a backup vault.

    Args:
        backup_vault_name(str): The name of a logical container where backups are stored. Backup vaults are identified by names that are unique to the account used to create them and the AWS Region where they are created. They consist of lowercase letters, numbers, and hyphens.
        next_token(str, optional): The next item following a partial list of returned items. For example, if a request is made to return maxResults number of items, NextToken allows you to return more items in your list starting at the location pointed to by the next token. Defaults to None.
        max_results(int, optional): The maximum number of items to be returned. Defaults to None.
        by_resource_arn(str, optional): Returns only recovery points that match the specified resource Amazon Resource Name (ARN). Defaults to None.
        by_resource_type(str, optional): Returns only recovery points that match the specified resource type. Defaults to None.
        by_backup_plan_id(str, optional): Returns only recovery points that match the specified backup plan ID. Defaults to None.
        by_created_before(str, optional): Returns only recovery points that were created before the specified timestamp. Defaults to None.
        by_created_after(str, optional): Returns only recovery points that were created after the specified timestamp. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.recovery_point.by.backup_vault.list(
                ctx, backup_vault_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.recovery_point.by.backup_vault.list backup_vault_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="list_recovery_points_by_backup_vault",
        op_kwargs={
            "BackupVaultName": backup_vault_name,
            "NextToken": next_token,
            "MaxResults": max_results,
            "ByResourceArn": by_resource_arn,
            "ByResourceType": by_resource_type,
            "ByBackupPlanId": by_backup_plan_id,
            "ByCreatedBefore": by_created_before,
            "ByCreatedAfter": by_created_after,
        },
    )
