"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(hub, ctx, restore_job_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns metadata associated with a restore job that is specified by a job ID.

    Args:
        restore_job_id(str): Uniquely identifies the job that restores a recovery point.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.restore_job.describe(ctx, restore_job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.restore_job.describe restore_job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="describe_restore_job",
        op_kwargs={"RestoreJobId": restore_job_id},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    by_account_id: str = None,
    by_created_before: str = None,
    by_created_after: str = None,
    by_status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of jobs that AWS Backup initiated to restore a saved resource, including metadata about the recovery process.

    Args:
        next_token(str, optional): The next item following a partial list of returned items. For example, if a request is made to return maxResults number of items, NextToken allows you to return more items in your list starting at the location pointed to by the next token. Defaults to None.
        max_results(int, optional): The maximum number of items to be returned. Defaults to None.
        by_account_id(str, optional): The account ID to list the jobs from. Returns only restore jobs associated with the specified account ID. Defaults to None.
        by_created_before(str, optional): Returns only restore jobs that were created before the specified date. Defaults to None.
        by_created_after(str, optional): Returns only restore jobs that were created after the specified date. Defaults to None.
        by_status(str, optional): Returns only restore jobs associated with the specified job status. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.restore_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.restore_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="list_restore_jobs",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "ByAccountId": by_account_id,
            "ByCreatedBefore": by_created_before,
            "ByCreatedAfter": by_created_after,
            "ByStatus": by_status,
        },
    )


async def start(
    hub,
    ctx,
    recovery_point_arn: str,
    metadata: Dict,
    iam_role_arn: str,
    idempotency_token: str = None,
    resource_type: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Recovers the saved resource identified by an Amazon Resource Name (ARN).

    Args:
        recovery_point_arn(str): An ARN that uniquely identifies a recovery point; for example, arn:aws:backup:us-east-1:123456789012:recovery-point:1EB3B5E7-9EB0-435A-A80B-108B488B0D45.
        metadata(Dict): A set of metadata key-value pairs. Contains information, such as a resource name, required to restore a recovery point.  You can get configuration metadata about a resource at the time it was backed up by calling GetRecoveryPointRestoreMetadata. However, values in addition to those provided by GetRecoveryPointRestoreMetadata might be required to restore a resource. For example, you might need to provide a new resource name if the original already exists. You need to specify specific metadata to restore an Amazon Elastic File System (Amazon EFS) instance:    file-system-id: The ID of the Amazon EFS file system that is backed up by AWS Backup. Returned in GetRecoveryPointRestoreMetadata.    Encrypted: A Boolean value that, if true, specifies that the file system is encrypted. If KmsKeyId is specified, Encrypted must be set to true.    KmsKeyId: Specifies the AWS KMS key that is used to encrypt the restored file system. You can specify a key from another AWS account provided that key it is properly shared with your account via AWS KMS.    PerformanceMode: Specifies the throughput mode of the file system.    CreationToken: A user-supplied value that ensures the uniqueness (idempotency) of the request.    newFileSystem: A Boolean value that, if true, specifies that the recovery point is restored to a new Amazon EFS file system.    ItemsToRestore : An array of one to five strings where each string is a file path. Use ItemsToRestore to restore specific files or directories rather than the entire file system. This parameter is optional. For example, "itemsToRestore":"[\"/my.test\"]".  .
        iam_role_arn(str): The Amazon Resource Name (ARN) of the IAM role that AWS Backup uses to create the target recovery point; for example, arn:aws:iam::123456789012:role/S3Access.
        idempotency_token(str, optional): A customer chosen string that can be used to distinguish between calls to StartRestoreJob. Defaults to None.
        resource_type(str, optional): Starts a job to restore a recovery point for one of the following resources:    DynamoDB for Amazon DynamoDB    EBS for Amazon Elastic Block Store    EC2 for Amazon Elastic Compute Cloud    EFS for Amazon Elastic File System    RDS for Amazon Relational Database Service    Aurora for Amazon Aurora    Storage Gateway for AWS Storage Gateway  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.restore_job.start(
                ctx, recovery_point_arn=value, metadata=value, iam_role_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.restore_job.start recovery_point_arn=value, metadata=value, iam_role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="start_restore_job",
        op_kwargs={
            "RecoveryPointArn": recovery_point_arn,
            "Metadata": metadata,
            "IamRoleArn": iam_role_arn,
            "IdempotencyToken": idempotency_token,
            "ResourceType": resource_type,
        },
    )
