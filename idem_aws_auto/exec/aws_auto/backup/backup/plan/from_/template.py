"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, backup_plan_template_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the template specified by its templateId as a backup plan.

    Args:
        backup_plan_template_id(str): Uniquely identifies a stored backup plan template.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.backup.backup.plan.from_.template.get(
                ctx, backup_plan_template_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.backup.backup.plan.from_.template.get backup_plan_template_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="backup",
        operation="get_backup_plan_from_template",
        op_kwargs={"BackupPlanTemplateId": backup_plan_template_id},
    )
