"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def upgrade(
    hub, ctx, published_schema_arn: str, directory_arn: str, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Upgrades a single directory in-place using the PublishedSchemaArn with schema updates found in MinorVersion. Backwards-compatible minor version upgrades are instantaneously available for readers on all objects in the directory. Note: This is a synchronous API call and upgrades only one schema on a given directory per call. To upgrade multiple directories from one schema, you would need to call this API on each directory.

    Args:
        published_schema_arn(str): The revision of the published schema to upgrade the directory to.
        directory_arn(str): The ARN for the directory to which the upgraded schema will be applied.
        dry_run(bool, optional): Used for testing whether the major version schemas are backward compatible or not. If schema compatibility fails, an exception would be thrown else the call would succeed but no changes will be saved. This parameter is optional. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.applied_schema.init.upgrade(
                ctx, published_schema_arn=value, directory_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.applied_schema.init.upgrade published_schema_arn=value, directory_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="upgrade_applied_schema",
        op_kwargs={
            "PublishedSchemaArn": published_schema_arn,
            "DirectoryArn": directory_arn,
            "DryRun": dry_run,
        },
    )
