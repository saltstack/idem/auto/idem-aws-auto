"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, schema_arn: str = None, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the major version families of each managed schema. If a major version ARN is provided as SchemaArn, the minor version revisions in that family are listed instead.

    Args:
        schema_arn(str, optional): The response for ListManagedSchemaArns. When this parameter is used, all minor version ARNs for a major version are listed. Defaults to None.
        next_token(str, optional): The pagination token. Defaults to None.
        max_results(int, optional): The maximum number of results to retrieve. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.managed_schema_arn.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.managed_schema_arn.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="list_managed_schema_arns",
        op_kwargs={
            "SchemaArn": schema_arn,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
