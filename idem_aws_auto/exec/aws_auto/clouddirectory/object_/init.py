"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def attach(
    hub,
    ctx,
    directory_arn: str,
    parent_reference: Dict,
    child_reference: Dict,
    link_name: str,
) -> Dict:
    r"""
    **Autogenerated function**

    Attaches an existing object to another object. An object can be accessed in two ways:   Using the path   Using ObjectIdentifier

    Args:
        directory_arn(str): Amazon Resource Name (ARN) that is associated with the Directory where both objects reside. For more information, see arns.
        parent_reference(Dict): The parent object reference.
        child_reference(Dict): The child object reference to be attached to the object.
        link_name(str): The link name with which the child object is attached to the parent.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.object_.init.attach(
                ctx,
                directory_arn=value,
                parent_reference=value,
                child_reference=value,
                link_name=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.object_.init.attach directory_arn=value, parent_reference=value, child_reference=value, link_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="attach_object",
        op_kwargs={
            "DirectoryArn": directory_arn,
            "ParentReference": parent_reference,
            "ChildReference": child_reference,
            "LinkName": link_name,
        },
    )


async def create(
    hub,
    ctx,
    directory_arn: str,
    schema_facets: List,
    object_attribute_list: List = None,
    parent_reference: Dict = None,
    link_name: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an object in a Directory. Additionally attaches the object to a parent, if a parent reference and LinkName is specified. An object is simply a collection of Facet attributes. You can also use this API call to create a policy object, if the facet from which you create the object is a policy facet.

    Args:
        directory_arn(str): The Amazon Resource Name (ARN) that is associated with the Directory in which the object will be created. For more information, see arns.
        schema_facets(List): A list of schema facets to be associated with the object. Do not provide minor version components. See SchemaFacet for details.
        object_attribute_list(List, optional): The attribute map whose attribute ARN contains the key and attribute value as the map value. Defaults to None.
        parent_reference(Dict, optional): If specified, the parent reference to which this object will be attached. Defaults to None.
        link_name(str, optional): The name of link that is used to attach this object to a parent. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.object_.init.create(
                ctx, directory_arn=value, schema_facets=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.object_.init.create directory_arn=value, schema_facets=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="create_object",
        op_kwargs={
            "DirectoryArn": directory_arn,
            "SchemaFacets": schema_facets,
            "ObjectAttributeList": object_attribute_list,
            "ParentReference": parent_reference,
            "LinkName": link_name,
        },
    )


async def delete(hub, ctx, directory_arn: str, object_reference: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an object and its associated attributes. Only objects with no children and no parents can be deleted. The maximum number of attributes that can be deleted during an object deletion is 30. For more information, see Amazon Cloud Directory Limits.

    Args:
        directory_arn(str): The Amazon Resource Name (ARN) that is associated with the Directory where the object resides. For more information, see arns.
        object_reference(Dict): A reference that identifies the object.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.object_.init.delete(
                ctx, directory_arn=value, object_reference=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.object_.init.delete directory_arn=value, object_reference=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="delete_object",
        op_kwargs={"DirectoryArn": directory_arn, "ObjectReference": object_reference},
    )


async def detach(
    hub, ctx, directory_arn: str, parent_reference: Dict, link_name: str
) -> Dict:
    r"""
    **Autogenerated function**

    Detaches a given object from the parent object. The object that is to be detached from the parent is specified by the link name.

    Args:
        directory_arn(str): The Amazon Resource Name (ARN) that is associated with the Directory where objects reside. For more information, see arns.
        parent_reference(Dict): The parent reference from which the object with the specified link name is detached.
        link_name(str): The link name associated with the object that needs to be detached.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.object_.init.detach(
                ctx, directory_arn=value, parent_reference=value, link_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.object_.init.detach directory_arn=value, parent_reference=value, link_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="detach_object",
        op_kwargs={
            "DirectoryArn": directory_arn,
            "ParentReference": parent_reference,
            "LinkName": link_name,
        },
    )
