"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    directory_arn: str,
    object_reference: Dict,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves all available parent paths for any object type such as node, leaf node, policy node, and index node objects. For more information about objects, see Directory Structure. Use this API to evaluate all parents for an object. The call returns all objects from the root of the directory up to the requested object. The API returns the number of paths based on user-defined MaxResults, in case there are multiple paths to the parent. The order of the paths and nodes returned is consistent among multiple API calls unless the objects are deleted or moved. Paths not leading to the directory root are ignored from the target object.

    Args:
        directory_arn(str): The ARN of the directory to which the parent path applies.
        object_reference(Dict): The reference that identifies the object whose parent paths are listed.
        next_token(str, optional): The pagination token. Defaults to None.
        max_results(int, optional): The maximum number of items to be retrieved in a single call. This is an approximate number. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.clouddirectory.object_.parent.path.list_all(
                ctx, directory_arn=value, object_reference=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.clouddirectory.object_.parent.path.list_all directory_arn=value, object_reference=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="clouddirectory",
        operation="list_object_parent_paths",
        op_kwargs={
            "DirectoryArn": directory_arn,
            "ObjectReference": object_reference,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
