"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(hub, ctx, alarm_model_name: str, key_value: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about an alarm.

    Args:
        alarm_model_name(str): The name of the alarm model.
        key_value(str, optional): The value of the key used as a filter to select only the alarms associated with the key. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotevent_data.alarm.describe(ctx, alarm_model_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotevent_data.alarm.describe alarm_model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotevents-data",
        operation="describe_alarm",
        op_kwargs={"alarmModelName": alarm_model_name, "keyValue": key_value},
    )


async def list_all(
    hub, ctx, alarm_model_name: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists one or more alarms. The operation returns only the metadata associated with each alarm.

    Args:
        alarm_model_name(str): The name of the alarm model.
        next_token(str, optional): The token that you can use to return the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to be returned per request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotevent_data.alarm.list_all(ctx, alarm_model_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotevent_data.alarm.list_all alarm_model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotevents-data",
        operation="list_alarms",
        op_kwargs={
            "alarmModelName": alarm_model_name,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
