"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def count(
    hub,
    ctx,
    domain: str,
    start_time_filter: Dict = None,
    close_time_filter: Dict = None,
    execution_filter: Dict = None,
    type_filter: Dict = None,
    tag_filter: Dict = None,
    close_status_filter: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the number of closed workflow executions within the given domain that meet the specified filtering criteria.  This operation is eventually consistent. The results are best effort and may not exactly reflect recent updates and changes.   Access Control  You can use IAM policies to control this action's access to Amazon SWF resources as follows:   Use a Resource element with the domain name to limit the action to only specified domains.   Use an Action element to allow or deny permission to call this action.   Constrain the following parameters by using a Condition element with the appropriate keys.    tagFilter.tag: String constraint. The key is swf:tagFilter.tag.    typeFilter.name: String constraint. The key is swf:typeFilter.name.    typeFilter.version: String constraint. The key is swf:typeFilter.version.     If the caller doesn't have sufficient permissions to invoke the action, or the parameter values fall outside the specified constraints, the action fails. The associated event attribute's cause parameter is set to OPERATION_NOT_PERMITTED. For details and example IAM policies, see Using IAM to Manage Access to Amazon SWF Workflows in the Amazon SWF Developer Guide.

    Args:
        domain(str): The name of the domain containing the workflow executions to count.
        start_time_filter(Dict, optional): If specified, only workflow executions that meet the start time criteria of the filter are counted.   startTimeFilter and closeTimeFilter are mutually exclusive. You must specify one of these in a request but not both. . Defaults to None.
        close_time_filter(Dict, optional): If specified, only workflow executions that meet the close time criteria of the filter are counted.   startTimeFilter and closeTimeFilter are mutually exclusive. You must specify one of these in a request but not both. . Defaults to None.
        execution_filter(Dict, optional): If specified, only workflow executions matching the WorkflowId in the filter are counted.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        type_filter(Dict, optional): If specified, indicates the type of the workflow executions to be counted.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        tag_filter(Dict, optional): If specified, only executions that have a tag that matches the filter are counted.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        close_status_filter(Dict, optional): If specified, only workflow executions that match this close status are counted. This filter has an affect only if executionStatus is specified as CLOSED.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.swf.closed_workflow_execution.count(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.swf.closed_workflow_execution.count domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="swf",
        operation="count_closed_workflow_executions",
        op_kwargs={
            "domain": domain,
            "startTimeFilter": start_time_filter,
            "closeTimeFilter": close_time_filter,
            "executionFilter": execution_filter,
            "typeFilter": type_filter,
            "tagFilter": tag_filter,
            "closeStatusFilter": close_status_filter,
        },
    )


async def list_all(
    hub,
    ctx,
    domain: str,
    start_time_filter: Dict = None,
    close_time_filter: Dict = None,
    execution_filter: Dict = None,
    close_status_filter: Dict = None,
    type_filter: Dict = None,
    tag_filter: Dict = None,
    next_page_token: str = None,
    maximum_page_size: int = None,
    reverse_order: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of closed workflow executions in the specified domain that meet the filtering criteria. The results may be split into multiple pages. To retrieve subsequent pages, make the call again using the nextPageToken returned by the initial call.  This operation is eventually consistent. The results are best effort and may not exactly reflect recent updates and changes.   Access Control  You can use IAM policies to control this action's access to Amazon SWF resources as follows:   Use a Resource element with the domain name to limit the action to only specified domains.   Use an Action element to allow or deny permission to call this action.   Constrain the following parameters by using a Condition element with the appropriate keys.    tagFilter.tag: String constraint. The key is swf:tagFilter.tag.    typeFilter.name: String constraint. The key is swf:typeFilter.name.    typeFilter.version: String constraint. The key is swf:typeFilter.version.     If the caller doesn't have sufficient permissions to invoke the action, or the parameter values fall outside the specified constraints, the action fails. The associated event attribute's cause parameter is set to OPERATION_NOT_PERMITTED. For details and example IAM policies, see Using IAM to Manage Access to Amazon SWF Workflows in the Amazon SWF Developer Guide.

    Args:
        domain(str): The name of the domain that contains the workflow executions to list.
        start_time_filter(Dict, optional): If specified, the workflow executions are included in the returned results based on whether their start times are within the range specified by this filter. Also, if this parameter is specified, the returned results are ordered by their start times.   startTimeFilter and closeTimeFilter are mutually exclusive. You must specify one of these in a request but not both. . Defaults to None.
        close_time_filter(Dict, optional): If specified, the workflow executions are included in the returned results based on whether their close times are within the range specified by this filter. Also, if this parameter is specified, the returned results are ordered by their close times.   startTimeFilter and closeTimeFilter are mutually exclusive. You must specify one of these in a request but not both. . Defaults to None.
        execution_filter(Dict, optional): If specified, only workflow executions matching the workflow ID specified in the filter are returned.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        close_status_filter(Dict, optional): If specified, only workflow executions that match this close status are listed. For example, if TERMINATED is specified, then only TERMINATED workflow executions are listed.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        type_filter(Dict, optional): If specified, only executions of the type specified in the filter are returned.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        tag_filter(Dict, optional): If specified, only executions that have the matching tag are listed.   closeStatusFilter, executionFilter, typeFilter and tagFilter are mutually exclusive. You can specify at most one of these in a request. . Defaults to None.
        next_page_token(str, optional): If NextPageToken is returned there are more results available. The value of NextPageToken is a unique pagination token for each page. Make the call again using the returned token to retrieve the next page. Keep all other arguments unchanged. Each pagination token expires after 60 seconds. Using an expired pagination token will return a 400 error: "Specified token has exceeded its maximum lifetime".  The configured maximumPageSize determines how many results can be returned in a single call. . Defaults to None.
        maximum_page_size(int, optional): The maximum number of results that are returned per call. Use nextPageToken to obtain further pages of results. . Defaults to None.
        reverse_order(bool, optional): When set to true, returns the results in reverse order. By default the results are returned in descending order of the start or the close time of the executions. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.swf.closed_workflow_execution.list_all(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.swf.closed_workflow_execution.list_all domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="swf",
        operation="list_closed_workflow_executions",
        op_kwargs={
            "domain": domain,
            "startTimeFilter": start_time_filter,
            "closeTimeFilter": close_time_filter,
            "executionFilter": execution_filter,
            "closeStatusFilter": close_status_filter,
            "typeFilter": type_filter,
            "tagFilter": tag_filter,
            "nextPageToken": next_page_token,
            "maximumPageSize": maximum_page_size,
            "reverseOrder": reverse_order,
        },
    )
