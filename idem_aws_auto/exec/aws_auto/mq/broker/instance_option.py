"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(
    hub,
    ctx,
    engine_type: str = None,
    host_instance_type: str = None,
    max_results: int = None,
    next_token: str = None,
    storage_type: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describe available broker instance options.

    Args:
        engine_type(str, optional): Filter response by engine type. Defaults to None.
        host_instance_type(str, optional): Filter response by host instance type. Defaults to None.
        max_results(int, optional): The maximum number of brokers that Amazon MQ can return per page (20 by default). This value must be an integer from 5 to 100. Defaults to None.
        next_token(str, optional): The token that specifies the next page of results Amazon MQ should return. To request the first page, leave nextToken empty. Defaults to None.
        storage_type(str, optional): Filter response by storage type. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mq.broker.instance_option.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mq.broker.instance_option.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mq",
        operation="describe_broker_instance_options",
        op_kwargs={
            "EngineType": engine_type,
            "HostInstanceType": host_instance_type,
            "MaxResults": max_results,
            "NextToken": next_token,
            "StorageType": storage_type,
        },
    )
