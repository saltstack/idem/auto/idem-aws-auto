"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    engine_type: str,
    engine_version: str,
    name: str,
    authentication_strategy: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new configuration for the specified configuration name. Amazon MQ uses the default configuration (the engine type and version).

    Args:
        authentication_strategy(str, optional): Optional. The authentication strategy associated with the configuration. The default is SIMPLE. Defaults to None.
        engine_type(str): Required. The type of broker engine. Currently, Amazon MQ supports ACTIVEMQ and RABBITMQ.
        engine_version(str): Required. The broker engine's version. For a list of supported engine versions, see Supported engines.
        name(str): Required. The name of the configuration. This value can contain only alphanumeric characters, dashes, periods, underscores, and tildes (- . _ ~). This value must be 1-150 characters long.
        tags(Dict, optional): Create tags when creating the configuration. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mq.configuration.init.create(
                ctx, engine_type=value, engine_version=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mq.configuration.init.create engine_type=value, engine_version=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mq",
        operation="create_configuration",
        op_kwargs={
            "AuthenticationStrategy": authentication_strategy,
            "EngineType": engine_type,
            "EngineVersion": engine_version,
            "Name": name,
            "Tags": tags,
        },
    )


async def describe(hub, ctx, configuration_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the specified configuration.

    Args:
        configuration_id(str): The unique ID that Amazon MQ generates for the configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mq.configuration.init.describe(ctx, configuration_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mq.configuration.init.describe configuration_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mq",
        operation="describe_configuration",
        op_kwargs={"ConfigurationId": configuration_id},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of all configurations.

    Args:
        max_results(int, optional): The maximum number of brokers that Amazon MQ can return per page (20 by default). This value must be an integer from 5 to 100. Defaults to None.
        next_token(str, optional): The token that specifies the next page of results Amazon MQ should return. To request the first page, leave nextToken empty. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mq.configuration.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mq.configuration.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mq",
        operation="list_configurations",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(
    hub, ctx, configuration_id: str, data: str, description: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified configuration.

    Args:
        configuration_id(str): The unique ID that Amazon MQ generates for the configuration.
        data(str): Required. The base64-encoded XML configuration.
        description(str, optional): The description of the configuration. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mq.configuration.init.update(
                ctx, configuration_id=value, data=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mq.configuration.init.update configuration_id=value, data=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mq",
        operation="update_configuration",
        op_kwargs={
            "ConfigurationId": configuration_id,
            "Data": data,
            "Description": description,
        },
    )
