"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, user_pool_id: str, identifier: str, name: str, scopes: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new OAuth2.0 resource server and defines custom scopes in it.

    Args:
        user_pool_id(str): The user pool ID for the user pool.
        identifier(str): A unique resource server identifier for the resource server. This could be an HTTPS endpoint where the resource server is located. For example, https://my-weather-api.example.com.
        name(str): A friendly name for the resource server.
        scopes(List, optional): A list of scopes. Each scope is map, where the keys are name and description. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.resource.server.create(
                ctx, user_pool_id=value, identifier=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.resource.server.create user_pool_id=value, identifier=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="create_resource_server",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "Identifier": identifier,
            "Name": name,
            "Scopes": scopes,
        },
    )


async def delete(hub, ctx, user_pool_id: str, identifier: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a resource server.

    Args:
        user_pool_id(str): The user pool ID for the user pool that hosts the resource server.
        identifier(str): The identifier for the resource server.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.resource.server.delete(
                ctx, user_pool_id=value, identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.resource.server.delete user_pool_id=value, identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="delete_resource_server",
        op_kwargs={"UserPoolId": user_pool_id, "Identifier": identifier},
    )


async def describe(hub, ctx, user_pool_id: str, identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes a resource server.

    Args:
        user_pool_id(str): The user pool ID for the user pool that hosts the resource server.
        identifier(str): The identifier for the resource server.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.resource.server.describe(
                ctx, user_pool_id=value, identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.resource.server.describe user_pool_id=value, identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="describe_resource_server",
        op_kwargs={"UserPoolId": user_pool_id, "Identifier": identifier},
    )


async def list_all(
    hub, ctx, user_pool_id: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the resource servers for a user pool.

    Args:
        user_pool_id(str): The user pool ID for the user pool.
        max_results(int, optional): The maximum number of resource servers to return. Defaults to None.
        next_token(str, optional): A pagination token. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.resource.server.list_all(ctx, user_pool_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.resource.server.list_all user_pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="list_resource_servers",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )


async def update(
    hub, ctx, user_pool_id: str, identifier: str, name: str, scopes: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the name and scopes of resource server. All other fields are read-only.  If you don't provide a value for an attribute, it will be set to the default value.

    Args:
        user_pool_id(str): The user pool ID for the user pool.
        identifier(str): The identifier for the resource server.
        name(str): The name of the resource server.
        scopes(List, optional): The scope values to be set for the resource server. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.resource.server.update(
                ctx, user_pool_id=value, identifier=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.resource.server.update user_pool_id=value, identifier=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="update_resource_server",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "Identifier": identifier,
            "Name": name,
            "Scopes": scopes,
        },
    )
