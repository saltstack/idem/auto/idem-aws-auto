"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    user_pool_id: str,
    group_name: str,
    limit: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the users in the specified group. Calling this action requires developer credentials.

    Args:
        user_pool_id(str): The user pool ID for the user pool.
        group_name(str): The name of the group.
        limit(int, optional): The limit of the request to list users. Defaults to None.
        next_token(str, optional): An identifier that was returned from the previous call to this operation, which can be used to return the next set of items in the list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.in_group.list(
                ctx, user_pool_id=value, group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.in_group.list user_pool_id=value, group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="list_users_in_group",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "GroupName": group_name,
            "Limit": limit,
            "NextToken": next_token,
        },
    )
