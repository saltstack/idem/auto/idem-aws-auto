"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    user_pool_id: str,
    client_name: str,
    generate_secret: bool = None,
    refresh_token_validity: int = None,
    access_token_validity: int = None,
    id_token_validity: int = None,
    token_validity_units: Dict = None,
    read_attributes: List = None,
    write_attributes: List = None,
    explicit_auth_flows: List = None,
    supported_identity_providers: List = None,
    callback_ur_ls: List = None,
    logout_ur_ls: List = None,
    default_redirect_uri: str = None,
    allowed_o_auth_flows: List = None,
    allowed_o_auth_scopes: List = None,
    allowed_o_auth_flows_user_pool_client: bool = None,
    analytics_configuration: Dict = None,
    prevent_user_existence_errors: str = None,
    enable_token_revocation: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates the user pool client. When you create a new user pool client, token revocation is automatically enabled. For more information about revoking tokens, see RevokeToken.

    Args:
        user_pool_id(str): The user pool ID for the user pool where you want to create a user pool client.
        client_name(str): The client name for the user pool client you would like to create.
        generate_secret(bool, optional): Boolean to specify whether you want to generate a secret for the user pool client being created. Defaults to None.
        refresh_token_validity(int, optional): The time limit, in days, after which the refresh token is no longer valid and cannot be used. Defaults to None.
        access_token_validity(int, optional): The time limit, between 5 minutes and 1 day, after which the access token is no longer valid and cannot be used. This value will be overridden if you have entered a value in TokenValidityUnits. Defaults to None.
        id_token_validity(int, optional): The time limit, between 5 minutes and 1 day, after which the ID token is no longer valid and cannot be used. This value will be overridden if you have entered a value in TokenValidityUnits. Defaults to None.
        token_validity_units(Dict, optional): The units in which the validity times are represented in. Default for RefreshToken is days, and default for ID and access tokens are hours. Defaults to None.
        read_attributes(List, optional): The read attributes. Defaults to None.
        write_attributes(List, optional): The user pool attributes that the app client can write to. If your app client allows users to sign in through an identity provider, this array must include all attributes that are mapped to identity provider attributes. Amazon Cognito updates mapped attributes when users sign in to your application through an identity provider. If your app client lacks write access to a mapped attribute, Amazon Cognito throws an error when it attempts to update the attribute. For more information, see Specifying Identity Provider Attribute Mappings for Your User Pool. Defaults to None.
        explicit_auth_flows(List, optional): The authentication flows that are supported by the user pool clients. Flow names without the ALLOW_ prefix are deprecated in favor of new names with the ALLOW_ prefix. Note that values with ALLOW_ prefix cannot be used along with values without ALLOW_ prefix. Valid values include:    ALLOW_ADMIN_USER_PASSWORD_AUTH: Enable admin based user password authentication flow ADMIN_USER_PASSWORD_AUTH. This setting replaces the ADMIN_NO_SRP_AUTH setting. With this authentication flow, Cognito receives the password in the request instead of using the SRP (Secure Remote Password protocol) protocol to verify passwords.    ALLOW_CUSTOM_AUTH: Enable Lambda trigger based authentication.    ALLOW_USER_PASSWORD_AUTH: Enable user password-based authentication. In this flow, Cognito receives the password in the request instead of using the SRP protocol to verify passwords.    ALLOW_USER_SRP_AUTH: Enable SRP based authentication.    ALLOW_REFRESH_TOKEN_AUTH: Enable authflow to refresh tokens.  . Defaults to None.
        supported_identity_providers(List, optional): A list of provider names for the identity providers that are supported on this client. The following are supported: COGNITO, Facebook, Google and LoginWithAmazon. Defaults to None.
        callback_ur_ls(List, optional): A list of allowed redirect (callback) URLs for the identity providers. A redirect URI must:   Be an absolute URI.   Be registered with the authorization server.   Not include a fragment component.   See OAuth 2.0 - Redirection Endpoint. Amazon Cognito requires HTTPS over HTTP except for http://localhost for testing purposes only. App callback URLs such as myapp://example are also supported. Defaults to None.
        logout_ur_ls(List, optional): A list of allowed logout URLs for the identity providers. Defaults to None.
        default_redirect_uri(str, optional): The default redirect URI. Must be in the CallbackURLs list. A redirect URI must:   Be an absolute URI.   Be registered with the authorization server.   Not include a fragment component.   See OAuth 2.0 - Redirection Endpoint. Amazon Cognito requires HTTPS over HTTP except for http://localhost for testing purposes only. App callback URLs such as myapp://example are also supported. Defaults to None.
        allowed_o_auth_flows(List, optional): The allowed OAuth flows. Set to code to initiate a code grant flow, which provides an authorization code as the response. This code can be exchanged for access tokens with the token endpoint. Set to implicit to specify that the client should get the access token (and, optionally, ID token, based on scopes) directly. Set to client_credentials to specify that the client should get the access token (and, optionally, ID token, based on scopes) from the token endpoint using a combination of client and client_secret. Defaults to None.
        allowed_o_auth_scopes(List, optional): The allowed OAuth scopes. Possible values provided by OAuth are: phone, email, openid, and profile. Possible values provided by Amazon Web Services are: aws.cognito.signin.user.admin. Custom scopes created in Resource Servers are also supported. Defaults to None.
        allowed_o_auth_flows_user_pool_client(bool, optional): Set to true if the client is allowed to follow the OAuth protocol when interacting with Cognito user pools. Defaults to None.
        analytics_configuration(Dict, optional): The Amazon Pinpoint analytics configuration for collecting metrics for this user pool.  In regions where Pinpoint is not available, Cognito User Pools only supports sending events to Amazon Pinpoint projects in us-east-1. In regions where Pinpoint is available, Cognito User Pools will support sending events to Amazon Pinpoint projects within that same region.  . Defaults to None.
        prevent_user_existence_errors(str, optional): Use this setting to choose which errors and responses are returned by Cognito APIs during authentication, account confirmation, and password recovery when the user does not exist in the user pool. When set to ENABLED and the user does not exist, authentication returns an error indicating either the username or password was incorrect, and account confirmation and password recovery return a response indicating a code was sent to a simulated destination. When set to LEGACY, those APIs will return a UserNotFoundException exception if the user does not exist in the user pool. Valid values include:    ENABLED - This prevents user existence-related errors.    LEGACY - This represents the old behavior of Cognito where user existence related errors are not prevented.    After February 15th 2020, the value of PreventUserExistenceErrors will default to ENABLED for newly created user pool clients if no value is provided. . Defaults to None.
        enable_token_revocation(bool, optional): Enables or disables token revocation. For more information about revoking tokens, see RevokeToken. If you don't include this parameter, token revocation is automatically enabled for the new user pool client. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.pool.client.create(
                ctx, user_pool_id=value, client_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.pool.client.create user_pool_id=value, client_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="create_user_pool_client",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "ClientName": client_name,
            "GenerateSecret": generate_secret,
            "RefreshTokenValidity": refresh_token_validity,
            "AccessTokenValidity": access_token_validity,
            "IdTokenValidity": id_token_validity,
            "TokenValidityUnits": token_validity_units,
            "ReadAttributes": read_attributes,
            "WriteAttributes": write_attributes,
            "ExplicitAuthFlows": explicit_auth_flows,
            "SupportedIdentityProviders": supported_identity_providers,
            "CallbackURLs": callback_ur_ls,
            "LogoutURLs": logout_ur_ls,
            "DefaultRedirectURI": default_redirect_uri,
            "AllowedOAuthFlows": allowed_o_auth_flows,
            "AllowedOAuthScopes": allowed_o_auth_scopes,
            "AllowedOAuthFlowsUserPoolClient": allowed_o_auth_flows_user_pool_client,
            "AnalyticsConfiguration": analytics_configuration,
            "PreventUserExistenceErrors": prevent_user_existence_errors,
            "EnableTokenRevocation": enable_token_revocation,
        },
    )


async def delete(hub, ctx, user_pool_id: str, client_id: str) -> None:
    r"""
    **Autogenerated function**

    Allows the developer to delete the user pool client.

    Args:
        user_pool_id(str): The user pool ID for the user pool where you want to delete the client.
        client_id(str): The app client ID of the app associated with the user pool.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.pool.client.delete(
                ctx, user_pool_id=value, client_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.pool.client.delete user_pool_id=value, client_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="delete_user_pool_client",
        op_kwargs={"UserPoolId": user_pool_id, "ClientId": client_id},
    )


async def describe(hub, ctx, user_pool_id: str, client_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Client method for returning the configuration information and metadata of the specified user pool app client.

    Args:
        user_pool_id(str): The user pool ID for the user pool you want to describe.
        client_id(str): The app client ID of the app associated with the user pool.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.pool.client.describe(
                ctx, user_pool_id=value, client_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.pool.client.describe user_pool_id=value, client_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="describe_user_pool_client",
        op_kwargs={"UserPoolId": user_pool_id, "ClientId": client_id},
    )


async def list_all(
    hub, ctx, user_pool_id: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the clients that have been created for the specified user pool.

    Args:
        user_pool_id(str): The user pool ID for the user pool where you want to list user pool clients.
        max_results(int, optional): The maximum number of results you want the request to return when listing the user pool clients. Defaults to None.
        next_token(str, optional): An identifier that was returned from the previous call to this operation, which can be used to return the next set of items in the list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.pool.client.list_all(ctx, user_pool_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.pool.client.list_all user_pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="list_user_pool_clients",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )


async def update(
    hub,
    ctx,
    user_pool_id: str,
    client_id: str,
    client_name: str = None,
    refresh_token_validity: int = None,
    access_token_validity: int = None,
    id_token_validity: int = None,
    token_validity_units: Dict = None,
    read_attributes: List = None,
    write_attributes: List = None,
    explicit_auth_flows: List = None,
    supported_identity_providers: List = None,
    callback_ur_ls: List = None,
    logout_ur_ls: List = None,
    default_redirect_uri: str = None,
    allowed_o_auth_flows: List = None,
    allowed_o_auth_scopes: List = None,
    allowed_o_auth_flows_user_pool_client: bool = None,
    analytics_configuration: Dict = None,
    prevent_user_existence_errors: str = None,
    enable_token_revocation: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified user pool app client with the specified attributes. You can get a list of the current user pool app client settings using DescribeUserPoolClient.  If you don't provide a value for an attribute, it will be set to the default value.  You can also use this operation to enable token revocation for user pool clients. For more information about revoking tokens, see RevokeToken.

    Args:
        user_pool_id(str): The user pool ID for the user pool where you want to update the user pool client.
        client_id(str): The ID of the client associated with the user pool.
        client_name(str, optional): The client name from the update user pool client request. Defaults to None.
        refresh_token_validity(int, optional): The time limit, in days, after which the refresh token is no longer valid and cannot be used. Defaults to None.
        access_token_validity(int, optional): The time limit, after which the access token is no longer valid and cannot be used. Defaults to None.
        id_token_validity(int, optional): The time limit, after which the ID token is no longer valid and cannot be used. Defaults to None.
        token_validity_units(Dict, optional): The units in which the validity times are represented in. Default for RefreshToken is days, and default for ID and access tokens are hours. Defaults to None.
        read_attributes(List, optional): The read-only attributes of the user pool. Defaults to None.
        write_attributes(List, optional): The writeable attributes of the user pool. Defaults to None.
        explicit_auth_flows(List, optional): The authentication flows that are supported by the user pool clients. Flow names without the ALLOW_ prefix are deprecated in favor of new names with the ALLOW_ prefix. Note that values with ALLOW_ prefix cannot be used along with values without ALLOW_ prefix. Valid values include:    ALLOW_ADMIN_USER_PASSWORD_AUTH: Enable admin based user password authentication flow ADMIN_USER_PASSWORD_AUTH. This setting replaces the ADMIN_NO_SRP_AUTH setting. With this authentication flow, Cognito receives the password in the request instead of using the SRP (Secure Remote Password protocol) protocol to verify passwords.    ALLOW_CUSTOM_AUTH: Enable Lambda trigger based authentication.    ALLOW_USER_PASSWORD_AUTH: Enable user password-based authentication. In this flow, Cognito receives the password in the request instead of using the SRP protocol to verify passwords.    ALLOW_USER_SRP_AUTH: Enable SRP based authentication.    ALLOW_REFRESH_TOKEN_AUTH: Enable authflow to refresh tokens.  . Defaults to None.
        supported_identity_providers(List, optional): A list of provider names for the identity providers that are supported on this client. Defaults to None.
        callback_ur_ls(List, optional): A list of allowed redirect (callback) URLs for the identity providers. A redirect URI must:   Be an absolute URI.   Be registered with the authorization server.   Not include a fragment component.   See OAuth 2.0 - Redirection Endpoint. Amazon Cognito requires HTTPS over HTTP except for http://localhost for testing purposes only. App callback URLs such as myapp://example are also supported. Defaults to None.
        logout_ur_ls(List, optional): A list of allowed logout URLs for the identity providers. Defaults to None.
        default_redirect_uri(str, optional): The default redirect URI. Must be in the CallbackURLs list. A redirect URI must:   Be an absolute URI.   Be registered with the authorization server.   Not include a fragment component.   See OAuth 2.0 - Redirection Endpoint. Amazon Cognito requires HTTPS over HTTP except for http://localhost for testing purposes only. App callback URLs such as myapp://example are also supported. Defaults to None.
        allowed_o_auth_flows(List, optional): The allowed OAuth flows. Set to code to initiate a code grant flow, which provides an authorization code as the response. This code can be exchanged for access tokens with the token endpoint. Set to implicit to specify that the client should get the access token (and, optionally, ID token, based on scopes) directly. Set to client_credentials to specify that the client should get the access token (and, optionally, ID token, based on scopes) from the token endpoint using a combination of client and client_secret. Defaults to None.
        allowed_o_auth_scopes(List, optional): The allowed OAuth scopes. Possible values provided by OAuth are: phone, email, openid, and profile. Possible values provided by Amazon Web Services are: aws.cognito.signin.user.admin. Custom scopes created in Resource Servers are also supported. Defaults to None.
        allowed_o_auth_flows_user_pool_client(bool, optional): Set to true if the client is allowed to follow the OAuth protocol when interacting with Cognito user pools. Defaults to None.
        analytics_configuration(Dict, optional): The Amazon Pinpoint analytics configuration for collecting metrics for this user pool.  In regions where Pinpoint is not available, Cognito User Pools only supports sending events to Amazon Pinpoint projects in us-east-1. In regions where Pinpoint is available, Cognito User Pools will support sending events to Amazon Pinpoint projects within that same region.  . Defaults to None.
        prevent_user_existence_errors(str, optional): Use this setting to choose which errors and responses are returned by Cognito APIs during authentication, account confirmation, and password recovery when the user does not exist in the user pool. When set to ENABLED and the user does not exist, authentication returns an error indicating either the username or password was incorrect, and account confirmation and password recovery return a response indicating a code was sent to a simulated destination. When set to LEGACY, those APIs will return a UserNotFoundException exception if the user does not exist in the user pool. Valid values include:    ENABLED - This prevents user existence-related errors.    LEGACY - This represents the old behavior of Cognito where user existence related errors are not prevented.    After February 15th 2020, the value of PreventUserExistenceErrors will default to ENABLED for newly created user pool clients if no value is provided. . Defaults to None.
        enable_token_revocation(bool, optional): Enables or disables token revocation. For more information about revoking tokens, see RevokeToken. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.user.pool.client.update(
                ctx, user_pool_id=value, client_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.user.pool.client.update user_pool_id=value, client_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="update_user_pool_client",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "ClientId": client_id,
            "ClientName": client_name,
            "RefreshTokenValidity": refresh_token_validity,
            "AccessTokenValidity": access_token_validity,
            "IdTokenValidity": id_token_validity,
            "TokenValidityUnits": token_validity_units,
            "ReadAttributes": read_attributes,
            "WriteAttributes": write_attributes,
            "ExplicitAuthFlows": explicit_auth_flows,
            "SupportedIdentityProviders": supported_identity_providers,
            "CallbackURLs": callback_ur_ls,
            "LogoutURLs": logout_ur_ls,
            "DefaultRedirectURI": default_redirect_uri,
            "AllowedOAuthFlows": allowed_o_auth_flows,
            "AllowedOAuthScopes": allowed_o_auth_scopes,
            "AllowedOAuthFlowsUserPoolClient": allowed_o_auth_flows_user_pool_client,
            "AnalyticsConfiguration": analytics_configuration,
            "PreventUserExistenceErrors": prevent_user_existence_errors,
            "EnableTokenRevocation": enable_token_revocation,
        },
    )
