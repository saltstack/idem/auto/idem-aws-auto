"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def admin(
    hub,
    ctx,
    user_pool_id: str,
    username: str,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists a history of user activity and any risks detected as part of Amazon Cognito advanced security.

    Args:
        user_pool_id(str): The user pool ID.
        username(str): The user pool username or an alias.
        max_results(int, optional): The maximum number of authentication events to return. Defaults to None.
        next_token(str, optional): A pagination token. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_idp.list_.user_auth_event.admin(
                ctx, user_pool_id=value, username=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_idp.list_.user_auth_event.admin user_pool_id=value, username=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-idp",
        operation="admin_list_user_auth_events",
        op_kwargs={
            "UserPoolId": user_pool_id,
            "Username": username,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
