"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def activate(hub, ctx, hosted_zone_id: str, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Activates a key-signing key (KSK) so that it can be used for signing by DNSSEC. This operation changes the KSK status to ACTIVE.

    Args:
        hosted_zone_id(str): A unique string used to identify a hosted zone.
        name(str): A string used to identify a key-signing key (KSK). Name can include numbers, letters, and underscores (_). Name must be unique for each key-signing key in the same hosted zone.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53.key_signing_key.activate(
                ctx, hosted_zone_id=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53.key_signing_key.activate hosted_zone_id=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53",
        operation="activate_key_signing_key",
        op_kwargs={"HostedZoneId": hosted_zone_id, "Name": name},
    )


async def create(
    hub,
    ctx,
    caller_reference: str,
    hosted_zone_id: str,
    key_management_service_arn: str,
    name: str,
    status: str,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new key-signing key (KSK) associated with a hosted zone. You can only have two KSKs per hosted zone.

    Args:
        caller_reference(str): A unique string that identifies the request.
        hosted_zone_id(str): The unique string (ID) used to identify a hosted zone.
        key_management_service_arn(str): The Amazon resource name (ARN) for a customer managed customer master key (CMK) in Key Management Service (KMS). The KeyManagementServiceArn must be unique for each key-signing key (KSK) in a single hosted zone. To see an example of KeyManagementServiceArn that grants the correct permissions for DNSSEC, scroll down to Example.  You must configure the customer managed CMK as follows:  Status  Enabled  Key spec  ECC_NIST_P256  Key usage  Sign and verify  Key policy  The key policy must give permission for the following actions:   DescribeKey   GetPublicKey   Sign   The key policy must also include the Amazon Route 53 service in the principal for your account. Specify the following:    "Service": "dnssec-route53.amazonaws.com"      For more information about working with a customer managed CMK in KMS, see Key Management Service concepts.
        name(str): A string used to identify a key-signing key (KSK). Name can include numbers, letters, and underscores (_). Name must be unique for each key-signing key in the same hosted zone.
        status(str): A string specifying the initial status of the key-signing key (KSK). You can set the value to ACTIVE or INACTIVE.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53.key_signing_key.create(
                ctx,
                caller_reference=value,
                hosted_zone_id=value,
                key_management_service_arn=value,
                name=value,
                status=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53.key_signing_key.create caller_reference=value, hosted_zone_id=value, key_management_service_arn=value, name=value, status=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53",
        operation="create_key_signing_key",
        op_kwargs={
            "CallerReference": caller_reference,
            "HostedZoneId": hosted_zone_id,
            "KeyManagementServiceArn": key_management_service_arn,
            "Name": name,
            "Status": status,
        },
    )


async def deactivate(hub, ctx, hosted_zone_id: str, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deactivates a key-signing key (KSK) so that it will not be used for signing by DNSSEC. This operation changes the KSK status to INACTIVE.

    Args:
        hosted_zone_id(str): A unique string used to identify a hosted zone.
        name(str): A string used to identify a key-signing key (KSK).

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53.key_signing_key.deactivate(
                ctx, hosted_zone_id=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53.key_signing_key.deactivate hosted_zone_id=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53",
        operation="deactivate_key_signing_key",
        op_kwargs={"HostedZoneId": hosted_zone_id, "Name": name},
    )


async def delete(hub, ctx, hosted_zone_id: str, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a key-signing key (KSK). Before you can delete a KSK, you must deactivate it. The KSK must be deactivated before you can delete it regardless of whether the hosted zone is enabled for DNSSEC signing.

    Args:
        hosted_zone_id(str): A unique string used to identify a hosted zone.
        name(str): A string used to identify a key-signing key (KSK).

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53.key_signing_key.delete(
                ctx, hosted_zone_id=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53.key_signing_key.delete hosted_zone_id=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53",
        operation="delete_key_signing_key",
        op_kwargs={"HostedZoneId": hosted_zone_id, "Name": name},
    )
