"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(
    hub, ctx, hosted_zone_id: str, vpc: Dict, comment: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Associates an Amazon VPC with a private hosted zone.   To perform the association, the VPC and the private hosted zone must already exist. You can't convert a public hosted zone into a private hosted zone.   If you want to associate a VPC that was created by using one account with a private hosted zone that was created by using a different account, the account that created the private hosted zone must first submit a CreateVPCAssociationAuthorization request. Then the account that created the VPC must submit an AssociateVPCWithHostedZone request.

    Args:
        hosted_zone_id(str): The ID of the private hosted zone that you want to associate an Amazon VPC with. Note that you can't associate a VPC with a hosted zone that doesn't have an existing VPC association.
        vpc(Dict): A complex type that contains information about the VPC that you want to associate with a private hosted zone.
        comment(str, optional):  Optional: A comment about the association request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53.vpc.with_hosted_zone.associate(
                ctx, hosted_zone_id=value, vpc=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53.vpc.with_hosted_zone.associate hosted_zone_id=value, vpc=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53",
        operation="associate_vpc_with_hosted_zone",
        op_kwargs={"HostedZoneId": hosted_zone_id, "VPC": vpc, "Comment": comment},
    )
