"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the Snow Family service limit for your account, and also the number of Snow devices your account has in use. The default service limit for the number of Snow devices that you can have at one time is 1. If you want to increase your service limit, contact AWS Support.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.snowball.snowball_usage.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.snowball.snowball_usage.get
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="snowball", operation="get_snowball_usage", op_kwargs={}
    )
