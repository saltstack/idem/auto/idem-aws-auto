"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx, job_id: str, shipping_option: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Creates a shipping label that will be used to return the Snow device to AWS.

    Args:
        job_id(str): The ID for a job that you want to create the return shipping label for; for example, JID123e4567-e89b-12d3-a456-426655440000.
        shipping_option(str, optional): The shipping speed for a particular job. This speed doesn't dictate how soon the device is returned to AWS. This speed represents how quickly it moves to its destination while in transit. Regional shipping speeds are as follows:. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.snowball.return_shipping_label.create(ctx, job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.snowball.return_shipping_label.create job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="snowball",
        operation="create_return_shipping_label",
        op_kwargs={"JobId": job_id, "ShippingOption": shipping_option},
    )


async def describe(hub, ctx, job_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Information on the shipping label of a Snow device that is being returned to AWS.

    Args:
        job_id(str): The automatically generated ID for a job, for example JID123e4567-e89b-12d3-a456-426655440000.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.snowball.return_shipping_label.describe(ctx, job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.snowball.return_shipping_label.describe job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="snowball",
        operation="describe_return_shipping_label",
        op_kwargs={"JobId": job_id},
    )
