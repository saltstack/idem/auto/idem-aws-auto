"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create_multiple(
    hub,
    ctx,
    graph_arn: str,
    accounts: List,
    message: str = None,
    disable_email_notification: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Sends a request to invite the specified AWS accounts to be member accounts in the behavior graph. This operation can only be called by the administrator account for a behavior graph.   CreateMembers verifies the accounts and then invites the verified accounts. The administrator can optionally specify to not send invitation emails to the member accounts. This would be used when the administrator manages their member accounts centrally. The request provides the behavior graph ARN and the list of accounts to invite. The response separates the requested accounts into two lists:   The accounts that CreateMembers was able to start the verification for. This list includes member accounts that are being verified, that have passed verification and are to be invited, and that have failed verification.   The accounts that CreateMembers was unable to process. This list includes accounts that were already invited to be member accounts in the behavior graph.

    Args:
        graph_arn(str): The ARN of the behavior graph to invite the member accounts to contribute their data to.
        message(str, optional): Customized message text to include in the invitation email message to the invited member accounts. Defaults to None.
        disable_email_notification(bool, optional): if set to true, then the member accounts do not receive email notifications. By default, this is set to false, and the member accounts receive email notifications. Defaults to None.
        accounts(List): The list of AWS accounts to invite to become member accounts in the behavior graph. You can invite up to 50 accounts at a time. For each invited account, the account list contains the account identifier and the AWS account root user email address.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.detective.member.create_multiple(
                ctx, graph_arn=value, accounts=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.detective.member.create_multiple graph_arn=value, accounts=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="detective",
        operation="create_members",
        op_kwargs={
            "GraphArn": graph_arn,
            "Message": message,
            "DisableEmailNotification": disable_email_notification,
            "Accounts": accounts,
        },
    )


async def delete_multiple(hub, ctx, graph_arn: str, account_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Deletes one or more member accounts from the administrator account's behavior graph. This operation can only be called by a Detective administrator account. That account cannot use DeleteMembers to delete their own account from the behavior graph. To disable a behavior graph, the administrator account uses the DeleteGraph API method.

    Args:
        graph_arn(str): The ARN of the behavior graph to delete members from.
        account_ids(List): The list of AWS account identifiers for the member accounts to delete from the behavior graph. You can delete up to 50 member accounts at a time.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.detective.member.delete_multiple(
                ctx, graph_arn=value, account_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.detective.member.delete_multiple graph_arn=value, account_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="detective",
        operation="delete_members",
        op_kwargs={"GraphArn": graph_arn, "AccountIds": account_ids},
    )


async def get_all(hub, ctx, graph_arn: str, account_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Returns the membership details for specified member accounts for a behavior graph.

    Args:
        graph_arn(str): The ARN of the behavior graph for which to request the member details.
        account_ids(List): The list of AWS account identifiers for the member account for which to return member details. You can request details for up to 50 member accounts at a time. You cannot use GetMembers to retrieve information about member accounts that were removed from the behavior graph.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.detective.member.get_all(
                ctx, graph_arn=value, account_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.detective.member.get_all graph_arn=value, account_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="detective",
        operation="get_members",
        op_kwargs={"GraphArn": graph_arn, "AccountIds": account_ids},
    )


async def list_all(
    hub, ctx, graph_arn: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the list of member accounts for a behavior graph. Does not return member accounts that were removed from the behavior graph.

    Args:
        graph_arn(str): The ARN of the behavior graph for which to retrieve the list of member accounts.
        next_token(str, optional): For requests to retrieve the next page of member account results, the pagination token that was returned with the previous page of results. The initial request does not include a pagination token. Defaults to None.
        max_results(int, optional): The maximum number of member accounts to include in the response. The total must be less than the overall limit on the number of results to return, which is currently 200. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.detective.member.list_all(ctx, graph_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.detective.member.list_all graph_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="detective",
        operation="list_members",
        op_kwargs={
            "GraphArn": graph_arn,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
