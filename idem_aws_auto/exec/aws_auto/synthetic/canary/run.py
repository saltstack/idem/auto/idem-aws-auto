"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(
    hub, ctx, name: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of runs for a specified canary.

    Args:
        name(str): The name of the canary that you want to see runs for.
        next_token(str, optional): A token that indicates that there is more data available. You can use this token in a subsequent GetCanaryRuns operation to retrieve the next set of results. Defaults to None.
        max_results(int, optional): Specify this parameter to limit how many runs are returned each time you use the GetCanaryRuns operation. If you omit this parameter, the default of 100 is used. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.synthetic.canary.run.get_all(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.synthetic.canary.run.get_all name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="synthetics",
        operation="get_canary_runs",
        op_kwargs={"Name": name, "NextToken": next_token, "MaxResults": max_results},
    )
