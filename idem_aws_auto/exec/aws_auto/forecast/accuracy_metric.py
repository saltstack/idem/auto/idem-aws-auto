"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, predictor_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Provides metrics on the accuracy of the models that were trained by the CreatePredictor operation. Use metrics to see how well the model performed and to decide whether to use the predictor to generate a forecast. For more information, see Predictor Metrics. This operation generates metrics for each backtest window that was evaluated. The number of backtest windows (NumberOfBacktestWindows) is specified using the EvaluationParameters object, which is optionally included in the CreatePredictor request. If NumberOfBacktestWindows isn't specified, the number defaults to one. The parameters of the filling method determine which items contribute to the metrics. If you want all items to contribute, specify zero. If you want only those items that have complete data in the range being evaluated to contribute, specify nan. For more information, see FeaturizationMethod.  Before you can get accuracy metrics, the Status of the predictor must be ACTIVE, signifying that training has completed. To get the status, use the DescribePredictor operation.

    Args:
        predictor_arn(str): The Amazon Resource Name (ARN) of the predictor to get metrics for.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.forecast.accuracy_metric.get_all(ctx, predictor_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.forecast.accuracy_metric.get_all predictor_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="forecast",
        operation="get_accuracy_metrics",
        op_kwargs={"PredictorArn": predictor_arn},
    )
