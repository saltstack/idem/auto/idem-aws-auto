"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    dataset_name: str,
    domain: str,
    dataset_type: str,
    schema: Dict,
    data_frequency: str = None,
    encryption_config: Dict = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an Amazon Forecast dataset. The information about the dataset that you provide helps Forecast understand how to consume the data for model training. This includes the following:     DataFrequency  - How frequently your historical time-series data is collected.     Domain  and  DatasetType  - Each dataset has an associated dataset domain and a type within the domain. Amazon Forecast provides a list of predefined domains and types within each domain. For each unique dataset domain and type within the domain, Amazon Forecast requires your data to include a minimum set of predefined fields.     Schema  - A schema specifies the fields in the dataset, including the field name and data type.   After creating a dataset, you import your training data into it and add the dataset to a dataset group. You use the dataset group to create a predictor. For more information, see howitworks-datasets-groups. To get a list of all your datasets, use the ListDatasets operation. For example Forecast datasets, see the Amazon Forecast Sample GitHub repository.  The Status of a dataset must be ACTIVE before you can import training data. Use the DescribeDataset operation to get the status.

    Args:
        dataset_name(str): A name for the dataset.
        domain(str): The domain associated with the dataset. When you add a dataset to a dataset group, this value and the value specified for the Domain parameter of the CreateDatasetGroup operation must match. The Domain and DatasetType that you choose determine the fields that must be present in the training data that you import to the dataset. For example, if you choose the RETAIL domain and TARGET_TIME_SERIES as the DatasetType, Amazon Forecast requires item_id, timestamp, and demand fields to be present in your data. For more information, see howitworks-datasets-groups.
        dataset_type(str): The dataset type. Valid values depend on the chosen Domain.
        data_frequency(str, optional): The frequency of data collection. This parameter is required for RELATED_TIME_SERIES datasets. Valid intervals are Y (Year), M (Month), W (Week), D (Day), H (Hour), 30min (30 minutes), 15min (15 minutes), 10min (10 minutes), 5min (5 minutes), and 1min (1 minute). For example, "D" indicates every day and "15min" indicates every 15 minutes. Defaults to None.
        schema(Dict): The schema for the dataset. The schema attributes and their order must match the fields in your data. The dataset Domain and DatasetType that you choose determine the minimum required fields in your training data. For information about the required fields for a specific dataset domain and type, see howitworks-domains-ds-types.
        encryption_config(Dict, optional): An AWS Key Management Service (KMS) key and the AWS Identity and Access Management (IAM) role that Amazon Forecast can assume to access the key. Defaults to None.
        tags(List, optional): The optional metadata that you apply to the dataset to help you categorize and organize them. Each tag consists of a key and an optional value, both of which you define. The following basic restrictions apply to tags:   Maximum number of tags per resource - 50.   For each resource, each tag key must be unique, and each tag key can have only one value.   Maximum key length - 128 Unicode characters in UTF-8.   Maximum value length - 256 Unicode characters in UTF-8.   If your tagging schema is used across multiple services and resources, remember that other services may have restrictions on allowed characters. Generally allowed characters are: letters, numbers, and spaces representable in UTF-8, and the following characters: + - = . _ : / @.   Tag keys and values are case sensitive.   Do not use aws:, AWS:, or any upper or lowercase combination of such as a prefix for keys as it is reserved for AWS use. You cannot edit or delete tag keys with this prefix. Values can have this prefix. If a tag value has aws as its prefix but the key does not, then Forecast considers it to be a user tag and will count against the limit of 50 tags. Tags with only the key prefix of aws do not count against your tags per resource limit.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.forecast.dataset.init.create(
                ctx, dataset_name=value, domain=value, dataset_type=value, schema=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.forecast.dataset.init.create dataset_name=value, domain=value, dataset_type=value, schema=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="forecast",
        operation="create_dataset",
        op_kwargs={
            "DatasetName": dataset_name,
            "Domain": domain,
            "DatasetType": dataset_type,
            "DataFrequency": data_frequency,
            "Schema": schema,
            "EncryptionConfig": encryption_config,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, dataset_arn: str) -> None:
    r"""
    **Autogenerated function**

    Deletes an Amazon Forecast dataset that was created using the CreateDataset operation. You can only delete datasets that have a status of ACTIVE or CREATE_FAILED. To get the status use the DescribeDataset operation.  Forecast does not automatically update any dataset groups that contain the deleted dataset. In order to update the dataset group, use the operation, omitting the deleted dataset's ARN.

    Args:
        dataset_arn(str): The Amazon Resource Name (ARN) of the dataset to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.forecast.dataset.init.delete(ctx, dataset_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.forecast.dataset.init.delete dataset_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="forecast",
        operation="delete_dataset",
        op_kwargs={"DatasetArn": dataset_arn},
    )


async def describe(hub, ctx, dataset_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes an Amazon Forecast dataset created using the CreateDataset operation. In addition to listing the parameters specified in the CreateDataset request, this operation includes the following dataset properties:    CreationTime     LastModificationTime     Status

    Args:
        dataset_arn(str): The Amazon Resource Name (ARN) of the dataset.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.forecast.dataset.init.describe(ctx, dataset_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.forecast.dataset.init.describe dataset_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="forecast",
        operation="describe_dataset",
        op_kwargs={"DatasetArn": dataset_arn},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of datasets created using the CreateDataset operation. For each dataset, a summary of its properties, including its Amazon Resource Name (ARN), is returned. To retrieve the complete set of properties, use the ARN with the DescribeDataset operation.

    Args:
        next_token(str, optional): If the result of the previous request was truncated, the response includes a NextToken. To retrieve the next set of results, use the token in the next request. Tokens expire after 24 hours. Defaults to None.
        max_results(int, optional): The number of items to return in the response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.forecast.dataset.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.forecast.dataset.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="forecast",
        operation="list_datasets",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
