"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    outpost_id: str,
    subnet_id: str,
    security_group_id: str,
    access_type: str = None,
    customer_owned_ipv4_pool: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Amazon S3 on Outposts Access Points simplify managing data access at scale for shared datasets in S3 on Outposts. S3 on Outposts uses endpoints to connect to Outposts buckets so that you can perform actions within your virtual private cloud (VPC). For more information, see  Accessing S3 on Outposts using VPC only access points. This action creates an endpoint and associates it with the specified Outposts.  It can take up to 5 minutes for this action to complete.   Related actions include:    DeleteEndpoint     ListEndpoints

    Args:
        outpost_id(str): The ID of the AWS Outposts. .
        subnet_id(str): The ID of the subnet in the selected VPC. The endpoint subnet must belong to the Outpost that has the Amazon S3 on Outposts provisioned.
        security_group_id(str): The ID of the security group to use with the endpoint.
        access_type(str, optional): The type of access for the on-premise network connectivity for the Outpost endpoint. To access the endpoint from an on-premises network, you must specify the access type and provide the customer owned IPv4 pool. Defaults to None.
        customer_owned_ipv4_pool(str, optional): The ID of the customer-owned IPv4 pool for the endpoint. IP addresses will be allocated from this pool for the endpoint. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.s3outpost_endpoint.create(
                ctx, outpost_id=value, subnet_id=value, security_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.s3outpost_endpoint.create outpost_id=value, subnet_id=value, security_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="s3outposts",
        operation="create_endpoint",
        op_kwargs={
            "OutpostId": outpost_id,
            "SubnetId": subnet_id,
            "SecurityGroupId": security_group_id,
            "AccessType": access_type,
            "CustomerOwnedIpv4Pool": customer_owned_ipv4_pool,
        },
    )


async def delete(hub, ctx, endpoint_id: str, outpost_id: str) -> None:
    r"""
    **Autogenerated function**

    Amazon S3 on Outposts Access Points simplify managing data access at scale for shared datasets in S3 on Outposts. S3 on Outposts uses endpoints to connect to Outposts buckets so that you can perform actions within your virtual private cloud (VPC). For more information, see  Accessing S3 on Outposts using VPC only access points. This action deletes an endpoint.  It can take up to 5 minutes for this action to complete.   Related actions include:    CreateEndpoint     ListEndpoints

    Args:
        endpoint_id(str): The ID of the endpoint.
        outpost_id(str): The ID of the AWS Outposts. .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.s3outpost_endpoint.delete(
                ctx, endpoint_id=value, outpost_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.s3outpost_endpoint.delete endpoint_id=value, outpost_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="s3outposts",
        operation="delete_endpoint",
        op_kwargs={"EndpointId": endpoint_id, "OutpostId": outpost_id},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Amazon S3 on Outposts Access Points simplify managing data access at scale for shared datasets in S3 on Outposts. S3 on Outposts uses endpoints to connect to Outposts buckets so that you can perform actions within your virtual private cloud (VPC). For more information, see  Accessing S3 on Outposts using VPC only access points. This action lists endpoints associated with the Outposts.   Related actions include:    CreateEndpoint     DeleteEndpoint

    Args:
        next_token(str, optional): The next endpoint requested in the list. Defaults to None.
        max_results(int, optional): The max number of endpoints that can be returned on the request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.s3outpost_endpoint.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.s3outpost_endpoint.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="s3outposts",
        operation="list_endpoints",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
