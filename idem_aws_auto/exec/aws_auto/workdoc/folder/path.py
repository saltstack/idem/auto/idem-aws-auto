"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub,
    ctx,
    folder_id: str,
    authentication_token: str = None,
    limit: int = None,
    fields: str = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the path information (the hierarchy from the root folder) for the specified folder. By default, Amazon WorkDocs returns a maximum of 100 levels upwards from the requested folder and only includes the IDs of the parent folders in the path. You can limit the maximum number of levels. You can also request the parent folder names.

    Args:
        authentication_token(str, optional): Amazon WorkDocs authentication token. Not required when using AWS administrator credentials to access the API. Defaults to None.
        folder_id(str): The ID of the folder.
        limit(int, optional): The maximum number of levels in the hierarchy to return. Defaults to None.
        fields(str, optional): A comma-separated list of values. Specify "NAME" to include the names of the parent folders. Defaults to None.
        marker(str, optional): This value is not supported. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.workdoc.folder.path.get(ctx, folder_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.workdoc.folder.path.get folder_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="workdocs",
        operation="get_folder_path",
        op_kwargs={
            "AuthenticationToken": authentication_token,
            "FolderId": folder_id,
            "Limit": limit,
            "Fields": fields,
            "Marker": marker,
        },
    )
