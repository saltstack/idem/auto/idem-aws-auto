"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(
    hub, ctx, service_action_associations: List, accept_language: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Associates multiple self-service actions with provisioning artifacts.

    Args:
        service_action_associations(List): One or more associations, each consisting of the Action ID, the Product ID, and the Provisioning Artifact ID.
        accept_language(str, optional): The language code.    en - English (default)    jp - Japanese    zh - Chinese  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicecatalog.associate_service_action_with_provisioning_artifact.batch(
                ctx, service_action_associations=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicecatalog.associate_service_action_with_provisioning_artifact.batch service_action_associations=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicecatalog",
        operation="batch_associate_service_action_with_provisioning_artifact",
        op_kwargs={
            "ServiceActionAssociations": service_action_associations,
            "AcceptLanguage": accept_language,
        },
    )
