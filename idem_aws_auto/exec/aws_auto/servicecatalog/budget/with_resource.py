"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(hub, ctx, budget_name: str, resource_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Associates the specified budget with the specified resource.

    Args:
        budget_name(str): The name of the budget you want to associate.
        resource_id(str):  The resource identifier. Either a portfolio-id or a product-id.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicecatalog.budget.with_resource.associate(
                ctx, budget_name=value, resource_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicecatalog.budget.with_resource.associate budget_name=value, resource_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicecatalog",
        operation="associate_budget_with_resource",
        op_kwargs={"BudgetName": budget_name, "ResourceId": resource_id},
    )
