"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub,
    ctx,
    accept_language: str = None,
    id_: str = None,
    name: str = None,
    source_portfolio_id: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about the specified product. This operation is run with administrator access.

    Args:
        accept_language(str, optional): The language code.    en - English (default)    jp - Japanese    zh - Chinese  . Defaults to None.
        id_(str, optional): The product identifier. Defaults to None.
        name(str, optional): The product name. Defaults to None.
        source_portfolio_id(str, optional): The unique identifier of the shared portfolio that the specified product is associated with. You can provide this parameter to retrieve the shared TagOptions associated with the product. If this parameter is provided and if TagOptions sharing is enabled in the portfolio share, the API returns both local and shared TagOptions associated with the product. Otherwise only local TagOptions will be returned. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicecatalog.product.as_admin.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicecatalog.product.as_admin.describe
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicecatalog",
        operation="describe_product_as_admin",
        op_kwargs={
            "AcceptLanguage": accept_language,
            "Id": id_,
            "Name": name,
            "SourcePortfolioId": source_portfolio_id,
        },
    )


async def search(
    hub,
    ctx,
    accept_language: str = None,
    portfolio_id: str = None,
    filters: Dict = None,
    sort_by: str = None,
    sort_order: str = None,
    page_token: str = None,
    page_size: int = None,
    product_source: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about the products for the specified portfolio or all products.

    Args:
        accept_language(str, optional): The language code.    en - English (default)    jp - Japanese    zh - Chinese  . Defaults to None.
        portfolio_id(str, optional): The portfolio identifier. Defaults to None.
        filters(Dict, optional): The search filters. If no search filters are specified, the output includes all products to which the administrator has access. Defaults to None.
        sort_by(str, optional): The sort field. If no value is specified, the results are not sorted. Defaults to None.
        sort_order(str, optional): The sort order. If no value is specified, the results are not sorted. Defaults to None.
        page_token(str, optional): The page token for the next set of results. To retrieve the first set of results, use null. Defaults to None.
        page_size(int, optional): The maximum number of items to return with this call. Defaults to None.
        product_source(str, optional): Access level of the source of the product. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.servicecatalog.product.as_admin.search(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.servicecatalog.product.as_admin.search
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="servicecatalog",
        operation="search_products_as_admin",
        op_kwargs={
            "AcceptLanguage": accept_language,
            "PortfolioId": portfolio_id,
            "Filters": filters,
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "PageToken": page_token,
            "PageSize": page_size,
            "ProductSource": product_source,
        },
    )
