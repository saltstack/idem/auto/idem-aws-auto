"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, domain: str, encryption_key: str = None, tags: List = None
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a domain. CodeArtifact domains make it easier to manage multiple repositories across an organization. You can use a domain to apply permissions across many repositories owned by different AWS accounts. An asset is stored only once in a domain, even if it's in multiple repositories.  Although you can have multiple domains, we recommend a single production domain that contains all published artifacts so that your development teams can find and share packages. You can use a second pre-production domain to test changes to the production domain configuration.

    Args:
        domain(str):  The name of the domain to create. All domain names in an AWS Region that are in the same AWS account must be unique. The domain name is used as the prefix in DNS hostnames. Do not use sensitive information in a domain name because it is publicly discoverable. .
        encryption_key(str, optional):  The encryption key for the domain. This is used to encrypt content stored in a domain. An encryption key can be a key ID, a key Amazon Resource Name (ARN), a key alias, or a key alias ARN. To specify an encryptionKey, your IAM role must have kms:DescribeKey and kms:CreateGrant permissions on the encryption key that is used. For more information, see DescribeKey in the AWS Key Management Service API Reference and AWS KMS API Permissions Reference in the AWS Key Management Service Developer Guide.    CodeArtifact supports only symmetric CMKs. Do not associate an asymmetric CMK with your domain. For more information, see Using symmetric and asymmetric keys in the AWS Key Management Service Developer Guide.  . Defaults to None.
        tags(List, optional): One or more tag key-value pairs for the domain. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeartifact.domain.init.create(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeartifact.domain.init.create domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeartifact",
        operation="create_domain",
        op_kwargs={"domain": domain, "encryptionKey": encryption_key, "tags": tags},
    )


async def delete(hub, ctx, domain: str, domain_owner: str = None) -> Dict:
    r"""
    **Autogenerated function**

     Deletes a domain. You cannot delete a domain that contains repositories. If you want to delete a domain with repositories, first delete its repositories.

    Args:
        domain(str):  The name of the domain to delete. .
        domain_owner(str, optional):  The 12-digit account number of the AWS account that owns the domain. It does not include dashes or spaces. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeartifact.domain.init.delete(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeartifact.domain.init.delete domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeartifact",
        operation="delete_domain",
        op_kwargs={"domain": domain, "domainOwner": domain_owner},
    )


async def describe(hub, ctx, domain: str, domain_owner: str = None) -> Dict:
    r"""
    **Autogenerated function**

     Returns a DomainDescription object that contains information about the requested domain.

    Args:
        domain(str):  A string that specifies the name of the requested domain. .
        domain_owner(str, optional):  The 12-digit account number of the AWS account that owns the domain. It does not include dashes or spaces. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeartifact.domain.init.describe(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeartifact.domain.init.describe domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeartifact",
        operation="describe_domain",
        op_kwargs={"domain": domain, "domainOwner": domain_owner},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of DomainSummary objects for all domains owned by the AWS account that makes this call. Each returned DomainSummary object contains information about a domain.

    Args:
        max_results(int, optional):  The maximum number of results to return per page. . Defaults to None.
        next_token(str, optional):  The token for the next set of results. Use the value returned in the previous response in the next request to retrieve the next set of results. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeartifact.domain.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeartifact.domain.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeartifact",
        operation="list_domains",
        op_kwargs={"maxResults": max_results, "nextToken": next_token},
    )
