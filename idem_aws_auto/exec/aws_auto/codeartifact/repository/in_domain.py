"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    domain: str,
    domain_owner: str = None,
    administrator_account: str = None,
    repository_prefix: str = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Returns a list of RepositorySummary objects. Each RepositorySummary contains information about a repository in the specified domain and that matches the input parameters.

    Args:
        domain(str):  The name of the domain that contains the returned list of repositories. .
        domain_owner(str, optional):  The 12-digit account number of the AWS account that owns the domain. It does not include dashes or spaces. . Defaults to None.
        administrator_account(str, optional):  Filter the list of repositories to only include those that are managed by the AWS account ID. . Defaults to None.
        repository_prefix(str, optional):  A prefix used to filter returned repositories. Only repositories with names that start with repositoryPrefix are returned. . Defaults to None.
        max_results(int, optional):  The maximum number of results to return per page. . Defaults to None.
        next_token(str, optional):  The token for the next set of results. Use the value returned in the previous response in the next request to retrieve the next set of results. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeartifact.repository.in_domain.list(ctx, domain=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeartifact.repository.in_domain.list domain=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeartifact",
        operation="list_repositories_in_domain",
        op_kwargs={
            "domain": domain,
            "domainOwner": domain_owner,
            "administratorAccount": administrator_account,
            "repositoryPrefix": repository_prefix,
            "maxResults": max_results,
            "nextToken": next_token,
        },
    )
