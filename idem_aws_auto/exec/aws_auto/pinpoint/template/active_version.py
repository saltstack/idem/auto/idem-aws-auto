"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(
    hub,
    ctx,
    template_active_version_request: Dict,
    template_name: str,
    template_type: str,
) -> Dict:
    r"""
    **Autogenerated function**

    Changes the status of a specific version of a message template to active.

    Args:
        template_active_version_request(Dict): Specifies which version of a message template to use as the active version of the template.
        template_name(str): The name of the message template. A template name must start with an alphanumeric character and can contain a maximum of 128 characters. The characters can be alphanumeric characters, underscores (_), or hyphens (-). Template names are case sensitive.
        template_type(str): The type of channel that the message template is designed for. Valid values are: EMAIL, PUSH, SMS, and VOICE.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.template.active_version.update(
                ctx, template_active_version_request=value, template_name=value, template_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.template.active_version.update template_active_version_request=value, template_name=value, template_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="update_template_active_version",
        op_kwargs={
            "TemplateActiveVersionRequest": template_active_version_request,
            "TemplateName": template_name,
            "TemplateType": template_type,
        },
    )
