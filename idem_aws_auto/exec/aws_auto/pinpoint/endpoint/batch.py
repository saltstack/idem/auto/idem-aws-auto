"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(hub, ctx, application_id: str, endpoint_batch_request: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new batch of endpoints for an application or updates the settings and attributes of a batch of existing endpoints for an application. You can also use this operation to define custom attributes for a batch of endpoints. If an update includes one or more values for a custom attribute, Amazon Pinpoint replaces (overwrites) any existing values with the new values.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.
        endpoint_batch_request(Dict): Specifies a batch of endpoints to create or update and the settings and attributes to set or change for each endpoint.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.endpoint.batch.update(
                ctx, application_id=value, endpoint_batch_request=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.endpoint.batch.update application_id=value, endpoint_batch_request=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="update_endpoints_batch",
        op_kwargs={
            "ApplicationId": application_id,
            "EndpointBatchRequest": endpoint_batch_request,
        },
    )
