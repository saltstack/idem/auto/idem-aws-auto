"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, application_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Disables the voice channel for an application and deletes any existing settings for the channel.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.voice.channel.delete(ctx, application_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.voice.channel.delete application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="delete_voice_channel",
        op_kwargs={"ApplicationId": application_id},
    )


async def get(hub, ctx, application_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about the status and settings of the voice channel for an application.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.voice.channel.get(ctx, application_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.voice.channel.get application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="get_voice_channel",
        op_kwargs={"ApplicationId": application_id},
    )


async def update(hub, ctx, application_id: str, voice_channel_request: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Enables the voice channel for an application or updates the status and settings of the voice channel for an application.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.
        voice_channel_request(Dict): Specifies the status and settings of the voice channel for an application.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.voice.channel.update(
                ctx, application_id=value, voice_channel_request=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.voice.channel.update application_id=value, voice_channel_request=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="update_voice_channel",
        op_kwargs={
            "ApplicationId": application_id,
            "VoiceChannelRequest": voice_channel_request,
        },
    )
