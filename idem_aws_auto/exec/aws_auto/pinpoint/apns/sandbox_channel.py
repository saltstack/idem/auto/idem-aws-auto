"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, application_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Disables the APNs sandbox channel for an application and deletes any existing settings for the channel.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.apns.sandbox_channel.delete(ctx, application_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.apns.sandbox_channel.delete application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="delete_apns_sandbox_channel",
        op_kwargs={"ApplicationId": application_id},
    )


async def get(hub, ctx, application_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about the status and settings of the APNs sandbox channel for an application.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.apns.sandbox_channel.get(ctx, application_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.apns.sandbox_channel.get application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="get_apns_sandbox_channel",
        op_kwargs={"ApplicationId": application_id},
    )


async def update(
    hub, ctx, apns_sandbox_channel_request: Dict, application_id: str
) -> Dict:
    r"""
    **Autogenerated function**

    Enables the APNs sandbox channel for an application or updates the status and settings of the APNs sandbox channel for an application.

    Args:
        apns_sandbox_channel_request(Dict): Specifies the status and settings of the APNs (Apple Push Notification service) sandbox channel for an application.
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.apns.sandbox_channel.update(
                ctx, apns_sandbox_channel_request=value, application_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.apns.sandbox_channel.update apns_sandbox_channel_request=value, application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="update_apns_sandbox_channel",
        op_kwargs={
            "APNSSandboxChannelRequest": apns_sandbox_channel_request,
            "ApplicationId": application_id,
        },
    )
