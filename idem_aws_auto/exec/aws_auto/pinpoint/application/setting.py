"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, application_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about the settings for an application.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.application.setting.get_all(ctx, application_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.application.setting.get_all application_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="get_application_settings",
        op_kwargs={"ApplicationId": application_id},
    )


async def update_multiple(
    hub, ctx, application_id: str, write_application_settings_request: Dict
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the settings for an application.

    Args:
        application_id(str): The unique identifier for the application. This identifier is displayed as the Project ID on the Amazon Pinpoint console.
        write_application_settings_request(Dict): Specifies the default settings for an application.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pinpoint.application.setting.update_multiple(
                ctx, application_id=value, write_application_settings_request=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pinpoint.application.setting.update_multiple application_id=value, write_application_settings_request=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pinpoint",
        operation="update_application_settings",
        op_kwargs={
            "ApplicationId": application_id,
            "WriteApplicationSettingsRequest": write_application_settings_request,
        },
    )
