"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(
    hub,
    ctx,
    workbook_id: str,
    table_id: str,
    rows_to_create: List,
    client_request_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     The BatchCreateTableRows API allows you to create one or more rows at the end of a table in a workbook. The API allows you to specify the values to set in some or all of the columns in the new rows.   If a column is not explicitly set in a specific row, then the column level formula specified in the table will be applied to the new row. If there is no column level formula but the last row of the table has a formula, then that formula will be copied down to the new row. If there is no column level formula and no formula in the last row of the table, then that column will be left blank for the new rows.

    Args:
        workbook_id(str): The ID of the workbook where the new rows are being added.  If a workbook with the specified ID could not be found, this API throws ResourceNotFoundException. .
        table_id(str): The ID of the table where the new rows are being added.  If a table with the specified ID could not be found, this API throws ResourceNotFoundException. .
        rows_to_create(List):  The list of rows to create at the end of the table. Each item in this list needs to have a batch item id to uniquely identify the element in the request and the cells to create for that row. You need to specify at least one item in this list.   Note that if one of the column ids in any of the rows in the request does not exist in the table, then the request fails and no updates are made to the table. .
        client_request_token(str, optional):  The request token for performing the batch create operation. Request tokens help to identify duplicate requests. If a call times out or fails due to a transient error like a failed network connection, you can retry the call with the same request token. The service ensures that if the first call using that request token is successfully performed, the second call will not perform the operation again.   Note that request tokens are valid only for a few minutes. You cannot use request tokens to dedupe requests spanning hours or days. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.honeycode.create_table_row.batch(
                ctx, workbook_id=value, table_id=value, rows_to_create=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.honeycode.create_table_row.batch workbook_id=value, table_id=value, rows_to_create=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="honeycode",
        operation="batch_create_table_rows",
        op_kwargs={
            "workbookId": workbook_id,
            "tableId": table_id,
            "rowsToCreate": rows_to_create,
            "clientRequestToken": client_request_token,
        },
    )
