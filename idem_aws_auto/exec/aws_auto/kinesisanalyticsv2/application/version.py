"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub, ctx, application_name: str, application_version_id: int
) -> Dict:
    r"""
    **Autogenerated function**

    Provides a detailed description of a specified version of the application. To see a list of all the versions of an application, invoke the ListApplicationVersions operation.  This operation is supported only for Amazon Kinesis Data Analytics for Apache Flink.

    Args:
        application_name(str): The name of the application for which you want to get the version description.
        application_version_id(int): The ID of the application version for which you want to get the description.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kinesisanalyticsv2.application.version.describe(
                ctx, application_name=value, application_version_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kinesisanalyticsv2.application.version.describe application_name=value, application_version_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kinesisanalyticsv2",
        operation="describe_application_version",
        op_kwargs={
            "ApplicationName": application_name,
            "ApplicationVersionId": application_version_id,
        },
    )


async def list_all(
    hub, ctx, application_name: str, limit: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the versions for the specified application, including versions that were rolled back. The response also includes a summary of the configuration associated with each version. To get the complete description of a specific application version, invoke the DescribeApplicationVersion operation.  This operation is supported only for Amazon Kinesis Data Analytics for Apache Flink.

    Args:
        application_name(str): The name of the application for which you want to list all versions.
        limit(int, optional): The maximum number of versions to list in this invocation of the operation. Defaults to None.
        next_token(str, optional): If a previous invocation of this operation returned a pagination token, pass it into this value to retrieve the next set of results. For more information about pagination, see Using the AWS Command Line Interface's Pagination Options. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kinesisanalyticsv2.application.version.list_all(
                ctx, application_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kinesisanalyticsv2.application.version.list_all application_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kinesisanalyticsv2",
        operation="list_application_versions",
        op_kwargs={
            "ApplicationName": application_name,
            "Limit": limit,
            "NextToken": next_token,
        },
    )
