"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    scope: str,
    capacity: int,
    visibility_config: Dict,
    description: str = None,
    rules: List = None,
    tags: List = None,
    custom_response_bodies: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a RuleGroup per the specifications provided.   A rule group defines a collection of rules to inspect and control web requests that you can use in a WebACL. When you create a rule group, you define an immutable capacity limit. If you update a rule group, you must stay within the capacity. This allows others to reuse the rule group with confidence in its capacity requirements.

    Args:
        name(str): The name of the rule group. You cannot change the name of a rule group after you create it.
        scope(str): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   .
        capacity(int): The web ACL capacity units (WCUs) required for this rule group. When you create your own rule group, you define this, and you cannot change it after creation. When you add or modify the rules in a rule group, WAF enforces this limit. You can check the capacity for a set of rules using CheckCapacity. WAF uses WCUs to calculate and control the operating resources that are used to run your rules, rule groups, and web ACLs. WAF calculates capacity differently for each rule type, to reflect the relative cost of each rule. Simple rules that cost little to run use fewer WCUs than more complex rules that use more processing power. Rule group capacity is fixed at creation, which helps users plan their web ACL WCU usage when they use a rule group. The WCU limit for web ACLs is 1,500. .
        description(str, optional): A description of the rule group that helps with identification. . Defaults to None.
        rules(List, optional): The Rule statements used to identify the web requests that you want to allow, block, or count. Each rule includes one top-level statement that WAF uses to identify matching web requests, and parameters that govern how WAF handles them. . Defaults to None.
        visibility_config(Dict): Defines and enables Amazon CloudWatch metrics and web request sample collection. .
        tags(List, optional): An array of key:value pairs to associate with the resource. Defaults to None.
        custom_response_bodies(Dict, optional): A map of custom response keys and content bodies. When you create a rule with a block action, you can send a custom response to the web request. You define these for the rule group, and then use them in the rules that you define in the rule group.  For information about customizing web requests and responses, see Customizing web requests and responses in WAF in the WAF Developer Guide.  For information about the limits on count and size for custom request and response settings, see WAF quotas in the WAF Developer Guide. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.rule_group.create(
                ctx, name=value, scope=value, capacity=value, visibility_config=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.rule_group.create name=value, scope=value, capacity=value, visibility_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="create_rule_group",
        op_kwargs={
            "Name": name,
            "Scope": scope,
            "Capacity": capacity,
            "Description": description,
            "Rules": rules,
            "VisibilityConfig": visibility_config,
            "Tags": tags,
            "CustomResponseBodies": custom_response_bodies,
        },
    )


async def delete(hub, ctx, name: str, scope: str, id_: str, lock_token: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified RuleGroup.

    Args:
        name(str): The name of the rule group. You cannot change the name of a rule group after you create it.
        scope(str): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   .
        id_(str): A unique identifier for the rule group. This ID is returned in the responses to create and list commands. You provide it to operations like update and delete.
        lock_token(str): A token used for optimistic locking. WAF returns a token to your get and list requests, to mark the state of the entity at the time of the request. To make changes to the entity associated with the token, you provide the token to operations like update and delete. WAF uses the token to ensure that no changes have been made to the entity since you last retrieved it. If a change has been made, the update fails with a WAFOptimisticLockException. If this happens, perform another get, and use the new token returned by that operation. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.rule_group.delete(
                ctx, name=value, scope=value, id_=value, lock_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.rule_group.delete name=value, scope=value, id_=value, lock_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="delete_rule_group",
        op_kwargs={"Name": name, "Scope": scope, "Id": id_, "LockToken": lock_token},
    )


async def get(
    hub, ctx, name: str = None, scope: str = None, id_: str = None, arn: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the specified RuleGroup.

    Args:
        name(str, optional): The name of the rule group. You cannot change the name of a rule group after you create it. Defaults to None.
        scope(str, optional): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   . Defaults to None.
        id_(str, optional): A unique identifier for the rule group. This ID is returned in the responses to create and list commands. You provide it to operations like update and delete. Defaults to None.
        arn(str, optional): The Amazon Resource Name (ARN) of the entity. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.rule_group.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.rule_group.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="get_rule_group",
        op_kwargs={"Name": name, "Scope": scope, "Id": id_, "ARN": arn},
    )


async def list_all(
    hub, ctx, scope: str, next_marker: str = None, limit: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves an array of RuleGroupSummary objects for the rule groups that you manage.

    Args:
        scope(str): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   .
        next_marker(str, optional): When you request a list of objects with a Limit setting, if the number of objects that are still available for retrieval exceeds the limit, WAF returns a NextMarker value in the response. To retrieve the next batch of objects, provide the marker from the prior call in your next request. Defaults to None.
        limit(int, optional): The maximum number of objects that you want WAF to return for this request. If more objects are available, in the response, WAF provides a NextMarker value that you can use in a subsequent call to get the next batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.rule_group.list_all(ctx, scope=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.rule_group.list_all scope=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="list_rule_groups",
        op_kwargs={"Scope": scope, "NextMarker": next_marker, "Limit": limit},
    )


async def update(
    hub,
    ctx,
    name: str,
    scope: str,
    id_: str,
    visibility_config: Dict,
    lock_token: str,
    description: str = None,
    rules: List = None,
    custom_response_bodies: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified RuleGroup.  This operation completely replaces the mutable specifications that you already have for the rule group with the ones that you provide to this call. To modify the rule group, retrieve it by calling GetRuleGroup, update the settings as needed, and then provide the complete rule group specification to this call.   A rule group defines a collection of rules to inspect and control web requests that you can use in a WebACL. When you create a rule group, you define an immutable capacity limit. If you update a rule group, you must stay within the capacity. This allows others to reuse the rule group with confidence in its capacity requirements.

    Args:
        name(str): The name of the rule group. You cannot change the name of a rule group after you create it.
        scope(str): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   .
        id_(str): A unique identifier for the rule group. This ID is returned in the responses to create and list commands. You provide it to operations like update and delete.
        description(str, optional): A description of the rule group that helps with identification. . Defaults to None.
        rules(List, optional): The Rule statements used to identify the web requests that you want to allow, block, or count. Each rule includes one top-level statement that WAF uses to identify matching web requests, and parameters that govern how WAF handles them. . Defaults to None.
        visibility_config(Dict): Defines and enables Amazon CloudWatch metrics and web request sample collection. .
        lock_token(str): A token used for optimistic locking. WAF returns a token to your get and list requests, to mark the state of the entity at the time of the request. To make changes to the entity associated with the token, you provide the token to operations like update and delete. WAF uses the token to ensure that no changes have been made to the entity since you last retrieved it. If a change has been made, the update fails with a WAFOptimisticLockException. If this happens, perform another get, and use the new token returned by that operation. .
        custom_response_bodies(Dict, optional): A map of custom response keys and content bodies. When you create a rule with a block action, you can send a custom response to the web request. You define these for the rule group, and then use them in the rules that you define in the rule group.  For information about customizing web requests and responses, see Customizing web requests and responses in WAF in the WAF Developer Guide.  For information about the limits on count and size for custom request and response settings, see WAF quotas in the WAF Developer Guide. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.rule_group.update(
                ctx, name=value, scope=value, id_=value, visibility_config=value, lock_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.rule_group.update name=value, scope=value, id_=value, visibility_config=value, lock_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="update_rule_group",
        op_kwargs={
            "Name": name,
            "Scope": scope,
            "Id": id_,
            "Description": description,
            "Rules": rules,
            "VisibilityConfig": visibility_config,
            "LockToken": lock_token,
            "CustomResponseBodies": custom_response_bodies,
        },
    )
