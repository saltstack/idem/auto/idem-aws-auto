"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the LoggingConfiguration from the specified web ACL.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the web ACL from which you want to delete the LoggingConfiguration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.logging_configuration.delete(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.logging_configuration.delete resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="delete_logging_configuration",
        op_kwargs={"ResourceArn": resource_arn},
    )


async def get(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the LoggingConfiguration for the specified web ACL.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the web ACL for which you want to get the LoggingConfiguration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.logging_configuration.get(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.logging_configuration.get resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="get_logging_configuration",
        op_kwargs={"ResourceArn": resource_arn},
    )


async def list_all(
    hub, ctx, scope: str = None, next_marker: str = None, limit: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves an array of your LoggingConfiguration objects.

    Args:
        scope(str, optional): Specifies whether this is for an Amazon CloudFront distribution or for a regional application. A regional application can be an Application Load Balancer (ALB), an Amazon API Gateway REST API, or an AppSync GraphQL API.  To work with CloudFront, you must also specify the Region US East (N. Virginia) as follows:    CLI - Specify the Region when you use the CloudFront scope: --scope=CLOUDFRONT --region=us-east-1.    API and SDKs - For all calls, use the Region endpoint us-east-1.   . Defaults to None.
        next_marker(str, optional): When you request a list of objects with a Limit setting, if the number of objects that are still available for retrieval exceeds the limit, WAF returns a NextMarker value in the response. To retrieve the next batch of objects, provide the marker from the prior call in your next request. Defaults to None.
        limit(int, optional): The maximum number of objects that you want WAF to return for this request. If more objects are available, in the response, WAF provides a NextMarker value that you can use in a subsequent call to get the next batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.logging_configuration.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.logging_configuration.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="list_logging_configurations",
        op_kwargs={"Scope": scope, "NextMarker": next_marker, "Limit": limit},
    )


async def put(hub, ctx, logging_configuration: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Enables the specified LoggingConfiguration, to start logging from a web ACL, according to the configuration provided. You can access information about all traffic that WAF inspects using the following steps:   Create an Amazon Kinesis Data Firehose.  Create the data firehose with a PUT source and in the Region that you are operating. If you are capturing logs for Amazon CloudFront, always create the firehose in US East (N. Virginia).  Give the data firehose a name that starts with the prefix aws-waf-logs-. For example, aws-waf-logs-us-east-2-analytics.  Do not create the data firehose using a Kinesis stream as your source.    Associate that firehose to your web ACL using a PutLoggingConfiguration request.   When you successfully enable logging using a PutLoggingConfiguration request, WAF will create a service linked role with the necessary permissions to write logs to the Amazon Kinesis Data Firehose. For more information, see Logging Web ACL Traffic Information in the WAF Developer Guide.  This operation completely replaces the mutable specifications that you already have for the logging configuration with the ones that you provide to this call. To modify the logging configuration, retrieve it by calling GetLoggingConfiguration, update the settings as needed, and then provide the complete logging configuration specification to this call.

    Args:
        logging_configuration(Dict): .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.logging_configuration.put(
                ctx, logging_configuration=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.logging_configuration.put logging_configuration=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="put_logging_configuration",
        op_kwargs={"LoggingConfiguration": logging_configuration},
    )
