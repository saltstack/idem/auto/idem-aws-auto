"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Permanently deletes an IAM policy from the specified rule group. You must be the owner of the rule group to perform this operation.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the rule group from which you want to delete the policy. You must be the owner of the rule group to perform this operation.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.permission_policy.delete(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.permission_policy.delete resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="delete_permission_policy",
        op_kwargs={"ResourceArn": resource_arn},
    )


async def get(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the IAM policy that is attached to the specified rule group. You must be the owner of the rule group to perform this operation.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the rule group for which you want to get the policy.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.permission_policy.get(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.permission_policy.get resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="get_permission_policy",
        op_kwargs={"ResourceArn": resource_arn},
    )


async def put(hub, ctx, resource_arn: str, policy: str) -> Dict:
    r"""
    **Autogenerated function**

    Attaches an IAM policy to the specified resource. Use this to share a rule group across accounts. You must be the owner of the rule group to perform this operation. This action is subject to the following restrictions:   You can attach only one policy with each PutPermissionPolicy request.   The ARN in the request must be a valid WAF RuleGroup ARN and the rule group must exist in the same Region.   The user making the request must be the owner of the rule group.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the RuleGroup to which you want to attach the policy.
        policy(str): The policy to attach to the specified rule group.  The policy specifications must conform to the following:   The policy must be composed using IAM Policy version 2012-10-17 or version 2015-01-01.   The policy must include specifications for Effect, Action, and Principal.    Effect must specify Allow.    Action must specify wafv2:CreateWebACL, wafv2:UpdateWebACL, and wafv2:PutFirewallManagerRuleGroups. WAF rejects any extra actions or wildcard actions in the policy.   The policy must not include a Resource parameter.   For more information, see IAM Policies. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.wafv2.permission_policy.put(
                ctx, resource_arn=value, policy=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.wafv2.permission_policy.put resource_arn=value, policy=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="wafv2",
        operation="put_permission_policy",
        op_kwargs={"ResourceArn": resource_arn, "Policy": policy},
    )
