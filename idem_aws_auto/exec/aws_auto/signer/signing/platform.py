"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, platform_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information on a specific signing platform.

    Args:
        platform_id(str): The ID of the target signing platform.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.signer.signing.platform.get(ctx, platform_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.signer.signing.platform.get platform_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="signer",
        operation="get_signing_platform",
        op_kwargs={"platformId": platform_id},
    )


async def list_all(
    hub,
    ctx,
    category: str = None,
    partner: str = None,
    target: str = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all signing platforms available in code signing that match the request parameters. If additional jobs remain to be listed, code signing returns a nextToken value. Use this value in subsequent calls to ListSigningJobs to fetch the remaining values. You can continue calling ListSigningJobs with your maxResults parameter and with new values that code signing returns in the nextToken parameter until all of your signing jobs have been returned.

    Args:
        category(str, optional): The category type of a signing platform. Defaults to None.
        partner(str, optional): Any partner entities connected to a signing platform. Defaults to None.
        target(str, optional): The validation template that is used by the target signing platform. Defaults to None.
        max_results(int, optional): The maximum number of results to be returned by this operation. Defaults to None.
        next_token(str, optional): Value for specifying the next set of paginated results to return. After you receive a response with truncated results, use this parameter in a subsequent request. Set it to the value of nextToken from the response that you just received. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.signer.signing.platform.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.signer.signing.platform.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="signer",
        operation="list_signing_platforms",
        op_kwargs={
            "category": category,
            "partner": partner,
            "target": target,
            "maxResults": max_results,
            "nextToken": next_token,
        },
    )
