"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def revoke(hub, ctx, job_id: str, reason: str, job_owner: str = None) -> None:
    r"""
    **Autogenerated function**

    Changes the state of a signing job to REVOKED. This indicates that the signature is no longer valid.

    Args:
        job_id(str): ID of the signing job to be revoked.
        job_owner(str, optional): AWS account ID of the job owner. Defaults to None.
        reason(str): The reason for revoking the signing job.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.signer.signature.revoke(ctx, job_id=value, reason=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.signer.signature.revoke job_id=value, reason=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="signer",
        operation="revoke_signature",
        op_kwargs={"jobId": job_id, "jobOwner": job_owner, "reason": reason},
    )
