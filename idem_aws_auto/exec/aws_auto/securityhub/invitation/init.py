"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def accept(hub, ctx, master_id: str, invitation_id: str) -> Dict:
    r"""
    **Autogenerated function**

    This method is deprecated. Instead, use AcceptAdministratorInvitation. The Security Hub console continues to use AcceptInvitation. It will eventually change to use AcceptAdministratorInvitation. Any IAM policies that specifically control access to this function must continue to use AcceptInvitation. You should also add AcceptAdministratorInvitation to your policies to ensure that the correct permissions are in place after the console begins to use AcceptAdministratorInvitation. Accepts the invitation to be a member account and be monitored by the Security Hub administrator account that the invitation was sent from. This operation is only used by member accounts that are not added through Organizations. When the member account accepts the invitation, permission is granted to the administrator account to view findings generated in the member account.

    Args:
        master_id(str): The account ID of the Security Hub administrator account that sent the invitation.
        invitation_id(str): The identifier of the invitation sent from the Security Hub administrator account.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.securityhub.invitation.init.accept(
                ctx, master_id=value, invitation_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.securityhub.invitation.init.accept master_id=value, invitation_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="securityhub",
        operation="accept_invitation",
        op_kwargs={"MasterId": master_id, "InvitationId": invitation_id},
    )


async def decline(hub, ctx, account_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Declines invitations to become a member account. This operation is only used by accounts that are not part of an organization. Organization accounts do not receive invitations.

    Args:
        account_ids(List): The list of account IDs for the accounts from which to decline the invitations to Security Hub.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.securityhub.invitation.init.decline(ctx, account_ids=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.securityhub.invitation.init.decline account_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="securityhub",
        operation="decline_invitations",
        op_kwargs={"AccountIds": account_ids},
    )


async def delete_multiple(hub, ctx, account_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Deletes invitations received by the Amazon Web Services account to become a member account. This operation is only used by accounts that are not part of an organization. Organization accounts do not receive invitations.

    Args:
        account_ids(List): The list of the account IDs that sent the invitations to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.securityhub.invitation.init.delete_multiple(
                ctx, account_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.securityhub.invitation.init.delete_multiple account_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="securityhub",
        operation="delete_invitations",
        op_kwargs={"AccountIds": account_ids},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all Security Hub membership invitations that were sent to the current Amazon Web Services account. This operation is only used by accounts that are managed by invitation. Accounts that are managed using the integration with Organizations do not receive invitations.

    Args:
        max_results(int, optional): The maximum number of items to return in the response. . Defaults to None.
        next_token(str, optional): The token that is required for pagination. On your first call to the ListInvitations operation, set the value of this parameter to NULL. For subsequent calls to the operation, to continue listing data, set the value of this parameter to the value returned from the previous response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.securityhub.invitation.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.securityhub.invitation.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="securityhub",
        operation="list_invitations",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )
