"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "product_for_import"


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all findings-generating solutions (products) that you are subscribed to receive findings from in Security Hub.

    Args:
        next_token(str, optional): The token that is required for pagination. On your first call to the ListEnabledProductsForImport operation, set the value of this parameter to NULL. For subsequent calls to the operation, to continue listing data, set the value of this parameter to the value returned from the previous response. Defaults to None.
        max_results(int, optional): The maximum number of items to return in the response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.securityhub.enabled.product_for_import_.list(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.securityhub.enabled.product_for_import.list
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="securityhub",
        operation="list_enabled_products_for_import",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
