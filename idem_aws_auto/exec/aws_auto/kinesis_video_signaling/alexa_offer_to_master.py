"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def send(
    hub, ctx, channel_arn: str, sender_client_id: str, message_payload: str
) -> Dict:
    r"""
    **Autogenerated function**

    This API allows you to connect WebRTC-enabled devices with Alexa display devices. When invoked, it sends the Alexa Session Description Protocol (SDP) offer to the master peer. The offer is delivered as soon as the master is connected to the specified signaling channel. This API returns the SDP answer from the connected master. If the master is not connected to the signaling channel, redelivery requests are made until the message expires.

    Args:
        channel_arn(str): The ARN of the signaling channel by which Alexa and the master peer communicate.
        sender_client_id(str): The unique identifier for the sender client.
        message_payload(str): The base64-encoded SDP offer content.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kinesis_video_signaling.alexa_offer_to_master.send(
                ctx, channel_arn=value, sender_client_id=value, message_payload=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kinesis_video_signaling.alexa_offer_to_master.send channel_arn=value, sender_client_id=value, message_payload=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kinesis-video-signaling",
        operation="send_alexa_offer_to_master",
        op_kwargs={
            "ChannelARN": channel_arn,
            "SenderClientId": sender_client_id,
            "MessagePayload": message_payload,
        },
    )
