"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe(
    hub,
    ctx,
    config_rule_name: str,
    resource_keys: List = None,
    limit: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Provides a detailed view of a Remediation Execution for a set of resources including state, timestamps for when steps for the remediation execution occur, and any error messages for steps that have failed. When you specify the limit and the next token, you receive a paginated response.

    Args:
        config_rule_name(str): A list of AWS Config rule names.
        resource_keys(List, optional): A list of resource keys to be processed with the current request. Each element in the list consists of the resource type and resource ID. . Defaults to None.
        limit(int, optional): The maximum number of RemediationExecutionStatuses returned on each page. The default is maximum. If you specify 0, AWS Config uses the default. . Defaults to None.
        next_token(str, optional): The nextToken string returned on a previous page that you use to get the next page of results in a paginated response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.config.remediation.execution.status.describe(
                ctx, config_rule_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.config.remediation.execution.status.describe config_rule_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="config",
        operation="describe_remediation_execution_status",
        op_kwargs={
            "ConfigRuleName": config_rule_name,
            "ResourceKeys": resource_keys,
            "Limit": limit,
            "NextToken": next_token,
        },
    )
