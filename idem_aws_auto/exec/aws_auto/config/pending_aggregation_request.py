"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(
    hub, ctx, requester_account_id: str, requester_aws_region: str
) -> None:
    r"""
    **Autogenerated function**

    Deletes pending authorization requests for a specified aggregator account in a specified region.

    Args:
        requester_account_id(str): The 12-digit account ID of the account requesting to aggregate data.
        requester_aws_region(str): The region requesting to aggregate data.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.config.pending_aggregation_request.delete(
                ctx, requester_account_id=value, requester_aws_region=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.config.pending_aggregation_request.delete requester_account_id=value, requester_aws_region=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="config",
        operation="delete_pending_aggregation_request",
        op_kwargs={
            "RequesterAccountId": requester_account_id,
            "RequesterAwsRegion": requester_aws_region,
        },
    )


async def describe_all(hub, ctx, limit: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of all pending aggregation requests.

    Args:
        limit(int, optional): The maximum number of evaluation results returned on each page. The default is maximum. If you specify 0, AWS Config uses the default. Defaults to None.
        next_token(str, optional): The nextToken string returned on a previous page that you use to get the next page of results in a paginated response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.config.pending_aggregation_request.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.config.pending_aggregation_request.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="config",
        operation="describe_pending_aggregation_requests",
        op_kwargs={"Limit": limit, "NextToken": next_token},
    )
