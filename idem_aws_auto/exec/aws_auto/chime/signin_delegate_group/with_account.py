"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def associate(hub, ctx, account_id: str, signin_delegate_groups: List) -> Dict:
    r"""
    **Autogenerated function**

    Associates the specified sign-in delegate groups with the specified Amazon Chime account.

    Args:
        account_id(str): The Amazon Chime account ID.
        signin_delegate_groups(List): The sign-in delegate groups.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.signin_delegate_group.with_account.associate(
                ctx, account_id=value, signin_delegate_groups=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.signin_delegate_group.with_account.associate account_id=value, signin_delegate_groups=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="associate_signin_delegate_groups_with_account",
        op_kwargs={
            "AccountId": account_id,
            "SigninDelegateGroups": signin_delegate_groups,
        },
    )
