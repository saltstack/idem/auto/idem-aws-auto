"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, account_id: str, bot_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the events configuration that allows a bot to receive outgoing events.

    Args:
        account_id(str): The Amazon Chime account ID.
        bot_id(str): The bot ID.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.event_configuration.delete(
                ctx, account_id=value, bot_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.event_configuration.delete account_id=value, bot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="delete_events_configuration",
        op_kwargs={"AccountId": account_id, "BotId": bot_id},
    )


async def get(hub, ctx, account_id: str, bot_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets details for an events configuration that allows a bot to receive outgoing events, such as an HTTPS endpoint or Lambda function ARN.

    Args:
        account_id(str): The Amazon Chime account ID.
        bot_id(str): The bot ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.event_configuration.get(
                ctx, account_id=value, bot_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.event_configuration.get account_id=value, bot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="get_events_configuration",
        op_kwargs={"AccountId": account_id, "BotId": bot_id},
    )


async def put(
    hub,
    ctx,
    account_id: str,
    bot_id: str,
    outbound_events_https_endpoint: str = None,
    lambda_function_arn: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an events configuration that allows a bot to receive outgoing events sent by Amazon Chime. Choose either an HTTPS endpoint or a Lambda function ARN. For more information, see Bot.

    Args:
        account_id(str): The Amazon Chime account ID.
        bot_id(str): The bot ID.
        outbound_events_https_endpoint(str, optional): HTTPS endpoint that allows the bot to receive outgoing events. Defaults to None.
        lambda_function_arn(str, optional): Lambda function ARN that allows the bot to receive outgoing events. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.event_configuration.put(
                ctx, account_id=value, bot_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.event_configuration.put account_id=value, bot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="put_events_configuration",
        op_kwargs={
            "AccountId": account_id,
            "BotId": bot_id,
            "OutboundEventsHTTPSEndpoint": outbound_events_https_endpoint,
            "LambdaFunctionArn": lambda_function_arn,
        },
    )
