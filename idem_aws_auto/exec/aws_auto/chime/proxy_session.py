"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    voice_connector_id: str,
    participant_phone_numbers: List,
    capabilities: List,
    name: str = None,
    expiry_minutes: int = None,
    number_selection_behavior: str = None,
    geo_match_level: str = None,
    geo_match_params: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a proxy session on the specified Amazon Chime Voice Connector for the specified participant phone numbers.

    Args:
        voice_connector_id(str): The Amazon Chime voice connector ID.
        participant_phone_numbers(List): The participant phone numbers.
        name(str, optional): The name of the proxy session. Defaults to None.
        expiry_minutes(int, optional): The number of minutes allowed for the proxy session. Defaults to None.
        capabilities(List): The proxy session capabilities.
        number_selection_behavior(str, optional): The preference for proxy phone number reuse, or stickiness, between the same participants across sessions. Defaults to None.
        geo_match_level(str, optional): The preference for matching the country or area code of the proxy phone number with that of the first participant. Defaults to None.
        geo_match_params(Dict, optional): The country and area code for the proxy phone number. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.proxy_session.create(
                ctx, voice_connector_id=value, participant_phone_numbers=value, capabilities=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.proxy_session.create voice_connector_id=value, participant_phone_numbers=value, capabilities=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="create_proxy_session",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "ParticipantPhoneNumbers": participant_phone_numbers,
            "Name": name,
            "ExpiryMinutes": expiry_minutes,
            "Capabilities": capabilities,
            "NumberSelectionBehavior": number_selection_behavior,
            "GeoMatchLevel": geo_match_level,
            "GeoMatchParams": geo_match_params,
        },
    )


async def delete(hub, ctx, voice_connector_id: str, proxy_session_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified proxy session from the specified Amazon Chime Voice Connector.

    Args:
        voice_connector_id(str): The Amazon Chime voice connector ID.
        proxy_session_id(str): The proxy session ID.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.proxy_session.delete(
                ctx, voice_connector_id=value, proxy_session_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.proxy_session.delete voice_connector_id=value, proxy_session_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="delete_proxy_session",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "ProxySessionId": proxy_session_id,
        },
    )


async def get(hub, ctx, voice_connector_id: str, proxy_session_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets the specified proxy session details for the specified Amazon Chime Voice Connector.

    Args:
        voice_connector_id(str): The Amazon Chime voice connector ID.
        proxy_session_id(str): The proxy session ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.proxy_session.get(
                ctx, voice_connector_id=value, proxy_session_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.proxy_session.get voice_connector_id=value, proxy_session_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="get_proxy_session",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "ProxySessionId": proxy_session_id,
        },
    )


async def list_all(
    hub,
    ctx,
    voice_connector_id: str,
    status: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the proxy sessions for the specified Amazon Chime Voice Connector.

    Args:
        voice_connector_id(str): The Amazon Chime voice connector ID.
        status(str, optional): The proxy session status. Defaults to None.
        next_token(str, optional): The token to use to retrieve the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return in a single call. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.proxy_session.list_all(ctx, voice_connector_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.proxy_session.list_all voice_connector_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="list_proxy_sessions",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "Status": status,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    voice_connector_id: str,
    proxy_session_id: str,
    capabilities: List,
    expiry_minutes: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified proxy session details, such as voice or SMS capabilities.

    Args:
        voice_connector_id(str): The Amazon Chime voice connector ID.
        proxy_session_id(str): The proxy session ID.
        capabilities(List): The proxy session capabilities.
        expiry_minutes(int, optional): The number of minutes allowed for the proxy session. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.proxy_session.update(
                ctx, voice_connector_id=value, proxy_session_id=value, capabilities=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.proxy_session.update voice_connector_id=value, proxy_session_id=value, capabilities=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="update_proxy_session",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "ProxySessionId": proxy_session_id,
            "Capabilities": capabilities,
            "ExpiryMinutes": expiry_minutes,
        },
    )
