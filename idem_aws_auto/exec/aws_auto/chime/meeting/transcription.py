"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def start(hub, ctx, meeting_id: str, transcription_configuration: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Start transcription for the specified meetingId.

    Args:
        meeting_id(str): The unique ID of the meeting being transcribed.
        transcription_configuration(Dict): The configuration for the current transcription operation. Must contain EngineTranscribeSettings or EngineTranscribeMedicalSettings.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.meeting.transcription.start(
                ctx, meeting_id=value, transcription_configuration=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.meeting.transcription.start meeting_id=value, transcription_configuration=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="start_meeting_transcription",
        op_kwargs={
            "MeetingId": meeting_id,
            "TranscriptionConfiguration": transcription_configuration,
        },
    )


async def stop(hub, ctx, meeting_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Stops transcription for the specified meetingId.

    Args:
        meeting_id(str): The unique ID of the meeting for which you stop transcription.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.meeting.transcription.stop(ctx, meeting_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.meeting.transcription.stop meeting_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="stop_meeting_transcription",
        op_kwargs={"MeetingId": meeting_id},
    )
