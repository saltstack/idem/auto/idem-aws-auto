"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, account_id: str) -> Dict:
    r"""
    **Autogenerated function**

     Gets the retention settings for the specified Amazon Chime Enterprise account. For more information about retention settings, see Managing Chat Retention Policies in the Amazon Chime Administration Guide.

    Args:
        account_id(str): The Amazon Chime account ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.retention_setting.get_all(ctx, account_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.retention_setting.get_all account_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="get_retention_settings",
        op_kwargs={"AccountId": account_id},
    )


async def put_multiple(hub, ctx, account_id: str, retention_settings: Dict) -> Dict:
    r"""
    **Autogenerated function**

     Puts retention settings for the specified Amazon Chime Enterprise account. We recommend using AWS CloudTrail to monitor usage of this API for your account. For more information, see Logging Amazon Chime API Calls with AWS CloudTrail in the Amazon Chime Administration Guide.  To turn off existing retention settings, remove the number of days from the corresponding RetentionDays field in the RetentionSettings object. For more information about retention settings, see Managing Chat Retention Policies in the Amazon Chime Administration Guide.

    Args:
        account_id(str): The Amazon Chime account ID.
        retention_settings(Dict): The retention settings.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.retention_setting.put_multiple(
                ctx, account_id=value, retention_settings=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.retention_setting.put_multiple account_id=value, retention_settings=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="put_retention_settings",
        op_kwargs={"AccountId": account_id, "RetentionSettings": retention_settings},
    )
