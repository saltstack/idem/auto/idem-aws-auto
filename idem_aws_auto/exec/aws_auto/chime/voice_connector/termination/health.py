"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, voice_connector_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about the last time a SIP OPTIONS ping was received from your SIP infrastructure for the specified Amazon Chime Voice Connector.

    Args:
        voice_connector_id(str): The Amazon Chime Voice Connector ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.termination.health.get(
                ctx, voice_connector_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.termination.health.get voice_connector_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="get_voice_connector_termination_health",
        op_kwargs={"VoiceConnectorId": voice_connector_id},
    )
