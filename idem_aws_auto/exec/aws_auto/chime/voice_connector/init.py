"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub, ctx, name: str, require_encryption: bool, aws_region: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an Amazon Chime Voice Connector under the administrator's AWS account. You can choose to create an Amazon Chime Voice Connector in a specific AWS Region.  Enabling CreateVoiceConnectorRequest$RequireEncryption configures your Amazon Chime Voice Connector to use TLS transport for SIP signaling and Secure RTP (SRTP) for media. Inbound calls use TLS transport, and unencrypted outbound calls are blocked.

    Args:
        name(str): The name of the Amazon Chime Voice Connector.
        aws_region(str, optional):  The AWS Region in which the Amazon Chime Voice Connector is created. Default value: us-east-1 . . Defaults to None.
        require_encryption(bool): When enabled, requires encryption for the Amazon Chime Voice Connector.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.init.create(
                ctx, name=value, require_encryption=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.init.create name=value, require_encryption=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="create_voice_connector",
        op_kwargs={
            "Name": name,
            "AwsRegion": aws_region,
            "RequireEncryption": require_encryption,
        },
    )


async def delete(hub, ctx, voice_connector_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified Amazon Chime Voice Connector. Any phone numbers associated with the Amazon Chime Voice Connector must be disassociated from it before it can be deleted.

    Args:
        voice_connector_id(str): The Amazon Chime Voice Connector ID.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.init.delete(ctx, voice_connector_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.init.delete voice_connector_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="delete_voice_connector",
        op_kwargs={"VoiceConnectorId": voice_connector_id},
    )


async def get(hub, ctx, voice_connector_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves details for the specified Amazon Chime Voice Connector, such as timestamps,name, outbound host, and encryption requirements.

    Args:
        voice_connector_id(str): The Amazon Chime Voice Connector ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.init.get(ctx, voice_connector_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.init.get voice_connector_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="get_voice_connector",
        op_kwargs={"VoiceConnectorId": voice_connector_id},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the Amazon Chime Voice Connectors for the administrator's AWS account.

    Args:
        next_token(str, optional): The token to use to retrieve the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return in a single call. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="list_voice_connectors",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )


async def update(
    hub, ctx, voice_connector_id: str, name: str, require_encryption: bool
) -> Dict:
    r"""
    **Autogenerated function**

    Updates details for the specified Amazon Chime Voice Connector.

    Args:
        voice_connector_id(str): The Amazon Chime Voice Connector ID.
        name(str): The name of the Amazon Chime Voice Connector.
        require_encryption(bool): When enabled, requires encryption for the Amazon Chime Voice Connector.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.chime.voice_connector.init.update(
                ctx, voice_connector_id=value, name=value, require_encryption=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.chime.voice_connector.init.update voice_connector_id=value, name=value, require_encryption=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="chime",
        operation="update_voice_connector",
        op_kwargs={
            "VoiceConnectorId": voice_connector_id,
            "Name": name,
            "RequireEncryption": require_encryption,
        },
    )
