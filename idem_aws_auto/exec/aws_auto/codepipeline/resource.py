"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Adds to or modifies the tags of the given resource. Tags are metadata that can be used to manage a resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource you want to add tags to.
        tags(List): The tags you want to modify or add to the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codepipeline.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codepipeline.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codepipeline",
        operation="tag_resource",
        op_kwargs={"resourceArn": resource_arn, "tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes tags from an AWS resource.

    Args:
        resource_arn(str):  The Amazon Resource Name (ARN) of the resource to remove tags from.
        tag_keys(List): The list of keys for the tags to be removed from the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codepipeline.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codepipeline.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codepipeline",
        operation="untag_resource",
        op_kwargs={"resourceArn": resource_arn, "tagKeys": tag_keys},
    )
