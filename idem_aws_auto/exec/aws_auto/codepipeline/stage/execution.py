"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def retry(
    hub,
    ctx,
    pipeline_name: str,
    stage_name: str,
    pipeline_execution_id: str,
    retry_mode: str,
) -> Dict:
    r"""
    **Autogenerated function**

    Resumes the pipeline execution by retrying the last failed actions in a stage. You can retry a stage immediately if any of the actions in the stage fail. When you retry, all actions that are still in progress continue working, and failed actions are triggered again.

    Args:
        pipeline_name(str): The name of the pipeline that contains the failed stage.
        stage_name(str): The name of the failed stage to be retried.
        pipeline_execution_id(str): The ID of the pipeline execution in the failed stage to be retried. Use the GetPipelineState action to retrieve the current pipelineExecutionId of the failed stage.
        retry_mode(str): The scope of the retry attempt. Currently, the only supported value is FAILED_ACTIONS.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codepipeline.stage.execution.retry(
                ctx,
                pipeline_name=value,
                stage_name=value,
                pipeline_execution_id=value,
                retry_mode=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codepipeline.stage.execution.retry pipeline_name=value, stage_name=value, pipeline_execution_id=value, retry_mode=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codepipeline",
        operation="retry_stage_execution",
        op_kwargs={
            "pipelineName": pipeline_name,
            "stageName": stage_name,
            "pipelineExecutionId": pipeline_execution_id,
            "retryMode": retry_mode,
        },
    )
