"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def delete(hub, ctx, assessment_run_arn: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the assessment run that is specified by the ARN of the assessment run.

    Args:
        assessment_run_arn(str): The ARN that specifies the assessment run that you want to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.inspector.assessment.run.init.delete(
                ctx, assessment_run_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.inspector.assessment.run.init.delete assessment_run_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="inspector",
        operation="delete_assessment_run",
        op_kwargs={"assessmentRunArn": assessment_run_arn},
    )


async def describe_all(hub, ctx, assessment_run_arns: List) -> Dict:
    r"""
    **Autogenerated function**

    Describes the assessment runs that are specified by the ARNs of the assessment runs.

    Args:
        assessment_run_arns(List): The ARN that specifies the assessment run that you want to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.inspector.assessment.run.init.describe_all(
                ctx, assessment_run_arns=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.inspector.assessment.run.init.describe_all assessment_run_arns=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="inspector",
        operation="describe_assessment_runs",
        op_kwargs={"assessmentRunArns": assessment_run_arns},
    )


async def list_all(
    hub,
    ctx,
    assessment_template_arns: List = None,
    filter_: Dict = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the assessment runs that correspond to the assessment templates that are specified by the ARNs of the assessment templates.

    Args:
        assessment_template_arns(List, optional): The ARNs that specify the assessment templates whose assessment runs you want to list. Defaults to None.
        filter_(Dict, optional): You can use this parameter to specify a subset of data to be included in the action's response. For a record to match a filter, all specified filter attributes must match. When multiple values are specified for a filter attribute, any of the values can match. Defaults to None.
        next_token(str, optional): You can use this parameter when paginating results. Set the value of this parameter to null on your first call to the ListAssessmentRuns action. Subsequent calls to the action fill nextToken in the request with the value of NextToken from the previous response to continue listing data. Defaults to None.
        max_results(int, optional): You can use this parameter to indicate the maximum number of items that you want in the response. The default value is 10. The maximum value is 500. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.inspector.assessment.run.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.inspector.assessment.run.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="inspector",
        operation="list_assessment_runs",
        op_kwargs={
            "assessmentTemplateArns": assessment_template_arns,
            "filter": filter_,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )


async def start(
    hub, ctx, assessment_template_arn: str, assessment_run_name: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Starts the assessment run specified by the ARN of the assessment template. For this API to function properly, you must not exceed the limit of running up to 500 concurrent agents per AWS account.

    Args:
        assessment_template_arn(str): The ARN of the assessment template of the assessment run that you want to start.
        assessment_run_name(str, optional): You can specify the name for the assessment run. The name must be unique for the assessment template whose ARN is used to start the assessment run. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.inspector.assessment.run.init.start(
                ctx, assessment_template_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.inspector.assessment.run.init.start assessment_template_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="inspector",
        operation="start_assessment_run",
        op_kwargs={
            "assessmentTemplateArn": assessment_template_arn,
            "assessmentRunName": assessment_run_name,
        },
    )


async def stop(hub, ctx, assessment_run_arn: str, stop_action: str = None) -> None:
    r"""
    **Autogenerated function**

    Stops the assessment run that is specified by the ARN of the assessment run.

    Args:
        assessment_run_arn(str): The ARN of the assessment run that you want to stop.
        stop_action(str, optional): An input option that can be set to either START_EVALUATION or SKIP_EVALUATION. START_EVALUATION (the default value), stops the AWS agent from collecting data and begins the results evaluation and the findings generation process. SKIP_EVALUATION cancels the assessment run immediately, after which no findings are generated. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.inspector.assessment.run.init.stop(
                ctx, assessment_run_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.inspector.assessment.run.init.stop assessment_run_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="inspector",
        operation="stop_assessment_run",
        op_kwargs={"assessmentRunArn": assessment_run_arn, "stopAction": stop_action},
    )
