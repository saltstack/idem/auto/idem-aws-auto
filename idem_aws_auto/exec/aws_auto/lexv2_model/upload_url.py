"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Gets a pre-signed S3 write URL that you use to upload the zip archive when importing a bot or a bot locale.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lexv2_model.upload_url.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lexv2_model.upload_url.create
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="lexv2-models", operation="create_upload_url", op_kwargs={}
    )
