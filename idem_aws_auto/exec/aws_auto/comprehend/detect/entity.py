"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, text_list: List, language_code: str) -> Dict:
    r"""
    **Autogenerated function**

    Inspects the text of a batch of documents for named entities and returns information about them. For more information about named entities, see how-entities

    Args:
        text_list(List): A list containing the text of the input documents. The list can contain a maximum of 25 documents. Each document must contain fewer than 5,000 bytes of UTF-8 encoded characters.
        language_code(str): The language of the input documents. You can specify any of the primary languages supported by Amazon Comprehend. All documents must be in the same language.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.comprehend.detect.entity.batch(
                ctx, text_list=value, language_code=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.comprehend.detect.entity.batch text_list=value, language_code=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="comprehend",
        operation="batch_detect_entities",
        op_kwargs={"TextList": text_list, "LanguageCode": language_code},
    )
