"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, text_list: List) -> Dict:
    r"""
    **Autogenerated function**

    Determines the dominant language of the input text for a batch of documents. For a list of languages that Amazon Comprehend can detect, see Amazon Comprehend Supported Languages.

    Args:
        text_list(List): A list containing the text of the input documents. The list can contain a maximum of 25 documents. Each document should contain at least 20 characters and must contain fewer than 5,000 bytes of UTF-8 encoded characters.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.comprehend.detect.dominant_language.batch(ctx, text_list=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.comprehend.detect.dominant_language.batch text_list=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="comprehend",
        operation="batch_detect_dominant_language",
        op_kwargs={"TextList": text_list},
    )
