"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(hub, ctx, aws_account_id: str, template_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes read and write permissions on a template.

    Args:
        aws_account_id(str): The ID of the Amazon Web Services account; that contains the template that you're describing.
        template_id(str): The ID for the template.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.template.permission.describe_all(
                ctx, aws_account_id=value, template_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.template.permission.describe_all aws_account_id=value, template_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="describe_template_permissions",
        op_kwargs={"AwsAccountId": aws_account_id, "TemplateId": template_id},
    )


async def update_multiple(
    hub,
    ctx,
    aws_account_id: str,
    template_id: str,
    grant_permissions: List = None,
    revoke_permissions: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the resource permissions for a template.

    Args:
        aws_account_id(str): The ID of the Amazon Web Services account; that contains the template.
        template_id(str): The ID for the template.
        grant_permissions(List, optional): A list of resource permissions to be granted on the template. . Defaults to None.
        revoke_permissions(List, optional): A list of resource permissions to be revoked from the template. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.template.permission.update_multiple(
                ctx, aws_account_id=value, template_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.template.permission.update_multiple aws_account_id=value, template_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="update_template_permissions",
        op_kwargs={
            "AwsAccountId": aws_account_id,
            "TemplateId": template_id,
            "GrantPermissions": grant_permissions,
            "RevokePermissions": revoke_permissions,
        },
    )
