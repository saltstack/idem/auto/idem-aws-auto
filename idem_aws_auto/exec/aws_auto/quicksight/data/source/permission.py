"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(hub, ctx, aws_account_id: str, data_source_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes the resource permissions for a data source.

    Args:
        aws_account_id(str): The Amazon Web Services account; ID.
        data_source_id(str): The ID of the data source. This ID is unique per Region; for each Amazon Web Services account;.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.data.source.permission.describe_all(
                ctx, aws_account_id=value, data_source_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.data.source.permission.describe_all aws_account_id=value, data_source_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="describe_data_source_permissions",
        op_kwargs={"AwsAccountId": aws_account_id, "DataSourceId": data_source_id},
    )


async def update_multiple(
    hub,
    ctx,
    aws_account_id: str,
    data_source_id: str,
    grant_permissions: List = None,
    revoke_permissions: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the permissions to a data source.

    Args:
        aws_account_id(str): The Amazon Web Services account; ID.
        data_source_id(str): The ID of the data source. This ID is unique per Region; for each Amazon Web Services account;. .
        grant_permissions(List, optional): A list of resource permissions that you want to grant on the data source. Defaults to None.
        revoke_permissions(List, optional): A list of resource permissions that you want to revoke on the data source. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.data.source.permission.update_multiple(
                ctx, aws_account_id=value, data_source_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.data.source.permission.update_multiple aws_account_id=value, data_source_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="update_data_source_permissions",
        op_kwargs={
            "AwsAccountId": aws_account_id,
            "DataSourceId": data_source_id,
            "GrantPermissions": grant_permissions,
            "RevokePermissions": revoke_permissions,
        },
    )
