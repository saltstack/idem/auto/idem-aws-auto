"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Assigns one or more tags (key-value pairs) to the specified QuickSight resource.  Tags can help you organize and categorize your resources. You can also use them to scope user permissions, by granting a user permission to access or change only resources with certain tag values. You can use the TagResource operation with a resource that already has tags. If you specify a new tag key for the resource, this tag is appended to the list of tags associated with the resource. If you specify a tag key that is already associated with the resource, the new tag value that you specify replaces the previous value for that tag. You can associate as many as 50 tags with a resource. QuickSight supports tagging on data set, data source, dashboard, and template.  Tagging for QuickSight works in a similar way to tagging for other AWS services, except for the following:   You can't use tags to track AWS costs for QuickSight. This restriction is because QuickSight costs are based on users and SPICE capacity, which aren't taggable resources.   QuickSight doesn't currently support the Tag Editor for Resource Groups.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource that you want to tag.
        tags(List): Contains a map of the key-value pairs for the resource tag or tags assigned to the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="tag_resource",
        op_kwargs={"ResourceArn": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes a tag or tags from a resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource that you want to untag.
        tag_keys(List): The keys of the key-value pairs for the resource tag or tags assigned to the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.quicksight.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.quicksight.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="quicksight",
        operation="untag_resource",
        op_kwargs={"ResourceArn": resource_arn, "TagKeys": tag_keys},
    )
