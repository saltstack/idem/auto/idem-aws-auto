"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def list_all(
    hub,
    ctx,
    configuration_id: str,
    port_information_needed: bool = None,
    neighbor_configuration_ids: List = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of servers that are one network hop away from a specified server.

    Args:
        configuration_id(str): Configuration ID of the server for which neighbors are being listed.
        port_information_needed(bool, optional): Flag to indicate if port and protocol information is needed as part of the response. Defaults to None.
        neighbor_configuration_ids(List, optional): List of configuration IDs to test for one-hop-away. Defaults to None.
        max_results(int, optional): Maximum number of results to return in a single page of output. Defaults to None.
        next_token(str, optional): Token to retrieve the next set of results. For example, if you previously specified 100 IDs for ListServerNeighborsRequest$neighborConfigurationIds but set ListServerNeighborsRequest$maxResults to 10, you received a set of 10 results along with a token. Use that token in this query to get the next set of 10. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.discovery.server_neighbor.list_all(ctx, configuration_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.discovery.server_neighbor.list_all configuration_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="discovery",
        operation="list_server_neighbors",
        op_kwargs={
            "configurationId": configuration_id,
            "portInformationNeeded": port_information_needed,
            "neighborConfigurationIds": neighbor_configuration_ids,
            "maxResults": max_results,
            "nextToken": next_token,
        },
    )
