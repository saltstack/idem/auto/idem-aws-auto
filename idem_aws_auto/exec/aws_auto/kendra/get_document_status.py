"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, index_id: str, document_info_list: List) -> Dict:
    r"""
    **Autogenerated function**

    Returns the indexing status for one or more documents submitted with the  BatchPutDocument operation. When you use the BatchPutDocument operation, documents are indexed asynchronously. You can use the BatchGetDocumentStatus operation to get the current status of a list of documents so that you can determine if they have been successfully indexed. You can also use the BatchGetDocumentStatus operation to check the status of the  BatchDeleteDocument operation. When a document is deleted from the index, Amazon Kendra returns NOT_FOUND as the status.

    Args:
        index_id(str): The identifier of the index to add documents to. The index ID is returned by the  CreateIndex  operation.
        document_info_list(List): A list of DocumentInfo objects that identify the documents for which to get the status. You identify the documents by their document ID and optional attributes.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.kendra.get_document_status.batch(
                ctx, index_id=value, document_info_list=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.kendra.get_document_status.batch index_id=value, document_info_list=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="kendra",
        operation="batch_get_document_status",
        op_kwargs={"IndexId": index_id, "DocumentInfoList": document_info_list},
    )
