"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    application_name: str,
    description: str = None,
    resource_lifecycle_config: Dict = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an application that has one configuration template named default and no application versions.

    Args:
        application_name(str): The name of the application. Must be unique within your account.
        description(str, optional): Your description of the application. Defaults to None.
        resource_lifecycle_config(Dict, optional): Specifies an application resource lifecycle configuration to prevent your application from accumulating too many versions. Defaults to None.
        tags(List, optional): Specifies the tags applied to the application. Elastic Beanstalk applies these tags only to the application. Environments that you create in the application don't inherit the tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticbeanstalk.application.init.create(
                ctx, application_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticbeanstalk.application.init.create application_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticbeanstalk",
        operation="create_application",
        op_kwargs={
            "ApplicationName": application_name,
            "Description": description,
            "ResourceLifecycleConfig": resource_lifecycle_config,
            "Tags": tags,
        },
    )


async def delete(
    hub, ctx, application_name: str, terminate_env_by_force: bool = None
) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified application along with all associated versions and configurations. The application versions will not be deleted from your Amazon S3 bucket.  You cannot delete an application that has a running environment.

    Args:
        application_name(str): The name of the application to delete.
        terminate_env_by_force(bool, optional): When set to true, running environments will be terminated before deleting the application. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticbeanstalk.application.init.delete(
                ctx, application_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticbeanstalk.application.init.delete application_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticbeanstalk",
        operation="delete_application",
        op_kwargs={
            "ApplicationName": application_name,
            "TerminateEnvByForce": terminate_env_by_force,
        },
    )


async def describe_all(hub, ctx, application_names: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns the descriptions of existing applications.

    Args:
        application_names(List, optional): If specified, AWS Elastic Beanstalk restricts the returned descriptions to only include those with the specified names. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticbeanstalk.application.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticbeanstalk.application.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticbeanstalk",
        operation="describe_applications",
        op_kwargs={"ApplicationNames": application_names},
    )


async def update(hub, ctx, application_name: str, description: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified application to have the specified properties.  If a property (for example, description) is not provided, the value remains unchanged. To clear these properties, specify an empty string.

    Args:
        application_name(str): The name of the application to update. If no such application is found, UpdateApplication returns an InvalidParameterValue error. .
        description(str, optional): A new description for the application. Default: If not specified, AWS Elastic Beanstalk does not update the description. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticbeanstalk.application.init.update(
                ctx, application_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticbeanstalk.application.init.update application_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticbeanstalk",
        operation="update_application",
        op_kwargs={"ApplicationName": application_name, "Description": description},
    )
