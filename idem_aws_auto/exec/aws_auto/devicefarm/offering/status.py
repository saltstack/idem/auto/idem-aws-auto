"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets the current status and future status of all offerings purchased by an AWS account. The response indicates how many offerings are currently available and the offerings that will be available in the next period. The API returns a NotEligible error if the user is not permitted to invoke the operation. If you must be able to invoke this operation, contact aws-devicefarm-support@amazon.com.

    Args:
        next_token(str, optional): An identifier that was returned from the previous call to this operation, which can be used to return the next set of items in the list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.devicefarm.offering.status.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.devicefarm.offering.status.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="devicefarm",
        operation="get_offering_status",
        op_kwargs={"nextToken": next_token},
    )
