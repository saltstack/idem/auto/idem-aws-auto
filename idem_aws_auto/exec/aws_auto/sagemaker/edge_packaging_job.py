"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    edge_packaging_job_name: str,
    compilation_job_name: str,
    model_name: str,
    model_version: str,
    role_arn: str,
    output_config: Dict,
    resource_key: str = None,
    tags: List = None,
) -> None:
    r"""
    **Autogenerated function**

    Starts a SageMaker Edge Manager model packaging job. Edge Manager will use the model artifacts from the Amazon Simple Storage Service bucket that you specify. After the model has been packaged, Amazon SageMaker saves the resulting artifacts to an S3 bucket that you specify.

    Args:
        edge_packaging_job_name(str): The name of the edge packaging job.
        compilation_job_name(str): The name of the SageMaker Neo compilation job that will be used to locate model artifacts for packaging.
        model_name(str): The name of the model.
        model_version(str): The version of the model.
        role_arn(str): The Amazon Resource Name (ARN) of an IAM role that enables Amazon SageMaker to download and upload the model, and to contact SageMaker Neo.
        output_config(Dict): Provides information about the output location for the packaged model.
        resource_key(str, optional): The CMK to use when encrypting the EBS volume the edge packaging job runs on. Defaults to None.
        tags(List, optional): Creates tags for the packaging job. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.edge_packaging_job.create(
                ctx,
                edge_packaging_job_name=value,
                compilation_job_name=value,
                model_name=value,
                model_version=value,
                role_arn=value,
                output_config=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.edge_packaging_job.create edge_packaging_job_name=value, compilation_job_name=value, model_name=value, model_version=value, role_arn=value, output_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_edge_packaging_job",
        op_kwargs={
            "EdgePackagingJobName": edge_packaging_job_name,
            "CompilationJobName": compilation_job_name,
            "ModelName": model_name,
            "ModelVersion": model_version,
            "RoleArn": role_arn,
            "OutputConfig": output_config,
            "ResourceKey": resource_key,
            "Tags": tags,
        },
    )


async def describe(hub, ctx, edge_packaging_job_name: str) -> Dict:
    r"""
    **Autogenerated function**

    A description of edge packaging jobs.

    Args:
        edge_packaging_job_name(str): The name of the edge packaging job.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.edge_packaging_job.describe(
                ctx, edge_packaging_job_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.edge_packaging_job.describe edge_packaging_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_edge_packaging_job",
        op_kwargs={"EdgePackagingJobName": edge_packaging_job_name},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    creation_time_after: str = None,
    creation_time_before: str = None,
    last_modified_time_after: str = None,
    last_modified_time_before: str = None,
    name_contains: str = None,
    model_name_contains: str = None,
    status_equals: str = None,
    sort_by: str = None,
    sort_order: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of edge packaging jobs.

    Args:
        next_token(str, optional): The response from the last list when returning a list large enough to need tokening. Defaults to None.
        max_results(int, optional): Maximum number of results to select. Defaults to None.
        creation_time_after(str, optional): Select jobs where the job was created after specified time. Defaults to None.
        creation_time_before(str, optional): Select jobs where the job was created before specified time. Defaults to None.
        last_modified_time_after(str, optional): Select jobs where the job was updated after specified time. Defaults to None.
        last_modified_time_before(str, optional): Select jobs where the job was updated before specified time. Defaults to None.
        name_contains(str, optional): Filter for jobs containing this name in their packaging job name. Defaults to None.
        model_name_contains(str, optional): Filter for jobs where the model name contains this string. Defaults to None.
        status_equals(str, optional): The job status to filter for. Defaults to None.
        sort_by(str, optional): Use to specify what column to sort by. Defaults to None.
        sort_order(str, optional): What direction to sort by. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.edge_packaging_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.edge_packaging_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_edge_packaging_jobs",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "CreationTimeAfter": creation_time_after,
            "CreationTimeBefore": creation_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "NameContains": name_contains,
            "ModelNameContains": model_name_contains,
            "StatusEquals": status_equals,
            "SortBy": sort_by,
            "SortOrder": sort_order,
        },
    )


async def stop(hub, ctx, edge_packaging_job_name: str) -> None:
    r"""
    **Autogenerated function**

    Request to stop an edge packaging job.

    Args:
        edge_packaging_job_name(str): The name of the edge packaging job.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.edge_packaging_job.stop(
                ctx, edge_packaging_job_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.edge_packaging_job.stop edge_packaging_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="stop_edge_packaging_job",
        op_kwargs={"EdgePackagingJobName": edge_packaging_job_name},
    )
