"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    training_job_name: str,
    algorithm_specification: Dict,
    role_arn: str,
    output_data_config: Dict,
    resource_config: Dict,
    stopping_condition: Dict,
    hyper_parameters: Dict = None,
    input_data_config: List = None,
    vpc_config: Dict = None,
    tags: List = None,
    enable_network_isolation: bool = None,
    enable_inter_container_traffic_encryption: bool = None,
    enable_managed_spot_training: bool = None,
    checkpoint_config: Dict = None,
    debug_hook_config: Dict = None,
    debug_rule_configurations: List = None,
    tensor_board_output_config: Dict = None,
    experiment_config: Dict = None,
    profiler_config: Dict = None,
    profiler_rule_configurations: List = None,
    environment: Dict = None,
    retry_strategy: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Starts a model training job. After training completes, Amazon SageMaker saves the resulting model artifacts to an Amazon S3 location that you specify.  If you choose to host your model using Amazon SageMaker hosting services, you can use the resulting model artifacts as part of the model. You can also use the artifacts in a machine learning service other than Amazon SageMaker, provided that you know how to use them for inference.  In the request body, you provide the following:     AlgorithmSpecification - Identifies the training algorithm to use.     HyperParameters - Specify these algorithm-specific parameters to enable the estimation of model parameters during training. Hyperparameters can be tuned to optimize this learning process. For a list of hyperparameters for each training algorithm provided by Amazon SageMaker, see Algorithms.     InputDataConfig - Describes the training dataset and the Amazon S3, EFS, or FSx location where it is stored.    OutputDataConfig - Identifies the Amazon S3 bucket where you want Amazon SageMaker to save the results of model training.     ResourceConfig - Identifies the resources, ML compute instances, and ML storage volumes to deploy for model training. In distributed training, you specify more than one instance.     EnableManagedSpotTraining - Optimize the cost of training machine learning models by up to 80% by using Amazon EC2 Spot instances. For more information, see Managed Spot Training.     RoleArn - The Amazon Resource Name (ARN) that Amazon SageMaker assumes to perform tasks on your behalf during model training. You must grant this role the necessary permissions so that Amazon SageMaker can successfully complete model training.     StoppingCondition - To help cap training costs, use MaxRuntimeInSeconds to set a time limit for training. Use MaxWaitTimeInSeconds to specify how long a managed spot training job has to complete.     Environment - The environment variables to set in the Docker container.    RetryStrategy - The number of times to retry the job when the job fails due to an InternalServerError.    For more information about Amazon SageMaker, see How It Works.

    Args:
        training_job_name(str): The name of the training job. The name must be unique within an Amazon Web Services Region in an Amazon Web Services account. .
        hyper_parameters(Dict, optional): Algorithm-specific parameters that influence the quality of the model. You set hyperparameters before you start the learning process. For a list of hyperparameters for each training algorithm provided by Amazon SageMaker, see Algorithms.  You can specify a maximum of 100 hyperparameters. Each hyperparameter is a key-value pair. Each key and value is limited to 256 characters, as specified by the Length Constraint. . Defaults to None.
        algorithm_specification(Dict): The registry path of the Docker image that contains the training algorithm and algorithm-specific metadata, including the input mode. For more information about algorithms provided by Amazon SageMaker, see Algorithms. For information about providing your own algorithms, see Using Your Own Algorithms with Amazon SageMaker. .
        role_arn(str): The Amazon Resource Name (ARN) of an IAM role that Amazon SageMaker can assume to perform tasks on your behalf.  During model training, Amazon SageMaker needs your permission to read input data from an S3 bucket, download a Docker image that contains training code, write model artifacts to an S3 bucket, write logs to Amazon CloudWatch Logs, and publish metrics to Amazon CloudWatch. You grant permissions for all of these tasks to an IAM role. For more information, see Amazon SageMaker Roles.   To be able to pass this role to Amazon SageMaker, the caller of this API must have the iam:PassRole permission. .
        input_data_config(List, optional): An array of Channel objects. Each channel is a named input source. InputDataConfig describes the input data and its location.  Algorithms can accept input data from one or more channels. For example, an algorithm might have two channels of input data, training_data and validation_data. The configuration for each channel provides the S3, EFS, or FSx location where the input data is stored. It also provides information about the stored data: the MIME type, compression method, and whether the data is wrapped in RecordIO format.  Depending on the input mode that the algorithm supports, Amazon SageMaker either copies input data files from an S3 bucket to a local directory in the Docker container, or makes it available as input streams. For example, if you specify an EFS location, input data files will be made available as input streams. They do not need to be downloaded. Defaults to None.
        output_data_config(Dict): Specifies the path to the S3 location where you want to store model artifacts. Amazon SageMaker creates subfolders for the artifacts. .
        resource_config(Dict): The resources, including the ML compute instances and ML storage volumes, to use for model training.  ML storage volumes store model artifacts and incremental states. Training algorithms might also use ML storage volumes for scratch space. If you want Amazon SageMaker to use the ML storage volume to store the training data, choose File as the TrainingInputMode in the algorithm specification. For distributed training algorithms, specify an instance count greater than 1.
        vpc_config(Dict, optional): A VpcConfig object that specifies the VPC that you want your training job to connect to. Control access to and from your training container by configuring the VPC. For more information, see Protect Training Jobs by Using an Amazon Virtual Private Cloud. Defaults to None.
        stopping_condition(Dict): Specifies a limit to how long a model training job can run. It also specifies how long a managed Spot training job has to complete. When the job reaches the time limit, Amazon SageMaker ends the training job. Use this API to cap model training costs. To stop a job, Amazon SageMaker sends the algorithm the SIGTERM signal, which delays job termination for 120 seconds. Algorithms can use this 120-second window to save the model artifacts, so the results of training are not lost. .
        tags(List, optional): An array of key-value pairs. You can use tags to categorize your Amazon Web Services resources in different ways, for example, by purpose, owner, or environment. For more information, see Tagging Amazon Web Services Resources. Defaults to None.
        enable_network_isolation(bool, optional): Isolates the training container. No inbound or outbound network calls can be made, except for calls between peers within a training cluster for distributed training. If you enable network isolation for training jobs that are configured to use a VPC, Amazon SageMaker downloads and uploads customer data and model artifacts through the specified VPC, but the training container does not have network access. Defaults to None.
        enable_inter_container_traffic_encryption(bool, optional): To encrypt all communications between ML compute instances in distributed training, choose True. Encryption provides greater security for distributed training, but training might take longer. How long it takes depends on the amount of communication between compute instances, especially if you use a deep learning algorithm in distributed training. For more information, see Protect Communications Between ML Compute Instances in a Distributed Training Job. Defaults to None.
        enable_managed_spot_training(bool, optional): To train models using managed spot training, choose True. Managed spot training provides a fully managed and scalable infrastructure for training machine learning models. this option is useful when training jobs can be interrupted and when there is flexibility when the training job is run.  The complete and intermediate results of jobs are stored in an Amazon S3 bucket, and can be used as a starting point to train models incrementally. Amazon SageMaker provides metrics and logs in CloudWatch. They can be used to see when managed spot training jobs are running, interrupted, resumed, or completed. . Defaults to None.
        checkpoint_config(Dict, optional): Contains information about the output location for managed spot training checkpoint data. Defaults to None.
        debug_hook_config(Dict, optional): Configuration information for the Debugger hook parameters, metric and tensor collections, and storage paths. To learn more about how to configure the DebugHookConfig parameter, see Use the SageMaker and Debugger Configuration API Operations to Create, Update, and Debug Your Training Job. Defaults to None.
        debug_rule_configurations(List, optional): Configuration information for Debugger rules for debugging output tensors. Defaults to None.
        tensor_board_output_config(Dict, optional): Configuration of storage locations for the Debugger TensorBoard output data. Defaults to None.
        experiment_config(Dict, optional): Associates a SageMaker job as a trial component with an experiment and trial. Specified when you call the following APIs:    CreateProcessingJob     CreateTrainingJob     CreateTransformJob   . Defaults to None.
        profiler_config(Dict, optional): Configuration information for Debugger system monitoring, framework profiling, and storage paths. Defaults to None.
        profiler_rule_configurations(List, optional): Configuration information for Debugger rules for profiling system and framework metrics. Defaults to None.
        environment(Dict, optional): The environment variables to set in the Docker container. Defaults to None.
        retry_strategy(Dict, optional): The number of times to retry the job when the job fails due to an InternalServerError. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.training_job.create(
                ctx,
                training_job_name=value,
                algorithm_specification=value,
                role_arn=value,
                output_data_config=value,
                resource_config=value,
                stopping_condition=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.training_job.create training_job_name=value, algorithm_specification=value, role_arn=value, output_data_config=value, resource_config=value, stopping_condition=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_training_job",
        op_kwargs={
            "TrainingJobName": training_job_name,
            "HyperParameters": hyper_parameters,
            "AlgorithmSpecification": algorithm_specification,
            "RoleArn": role_arn,
            "InputDataConfig": input_data_config,
            "OutputDataConfig": output_data_config,
            "ResourceConfig": resource_config,
            "VpcConfig": vpc_config,
            "StoppingCondition": stopping_condition,
            "Tags": tags,
            "EnableNetworkIsolation": enable_network_isolation,
            "EnableInterContainerTrafficEncryption": enable_inter_container_traffic_encryption,
            "EnableManagedSpotTraining": enable_managed_spot_training,
            "CheckpointConfig": checkpoint_config,
            "DebugHookConfig": debug_hook_config,
            "DebugRuleConfigurations": debug_rule_configurations,
            "TensorBoardOutputConfig": tensor_board_output_config,
            "ExperimentConfig": experiment_config,
            "ProfilerConfig": profiler_config,
            "ProfilerRuleConfigurations": profiler_rule_configurations,
            "Environment": environment,
            "RetryStrategy": retry_strategy,
        },
    )


async def describe(hub, ctx, training_job_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a training job.  Some of the attributes below only appear if the training job successfully starts. If the training job fails, TrainingJobStatus is Failed and, depending on the FailureReason, attributes like TrainingStartTime, TrainingTimeInSeconds, TrainingEndTime, and BillableTimeInSeconds may not be present in the response.

    Args:
        training_job_name(str): The name of the training job.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.training_job.describe(ctx, training_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.training_job.describe training_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_training_job",
        op_kwargs={"TrainingJobName": training_job_name},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    creation_time_after: str = None,
    creation_time_before: str = None,
    last_modified_time_after: str = None,
    last_modified_time_before: str = None,
    name_contains: str = None,
    status_equals: str = None,
    sort_by: str = None,
    sort_order: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists training jobs.  When StatusEquals and MaxResults are set at the same time, the MaxResults number of training jobs are first retrieved ignoring the StatusEquals parameter and then they are filtered by the StatusEquals parameter, which is returned as a response. For example, if ListTrainingJobs is invoked with the following parameters:  { ... MaxResults: 100, StatusEquals: InProgress ... }  First, 100 trainings jobs with any status, including those other than InProgress, are selected (sorted according to the creation time, from the most current to the oldest). Next, those with a status of InProgress are returned. You can quickly test the API using the following Amazon Web Services CLI code.  aws sagemaker list-training-jobs --max-results 100 --status-equals InProgress

    Args:
        next_token(str, optional): If the result of the previous ListTrainingJobs request was truncated, the response includes a NextToken. To retrieve the next set of training jobs, use the token in the next request. . Defaults to None.
        max_results(int, optional): The maximum number of training jobs to return in the response. Defaults to None.
        creation_time_after(str, optional): A filter that returns only training jobs created after the specified time (timestamp). Defaults to None.
        creation_time_before(str, optional): A filter that returns only training jobs created before the specified time (timestamp). Defaults to None.
        last_modified_time_after(str, optional): A filter that returns only training jobs modified after the specified time (timestamp). Defaults to None.
        last_modified_time_before(str, optional): A filter that returns only training jobs modified before the specified time (timestamp). Defaults to None.
        name_contains(str, optional): A string in the training job name. This filter returns only training jobs whose name contains the specified string. Defaults to None.
        status_equals(str, optional): A filter that retrieves only training jobs with a specific status. Defaults to None.
        sort_by(str, optional): The field to sort results by. The default is CreationTime. Defaults to None.
        sort_order(str, optional): The sort order for results. The default is Ascending. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.training_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.training_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_training_jobs",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "CreationTimeAfter": creation_time_after,
            "CreationTimeBefore": creation_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "NameContains": name_contains,
            "StatusEquals": status_equals,
            "SortBy": sort_by,
            "SortOrder": sort_order,
        },
    )


async def stop(hub, ctx, training_job_name: str) -> None:
    r"""
    **Autogenerated function**

    Stops a training job. To stop a job, Amazon SageMaker sends the algorithm the SIGTERM signal, which delays job termination for 120 seconds. Algorithms might use this 120-second window to save the model artifacts, so the results of the training is not lost.  When it receives a StopTrainingJob request, Amazon SageMaker changes the status of the job to Stopping. After Amazon SageMaker stops the job, it sets the status to Stopped.

    Args:
        training_job_name(str): The name of the training job to stop.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.training_job.stop(ctx, training_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.training_job.stop training_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="stop_training_job",
        op_kwargs={"TrainingJobName": training_job_name},
    )


async def update(
    hub,
    ctx,
    training_job_name: str,
    profiler_config: Dict = None,
    profiler_rule_configurations: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Update a model training job to request a new Debugger profiling configuration.

    Args:
        training_job_name(str): The name of a training job to update the Debugger profiling configuration.
        profiler_config(Dict, optional): Configuration information for Debugger system monitoring, framework profiling, and storage paths. Defaults to None.
        profiler_rule_configurations(List, optional): Configuration information for Debugger rules for profiling system and framework metrics. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.training_job.update(ctx, training_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.training_job.update training_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="update_training_job",
        op_kwargs={
            "TrainingJobName": training_job_name,
            "ProfilerConfig": profiler_config,
            "ProfilerRuleConfigurations": profiler_rule_configurations,
        },
    )
