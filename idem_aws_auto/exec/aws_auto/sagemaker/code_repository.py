"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, code_repository_name: str, git_config: Dict, tags: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Git repository as a resource in your Amazon SageMaker account. You can associate the repository with notebook instances so that you can use Git source control for the notebooks you create. The Git repository is a resource in your Amazon SageMaker account, so it can be associated with more than one notebook instance, and it persists independently from the lifecycle of any notebook instances it is associated with. The repository can be hosted either in Amazon Web Services CodeCommit or in any other Git repository.

    Args:
        code_repository_name(str): The name of the Git repository. The name must have 1 to 63 characters. Valid characters are a-z, A-Z, 0-9, and - (hyphen).
        git_config(Dict): Specifies details about the repository, including the URL where the repository is located, the default branch, and credentials to use to access the repository.
        tags(List, optional): An array of key-value pairs. You can use tags to categorize your Amazon Web Services resources in different ways, for example, by purpose, owner, or environment. For more information, see Tagging Amazon Web Services Resources. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.code_repository.create(
                ctx, code_repository_name=value, git_config=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.code_repository.create code_repository_name=value, git_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_code_repository",
        op_kwargs={
            "CodeRepositoryName": code_repository_name,
            "GitConfig": git_config,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, code_repository_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified Git repository from your account.

    Args:
        code_repository_name(str): The name of the Git repository to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.code_repository.delete(
                ctx, code_repository_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.code_repository.delete code_repository_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="delete_code_repository",
        op_kwargs={"CodeRepositoryName": code_repository_name},
    )


async def describe(hub, ctx, code_repository_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets details about the specified Git repository.

    Args:
        code_repository_name(str): The name of the Git repository to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.code_repository.describe(
                ctx, code_repository_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.code_repository.describe code_repository_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_code_repository",
        op_kwargs={"CodeRepositoryName": code_repository_name},
    )


async def list_all(
    hub,
    ctx,
    creation_time_after: str = None,
    creation_time_before: str = None,
    last_modified_time_after: str = None,
    last_modified_time_before: str = None,
    max_results: int = None,
    name_contains: str = None,
    next_token: str = None,
    sort_by: str = None,
    sort_order: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of the Git repositories in your account.

    Args:
        creation_time_after(str, optional): A filter that returns only Git repositories that were created after the specified time. Defaults to None.
        creation_time_before(str, optional): A filter that returns only Git repositories that were created before the specified time. Defaults to None.
        last_modified_time_after(str, optional): A filter that returns only Git repositories that were last modified after the specified time. Defaults to None.
        last_modified_time_before(str, optional): A filter that returns only Git repositories that were last modified before the specified time. Defaults to None.
        max_results(int, optional): The maximum number of Git repositories to return in the response. Defaults to None.
        name_contains(str, optional): A string in the Git repositories name. This filter returns only repositories whose name contains the specified string. Defaults to None.
        next_token(str, optional): If the result of a ListCodeRepositoriesOutput request was truncated, the response includes a NextToken. To get the next set of Git repositories, use the token in the next request. Defaults to None.
        sort_by(str, optional): The field to sort results by. The default is Name. Defaults to None.
        sort_order(str, optional): The sort order for results. The default is Ascending. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.code_repository.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.code_repository.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_code_repositories",
        op_kwargs={
            "CreationTimeAfter": creation_time_after,
            "CreationTimeBefore": creation_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "MaxResults": max_results,
            "NameContains": name_contains,
            "NextToken": next_token,
            "SortBy": sort_by,
            "SortOrder": sort_order,
        },
    )


async def update(hub, ctx, code_repository_name: str, git_config: Dict = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified Git repository with the specified values.

    Args:
        code_repository_name(str): The name of the Git repository to update.
        git_config(Dict, optional): The configuration of the git repository, including the URL and the Amazon Resource Name (ARN) of the Amazon Web Services Secrets Manager secret that contains the credentials used to access the repository. The secret must have a staging label of AWSCURRENT and must be in the following format:  {"username": UserName, "password": Password} . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.code_repository.update(
                ctx, code_repository_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.code_repository.update code_repository_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="update_code_repository",
        op_kwargs={"CodeRepositoryName": code_repository_name, "GitConfig": git_config},
    )
