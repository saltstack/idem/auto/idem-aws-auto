"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def update_multiple(
    hub, ctx, endpoint_name: str, desired_weights_and_capacities: List
) -> Dict:
    r"""
    **Autogenerated function**

    Updates variant weight of one or more variants associated with an existing endpoint, or capacity of one variant associated with an existing endpoint. When it receives the request, Amazon SageMaker sets the endpoint status to Updating. After updating the endpoint, it sets the status to InService. To check the status of an endpoint, use the DescribeEndpoint API.

    Args:
        endpoint_name(str): The name of an existing Amazon SageMaker endpoint.
        desired_weights_and_capacities(List): An object that provides new capacity and weight values for a variant.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.endpoint.weight_and_capacity.update_multiple(
                ctx, endpoint_name=value, desired_weights_and_capacities=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.endpoint.weight_and_capacity.update_multiple endpoint_name=value, desired_weights_and_capacities=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="update_endpoint_weights_and_capacities",
        op_kwargs={
            "EndpointName": endpoint_name,
            "DesiredWeightsAndCapacities": desired_weights_and_capacities,
        },
    )
