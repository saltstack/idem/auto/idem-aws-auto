"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    model_name: str,
    execution_role_arn: str,
    primary_container: Dict = None,
    containers: List = None,
    inference_execution_config: Dict = None,
    tags: List = None,
    vpc_config: Dict = None,
    enable_network_isolation: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a model in Amazon SageMaker. In the request, you name the model and describe a primary container. For the primary container, you specify the Docker image that contains inference code, artifacts (from prior training), and a custom environment map that the inference code uses when you deploy the model for predictions. Use this API to create a model if you want to use Amazon SageMaker hosting services or run a batch transform job. To host your model, you create an endpoint configuration with the CreateEndpointConfig API, and then create an endpoint with the CreateEndpoint API. Amazon SageMaker then deploys all of the containers that you defined for the model in the hosting environment.  For an example that calls this method when deploying a model to Amazon SageMaker hosting services, see Deploy the Model to Amazon SageMaker Hosting Services (Amazon Web Services SDK for Python (Boto 3)).  To run a batch transform using your model, you start a job with the CreateTransformJob API. Amazon SageMaker uses your model and your dataset to get inferences which are then saved to a specified S3 location. In the CreateModel request, you must define a container with the PrimaryContainer parameter. In the request, you also provide an IAM role that Amazon SageMaker can assume to access model artifacts and docker image for deployment on ML compute hosting instances or for batch transform jobs. In addition, you also use the IAM role to manage permissions the inference code needs. For example, if the inference code access any other Amazon Web Services resources, you grant necessary permissions via this role.

    Args:
        model_name(str): The name of the new model.
        primary_container(Dict, optional): The location of the primary docker image containing inference code, associated artifacts, and custom environment map that the inference code uses when the model is deployed for predictions. . Defaults to None.
        containers(List, optional): Specifies the containers in the inference pipeline. Defaults to None.
        inference_execution_config(Dict, optional): Specifies details of how containers in a multi-container endpoint are called. Defaults to None.
        execution_role_arn(str): The Amazon Resource Name (ARN) of the IAM role that Amazon SageMaker can assume to access model artifacts and docker image for deployment on ML compute instances or for batch transform jobs. Deploying on ML compute instances is part of model hosting. For more information, see Amazon SageMaker Roles.   To be able to pass this role to Amazon SageMaker, the caller of this API must have the iam:PassRole permission. .
        tags(List, optional): An array of key-value pairs. You can use tags to categorize your Amazon Web Services resources in different ways, for example, by purpose, owner, or environment. For more information, see Tagging Amazon Web Services Resources. Defaults to None.
        vpc_config(Dict, optional): A VpcConfig object that specifies the VPC that you want your model to connect to. Control access to and from your model container by configuring the VPC. VpcConfig is used in hosting services and in batch transform. For more information, see Protect Endpoints by Using an Amazon Virtual Private Cloud and Protect Data in Batch Transform Jobs by Using an Amazon Virtual Private Cloud. Defaults to None.
        enable_network_isolation(bool, optional): Isolates the model container. No inbound or outbound network calls can be made to or from the model container. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.model.init.create(
                ctx, model_name=value, execution_role_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.model.init.create model_name=value, execution_role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_model",
        op_kwargs={
            "ModelName": model_name,
            "PrimaryContainer": primary_container,
            "Containers": containers,
            "InferenceExecutionConfig": inference_execution_config,
            "ExecutionRoleArn": execution_role_arn,
            "Tags": tags,
            "VpcConfig": vpc_config,
            "EnableNetworkIsolation": enable_network_isolation,
        },
    )


async def delete(hub, ctx, model_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a model. The DeleteModel API deletes only the model entry that was created in Amazon SageMaker when you called the CreateModel API. It does not delete model artifacts, inference code, or the IAM role that you specified when creating the model.

    Args:
        model_name(str): The name of the model to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.model.init.delete(ctx, model_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.model.init.delete model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="delete_model",
        op_kwargs={"ModelName": model_name},
    )


async def describe(hub, ctx, model_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes a model that you created using the CreateModel API.

    Args:
        model_name(str): The name of the model.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.model.init.describe(ctx, model_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.model.init.describe model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_model",
        op_kwargs={"ModelName": model_name},
    )


async def list_all(
    hub,
    ctx,
    sort_by: str = None,
    sort_order: str = None,
    next_token: str = None,
    max_results: int = None,
    name_contains: str = None,
    creation_time_before: str = None,
    creation_time_after: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists models created with the CreateModel API.

    Args:
        sort_by(str, optional): Sorts the list of results. The default is CreationTime. Defaults to None.
        sort_order(str, optional): The sort order for results. The default is Descending. Defaults to None.
        next_token(str, optional): If the response to a previous ListModels request was truncated, the response includes a NextToken. To retrieve the next set of models, use the token in the next request. Defaults to None.
        max_results(int, optional): The maximum number of models to return in the response. Defaults to None.
        name_contains(str, optional): A string in the training job name. This filter returns only models in the training job whose name contains the specified string. Defaults to None.
        creation_time_before(str, optional): A filter that returns only models created before the specified time (timestamp). Defaults to None.
        creation_time_after(str, optional): A filter that returns only models with a creation time greater than or equal to the specified time (timestamp). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.model.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.model.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_models",
        op_kwargs={
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "NextToken": next_token,
            "MaxResults": max_results,
            "NameContains": name_contains,
            "CreationTimeBefore": creation_time_before,
            "CreationTimeAfter": creation_time_after,
        },
    )
