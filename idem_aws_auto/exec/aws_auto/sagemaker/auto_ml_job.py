"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    auto_ml_job_name: str,
    input_data_config: List,
    output_data_config: Dict,
    role_arn: str,
    problem_type: str = None,
    auto_ml_job_objective: Dict = None,
    auto_ml_job_config: Dict = None,
    generate_candidate_definitions_only: bool = None,
    tags: List = None,
    model_deploy_config: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an Autopilot job. Find the best performing model after you run an Autopilot job by calling . For information about how to use Autopilot, see Automate Model Development with Amazon SageMaker Autopilot.

    Args:
        auto_ml_job_name(str): Identifies an Autopilot job. The name must be unique to your account and is case-insensitive.
        input_data_config(List): An array of channel objects that describes the input data and its location. Each channel is a named input source. Similar to InputDataConfig supported by . Format(s) supported: CSV. Minimum of 500 rows.
        output_data_config(Dict): Provides information about encryption and the Amazon S3 output path needed to store artifacts from an AutoML job. Format(s) supported: CSV.
        problem_type(str, optional): Defines the type of supervised learning available for the candidates. Options include: BinaryClassification, MulticlassClassification, and Regression. For more information, see  Amazon SageMaker Autopilot problem types and algorithm support. Defaults to None.
        auto_ml_job_objective(Dict, optional): Defines the objective metric used to measure the predictive quality of an AutoML job. You provide an AutoMLJobObjective$MetricName and Autopilot infers whether to minimize or maximize it. Defaults to None.
        auto_ml_job_config(Dict, optional): Contains CompletionCriteria and SecurityConfig settings for the AutoML job. Defaults to None.
        role_arn(str): The ARN of the role that is used to access the data.
        generate_candidate_definitions_only(bool, optional): Generates possible candidates without training the models. A candidate is a combination of data preprocessors, algorithms, and algorithm parameter settings. Defaults to None.
        tags(List, optional): Each tag consists of a key and an optional value. Tag keys must be unique per resource. Defaults to None.
        model_deploy_config(Dict, optional): Specifies how to generate the endpoint name for an automatic one-click Autopilot model deployment. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.auto_ml_job.create(
                ctx,
                auto_ml_job_name=value,
                input_data_config=value,
                output_data_config=value,
                role_arn=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.auto_ml_job.create auto_ml_job_name=value, input_data_config=value, output_data_config=value, role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_auto_ml_job",
        op_kwargs={
            "AutoMLJobName": auto_ml_job_name,
            "InputDataConfig": input_data_config,
            "OutputDataConfig": output_data_config,
            "ProblemType": problem_type,
            "AutoMLJobObjective": auto_ml_job_objective,
            "AutoMLJobConfig": auto_ml_job_config,
            "RoleArn": role_arn,
            "GenerateCandidateDefinitionsOnly": generate_candidate_definitions_only,
            "Tags": tags,
            "ModelDeployConfig": model_deploy_config,
        },
    )


async def describe(hub, ctx, auto_ml_job_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about an Amazon SageMaker AutoML job.

    Args:
        auto_ml_job_name(str): Requests information about an AutoML job using its unique name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.auto_ml_job.describe(ctx, auto_ml_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.auto_ml_job.describe auto_ml_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_auto_ml_job",
        op_kwargs={"AutoMLJobName": auto_ml_job_name},
    )


async def list_all(
    hub,
    ctx,
    creation_time_after: str = None,
    creation_time_before: str = None,
    last_modified_time_after: str = None,
    last_modified_time_before: str = None,
    name_contains: str = None,
    status_equals: str = None,
    sort_order: str = None,
    sort_by: str = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Request a list of jobs.

    Args:
        creation_time_after(str, optional): Request a list of jobs, using a filter for time. Defaults to None.
        creation_time_before(str, optional): Request a list of jobs, using a filter for time. Defaults to None.
        last_modified_time_after(str, optional): Request a list of jobs, using a filter for time. Defaults to None.
        last_modified_time_before(str, optional): Request a list of jobs, using a filter for time. Defaults to None.
        name_contains(str, optional): Request a list of jobs, using a search filter for name. Defaults to None.
        status_equals(str, optional): Request a list of jobs, using a filter for status. Defaults to None.
        sort_order(str, optional): The sort order for the results. The default is Descending. Defaults to None.
        sort_by(str, optional): The parameter by which to sort the results. The default is Name. Defaults to None.
        max_results(int, optional): Request a list of jobs up to a specified limit. Defaults to None.
        next_token(str, optional): If the previous response was truncated, you receive this token. Use it in your next request to receive the next set of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.auto_ml_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.auto_ml_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_auto_ml_jobs",
        op_kwargs={
            "CreationTimeAfter": creation_time_after,
            "CreationTimeBefore": creation_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "NameContains": name_contains,
            "StatusEquals": status_equals,
            "SortOrder": sort_order,
            "SortBy": sort_by,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )


async def stop(hub, ctx, auto_ml_job_name: str) -> None:
    r"""
    **Autogenerated function**

    A method for forcing the termination of a running job.

    Args:
        auto_ml_job_name(str): The name of the object you are requesting.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.auto_ml_job.stop(ctx, auto_ml_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.auto_ml_job.stop auto_ml_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="stop_auto_ml_job",
        op_kwargs={"AutoMLJobName": auto_ml_job_name},
    )
