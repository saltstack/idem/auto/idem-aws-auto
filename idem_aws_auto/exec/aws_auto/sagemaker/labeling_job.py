"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    labeling_job_name: str,
    label_attribute_name: str,
    input_config: Dict,
    output_config: Dict,
    role_arn: str,
    human_task_config: Dict,
    label_category_config_s3_uri: str = None,
    stopping_conditions: Dict = None,
    labeling_job_algorithms_config: Dict = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a job that uses workers to label the data objects in your input dataset. You can use the labeled data to train machine learning models.  You can select your workforce from one of three providers:   A private workforce that you create. It can include employees, contractors, and outside experts. Use a private workforce when want the data to stay within your organization or when a specific set of skills is required.   One or more vendors that you select from the Amazon Web Services Marketplace. Vendors provide expertise in specific areas.    The Amazon Mechanical Turk workforce. This is the largest workforce, but it should only be used for public data or data that has been stripped of any personally identifiable information.   You can also use automated data labeling to reduce the number of data objects that need to be labeled by a human. Automated data labeling uses active learning to determine if a data object can be labeled by machine or if it needs to be sent to a human worker. For more information, see Using Automated Data Labeling. The data objects to be labeled are contained in an Amazon S3 bucket. You create a manifest file that describes the location of each object. For more information, see Using Input and Output Data. The output can be used as the manifest file for another labeling job or as training data for your machine learning models. You can use this operation to create a static labeling job or a streaming labeling job. A static labeling job stops if all data objects in the input manifest file identified in ManifestS3Uri have been labeled. A streaming labeling job runs perpetually until it is manually stopped, or remains idle for 10 days. You can send new data objects to an active (InProgress) streaming labeling job in real time. To learn how to create a static labeling job, see Create a Labeling Job (API)  in the Amazon SageMaker Developer Guide. To learn how to create a streaming labeling job, see Create a Streaming Labeling Job.

    Args:
        labeling_job_name(str): The name of the labeling job. This name is used to identify the job in a list of labeling jobs. Labeling job names must be unique within an Amazon Web Services account and region. LabelingJobName is not case sensitive. For example, Example-job and example-job are considered the same labeling job name by Ground Truth.
        label_attribute_name(str): The attribute name to use for the label in the output manifest file. This is the key for the key/value pair formed with the label that a worker assigns to the object. The LabelAttributeName must meet the following requirements.   The name can't end with "-metadata".    If you are using one of the following built-in task types, the attribute name must end with "-ref". If the task type you are using is not listed below, the attribute name must not end with "-ref".   Image semantic segmentation (SemanticSegmentation), and adjustment (AdjustmentSemanticSegmentation) and verification (VerificationSemanticSegmentation) labeling jobs for this task type.   Video frame object detection (VideoObjectDetection), and adjustment and verification (AdjustmentVideoObjectDetection) labeling jobs for this task type.   Video frame object tracking (VideoObjectTracking), and adjustment and verification (AdjustmentVideoObjectTracking) labeling jobs for this task type.   3D point cloud semantic segmentation (3DPointCloudSemanticSegmentation), and adjustment and verification (Adjustment3DPointCloudSemanticSegmentation) labeling jobs for this task type.    3D point cloud object tracking (3DPointCloudObjectTracking), and adjustment and verification (Adjustment3DPointCloudObjectTracking) labeling jobs for this task type.        If you are creating an adjustment or verification labeling job, you must use a different LabelAttributeName than the one used in the original labeling job. The original labeling job is the Ground Truth labeling job that produced the labels that you want verified or adjusted. To learn more about adjustment and verification labeling jobs, see Verify and Adjust Labels. .
        input_config(Dict): Input data for the labeling job, such as the Amazon S3 location of the data objects and the location of the manifest file that describes the data objects. You must specify at least one of the following: S3DataSource or SnsDataSource.    Use SnsDataSource to specify an SNS input topic for a streaming labeling job. If you do not specify and SNS input topic ARN, Ground Truth will create a one-time labeling job that stops after all data objects in the input manifest file have been labeled.   Use S3DataSource to specify an input manifest file for both streaming and one-time labeling jobs. Adding an S3DataSource is optional if you use SnsDataSource to create a streaming labeling job.   If you use the Amazon Mechanical Turk workforce, your input data should not include confidential information, personal information or protected health information. Use ContentClassifiers to specify that your data is free of personally identifiable information and adult content.
        output_config(Dict): The location of the output data and the Amazon Web Services Key Management Service key ID for the key used to encrypt the output data, if any.
        role_arn(str): The Amazon Resource Number (ARN) that Amazon SageMaker assumes to perform tasks on your behalf during data labeling. You must grant this role the necessary permissions so that Amazon SageMaker can successfully complete data labeling.
        label_category_config_s3_uri(str, optional): The S3 URI of the file, referred to as a label category configuration file, that defines the categories used to label the data objects. For 3D point cloud and video frame task types, you can add label category attributes and frame attributes to your label category configuration file. To learn how, see Create a Labeling Category Configuration File for 3D Point Cloud Labeling Jobs.  For all other built-in task types and custom tasks, your label category configuration file must be a JSON file in the following format. Identify the labels you want to use by replacing label_1, label_2,...,label_n with your label categories.  {    "document-version": "2018-11-28",   "labels": [{"label": "label_1"},{"label": "label_2"},...{"label": "label_n"}]   }  Note the following about the label category configuration file:   For image classification and text classification (single and multi-label) you must specify at least two label categories. For all other task types, the minimum number of label categories required is one.    Each label category must be unique, you cannot specify duplicate label categories.   If you create a 3D point cloud or video frame adjustment or verification labeling job, you must include auditLabelAttributeName in the label category configuration. Use this parameter to enter the  LabelAttributeName  of the labeling job you want to adjust or verify annotations of.  . Defaults to None.
        stopping_conditions(Dict, optional): A set of conditions for stopping the labeling job. If any of the conditions are met, the job is automatically stopped. You can use these conditions to control the cost of data labeling. Defaults to None.
        labeling_job_algorithms_config(Dict, optional): Configures the information required to perform automated data labeling. Defaults to None.
        human_task_config(Dict): Configures the labeling task and how it is presented to workers; including, but not limited to price, keywords, and batch size (task count).
        tags(List, optional): An array of key/value pairs. For more information, see Using Cost Allocation Tags in the Amazon Web Services Billing and Cost Management User Guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.labeling_job.create(
                ctx,
                labeling_job_name=value,
                label_attribute_name=value,
                input_config=value,
                output_config=value,
                role_arn=value,
                human_task_config=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.labeling_job.create labeling_job_name=value, label_attribute_name=value, input_config=value, output_config=value, role_arn=value, human_task_config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_labeling_job",
        op_kwargs={
            "LabelingJobName": labeling_job_name,
            "LabelAttributeName": label_attribute_name,
            "InputConfig": input_config,
            "OutputConfig": output_config,
            "RoleArn": role_arn,
            "LabelCategoryConfigS3Uri": label_category_config_s3_uri,
            "StoppingConditions": stopping_conditions,
            "LabelingJobAlgorithmsConfig": labeling_job_algorithms_config,
            "HumanTaskConfig": human_task_config,
            "Tags": tags,
        },
    )


async def describe(hub, ctx, labeling_job_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a labeling job.

    Args:
        labeling_job_name(str): The name of the labeling job to return information for.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.labeling_job.describe(ctx, labeling_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.labeling_job.describe labeling_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_labeling_job",
        op_kwargs={"LabelingJobName": labeling_job_name},
    )


async def list_all(
    hub,
    ctx,
    creation_time_after: str = None,
    creation_time_before: str = None,
    last_modified_time_after: str = None,
    last_modified_time_before: str = None,
    max_results: int = None,
    next_token: str = None,
    name_contains: str = None,
    sort_by: str = None,
    sort_order: str = None,
    status_equals: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of labeling jobs.

    Args:
        creation_time_after(str, optional): A filter that returns only labeling jobs created after the specified time (timestamp). Defaults to None.
        creation_time_before(str, optional): A filter that returns only labeling jobs created before the specified time (timestamp). Defaults to None.
        last_modified_time_after(str, optional): A filter that returns only labeling jobs modified after the specified time (timestamp). Defaults to None.
        last_modified_time_before(str, optional): A filter that returns only labeling jobs modified before the specified time (timestamp). Defaults to None.
        max_results(int, optional): The maximum number of labeling jobs to return in each page of the response. Defaults to None.
        next_token(str, optional): If the result of the previous ListLabelingJobs request was truncated, the response includes a NextToken. To retrieve the next set of labeling jobs, use the token in the next request. Defaults to None.
        name_contains(str, optional): A string in the labeling job name. This filter returns only labeling jobs whose name contains the specified string. Defaults to None.
        sort_by(str, optional): The field to sort results by. The default is CreationTime. Defaults to None.
        sort_order(str, optional): The sort order for results. The default is Ascending. Defaults to None.
        status_equals(str, optional): A filter that retrieves only labeling jobs with a specific status. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.labeling_job.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.labeling_job.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_labeling_jobs",
        op_kwargs={
            "CreationTimeAfter": creation_time_after,
            "CreationTimeBefore": creation_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "MaxResults": max_results,
            "NextToken": next_token,
            "NameContains": name_contains,
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "StatusEquals": status_equals,
        },
    )


async def stop(hub, ctx, labeling_job_name: str) -> None:
    r"""
    **Autogenerated function**

    Stops a running labeling job. A job that is stopped cannot be restarted. Any results obtained before the job is stopped are placed in the Amazon S3 output bucket.

    Args:
        labeling_job_name(str): The name of the labeling job to stop.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.labeling_job.stop(ctx, labeling_job_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.labeling_job.stop labeling_job_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="stop_labeling_job",
        op_kwargs={"LabelingJobName": labeling_job_name},
    )
