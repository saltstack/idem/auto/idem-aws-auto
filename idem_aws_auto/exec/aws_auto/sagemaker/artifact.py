"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    source: Dict,
    artifact_type: str,
    artifact_name: str = None,
    properties: Dict = None,
    metadata_properties: Dict = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an artifact. An artifact is a lineage tracking entity that represents a URI addressable object or data. Some examples are the S3 URI of a dataset and the ECR registry path of an image. For more information, see Amazon SageMaker ML Lineage Tracking.   CreateArtifact can only be invoked from within an SageMaker managed environment. This includes SageMaker training jobs, processing jobs, transform jobs, and SageMaker notebooks. A call to CreateArtifact from outside one of these environments results in an error.

    Args:
        artifact_name(str, optional): The name of the artifact. Must be unique to your account in an Amazon Web Services Region. Defaults to None.
        source(Dict): The ID, ID type, and URI of the source.
        artifact_type(str): The artifact type.
        properties(Dict, optional): A list of properties to add to the artifact. Defaults to None.
        metadata_properties(Dict, optional): Metadata properties of the tracking entity, trial, or trial component. Defaults to None.
        tags(List, optional): A list of tags to apply to the artifact. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.artifact.create(
                ctx, source=value, artifact_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.artifact.create source=value, artifact_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_artifact",
        op_kwargs={
            "ArtifactName": artifact_name,
            "Source": source,
            "ArtifactType": artifact_type,
            "Properties": properties,
            "MetadataProperties": metadata_properties,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, artifact_arn: str = None, source: Dict = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an artifact. Either ArtifactArn or Source must be specified.

    Args:
        artifact_arn(str, optional): The Amazon Resource Name (ARN) of the artifact to delete. Defaults to None.
        source(Dict, optional): The URI of the source. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.artifact.delete(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.artifact.delete
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="delete_artifact",
        op_kwargs={"ArtifactArn": artifact_arn, "Source": source},
    )


async def describe(hub, ctx, artifact_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes an artifact.

    Args:
        artifact_arn(str): The Amazon Resource Name (ARN) of the artifact to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.artifact.describe(ctx, artifact_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.artifact.describe artifact_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_artifact",
        op_kwargs={"ArtifactArn": artifact_arn},
    )


async def list_all(
    hub,
    ctx,
    source_uri: str = None,
    artifact_type: str = None,
    created_after: str = None,
    created_before: str = None,
    sort_by: str = None,
    sort_order: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the artifacts in your account and their properties.

    Args:
        source_uri(str, optional): A filter that returns only artifacts with the specified source URI. Defaults to None.
        artifact_type(str, optional): A filter that returns only artifacts of the specified type. Defaults to None.
        created_after(str, optional): A filter that returns only artifacts created on or after the specified time. Defaults to None.
        created_before(str, optional): A filter that returns only artifacts created on or before the specified time. Defaults to None.
        sort_by(str, optional): The property used to sort results. The default value is CreationTime. Defaults to None.
        sort_order(str, optional): The sort order. The default value is Descending. Defaults to None.
        next_token(str, optional): If the previous call to ListArtifacts didn't return the full set of artifacts, the call returns a token for getting the next set of artifacts. Defaults to None.
        max_results(int, optional): The maximum number of artifacts to return in the response. The default value is 10. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.artifact.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.artifact.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_artifacts",
        op_kwargs={
            "SourceUri": source_uri,
            "ArtifactType": artifact_type,
            "CreatedAfter": created_after,
            "CreatedBefore": created_before,
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    artifact_arn: str,
    artifact_name: str = None,
    properties: Dict = None,
    properties_to_remove: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an artifact.

    Args:
        artifact_arn(str): The Amazon Resource Name (ARN) of the artifact to update.
        artifact_name(str, optional): The new name for the artifact. Defaults to None.
        properties(Dict, optional): The new list of properties. Overwrites the current property list. Defaults to None.
        properties_to_remove(List, optional): A list of properties to remove. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.artifact.update(ctx, artifact_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.artifact.update artifact_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="update_artifact",
        op_kwargs={
            "ArtifactArn": artifact_arn,
            "ArtifactName": artifact_name,
            "Properties": properties,
            "PropertiesToRemove": properties_to_remove,
        },
    )
