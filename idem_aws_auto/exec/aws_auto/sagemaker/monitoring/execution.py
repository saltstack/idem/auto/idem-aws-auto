"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    monitoring_schedule_name: str = None,
    endpoint_name: str = None,
    sort_by: str = None,
    sort_order: str = None,
    next_token: str = None,
    max_results: int = None,
    scheduled_time_before: str = None,
    scheduled_time_after: str = None,
    creation_time_before: str = None,
    creation_time_after: str = None,
    last_modified_time_before: str = None,
    last_modified_time_after: str = None,
    status_equals: str = None,
    monitoring_job_definition_name: str = None,
    monitoring_type_equals: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns list of all monitoring job executions.

    Args:
        monitoring_schedule_name(str, optional): Name of a specific schedule to fetch jobs for. Defaults to None.
        endpoint_name(str, optional): Name of a specific endpoint to fetch jobs for. Defaults to None.
        sort_by(str, optional): Whether to sort results by Status, CreationTime, ScheduledTime field. The default is CreationTime. Defaults to None.
        sort_order(str, optional): Whether to sort the results in Ascending or Descending order. The default is Descending. Defaults to None.
        next_token(str, optional): The token returned if the response is truncated. To retrieve the next set of job executions, use it in the next request. Defaults to None.
        max_results(int, optional): The maximum number of jobs to return in the response. The default value is 10. Defaults to None.
        scheduled_time_before(str, optional): Filter for jobs scheduled before a specified time. Defaults to None.
        scheduled_time_after(str, optional): Filter for jobs scheduled after a specified time. Defaults to None.
        creation_time_before(str, optional): A filter that returns only jobs created before a specified time. Defaults to None.
        creation_time_after(str, optional): A filter that returns only jobs created after a specified time. Defaults to None.
        last_modified_time_before(str, optional): A filter that returns only jobs modified after a specified time. Defaults to None.
        last_modified_time_after(str, optional): A filter that returns only jobs modified before a specified time. Defaults to None.
        status_equals(str, optional): A filter that retrieves only jobs with a specific status. Defaults to None.
        monitoring_job_definition_name(str, optional): Gets a list of the monitoring job runs of the specified monitoring job definitions. Defaults to None.
        monitoring_type_equals(str, optional): A filter that returns only the monitoring job runs of the specified monitoring type. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.monitoring.execution.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.monitoring.execution.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_monitoring_executions",
        op_kwargs={
            "MonitoringScheduleName": monitoring_schedule_name,
            "EndpointName": endpoint_name,
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "NextToken": next_token,
            "MaxResults": max_results,
            "ScheduledTimeBefore": scheduled_time_before,
            "ScheduledTimeAfter": scheduled_time_after,
            "CreationTimeBefore": creation_time_before,
            "CreationTimeAfter": creation_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
            "StatusEquals": status_equals,
            "MonitoringJobDefinitionName": monitoring_job_definition_name,
            "MonitoringTypeEquals": monitoring_type_equals,
        },
    )
