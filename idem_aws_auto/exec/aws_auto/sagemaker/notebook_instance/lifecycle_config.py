"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    notebook_instance_lifecycle_config_name: str,
    on_create: List = None,
    on_start: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a lifecycle configuration that you can associate with a notebook instance. A lifecycle configuration is a collection of shell scripts that run when you create or start a notebook instance. Each lifecycle configuration script has a limit of 16384 characters. The value of the $PATH environment variable that is available to both scripts is /sbin:bin:/usr/sbin:/usr/bin. View CloudWatch Logs for notebook instance lifecycle configurations in log group /aws/sagemaker/NotebookInstances in log stream [notebook-instance-name]/[LifecycleConfigHook]. Lifecycle configuration scripts cannot run for longer than 5 minutes. If a script runs for longer than 5 minutes, it fails and the notebook instance is not created or started. For information about notebook instance lifestyle configurations, see Step 2.1: (Optional) Customize a Notebook Instance.

    Args:
        notebook_instance_lifecycle_config_name(str): The name of the lifecycle configuration.
        on_create(List, optional): A shell script that runs only once, when you create a notebook instance. The shell script must be a base64-encoded string. Defaults to None.
        on_start(List, optional): A shell script that runs every time you start a notebook instance, including when you create the notebook instance. The shell script must be a base64-encoded string. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.notebook_instance.lifecycle_config.create(
                ctx, notebook_instance_lifecycle_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.notebook_instance.lifecycle_config.create notebook_instance_lifecycle_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="create_notebook_instance_lifecycle_config",
        op_kwargs={
            "NotebookInstanceLifecycleConfigName": notebook_instance_lifecycle_config_name,
            "OnCreate": on_create,
            "OnStart": on_start,
        },
    )


async def delete(hub, ctx, notebook_instance_lifecycle_config_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a notebook instance lifecycle configuration.

    Args:
        notebook_instance_lifecycle_config_name(str): The name of the lifecycle configuration to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.notebook_instance.lifecycle_config.delete(
                ctx, notebook_instance_lifecycle_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.notebook_instance.lifecycle_config.delete notebook_instance_lifecycle_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="delete_notebook_instance_lifecycle_config",
        op_kwargs={
            "NotebookInstanceLifecycleConfigName": notebook_instance_lifecycle_config_name
        },
    )


async def describe(hub, ctx, notebook_instance_lifecycle_config_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns a description of a notebook instance lifecycle configuration. For information about notebook instance lifestyle configurations, see Step 2.1: (Optional) Customize a Notebook Instance.

    Args:
        notebook_instance_lifecycle_config_name(str): The name of the lifecycle configuration to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.notebook_instance.lifecycle_config.describe(
                ctx, notebook_instance_lifecycle_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.notebook_instance.lifecycle_config.describe notebook_instance_lifecycle_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="describe_notebook_instance_lifecycle_config",
        op_kwargs={
            "NotebookInstanceLifecycleConfigName": notebook_instance_lifecycle_config_name
        },
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    sort_by: str = None,
    sort_order: str = None,
    name_contains: str = None,
    creation_time_before: str = None,
    creation_time_after: str = None,
    last_modified_time_before: str = None,
    last_modified_time_after: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists notebook instance lifestyle configurations created with the CreateNotebookInstanceLifecycleConfig API.

    Args:
        next_token(str, optional): If the result of a ListNotebookInstanceLifecycleConfigs request was truncated, the response includes a NextToken. To get the next set of lifecycle configurations, use the token in the next request. Defaults to None.
        max_results(int, optional): The maximum number of lifecycle configurations to return in the response. Defaults to None.
        sort_by(str, optional): Sorts the list of results. The default is CreationTime. Defaults to None.
        sort_order(str, optional): The sort order for results. Defaults to None.
        name_contains(str, optional): A string in the lifecycle configuration name. This filter returns only lifecycle configurations whose name contains the specified string. Defaults to None.
        creation_time_before(str, optional): A filter that returns only lifecycle configurations that were created before the specified time (timestamp). Defaults to None.
        creation_time_after(str, optional): A filter that returns only lifecycle configurations that were created after the specified time (timestamp). Defaults to None.
        last_modified_time_before(str, optional): A filter that returns only lifecycle configurations that were modified before the specified time (timestamp). Defaults to None.
        last_modified_time_after(str, optional): A filter that returns only lifecycle configurations that were modified after the specified time (timestamp). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.notebook_instance.lifecycle_config.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.notebook_instance.lifecycle_config.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="list_notebook_instance_lifecycle_configs",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "SortBy": sort_by,
            "SortOrder": sort_order,
            "NameContains": name_contains,
            "CreationTimeBefore": creation_time_before,
            "CreationTimeAfter": creation_time_after,
            "LastModifiedTimeBefore": last_modified_time_before,
            "LastModifiedTimeAfter": last_modified_time_after,
        },
    )


async def update(
    hub,
    ctx,
    notebook_instance_lifecycle_config_name: str,
    on_create: List = None,
    on_start: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a notebook instance lifecycle configuration created with the CreateNotebookInstanceLifecycleConfig API.

    Args:
        notebook_instance_lifecycle_config_name(str): The name of the lifecycle configuration.
        on_create(List, optional): The shell script that runs only once, when you create a notebook instance. The shell script must be a base64-encoded string. Defaults to None.
        on_start(List, optional): The shell script that runs every time you start a notebook instance, including when you create the notebook instance. The shell script must be a base64-encoded string. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sagemaker.notebook_instance.lifecycle_config.update(
                ctx, notebook_instance_lifecycle_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sagemaker.notebook_instance.lifecycle_config.update notebook_instance_lifecycle_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sagemaker",
        operation="update_notebook_instance_lifecycle_config",
        op_kwargs={
            "NotebookInstanceLifecycleConfigName": notebook_instance_lifecycle_config_name,
            "OnCreate": on_create,
            "OnStart": on_start,
        },
    )
