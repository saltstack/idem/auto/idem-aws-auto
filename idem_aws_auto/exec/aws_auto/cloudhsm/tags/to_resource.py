"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def add(hub, ctx, resource_arn: str, tag_list: List) -> Dict:
    r"""
    **Autogenerated function**

    This is documentation for AWS CloudHSM Classic. For more information, see AWS CloudHSM Classic FAQs, the AWS CloudHSM Classic User Guide, and the AWS CloudHSM Classic API Reference.  For information about the current version of AWS CloudHSM, see AWS CloudHSM, the AWS CloudHSM User Guide, and the AWS CloudHSM API Reference. Adds or overwrites one or more tags for the specified AWS CloudHSM resource. Each tag consists of a key and a value. Tag keys must be unique to each resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the AWS CloudHSM resource to tag.
        tag_list(List): One or more tags.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cloudhsm.tags.to_resource.add(
                ctx, resource_arn=value, tag_list=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cloudhsm.tags.to_resource.add resource_arn=value, tag_list=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cloudhsm",
        operation="add_tags_to_resource",
        op_kwargs={"ResourceArn": resource_arn, "TagList": tag_list},
    )
