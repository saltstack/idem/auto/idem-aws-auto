"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, resource_name: str, date: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an automatic snapshot of an instance or disk. For more information, see the Amazon Lightsail Developer Guide.

    Args:
        resource_name(str): The name of the source instance or disk from which to delete the automatic snapshot.
        date(str): The date of the automatic snapshot to delete in YYYY-MM-DD format. Use the get auto snapshots operation to get the available automatic snapshots for a resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.auto_snapshot.delete(
                ctx, resource_name=value, date=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.auto_snapshot.delete resource_name=value, date=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="delete_auto_snapshot",
        op_kwargs={"resourceName": resource_name, "date": date},
    )


async def get_all(hub, ctx, resource_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the available automatic snapshots for an instance or disk. For more information, see the Amazon Lightsail Developer Guide.

    Args:
        resource_name(str): The name of the source instance or disk from which to get automatic snapshot information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.auto_snapshot.get_all(ctx, resource_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.auto_snapshot.get_all resource_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="get_auto_snapshots",
        op_kwargs={"resourceName": resource_name},
    )
