"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(
    hub,
    ctx,
    relational_database_name: str,
    duration_in_minutes: int = None,
    page_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of events for a specific database in Amazon Lightsail.

    Args:
        relational_database_name(str): The name of the database from which to get events.
        duration_in_minutes(int, optional): The number of minutes in the past from which to retrieve events. For example, to get all events from the past 2 hours, enter 120. Default: 60  The minimum is 1 and the maximum is 14 days (20160 minutes). Defaults to None.
        page_token(str, optional): The token to advance to the next page of results from your request. To get a page token, perform an initial GetRelationalDatabaseEvents request. If your results are paginated, the response will return a next page token that you can specify as the page token in a subsequent request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.relational_database.event.get_all(
                ctx, relational_database_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.relational_database.event.get_all relational_database_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="get_relational_database_events",
        op_kwargs={
            "relationalDatabaseName": relational_database_name,
            "durationInMinutes": duration_in_minutes,
            "pageToken": page_token,
        },
    )
