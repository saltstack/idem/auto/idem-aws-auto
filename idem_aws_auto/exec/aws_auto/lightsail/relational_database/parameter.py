"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get_all(
    hub, ctx, relational_database_name: str, page_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns all of the runtime parameters offered by the underlying database software, or engine, for a specific database in Amazon Lightsail. In addition to the parameter names and values, this operation returns other information about each parameter. This information includes whether changes require a reboot, whether the parameter is modifiable, the allowed values, and the data types.

    Args:
        relational_database_name(str): The name of your database for which to get parameters.
        page_token(str, optional): The token to advance to the next page of results from your request. To get a page token, perform an initial GetRelationalDatabaseParameters request. If your results are paginated, the response will return a next page token that you can specify as the page token in a subsequent request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.relational_database.parameter.get_all(
                ctx, relational_database_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.relational_database.parameter.get_all relational_database_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="get_relational_database_parameters",
        op_kwargs={
            "relationalDatabaseName": relational_database_name,
            "pageToken": page_token,
        },
    )


async def update_multiple(
    hub, ctx, relational_database_name: str, parameters: List
) -> Dict:
    r"""
    **Autogenerated function**

    Allows the update of one or more parameters of a database in Amazon Lightsail. Parameter updates don't cause outages; therefore, their application is not subject to the preferred maintenance window. However, there are two ways in which parameter updates are applied: dynamic or pending-reboot. Parameters marked with a dynamic apply type are applied immediately. Parameters marked with a pending-reboot apply type are applied only after the database is rebooted using the reboot relational database operation. The update relational database parameters operation supports tag-based access control via resource tags applied to the resource identified by relationalDatabaseName. For more information, see the Amazon Lightsail Developer Guide.

    Args:
        relational_database_name(str): The name of your database for which to update parameters.
        parameters(List): The database parameters to update.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.relational_database.parameter.update_multiple(
                ctx, relational_database_name=value, parameters=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.relational_database.parameter.update_multiple relational_database_name=value, parameters=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="update_relational_database_parameters",
        op_kwargs={
            "relationalDatabaseName": relational_database_name,
            "parameters": parameters,
        },
    )
