"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, page_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns the CloudFormation stack record created as a result of the create cloud formation stack operation. An AWS CloudFormation stack is used to create a new Amazon EC2 instance from an exported Lightsail snapshot.

    Args:
        page_token(str, optional): The token to advance to the next page of results from your request. To get a page token, perform an initial GetClouFormationStackRecords request. If your results are paginated, the response will return a next page token that you can specify as the page token in a subsequent request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.cloud_formation_stack.record.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.cloud_formation_stack.record.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="get_cloud_formation_stack_records",
        op_kwargs={"pageToken": page_token},
    )
