"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def detach(hub, ctx, distribution_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Detaches an SSL/TLS certificate from your Amazon Lightsail content delivery network (CDN) distribution. After the certificate is detached, your distribution stops accepting traffic for all of the domains that are associated with the certificate.

    Args:
        distribution_name(str): The name of the distribution from which to detach the certificate. Use the GetDistributions action to get a list of distribution names that you can specify.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lightsail.certificate.from_distribution.detach(
                ctx, distribution_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lightsail.certificate.from_distribution.detach distribution_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lightsail",
        operation="detach_certificate_from_distribution",
        op_kwargs={"distributionName": distribution_name},
    )
