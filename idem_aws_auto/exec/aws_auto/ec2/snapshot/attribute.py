"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe(
    hub, ctx, attribute: str, snapshot_id: str, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the specified attribute of the specified snapshot. You can specify only one attribute at a time. For more information about EBS snapshots, see Amazon EBS snapshots in the Amazon Elastic Compute Cloud User Guide.

    Args:
        attribute(str): The snapshot attribute you would like to view.
        snapshot_id(str): The ID of the EBS snapshot.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.snapshot.attribute.describe(
                ctx, attribute=value, snapshot_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.snapshot.attribute.describe attribute=value, snapshot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_snapshot_attribute",
        op_kwargs={
            "Attribute": attribute,
            "SnapshotId": snapshot_id,
            "DryRun": dry_run,
        },
    )


async def modify(
    hub,
    ctx,
    snapshot_id: str,
    attribute: str = None,
    create_volume_permission: Dict = None,
    group_names: List = None,
    operation_type: str = None,
    user_ids: List = None,
    dry_run: bool = None,
) -> None:
    r"""
    **Autogenerated function**

    Adds or removes permission settings for the specified snapshot. You may add or remove specified Amazon Web Services account IDs from a snapshot's list of create volume permissions, but you cannot do both in a single operation. If you need to both add and remove account IDs for a snapshot, you must use multiple operations. You can make up to 500 modifications to a snapshot in a single operation. Encrypted snapshots and snapshots with Amazon Web Services Marketplace product codes cannot be made public. Snapshots encrypted with your default KMS key cannot be shared with other accounts. For more information about modifying snapshot permissions, see Share a snapshot in the Amazon Elastic Compute Cloud User Guide.

    Args:
        attribute(str, optional): The snapshot attribute to modify. Only volume creation permissions can be modified. Defaults to None.
        create_volume_permission(Dict, optional): A JSON representation of the snapshot attribute modification. Defaults to None.
        group_names(List, optional): The group to modify for the snapshot. Defaults to None.
        operation_type(str, optional): The type of operation to perform to the attribute. Defaults to None.
        snapshot_id(str): The ID of the snapshot.
        user_ids(List, optional): The account ID to modify for the snapshot. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.snapshot.attribute.modify(ctx, snapshot_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.snapshot.attribute.modify snapshot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="modify_snapshot_attribute",
        op_kwargs={
            "Attribute": attribute,
            "CreateVolumePermission": create_volume_permission,
            "GroupNames": group_names,
            "OperationType": operation_type,
            "SnapshotId": snapshot_id,
            "UserIds": user_ids,
            "DryRun": dry_run,
        },
    )


async def reset(
    hub, ctx, attribute: str, snapshot_id: str, dry_run: bool = None
) -> None:
    r"""
    **Autogenerated function**

    Resets permission settings for the specified snapshot. For more information about modifying snapshot permissions, see Share a snapshot in the Amazon Elastic Compute Cloud User Guide.

    Args:
        attribute(str): The attribute to reset. Currently, only the attribute for permission to create volumes can be reset.
        snapshot_id(str): The ID of the snapshot.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.snapshot.attribute.reset(
                ctx, attribute=value, snapshot_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.snapshot.attribute.reset attribute=value, snapshot_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="reset_snapshot_attribute",
        op_kwargs={
            "Attribute": attribute,
            "SnapshotId": snapshot_id,
            "DryRun": dry_run,
        },
    )
