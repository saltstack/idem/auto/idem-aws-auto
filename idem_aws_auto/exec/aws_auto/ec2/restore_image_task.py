"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    bucket: str,
    object_key: str,
    name: str = None,
    tag_specifications: List = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Starts a task that restores an AMI from an S3 object that was previously created by using CreateStoreImageTask. To use this API, you must have the required permissions. For more information, see Permissions for storing and restoring AMIs using S3 in the Amazon Elastic Compute Cloud User Guide. For more information, see Store and restore an AMI using S3 in the Amazon Elastic Compute Cloud User Guide.

    Args:
        bucket(str): The name of the S3 bucket that contains the stored AMI object.
        object_key(str): The name of the stored AMI object in the bucket.
        name(str, optional): The name for the restored AMI. The name must be unique for AMIs in the Region for this account. If you do not provide a name, the new AMI gets the same name as the original AMI. Defaults to None.
        tag_specifications(List, optional): The tags to apply to the AMI and snapshots on restoration. You can tag the AMI, the snapshots, or both.   To tag the AMI, the value for ResourceType must be image.   To tag the snapshots, the value for ResourceType must be snapshot. The same tag is applied to all of the snapshots that are created.  . Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.restore_image_task.create(
                ctx, bucket=value, object_key=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.restore_image_task.create bucket=value, object_key=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="create_restore_image_task",
        op_kwargs={
            "Bucket": bucket,
            "ObjectKey": object_key,
            "Name": name,
            "TagSpecifications": tag_specifications,
            "DryRun": dry_run,
        },
    )
