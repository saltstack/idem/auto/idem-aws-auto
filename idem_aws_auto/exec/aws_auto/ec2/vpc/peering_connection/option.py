"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def modify(
    hub,
    ctx,
    vpc_peering_connection_id: str,
    accepter_peering_connection_options: Dict = None,
    dry_run: bool = None,
    requester_peering_connection_options: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the VPC peering connection options on one side of a VPC peering connection. You can do the following:   Enable/disable communication over the peering connection between an EC2-Classic instance that's linked to your VPC (using ClassicLink) and instances in the peer VPC.   Enable/disable communication over the peering connection between instances in your VPC and an EC2-Classic instance that's linked to the peer VPC.   Enable/disable the ability to resolve public DNS hostnames to private IP addresses when queried from instances in the peer VPC.   If the peered VPCs are in the same Amazon Web Services account, you can enable DNS resolution for queries from the local VPC. This ensures that queries from the local VPC resolve to private IP addresses in the peer VPC. This option is not available if the peered VPCs are in different different Amazon Web Services accounts or different Regions. For peered VPCs in different Amazon Web Services accounts, each Amazon Web Services account owner must initiate a separate request to modify the peering connection options. For inter-region peering connections, you must use the Region for the requester VPC to modify the requester VPC peering options and the Region for the accepter VPC to modify the accepter VPC peering options. To verify which VPCs are the accepter and the requester for a VPC peering connection, use the DescribeVpcPeeringConnections command.

    Args:
        accepter_peering_connection_options(Dict, optional): The VPC peering connection options for the accepter VPC. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        requester_peering_connection_options(Dict, optional): The VPC peering connection options for the requester VPC. Defaults to None.
        vpc_peering_connection_id(str): The ID of the VPC peering connection.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpc.peering_connection.option.modify(
                ctx, vpc_peering_connection_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpc.peering_connection.option.modify vpc_peering_connection_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="modify_vpc_peering_connection_options",
        op_kwargs={
            "AccepterPeeringConnectionOptions": accepter_peering_connection_options,
            "DryRun": dry_run,
            "RequesterPeeringConnectionOptions": requester_peering_connection_options,
            "VpcPeeringConnectionId": vpc_peering_connection_id,
        },
    )
