"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def modify(
    hub,
    ctx,
    vpn_connection_id: str,
    vpn_tunnel_outside_ip_address: str,
    tunnel_options: Dict,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the options for a VPN tunnel in an AWS Site-to-Site VPN connection. You can modify multiple options for a tunnel in a single request, but you can only modify one tunnel at a time. For more information, see Site-to-Site VPN Tunnel Options for Your Site-to-Site VPN Connection in the AWS Site-to-Site VPN User Guide.

    Args:
        vpn_connection_id(str): The ID of the AWS Site-to-Site VPN connection.
        vpn_tunnel_outside_ip_address(str): The external IP address of the VPN tunnel.
        tunnel_options(Dict): The tunnel options to modify.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.tunnel.option.modify(
                ctx,
                vpn_connection_id=value,
                vpn_tunnel_outside_ip_address=value,
                tunnel_options=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.tunnel.option.modify vpn_connection_id=value, vpn_tunnel_outside_ip_address=value, tunnel_options=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="modify_vpn_tunnel_options",
        op_kwargs={
            "VpnConnectionId": vpn_connection_id,
            "VpnTunnelOutsideIpAddress": vpn_tunnel_outside_ip_address,
            "TunnelOptions": tunnel_options,
            "DryRun": dry_run,
        },
    )
