"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def attach(
    hub, ctx, vpc_id: str, vpn_gateway_id: str, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Attaches a virtual private gateway to a VPC. You can attach one virtual private gateway to one VPC at a time. For more information, see AWS Site-to-Site VPN in the AWS Site-to-Site VPN User Guide.

    Args:
        vpc_id(str): The ID of the VPC.
        vpn_gateway_id(str): The ID of the virtual private gateway.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.gateway.attach(ctx, vpc_id=value, vpn_gateway_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.gateway.attach vpc_id=value, vpn_gateway_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="attach_vpn_gateway",
        op_kwargs={"VpcId": vpc_id, "VpnGatewayId": vpn_gateway_id, "DryRun": dry_run},
    )


async def create(
    hub,
    ctx,
    type_: str,
    availability_zone: str = None,
    tag_specifications: List = None,
    amazon_side_asn: int = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a virtual private gateway. A virtual private gateway is the endpoint on the VPC side of your VPN connection. You can create a virtual private gateway before creating the VPC itself. For more information, see AWS Site-to-Site VPN in the AWS Site-to-Site VPN User Guide.

    Args:
        availability_zone(str, optional): The Availability Zone for the virtual private gateway. Defaults to None.
        type_(str): The type of VPN connection this virtual private gateway supports.
        tag_specifications(List, optional): The tags to apply to the virtual private gateway. Defaults to None.
        amazon_side_asn(int, optional): A private Autonomous System Number (ASN) for the Amazon side of a BGP session. If you're using a 16-bit ASN, it must be in the 64512 to 65534 range. If you're using a 32-bit ASN, it must be in the 4200000000 to 4294967294 range. Default: 64512. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.gateway.create(ctx, type_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.gateway.create type_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="create_vpn_gateway",
        op_kwargs={
            "AvailabilityZone": availability_zone,
            "Type": type_,
            "TagSpecifications": tag_specifications,
            "AmazonSideAsn": amazon_side_asn,
            "DryRun": dry_run,
        },
    )


async def delete(hub, ctx, vpn_gateway_id: str, dry_run: bool = None) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified virtual private gateway. You must first detach the virtual private gateway from the VPC. Note that you don't need to delete the virtual private gateway if you plan to delete and recreate the VPN connection between your VPC and your network.

    Args:
        vpn_gateway_id(str): The ID of the virtual private gateway.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.gateway.delete(ctx, vpn_gateway_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.gateway.delete vpn_gateway_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="delete_vpn_gateway",
        op_kwargs={"VpnGatewayId": vpn_gateway_id, "DryRun": dry_run},
    )


async def describe_all(
    hub, ctx, filters: List = None, vpn_gateway_ids: List = None, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes one or more of your virtual private gateways. For more information, see AWS Site-to-Site VPN in the AWS Site-to-Site VPN User Guide.

    Args:
        filters(List, optional): One or more filters.    amazon-side-asn - The Autonomous System Number (ASN) for the Amazon side of the gateway.    attachment.state - The current state of the attachment between the gateway and the VPC (attaching | attached | detaching | detached).    attachment.vpc-id - The ID of an attached VPC.    availability-zone - The Availability Zone for the virtual private gateway (if applicable).    state - The state of the virtual private gateway (pending | available | deleting | deleted).    tag:<key> - The key/value combination of a tag assigned to the resource. Use the tag key in the filter name and the tag value as the filter value. For example, to find all resources that have a tag with the key Owner and the value TeamA, specify tag:Owner for the filter name and TeamA for the filter value.    tag-key - The key of a tag assigned to the resource. Use this filter to find all resources assigned a tag with a specific key, regardless of the tag value.    type - The type of virtual private gateway. Currently the only supported type is ipsec.1.    vpn-gateway-id - The ID of the virtual private gateway.  . Defaults to None.
        vpn_gateway_ids(List, optional): One or more virtual private gateway IDs. Default: Describes all your virtual private gateways. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.gateway.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.gateway.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_vpn_gateways",
        op_kwargs={
            "Filters": filters,
            "VpnGatewayIds": vpn_gateway_ids,
            "DryRun": dry_run,
        },
    )


async def detach(
    hub, ctx, vpc_id: str, vpn_gateway_id: str, dry_run: bool = None
) -> None:
    r"""
    **Autogenerated function**

    Detaches a virtual private gateway from a VPC. You do this if you're planning to turn off the VPC and not use it anymore. You can confirm a virtual private gateway has been completely detached from a VPC by describing the virtual private gateway (any attachments to the virtual private gateway are also described). You must wait for the attachment's state to switch to detached before you can delete the VPC or attach a different VPC to the virtual private gateway.

    Args:
        vpc_id(str): The ID of the VPC.
        vpn_gateway_id(str): The ID of the virtual private gateway.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.vpn.gateway.detach(ctx, vpc_id=value, vpn_gateway_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.vpn.gateway.detach vpc_id=value, vpn_gateway_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="detach_vpn_gateway",
        op_kwargs={"VpcId": vpc_id, "VpnGatewayId": vpn_gateway_id, "DryRun": dry_run},
    )
