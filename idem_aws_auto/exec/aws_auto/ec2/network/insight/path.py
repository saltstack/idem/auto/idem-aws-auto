"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    source: str,
    destination: str,
    protocol: str,
    client_token: str,
    source_ip: str = None,
    destination_ip: str = None,
    destination_port: int = None,
    tag_specifications: List = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a path to analyze for reachability. Reachability Analyzer enables you to analyze and debug network reachability between two resources in your virtual private cloud (VPC). For more information, see What is Reachability Analyzer.

    Args:
        source_ip(str, optional): The IP address of the Amazon Web Services resource that is the source of the path. Defaults to None.
        destination_ip(str, optional): The IP address of the Amazon Web Services resource that is the destination of the path. Defaults to None.
        source(str): The Amazon Web Services resource that is the source of the path.
        destination(str): The Amazon Web Services resource that is the destination of the path.
        protocol(str): The protocol.
        destination_port(int, optional): The destination port. Defaults to None.
        tag_specifications(List, optional): The tags to add to the path. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        client_token(str): Unique, case-sensitive identifier that you provide to ensure the idempotency of the request. For more information, see How to ensure idempotency.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.network.insight.path.create(
                ctx, source=value, destination=value, protocol=value, client_token=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.network.insight.path.create source=value, destination=value, protocol=value, client_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="create_network_insights_path",
        op_kwargs={
            "SourceIp": source_ip,
            "DestinationIp": destination_ip,
            "Source": source,
            "Destination": destination,
            "Protocol": protocol,
            "DestinationPort": destination_port,
            "TagSpecifications": tag_specifications,
            "DryRun": dry_run,
            "ClientToken": client_token,
        },
    )


async def delete(hub, ctx, network_insights_path_id: str, dry_run: bool = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified path.

    Args:
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        network_insights_path_id(str): The ID of the path.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.network.insight.path.delete(
                ctx, network_insights_path_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.network.insight.path.delete network_insights_path_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="delete_network_insights_path",
        op_kwargs={
            "DryRun": dry_run,
            "NetworkInsightsPathId": network_insights_path_id,
        },
    )


async def describe_all(
    hub,
    ctx,
    network_insights_path_ids: List = None,
    filters: List = None,
    max_results: int = None,
    dry_run: bool = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes one or more of your paths.

    Args:
        network_insights_path_ids(List, optional): The IDs of the paths. Defaults to None.
        filters(List, optional): The filters. The following are possible values:   Destination - The ID of the resource.   DestinationPort - The destination port.   Name - The path name.   Protocol - The protocol.   Source - The ID of the resource.  . Defaults to None.
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        next_token(str, optional): The token for the next page of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.network.insight.path.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.network.insight.path.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_network_insights_paths",
        op_kwargs={
            "NetworkInsightsPathIds": network_insights_path_ids,
            "Filters": filters,
            "MaxResults": max_results,
            "DryRun": dry_run,
            "NextToken": next_token,
        },
    )
