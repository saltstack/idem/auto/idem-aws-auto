"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(
    hub,
    ctx,
    pool_id: str,
    next_token: str = None,
    max_results: int = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about the IPv6 CIDR block associations for a specified IPv6 address pool.

    Args:
        pool_id(str): The ID of the IPv6 address pool.
        next_token(str, optional): The token for the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.associated.ipv6_pool_cidr.get_all(ctx, pool_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.associated.ipv6_pool_cidr.get_all pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="get_associated_ipv6_pool_cidrs",
        op_kwargs={
            "PoolId": pool_id,
            "NextToken": next_token,
            "MaxResults": max_results,
            "DryRun": dry_run,
        },
    )
