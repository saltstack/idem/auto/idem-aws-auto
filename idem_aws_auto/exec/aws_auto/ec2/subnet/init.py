"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    vpc_id: str,
    cidr_block: str,
    tag_specifications: List = None,
    availability_zone: str = None,
    availability_zone_id: str = None,
    ipv6_cidr_block: str = None,
    outpost_arn: str = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a subnet in a specified VPC. You must specify an IPv4 CIDR block for the subnet. After you create a subnet, you can't change its CIDR block. The allowed block size is between a /16 netmask (65,536 IP addresses) and /28 netmask (16 IP addresses). The CIDR block must not overlap with the CIDR block of an existing subnet in the VPC. If you've associated an IPv6 CIDR block with your VPC, you can create a subnet with an IPv6 CIDR block that uses a /64 prefix length.   Amazon Web Services reserves both the first four and the last IPv4 address in each subnet's CIDR block. They're not available for use.  If you add more than one subnet to a VPC, they're set up in a star topology with a logical router in the middle. When you stop an instance in a subnet, it retains its private IPv4 address. It's therefore possible to have a subnet with no running instances (they're all stopped), but no remaining IP addresses available. For more information about subnets, see Your VPC and subnets in the Amazon Virtual Private Cloud User Guide.

    Args:
        tag_specifications(List, optional): The tags to assign to the subnet. Defaults to None.
        availability_zone(str, optional): The Availability Zone or Local Zone for the subnet. Default: Amazon Web Services selects one for you. If you create more than one subnet in your VPC, we do not necessarily select a different zone for each subnet. To create a subnet in a Local Zone, set this value to the Local Zone ID, for example us-west-2-lax-1a. For information about the Regions that support Local Zones, see Available Regions in the Amazon Elastic Compute Cloud User Guide. To create a subnet in an Outpost, set this value to the Availability Zone for the Outpost and specify the Outpost ARN. Defaults to None.
        availability_zone_id(str, optional): The AZ ID or the Local Zone ID of the subnet. Defaults to None.
        ipv6_cidr_block(str, optional): The IPv6 network range for the subnet, in CIDR notation. The subnet size must use a /64 prefix length. Defaults to None.
        outpost_arn(str, optional): The Amazon Resource Name (ARN) of the Outpost. If you specify an Outpost ARN, you must also specify the Availability Zone of the Outpost subnet. Defaults to None.
        vpc_id(str): The ID of the VPC.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        cidr_block(str): The IPv4 network range for the subnet, in CIDR notation. For example, 10.0.0.0/24. We modify the specified CIDR block to its canonical form; for example, if you specify 100.68.0.18/18, we modify it to 100.68.0.0/18.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.subnet.init.create(ctx, vpc_id=value, cidr_block=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.subnet.init.create vpc_id=value, cidr_block=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="create_subnet",
        op_kwargs={
            "TagSpecifications": tag_specifications,
            "AvailabilityZone": availability_zone,
            "AvailabilityZoneId": availability_zone_id,
            "Ipv6CidrBlock": ipv6_cidr_block,
            "OutpostArn": outpost_arn,
            "VpcId": vpc_id,
            "DryRun": dry_run,
            "CidrBlock": cidr_block,
        },
    )


async def delete(hub, ctx, subnet_id: str, dry_run: bool = None) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified subnet. You must terminate all running instances in the subnet before you can delete the subnet.

    Args:
        subnet_id(str): The ID of the subnet.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.subnet.init.delete(ctx, subnet_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.subnet.init.delete subnet_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="delete_subnet",
        op_kwargs={"SubnetId": subnet_id, "DryRun": dry_run},
    )


async def describe_all(
    hub,
    ctx,
    filters: List = None,
    subnet_ids: List = None,
    dry_run: bool = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes one or more of your subnets. For more information, see Your VPC and subnets in the Amazon Virtual Private Cloud User Guide.

    Args:
        filters(List, optional): One or more filters.    availability-zone - The Availability Zone for the subnet. You can also use availabilityZone as the filter name.    availability-zone-id - The ID of the Availability Zone for the subnet. You can also use availabilityZoneId as the filter name.    available-ip-address-count - The number of IPv4 addresses in the subnet that are available.    cidr-block - The IPv4 CIDR block of the subnet. The CIDR block you specify must exactly match the subnet's CIDR block for information to be returned for the subnet. You can also use cidr or cidrBlock as the filter names.    default-for-az - Indicates whether this is the default subnet for the Availability Zone. You can also use defaultForAz as the filter name.    ipv6-cidr-block-association.ipv6-cidr-block - An IPv6 CIDR block associated with the subnet.    ipv6-cidr-block-association.association-id - An association ID for an IPv6 CIDR block associated with the subnet.    ipv6-cidr-block-association.state - The state of an IPv6 CIDR block associated with the subnet.    outpost-arn - The Amazon Resource Name (ARN) of the Outpost.    owner-id - The ID of the Amazon Web Services account that owns the subnet.    state - The state of the subnet (pending | available).    subnet-arn - The Amazon Resource Name (ARN) of the subnet.    subnet-id - The ID of the subnet.    tag:<key> - The key/value combination of a tag assigned to the resource. Use the tag key in the filter name and the tag value as the filter value. For example, to find all resources that have a tag with the key Owner and the value TeamA, specify tag:Owner for the filter name and TeamA for the filter value.    tag-key - The key of a tag assigned to the resource. Use this filter to find all resources assigned a tag with a specific key, regardless of the tag value.    vpc-id - The ID of the VPC for the subnet.  . Defaults to None.
        subnet_ids(List, optional): One or more subnet IDs. Default: Describes all your subnets. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        next_token(str, optional): The token for the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.subnet.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.subnet.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_subnets",
        op_kwargs={
            "Filters": filters,
            "SubnetIds": subnet_ids,
            "DryRun": dry_run,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
