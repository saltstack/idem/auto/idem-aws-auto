"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def modify(
    hub,
    ctx,
    subnet_id: str,
    assign_ipv6_address_on_creation: Dict = None,
    map_public_ip_on_launch: Dict = None,
    map_customer_owned_ip_on_launch: Dict = None,
    customer_owned_ipv4_pool: str = None,
) -> None:
    r"""
    **Autogenerated function**

    Modifies a subnet attribute. You can only modify one attribute at a time.

    Args:
        assign_ipv6_address_on_creation(Dict, optional): Specify true to indicate that network interfaces created in the specified subnet should be assigned an IPv6 address. This includes a network interface that's created when launching an instance into the subnet (the instance therefore receives an IPv6 address).  If you enable the IPv6 addressing feature for your subnet, your network interface or instance only receives an IPv6 address if it's created using version 2016-11-15 or later of the Amazon EC2 API. Defaults to None.
        map_public_ip_on_launch(Dict, optional): Specify true to indicate that network interfaces attached to instances created in the specified subnet should be assigned a public IPv4 address. Defaults to None.
        subnet_id(str): The ID of the subnet.
        map_customer_owned_ip_on_launch(Dict, optional): Specify true to indicate that network interfaces attached to instances created in the specified subnet should be assigned a customer-owned IPv4 address. When this value is true, you must specify the customer-owned IP pool using CustomerOwnedIpv4Pool. Defaults to None.
        customer_owned_ipv4_pool(str, optional): The customer-owned IPv4 address pool associated with the subnet. You must set this value when you specify true for MapCustomerOwnedIpOnLaunch. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.subnet.attribute.modify(ctx, subnet_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.subnet.attribute.modify subnet_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="modify_subnet_attribute",
        op_kwargs={
            "AssignIpv6AddressOnCreation": assign_ipv6_address_on_creation,
            "MapPublicIpOnLaunch": map_public_ip_on_launch,
            "SubnetId": subnet_id,
            "MapCustomerOwnedIpOnLaunch": map_customer_owned_ip_on_launch,
            "CustomerOwnedIpv4Pool": customer_owned_ipv4_pool,
        },
    )
