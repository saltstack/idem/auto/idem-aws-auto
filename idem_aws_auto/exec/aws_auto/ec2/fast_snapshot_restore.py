"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(
    hub,
    ctx,
    filters: List = None,
    max_results: int = None,
    next_token: str = None,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the state of fast snapshot restores for your snapshots.

    Args:
        filters(List, optional): The filters. The possible values are:    availability-zone: The Availability Zone of the snapshot.    owner-id: The ID of the Amazon Web Services account that enabled fast snapshot restore on the snapshot.    snapshot-id: The ID of the snapshot.    state: The state of fast snapshot restores for the snapshot (enabling | optimizing | enabled | disabling | disabled).  . Defaults to None.
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.
        next_token(str, optional): The token for the next page of results. Defaults to None.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.fast_snapshot_restore.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.fast_snapshot_restore.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_fast_snapshot_restores",
        op_kwargs={
            "Filters": filters,
            "MaxResults": max_results,
            "NextToken": next_token,
            "DryRun": dry_run,
        },
    )


async def disable(
    hub, ctx, availability_zones: List, source_snapshot_ids: List, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Disables fast snapshot restores for the specified snapshots in the specified Availability Zones.

    Args:
        availability_zones(List): One or more Availability Zones. For example, us-east-2a.
        source_snapshot_ids(List): The IDs of one or more snapshots. For example, snap-1234567890abcdef0.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.fast_snapshot_restore.disable(
                ctx, availability_zones=value, source_snapshot_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.fast_snapshot_restore.disable availability_zones=value, source_snapshot_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="disable_fast_snapshot_restores",
        op_kwargs={
            "AvailabilityZones": availability_zones,
            "SourceSnapshotIds": source_snapshot_ids,
            "DryRun": dry_run,
        },
    )


async def enable(
    hub, ctx, availability_zones: List, source_snapshot_ids: List, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Enables fast snapshot restores for the specified snapshots in the specified Availability Zones. You get the full benefit of fast snapshot restores after they enter the enabled state. To get the current state of fast snapshot restores, use DescribeFastSnapshotRestores. To disable fast snapshot restores, use DisableFastSnapshotRestores. For more information, see Amazon EBS fast snapshot restore in the Amazon Elastic Compute Cloud User Guide.

    Args:
        availability_zones(List): One or more Availability Zones. For example, us-east-2a.
        source_snapshot_ids(List): The IDs of one or more snapshots. For example, snap-1234567890abcdef0. You can specify a snapshot that was shared with you from another Amazon Web Services account.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.fast_snapshot_restore.enable(
                ctx, availability_zones=value, source_snapshot_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.fast_snapshot_restore.enable availability_zones=value, source_snapshot_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="enable_fast_snapshot_restores",
        op_kwargs={
            "AvailabilityZones": availability_zones,
            "SourceSnapshotIds": source_snapshot_ids,
            "DryRun": dry_run,
        },
    )
