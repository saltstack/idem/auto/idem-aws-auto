"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub, ctx, instance_id: str, dry_run: bool = None, latest: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets the console output for the specified instance. For Linux instances, the instance console output displays the exact console output that would normally be displayed on a physical monitor attached to a computer. For Windows instances, the instance console output includes the last three system event log errors. By default, the console output returns buffered information that was posted shortly after an instance transition state (start, stop, reboot, or terminate). This information is available for at least one hour after the most recent post. Only the most recent 64 KB of console output is available. You can optionally retrieve the latest serial console output at any time during the instance lifecycle. This option is supported on instance types that use the Nitro hypervisor. For more information, see Instance console output in the Amazon EC2 User Guide.

    Args:
        instance_id(str): The ID of the instance.
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        latest(bool, optional): When enabled, retrieves the latest console output for the instance. Default: disabled (false). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.console.output.get(ctx, instance_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.console.output.get instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="get_console_output",
        op_kwargs={"InstanceId": instance_id, "DryRun": dry_run, "Latest": latest},
    )
