"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, instance_family: str, dry_run: bool = None) -> Dict:
    r"""
    **Autogenerated function**

    Describes the default credit option for CPU usage of a burstable performance instance family. For more information, see Burstable performance instances in the Amazon EC2 User Guide.

    Args:
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        instance_family(str): The instance family.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.default.credit_specification.get(ctx, instance_family=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.default.credit_specification.get instance_family=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="get_default_credit_specification",
        op_kwargs={"DryRun": dry_run, "InstanceFamily": instance_family},
    )


async def modify(
    hub, ctx, instance_family: str, cpu_credits: str, dry_run: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the default credit option for CPU usage of burstable performance instances. The default credit option is set at the account level per Amazon Web Services Region, and is specified per instance family. All new burstable performance instances in the account launch using the default credit option.  ModifyDefaultCreditSpecification is an asynchronous operation, which works at an Amazon Web Services Region level and modifies the credit option for each Availability Zone. All zones in a Region are updated within five minutes. But if instances are launched during this operation, they might not get the new credit option until the zone is updated. To verify whether the update has occurred, you can call GetDefaultCreditSpecification and check DefaultCreditSpecification for updates. For more information, see Burstable performance instances in the Amazon EC2 User Guide.

    Args:
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        instance_family(str): The instance family.
        cpu_credits(str): The credit option for CPU usage of the instance family. Valid Values: standard | unlimited .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.default.credit_specification.modify(
                ctx, instance_family=value, cpu_credits=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.default.credit_specification.modify instance_family=value, cpu_credits=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="modify_default_credit_specification",
        op_kwargs={
            "DryRun": dry_run,
            "InstanceFamily": instance_family,
            "CpuCredits": cpu_credits,
        },
    )
