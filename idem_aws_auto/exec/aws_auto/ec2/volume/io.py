"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def enable(hub, ctx, volume_id: str, dry_run: bool = None) -> None:
    r"""
    **Autogenerated function**

    Enables I/O operations for a volume that had I/O operations disabled because the data on the volume was potentially inconsistent.

    Args:
        dry_run(bool, optional): Checks whether you have the required permissions for the action, without actually making the request, and provides an error response. If you have the required permissions, the error response is DryRunOperation. Otherwise, it is UnauthorizedOperation. Defaults to None.
        volume_id(str): The ID of the volume.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.volume.io.enable(ctx, volume_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.volume.io.enable volume_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="enable_volume_io",
        op_kwargs={"DryRun": dry_run, "VolumeId": volume_id},
    )
