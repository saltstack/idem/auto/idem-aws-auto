"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(
    hub,
    ctx,
    filters: List = None,
    reserved_instances_modification_ids: List = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the modifications made to your Reserved Instances. If no parameter is specified, information about all your Reserved Instances modification requests is returned. If a modification ID is specified, only information about the specific modification is returned. For more information, see Modifying Reserved Instances in the Amazon EC2 User Guide.

    Args:
        filters(List, optional): One or more filters.    client-token - The idempotency token for the modification request.    create-date - The time when the modification request was created.    effective-date - The time when the modification becomes effective.    modification-result.reserved-instances-id - The ID for the Reserved Instances created as part of the modification request. This ID is only available when the status of the modification is fulfilled.    modification-result.target-configuration.availability-zone - The Availability Zone for the new Reserved Instances.    modification-result.target-configuration.instance-count  - The number of new Reserved Instances.    modification-result.target-configuration.instance-type - The instance type of the new Reserved Instances.    modification-result.target-configuration.platform - The network platform of the new Reserved Instances (EC2-Classic | EC2-VPC).    reserved-instances-id - The ID of the Reserved Instances modified.    reserved-instances-modification-id - The ID of the modification request.    status - The status of the Reserved Instances modification request (processing | fulfilled | failed).    status-message - The reason for the status.    update-date - The time when the modification request was last updated.  . Defaults to None.
        reserved_instances_modification_ids(List, optional): IDs for the submitted modification request. Defaults to None.
        next_token(str, optional): The token to retrieve the next page of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ec2.reserved_instance.modification.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ec2.reserved_instance.modification.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ec2",
        operation="describe_reserved_instances_modifications",
        op_kwargs={
            "Filters": filters,
            "ReservedInstancesModificationIds": reserved_instances_modification_ids,
            "NextToken": next_token,
        },
    )
