"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    event_type_ids: List,
    resource: str,
    targets: List,
    detail_type: str,
    client_request_token: str = None,
    tags: Dict = None,
    status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a notification rule for a resource. The rule specifies the events you want notifications about and the targets (such as SNS topics) where you want to receive them.

    Args:
        name(str): The name for the notification rule. Notifictaion rule names must be unique in your AWS account.
        event_type_ids(List): A list of event types associated with this notification rule. For a list of allowed events, see EventTypeSummary.
        resource(str): The Amazon Resource Name (ARN) of the resource to associate with the notification rule. Supported resources include pipelines in AWS CodePipeline, repositories in AWS CodeCommit, and build projects in AWS CodeBuild.
        targets(List): A list of Amazon Resource Names (ARNs) of SNS topics to associate with the notification rule.
        detail_type(str): The level of detail to include in the notifications for this resource. BASIC will include only the contents of the event as it would appear in AWS CloudWatch. FULL will include any supplemental information provided by AWS CodeStar Notifications and/or the service for the resource for which the notification is created.
        client_request_token(str, optional): A unique, client-generated idempotency token that, when provided in a request, ensures the request cannot be repeated with a changed parameter. If a request with the same parameters is received and a token is included, the request returns information about the initial request that used that token.  The AWS SDKs prepopulate client request tokens. If you are using an AWS SDK, an idempotency token is created for you. . Defaults to None.
        tags(Dict, optional): A list of tags to apply to this notification rule. Key names cannot start with "aws". . Defaults to None.
        status(str, optional): The status of the notification rule. The default value is ENABLED. If the status is set to DISABLED, notifications aren't sent for the notification rule. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codestar_notification.notification_rule.create(
                ctx,
                name=value,
                event_type_ids=value,
                resource=value,
                targets=value,
                detail_type=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codestar_notification.notification_rule.create name=value, event_type_ids=value, resource=value, targets=value, detail_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codestar-notifications",
        operation="create_notification_rule",
        op_kwargs={
            "Name": name,
            "EventTypeIds": event_type_ids,
            "Resource": resource,
            "Targets": targets,
            "DetailType": detail_type,
            "ClientRequestToken": client_request_token,
            "Tags": tags,
            "Status": status,
        },
    )


async def delete(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a notification rule for a resource.

    Args:
        arn(str): The Amazon Resource Name (ARN) of the notification rule you want to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codestar_notification.notification_rule.delete(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codestar_notification.notification_rule.delete arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codestar-notifications",
        operation="delete_notification_rule",
        op_kwargs={"Arn": arn},
    )


async def describe(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a specified notification rule.

    Args:
        arn(str): The Amazon Resource Name (ARN) of the notification rule.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codestar_notification.notification_rule.describe(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codestar_notification.notification_rule.describe arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codestar-notifications",
        operation="describe_notification_rule",
        op_kwargs={"Arn": arn},
    )


async def list_all(
    hub, ctx, filters: List = None, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of the notification rules for an AWS account.

    Args:
        filters(List, optional): The filters to use to return information by service or resource type. For valid values, see ListNotificationRulesFilter.  A filter with the same name can appear more than once when used with OR statements. Filters with different names should be applied with AND statements. . Defaults to None.
        next_token(str, optional): An enumeration token that, when provided in a request, returns the next batch of the results. Defaults to None.
        max_results(int, optional): A non-negative integer used to limit the number of returned results. The maximum number of results that can be returned is 100. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codestar_notification.notification_rule.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codestar_notification.notification_rule.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codestar-notifications",
        operation="list_notification_rules",
        op_kwargs={
            "Filters": filters,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    arn: str,
    name: str = None,
    status: str = None,
    event_type_ids: List = None,
    targets: List = None,
    detail_type: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a notification rule for a resource. You can change the events that trigger the notification rule, the status of the rule, and the targets that receive the notifications.  To add or remove tags for a notification rule, you must use TagResource and UntagResource.

    Args:
        arn(str): The Amazon Resource Name (ARN) of the notification rule.
        name(str, optional): The name of the notification rule. Defaults to None.
        status(str, optional): The status of the notification rule. Valid statuses include enabled (sending notifications) or disabled (not sending notifications). Defaults to None.
        event_type_ids(List, optional): A list of event types associated with this notification rule. Defaults to None.
        targets(List, optional): The address and type of the targets to receive notifications from this notification rule. Defaults to None.
        detail_type(str, optional): The level of detail to include in the notifications for this resource. BASIC will include only the contents of the event as it would appear in AWS CloudWatch. FULL will include any supplemental information provided by AWS CodeStar Notifications and/or the service for the resource for which the notification is created. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codestar_notification.notification_rule.update(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codestar_notification.notification_rule.update arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codestar-notifications",
        operation="update_notification_rule",
        op_kwargs={
            "Arn": arn,
            "Name": name,
            "Status": status,
            "EventTypeIds": event_type_ids,
            "Targets": targets,
            "DetailType": detail_type,
        },
    )
