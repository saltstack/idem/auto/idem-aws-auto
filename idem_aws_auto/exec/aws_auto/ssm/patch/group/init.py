"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(
    hub, ctx, max_results: int = None, filters: List = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all patch groups that have been registered with patch baselines.

    Args:
        max_results(int, optional): The maximum number of patch groups to return (per page). Defaults to None.
        filters(List, optional): Each element in the array is a structure containing a key-value pair. Supported keys for DescribePatchGroups include the following:     NAME_PREFIX   Sample values: AWS- | My-.     OPERATING_SYSTEM   Sample values: AMAZON_LINUX | SUSE | WINDOWS   . Defaults to None.
        next_token(str, optional): The token for the next set of items to return. (You received this token from a previous call.). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm.patch.group.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm.patch.group.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm",
        operation="describe_patch_groups",
        op_kwargs={
            "MaxResults": max_results,
            "Filters": filters,
            "NextToken": next_token,
        },
    )
