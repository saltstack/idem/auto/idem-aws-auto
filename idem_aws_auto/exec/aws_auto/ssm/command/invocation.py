"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(
    hub, ctx, command_id: str, instance_id: str, plugin_name: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns detailed information about command execution for an invocation or plugin.  GetCommandInvocation only gives the execution status of a plugin in a document. To get the command execution status on a specific instance, use ListCommandInvocations. To get the command execution status across instances, use ListCommands.

    Args:
        command_id(str): (Required) The parent command ID of the invocation plugin.
        instance_id(str): (Required) The ID of the managed instance targeted by the command. A managed instance can be an Amazon Elastic Compute Cloud (Amazon EC2) instance or an instance in your hybrid environment that is configured for Amazon Web Services Systems Manager.
        plugin_name(str, optional): The name of the plugin for which you want detailed results. If the document contains only one plugin, you can omit the name and details for that plugin. If the document contains more than one plugin, you must specify the name of the plugin for which you want to view details. Plugin names are also referred to as step names in Systems Manager documents (SSM documents). For example, aws:RunShellScript is a plugin. To find the PluginName, check the document content and find the name of the plugin. Alternatively, use ListCommandInvocations with the CommandId and Details parameters. The PluginName is the Name attribute of the CommandPlugin object in the CommandPlugins list. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm.command.invocation.get(
                ctx, command_id=value, instance_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm.command.invocation.get command_id=value, instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm",
        operation="get_command_invocation",
        op_kwargs={
            "CommandId": command_id,
            "InstanceId": instance_id,
            "PluginName": plugin_name,
        },
    )


async def list_all(
    hub,
    ctx,
    command_id: str = None,
    instance_id: str = None,
    max_results: int = None,
    next_token: str = None,
    filters: List = None,
    details: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    An invocation is copy of a command sent to a specific instance. A command can apply to one or more instances. A command invocation applies to one instance. For example, if a user runs SendCommand against three instances, then a command invocation is created for each requested instance ID. ListCommandInvocations provide status about command execution.

    Args:
        command_id(str, optional): (Optional) The invocations for a specific command ID. Defaults to None.
        instance_id(str, optional): (Optional) The command execution details for a specific instance ID. Defaults to None.
        max_results(int, optional): (Optional) The maximum number of items to return for this call. The call also returns a token that you can specify in a subsequent call to get the next set of results. Defaults to None.
        next_token(str, optional): (Optional) The token for the next set of items to return. (You received this token from a previous call.). Defaults to None.
        filters(List, optional): (Optional) One or more filters. Use a filter to return a more specific list of results. Defaults to None.
        details(bool, optional): (Optional) If set this returns the response of the command executions and any command output. The default value is false. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ssm.command.invocation.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ssm.command.invocation.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ssm",
        operation="list_command_invocations",
        op_kwargs={
            "CommandId": command_id,
            "InstanceId": instance_id,
            "MaxResults": max_results,
            "NextToken": next_token,
            "Filters": filters,
            "Details": details,
        },
    )
