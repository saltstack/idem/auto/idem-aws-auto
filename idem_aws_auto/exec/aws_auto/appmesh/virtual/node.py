"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    mesh_name: str,
    spec: Dict,
    virtual_node_name: str,
    client_token: str = None,
    mesh_owner: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a virtual node within a service mesh.  A virtual node acts as a logical pointer to a particular task group, such as an Amazon ECS service or a Kubernetes deployment. When you create a virtual node, you can specify the service discovery information for your task group, and whether the proxy running in a task group will communicate with other proxies using Transport Layer Security (TLS). You define a listener for any inbound traffic that your virtual node expects. Any virtual service that your virtual node expects to communicate to is specified as a backend. The response metadata for your new virtual node contains the arn that is associated with the virtual node. Set this value to the full ARN; for example, arn:aws:appmesh:us-west-2:123456789012:myMesh/default/virtualNode/myApp) as the APPMESH_RESOURCE_ARN environment variable for your task group's Envoy proxy container in your task definition or pod spec. This is then mapped to the node.id and node.cluster Envoy parameters.  By default, App Mesh uses the name of the resource you specified in APPMESH_RESOURCE_ARN when Envoy is referring to itself in metrics and traces. You can override this behavior by setting the APPMESH_RESOURCE_CLUSTER environment variable with your own name.  For more information about virtual nodes, see Virtual nodes. You must be using 1.15.0 or later of the Envoy image when setting these variables. For more information aboutApp Mesh Envoy variables, see Envoy image in the AWS App Mesh User Guide.

    Args:
        client_token(str, optional): Unique, case-sensitive identifier that you provide to ensure the idempotency of the request. Up to 36 letters, numbers, hyphens, and underscores are allowed. Defaults to None.
        mesh_name(str): The name of the service mesh to create the virtual node in.
        mesh_owner(str, optional): The AWS IAM account ID of the service mesh owner. If the account ID is not your own, then the account that you specify must share the mesh with your account before you can create the resource in the service mesh. For more information about mesh sharing, see Working with shared meshes. Defaults to None.
        spec(Dict): The virtual node specification to apply.
        tags(List, optional): Optional metadata that you can apply to the virtual node to assist with categorization and organization. Each tag consists of a key and an optional value, both of which you define. Tag keys can have a maximum character length of 128 characters, and tag values can have a maximum length of 256 characters. Defaults to None.
        virtual_node_name(str): The name to use for the virtual node.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appmesh.virtual.node.create(
                ctx, mesh_name=value, spec=value, virtual_node_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appmesh.virtual.node.create mesh_name=value, spec=value, virtual_node_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appmesh",
        operation="create_virtual_node",
        op_kwargs={
            "clientToken": client_token,
            "meshName": mesh_name,
            "meshOwner": mesh_owner,
            "spec": spec,
            "tags": tags,
            "virtualNodeName": virtual_node_name,
        },
    )


async def delete(
    hub, ctx, mesh_name: str, virtual_node_name: str, mesh_owner: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an existing virtual node. You must delete any virtual services that list a virtual node as a service provider before you can delete the virtual node itself.

    Args:
        mesh_name(str): The name of the service mesh to delete the virtual node in.
        mesh_owner(str, optional): The AWS IAM account ID of the service mesh owner. If the account ID is not your own, then it's the ID of the account that shared the mesh with your account. For more information about mesh sharing, see Working with shared meshes. Defaults to None.
        virtual_node_name(str): The name of the virtual node to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appmesh.virtual.node.delete(
                ctx, mesh_name=value, virtual_node_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appmesh.virtual.node.delete mesh_name=value, virtual_node_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appmesh",
        operation="delete_virtual_node",
        op_kwargs={
            "meshName": mesh_name,
            "meshOwner": mesh_owner,
            "virtualNodeName": virtual_node_name,
        },
    )


async def describe(
    hub, ctx, mesh_name: str, virtual_node_name: str, mesh_owner: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes an existing virtual node.

    Args:
        mesh_name(str): The name of the service mesh that the virtual node resides in.
        mesh_owner(str, optional): The AWS IAM account ID of the service mesh owner. If the account ID is not your own, then it's the ID of the account that shared the mesh with your account. For more information about mesh sharing, see Working with shared meshes. Defaults to None.
        virtual_node_name(str): The name of the virtual node to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appmesh.virtual.node.describe(
                ctx, mesh_name=value, virtual_node_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appmesh.virtual.node.describe mesh_name=value, virtual_node_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appmesh",
        operation="describe_virtual_node",
        op_kwargs={
            "meshName": mesh_name,
            "meshOwner": mesh_owner,
            "virtualNodeName": virtual_node_name,
        },
    )


async def list_all(
    hub,
    ctx,
    mesh_name: str,
    limit: int = None,
    mesh_owner: str = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of existing virtual nodes.

    Args:
        limit(int, optional): The maximum number of results returned by ListVirtualNodes in paginated output. When you use this parameter, ListVirtualNodes returns only limit results in a single page along with a nextToken response element. You can see the remaining results of the initial request by sending another ListVirtualNodes request with the returned nextToken value. This value can be between 1 and 100. If you don't use this parameter, ListVirtualNodes returns up to 100 results and a nextToken value if applicable. Defaults to None.
        mesh_name(str): The name of the service mesh to list virtual nodes in.
        mesh_owner(str, optional): The AWS IAM account ID of the service mesh owner. If the account ID is not your own, then it's the ID of the account that shared the mesh with your account. For more information about mesh sharing, see Working with shared meshes. Defaults to None.
        next_token(str, optional): The nextToken value returned from a previous paginated ListVirtualNodes request where limit was used and the results exceeded the value of that parameter. Pagination continues from the end of the previous results that returned the nextToken value. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appmesh.virtual.node.list_all(ctx, mesh_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appmesh.virtual.node.list_all mesh_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appmesh",
        operation="list_virtual_nodes",
        op_kwargs={
            "limit": limit,
            "meshName": mesh_name,
            "meshOwner": mesh_owner,
            "nextToken": next_token,
        },
    )


async def update(
    hub,
    ctx,
    mesh_name: str,
    spec: Dict,
    virtual_node_name: str,
    client_token: str = None,
    mesh_owner: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an existing virtual node in a specified service mesh.

    Args:
        client_token(str, optional): Unique, case-sensitive identifier that you provide to ensure the idempotency of the request. Up to 36 letters, numbers, hyphens, and underscores are allowed. Defaults to None.
        mesh_name(str): The name of the service mesh that the virtual node resides in.
        mesh_owner(str, optional): The AWS IAM account ID of the service mesh owner. If the account ID is not your own, then it's the ID of the account that shared the mesh with your account. For more information about mesh sharing, see Working with shared meshes. Defaults to None.
        spec(Dict): The new virtual node specification to apply. This overwrites the existing data.
        virtual_node_name(str): The name of the virtual node to update.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appmesh.virtual.node.update(
                ctx, mesh_name=value, spec=value, virtual_node_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appmesh.virtual.node.update mesh_name=value, spec=value, virtual_node_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appmesh",
        operation="update_virtual_node",
        op_kwargs={
            "clientToken": client_token,
            "meshName": mesh_name,
            "meshOwner": mesh_owner,
            "spec": spec,
            "virtualNodeName": virtual_node_name,
        },
    )
