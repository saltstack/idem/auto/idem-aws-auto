"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"import_": "import"}


async def describe(
    hub, ctx, progress_update_stream: str, migration_task_name: str
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of all attributes associated with a specific migration task.

    Args:
        progress_update_stream(str): The name of the ProgressUpdateStream. .
        migration_task_name(str): The identifier given to the MigrationTask. Do not store personal data in this field. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mgh.migration_task.init.describe(ctx, progress_update_stream=value, migration_task_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mgh.migration_task.init.describe progress_update_stream=value, migration_task_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mgh",
        operation="describe_migration_task",
        op_kwargs={
            "ProgressUpdateStream": progress_update_stream,
            "MigrationTaskName": migration_task_name,
        },
    )


async def import_(
    hub,
    ctx,
    progress_update_stream: str,
    migration_task_name: str,
    dry_run: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Registers a new migration task which represents a server, database, etc., being migrated to AWS by a migration tool. This API is a prerequisite to calling the NotifyMigrationTaskState API as the migration tool must first register the migration task with Migration Hub.

    Args:
        progress_update_stream(str): The name of the ProgressUpdateStream. >.
        migration_task_name(str): Unique identifier that references the migration task. Do not store personal data in this field. .
        dry_run(bool, optional): Optional boolean flag to indicate whether any effect should take place. Used to test if the caller has permission to make the call. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mgh.migration_task.init.import(ctx, progress_update_stream=value, migration_task_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mgh.migration_task.init.import progress_update_stream=value, migration_task_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mgh",
        operation="import_migration_task",
        op_kwargs={
            "ProgressUpdateStream": progress_update_stream,
            "MigrationTaskName": migration_task_name,
            "DryRun": dry_run,
        },
    )


async def list_all(
    hub, ctx, next_token: str = None, max_results: int = None, resource_name: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all, or filtered by resource name, migration tasks associated with the user account making this call. This API has the following traits:   Can show a summary list of the most recent migration tasks.   Can show a summary list of migration tasks associated with a given discovered resource.   Lists migration tasks in a paginated interface.

    Args:
        next_token(str, optional): If a NextToken was returned by a previous call, there are more results available. To retrieve the next page of results, make the call again using the returned token in NextToken. Defaults to None.
        max_results(int, optional): Value to specify how many results are returned per page. Defaults to None.
        resource_name(str, optional): Filter migration tasks by discovered resource name. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mgh.migration_task.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mgh.migration_task.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mgh",
        operation="list_migration_tasks",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "ResourceName": resource_name,
        },
    )
