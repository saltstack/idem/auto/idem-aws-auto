"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, api_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the current status of a schema creation operation.

    Args:
        api_id(str): The API ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.schema_creation.status.get(ctx, api_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.schema_creation.status.get api_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="get_schema_creation_status",
        op_kwargs={"apiId": api_id},
    )
