"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "type"


async def create(hub, ctx, api_id: str, definition: str, format_: str) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Type object.

    Args:
        api_id(str): The API ID.
        definition(str): The type definition, in GraphQL Schema Definition Language (SDL) format. For more information, see the GraphQL SDL documentation.
        format_(str): The type format: SDL or JSON.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.type_.create(
                ctx, api_id=value, definition=value, format_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.type.create api_id=value, definition=value, format_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="create_type",
        op_kwargs={"apiId": api_id, "definition": definition, "format": format_},
    )


async def delete(hub, ctx, api_id: str, type_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a Type object.

    Args:
        api_id(str): The API ID.
        type_name(str): The type name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.type_.delete(ctx, api_id=value, type_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.type.delete api_id=value, type_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="delete_type",
        op_kwargs={"apiId": api_id, "typeName": type_name},
    )


async def get(hub, ctx, api_id: str, type_name: str, format_: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a Type object.

    Args:
        api_id(str): The API ID.
        type_name(str): The type name.
        format_(str): The type format: SDL or JSON.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.type_.get(
                ctx, api_id=value, type_name=value, format_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.type.get api_id=value, type_name=value, format_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="get_type",
        op_kwargs={"apiId": api_id, "typeName": type_name, "format": format_},
    )


async def list_all(
    hub, ctx, api_id: str, format_: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the types for a given API.

    Args:
        api_id(str): The API ID.
        format_(str): The type format: SDL or JSON.
        next_token(str, optional): An identifier that was returned from the previous call to this operation, which can be used to return the next set of items in the list. . Defaults to None.
        max_results(int, optional): The maximum number of results you want the request to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.type_.list_all(ctx, api_id=value, format_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.type.list_all api_id=value, format_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="list_types",
        op_kwargs={
            "apiId": api_id,
            "format": format_,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )


async def update(
    hub, ctx, api_id: str, type_name: str, format_: str, definition: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a Type object.

    Args:
        api_id(str): The API ID.
        type_name(str): The new type name.
        definition(str, optional): The new definition. Defaults to None.
        format_(str): The new type format: SDL or JSON.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.appsync.type_.update(
                ctx, api_id=value, type_name=value, format_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.appsync.type.update api_id=value, type_name=value, format_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="appsync",
        operation="update_type",
        op_kwargs={
            "apiId": api_id,
            "typeName": type_name,
            "definition": definition,
            "format": format_,
        },
    )
