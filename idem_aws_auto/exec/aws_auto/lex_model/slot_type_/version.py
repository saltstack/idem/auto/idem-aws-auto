"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(hub, ctx, name: str, checksum: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new version of a slot type based on the $LATEST version of the specified slot type. If the $LATEST version of this resource has not changed since the last version that you created, Amazon Lex doesn't create a new version. It returns the last version that you created.   You can update only the $LATEST version of a slot type. You can't update the numbered versions that you create with the CreateSlotTypeVersion operation.  When you create a version of a slot type, Amazon Lex sets the version to 1. Subsequent versions increment by 1. For more information, see versioning-intro.  This operation requires permissions for the lex:CreateSlotTypeVersion action.

    Args:
        name(str): The name of the slot type that you want to create a new version for. The name is case sensitive. .
        checksum(str, optional): Checksum for the $LATEST version of the slot type that you want to publish. If you specify a checksum and the $LATEST version of the slot type has a different checksum, Amazon Lex returns a PreconditionFailedException exception and doesn't publish the new version. If you don't specify a checksum, Amazon Lex publishes the $LATEST version. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.slot_type_.version.create(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.slot_type_.version.create name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="create_slot_type_version",
        op_kwargs={"name": name, "checksum": checksum},
    )


async def delete(hub, ctx, name: str, version: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a specific version of a slot type. To delete all versions of a slot type, use the DeleteSlotType operation.  This operation requires permissions for the lex:DeleteSlotTypeVersion action.

    Args:
        name(str): The name of the slot type.
        version(str): The version of the slot type to delete. You cannot delete the $LATEST version of the slot type. To delete the $LATEST version, use the DeleteSlotType operation.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.slot_type_.version.delete(
                ctx, name=value, version=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.slot_type_.version.delete name=value, version=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="delete_slot_type_version",
        op_kwargs={"name": name, "version": version},
    )


async def get_all(
    hub, ctx, name: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about all versions of a slot type. The GetSlotTypeVersions operation returns a SlotTypeMetadata object for each version of a slot type. For example, if a slot type has three numbered versions, the GetSlotTypeVersions operation returns four SlotTypeMetadata objects in the response, one for each numbered version and one for the $LATEST version.  The GetSlotTypeVersions operation always returns at least one version, the $LATEST version. This operation requires permissions for the lex:GetSlotTypeVersions action.

    Args:
        name(str): The name of the slot type for which versions should be returned.
        next_token(str, optional): A pagination token for fetching the next page of slot type versions. If the response to this call is truncated, Amazon Lex returns a pagination token in the response. To fetch the next page of versions, specify the pagination token in the next request. . Defaults to None.
        max_results(int, optional): The maximum number of slot type versions to return in the response. The default is 10. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.slot_type_.version.get_all(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.slot_type_.version.get_all name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="get_slot_type_versions",
        op_kwargs={"name": name, "nextToken": next_token, "maxResults": max_results},
    )
