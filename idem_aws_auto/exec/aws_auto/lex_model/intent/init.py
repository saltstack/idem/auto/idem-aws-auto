"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def delete(hub, ctx, name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes all versions of the intent, including the $LATEST version. To delete a specific version of the intent, use the DeleteIntentVersion operation.  You can delete a version of an intent only if it is not referenced. To delete an intent that is referred to in one or more bots (see how-it-works), you must remove those references first.    If you get the ResourceInUseException exception, it provides an example reference that shows where the intent is referenced. To remove the reference to the intent, either update the bot or delete it. If you get the same exception when you attempt to delete the intent again, repeat until the intent has no references and the call to DeleteIntent is successful.    This operation requires permission for the lex:DeleteIntent action.

    Args:
        name(str): The name of the intent. The name is case sensitive. .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.intent.init.delete(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.intent.init.delete name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="delete_intent",
        op_kwargs={"name": name},
    )


async def get(hub, ctx, name: str, version: str) -> Dict:
    r"""
    **Autogenerated function**

     Returns information about an intent. In addition to the intent name, you must specify the intent version.   This operation requires permissions to perform the lex:GetIntent action.

    Args:
        name(str): The name of the intent. The name is case sensitive. .
        version(str): The version of the intent.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.intent.init.get(ctx, name=value, version=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.intent.init.get name=value, version=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="get_intent",
        op_kwargs={"name": name, "version": version},
    )


async def get_all(
    hub, ctx, next_token: str = None, max_results: int = None, name_contains: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns intent information as follows:    If you specify the nameContains field, returns the $LATEST version of all intents that contain the specified string.    If you don't specify the nameContains field, returns information about the $LATEST version of all intents.     The operation requires permission for the lex:GetIntents action.

    Args:
        next_token(str, optional): A pagination token that fetches the next page of intents. If the response to this API call is truncated, Amazon Lex returns a pagination token in the response. To fetch the next page of intents, specify the pagination token in the next request. . Defaults to None.
        max_results(int, optional): The maximum number of intents to return in the response. The default is 10. Defaults to None.
        name_contains(str, optional): Substring to match in intent names. An intent will be returned if any part of its name matches the substring. For example, "xyz" matches both "xyzabc" and "abcxyz.". Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.intent.init.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.intent.init.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="get_intents",
        op_kwargs={
            "nextToken": next_token,
            "maxResults": max_results,
            "nameContains": name_contains,
        },
    )


async def put(
    hub,
    ctx,
    name: str,
    description: str = None,
    slots: List = None,
    sample_utterances: List = None,
    confirmation_prompt: Dict = None,
    rejection_statement: Dict = None,
    follow_up_prompt: Dict = None,
    conclusion_statement: Dict = None,
    dialog_code_hook: Dict = None,
    fulfillment_activity: Dict = None,
    parent_intent_signature: str = None,
    checksum: str = None,
    create_version: bool = None,
    kendra_configuration: Dict = None,
    input_contexts: List = None,
    output_contexts: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an intent or replaces an existing intent. To define the interaction between the user and your bot, you use one or more intents. For a pizza ordering bot, for example, you would create an OrderPizza intent.  To create an intent or replace an existing intent, you must provide the following:   Intent name. For example, OrderPizza.   Sample utterances. For example, "Can I order a pizza, please." and "I want to order a pizza."   Information to be gathered. You specify slot types for the information that your bot will request from the user. You can specify standard slot types, such as a date or a time, or custom slot types such as the size and crust of a pizza.   How the intent will be fulfilled. You can provide a Lambda function or configure the intent to return the intent information to the client application. If you use a Lambda function, when all of the intent information is available, Amazon Lex invokes your Lambda function. If you configure your intent to return the intent information to the client application.    You can specify other optional information in the request, such as:   A confirmation prompt to ask the user to confirm an intent. For example, "Shall I order your pizza?"   A conclusion statement to send to the user after the intent has been fulfilled. For example, "I placed your pizza order."   A follow-up prompt that asks the user for additional activity. For example, asking "Do you want to order a drink with your pizza?"   If you specify an existing intent name to update the intent, Amazon Lex replaces the values in the $LATEST version of the intent with the values in the request. Amazon Lex removes fields that you don't provide in the request. If you don't specify the required fields, Amazon Lex throws an exception. When you update the $LATEST version of an intent, the status field of any bot that uses the $LATEST version of the intent is set to NOT_BUILT. For more information, see how-it-works. This operation requires permissions for the lex:PutIntent action.

    Args:
        name(str): The name of the intent. The name is not case sensitive.  The name can't match a built-in intent name, or a built-in intent name with "AMAZON." removed. For example, because there is a built-in intent called AMAZON.HelpIntent, you can't create a custom intent called HelpIntent. For a list of built-in intents, see Standard Built-in Intents in the Alexa Skills Kit.
        description(str, optional): A description of the intent. Defaults to None.
        slots(List, optional): An array of intent slots. At runtime, Amazon Lex elicits required slot values from the user using prompts defined in the slots. For more information, see how-it-works. . Defaults to None.
        sample_utterances(List, optional): An array of utterances (strings) that a user might say to signal the intent. For example, "I want {PizzaSize} pizza", "Order {Quantity} {PizzaSize} pizzas".  In each utterance, a slot name is enclosed in curly braces. . Defaults to None.
        confirmation_prompt(Dict, optional): Prompts the user to confirm the intent. This question should have a yes or no answer. Amazon Lex uses this prompt to ensure that the user acknowledges that the intent is ready for fulfillment. For example, with the OrderPizza intent, you might want to confirm that the order is correct before placing it. For other intents, such as intents that simply respond to user questions, you might not need to ask the user for confirmation before providing the information.   You you must provide both the rejectionStatement and the confirmationPrompt, or neither. . Defaults to None.
        rejection_statement(Dict, optional): When the user answers "no" to the question defined in confirmationPrompt, Amazon Lex responds with this statement to acknowledge that the intent was canceled.   You must provide both the rejectionStatement and the confirmationPrompt, or neither. . Defaults to None.
        follow_up_prompt(Dict, optional): Amazon Lex uses this prompt to solicit additional activity after fulfilling an intent. For example, after the OrderPizza intent is fulfilled, you might prompt the user to order a drink. The action that Amazon Lex takes depends on the user's response, as follows:   If the user says "Yes" it responds with the clarification prompt that is configured for the bot.   if the user says "Yes" and continues with an utterance that triggers an intent it starts a conversation for the intent.   If the user says "No" it responds with the rejection statement configured for the the follow-up prompt.   If it doesn't recognize the utterance it repeats the follow-up prompt again.   The followUpPrompt field and the conclusionStatement field are mutually exclusive. You can specify only one. . Defaults to None.
        conclusion_statement(Dict, optional):  The statement that you want Amazon Lex to convey to the user after the intent is successfully fulfilled by the Lambda function.  This element is relevant only if you provide a Lambda function in the fulfillmentActivity. If you return the intent to the client application, you can't specify this element.  The followUpPrompt and conclusionStatement are mutually exclusive. You can specify only one. . Defaults to None.
        dialog_code_hook(Dict, optional):  Specifies a Lambda function to invoke for each user input. You can invoke this Lambda function to personalize user interaction.  For example, suppose your bot determines that the user is John. Your Lambda function might retrieve John's information from a backend database and prepopulate some of the values. For example, if you find that John is gluten intolerant, you might set the corresponding intent slot, GlutenIntolerant, to true. You might find John's phone number and set the corresponding session attribute. . Defaults to None.
        fulfillment_activity(Dict, optional): Required. Describes how the intent is fulfilled. For example, after a user provides all of the information for a pizza order, fulfillmentActivity defines how the bot places an order with a local pizza store.   You might configure Amazon Lex to return all of the intent information to the client application, or direct it to invoke a Lambda function that can process the intent (for example, place an order with a pizzeria). . Defaults to None.
        parent_intent_signature(str, optional): A unique identifier for the built-in intent to base this intent on. To find the signature for an intent, see Standard Built-in Intents in the Alexa Skills Kit. Defaults to None.
        checksum(str, optional): Identifies a specific revision of the $LATEST version. When you create a new intent, leave the checksum field blank. If you specify a checksum you get a BadRequestException exception. When you want to update a intent, set the checksum field to the checksum of the most recent revision of the $LATEST version. If you don't specify the  checksum field, or if the checksum does not match the $LATEST version, you get a PreconditionFailedException exception. Defaults to None.
        create_version(bool, optional): When set to true a new numbered version of the intent is created. This is the same as calling the CreateIntentVersion operation. If you do not specify createVersion, the default is false. Defaults to None.
        kendra_configuration(Dict, optional): Configuration information required to use the AMAZON.KendraSearchIntent intent to connect to an Amazon Kendra index. For more information, see  AMAZON.KendraSearchIntent. Defaults to None.
        input_contexts(List, optional): An array of InputContext objects that lists the contexts that must be active for Amazon Lex to choose the intent in a conversation with the user. Defaults to None.
        output_contexts(List, optional): An array of OutputContext objects that lists the contexts that the intent activates when the intent is fulfilled. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.intent.init.put(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.intent.init.put name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="put_intent",
        op_kwargs={
            "name": name,
            "description": description,
            "slots": slots,
            "sampleUtterances": sample_utterances,
            "confirmationPrompt": confirmation_prompt,
            "rejectionStatement": rejection_statement,
            "followUpPrompt": follow_up_prompt,
            "conclusionStatement": conclusion_statement,
            "dialogCodeHook": dialog_code_hook,
            "fulfillmentActivity": fulfillment_activity,
            "parentIntentSignature": parent_intent_signature,
            "checksum": checksum,
            "createVersion": create_version,
            "kendraConfiguration": kendra_configuration,
            "inputContexts": input_contexts,
            "outputContexts": output_contexts,
        },
    )
