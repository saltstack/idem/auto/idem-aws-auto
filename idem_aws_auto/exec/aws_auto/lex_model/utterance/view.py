"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(hub, ctx, bot_name: str, bot_versions: List, status_type: str) -> Dict:
    r"""
    **Autogenerated function**

    Use the GetUtterancesView operation to get information about the utterances that your users have made to your bot. You can use this list to tune the utterances that your bot responds to. For example, say that you have created a bot to order flowers. After your users have used your bot for a while, use the GetUtterancesView operation to see the requests that they have made and whether they have been successful. You might find that the utterance "I want flowers" is not being recognized. You could add this utterance to the OrderFlowers intent so that your bot recognizes that utterance. After you publish a new version of a bot, you can get information about the old version and the new so that you can compare the performance across the two versions.  Utterance statistics are generated once a day. Data is available for the last 15 days. You can request information for up to 5 versions of your bot in each request. Amazon Lex returns the most frequent utterances received by the bot in the last 15 days. The response contains information about a maximum of 100 utterances for each version. If you set childDirected field to true when you created your bot, if you are using slot obfuscation with one or more slots, or if you opted out of participating in improving Amazon Lex, utterances are not available. This operation requires permissions for the lex:GetUtterancesView action.

    Args:
        bot_name(str): The name of the bot for which utterance information should be returned.
        bot_versions(List): An array of bot versions for which utterance information should be returned. The limit is 5 versions per request.
        status_type(str): To return utterances that were recognized and handled, use Detected. To return utterances that were not recognized, use Missed.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.utterance.view.get(
                ctx, bot_name=value, bot_versions=value, status_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.utterance.view.get bot_name=value, bot_versions=value, status_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="get_utterances_view",
        op_kwargs={
            "botName": bot_name,
            "botVersions": bot_versions,
            "statusType": status_type,
        },
    )
