"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub, ctx, name: str, version: str, resource_type: str, export_type: str
) -> Dict:
    r"""
    **Autogenerated function**

    Exports the contents of a Amazon Lex resource in a specified format.

    Args:
        name(str): The name of the bot to export.
        version(str): The version of the bot to export.
        resource_type(str): The type of resource to export. .
        export_type(str): The format of the exported data.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lex_model.export.get(
                ctx, name=value, version=value, resource_type=value, export_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lex_model.export.get name=value, version=value, resource_type=value, export_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lex-models",
        operation="get_export",
        op_kwargs={
            "name": name,
            "version": version,
            "resourceType": resource_type,
            "exportType": export_type,
        },
    )
