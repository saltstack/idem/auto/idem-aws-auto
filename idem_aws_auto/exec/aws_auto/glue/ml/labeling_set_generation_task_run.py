"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def start(hub, ctx, transform_id: str, output_s3_path: str) -> Dict:
    r"""
    **Autogenerated function**

    Starts the active learning workflow for your machine learning transform to improve the transform's quality by generating label sets and adding labels. When the StartMLLabelingSetGenerationTaskRun finishes, Glue will have generated a "labeling set" or a set of questions for humans to answer. In the case of the FindMatches transform, these questions are of the form, “What is the correct way to group these rows together into groups composed entirely of matching records?”  After the labeling process is finished, you can upload your labels with a call to StartImportLabelsTaskRun. After StartImportLabelsTaskRun finishes, all future runs of the machine learning transform will use the new and improved labels and perform a higher-quality transformation.

    Args:
        transform_id(str): The unique identifier of the machine learning transform.
        output_s3_path(str): The Amazon Simple Storage Service (Amazon S3) path where you generate the labeling set.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.ml.labeling_set_generation_task_run.start(
                ctx, transform_id=value, output_s3_path=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.ml.labeling_set_generation_task_run.start transform_id=value, output_s3_path=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="start_ml_labeling_set_generation_task_run",
        op_kwargs={"TransformId": transform_id, "OutputS3Path": output_s3_path},
    )
