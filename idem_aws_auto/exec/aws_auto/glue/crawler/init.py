"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    role: str,
    targets: Dict,
    database_name: str = None,
    description: str = None,
    schedule: str = None,
    classifiers: List = None,
    table_prefix: str = None,
    schema_change_policy: Dict = None,
    recrawl_policy: Dict = None,
    lineage_configuration: Dict = None,
    configuration: str = None,
    crawler_security_configuration: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new crawler with specified targets, role, configuration, and optional schedule. At least one crawl target must be specified, in the s3Targets field, the jdbcTargets field, or the DynamoDBTargets field.

    Args:
        name(str): Name of the new crawler.
        role(str): The IAM role or Amazon Resource Name (ARN) of an IAM role used by the new crawler to access customer resources.
        database_name(str, optional): The Glue database where results are written, such as: arn:aws:daylight:us-east-1::database/sometable/*. Defaults to None.
        description(str, optional): A description of the new crawler. Defaults to None.
        targets(Dict): A list of collection of targets to crawl.
        schedule(str, optional): A cron expression used to specify the schedule (see Time-Based Schedules for Jobs and Crawlers. For example, to run something every day at 12:15 UTC, you would specify: cron(15 12 * * ? *). Defaults to None.
        classifiers(List, optional): A list of custom classifiers that the user has registered. By default, all built-in classifiers are included in a crawl, but these custom classifiers always override the default classifiers for a given classification. Defaults to None.
        table_prefix(str, optional): The table prefix used for catalog tables that are created. Defaults to None.
        schema_change_policy(Dict, optional): The policy for the crawler's update and deletion behavior. Defaults to None.
        recrawl_policy(Dict, optional): A policy that specifies whether to crawl the entire dataset again, or to crawl only folders that were added since the last crawler run. Defaults to None.
        lineage_configuration(Dict, optional): Specifies data lineage configuration settings for the crawler. Defaults to None.
        configuration(str, optional): Crawler configuration information. This versioned JSON string allows users to specify aspects of a crawler's behavior. For more information, see Configuring a Crawler. Defaults to None.
        crawler_security_configuration(str, optional): The name of the SecurityConfiguration structure to be used by this crawler. Defaults to None.
        tags(Dict, optional): The tags to use with this crawler request. You may use tags to limit access to the crawler. For more information about tags in Glue, see Amazon Web Services Tags in Glue in the developer guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.create(
                ctx, name=value, role=value, targets=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.create name=value, role=value, targets=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="create_crawler",
        op_kwargs={
            "Name": name,
            "Role": role,
            "DatabaseName": database_name,
            "Description": description,
            "Targets": targets,
            "Schedule": schedule,
            "Classifiers": classifiers,
            "TablePrefix": table_prefix,
            "SchemaChangePolicy": schema_change_policy,
            "RecrawlPolicy": recrawl_policy,
            "LineageConfiguration": lineage_configuration,
            "Configuration": configuration,
            "CrawlerSecurityConfiguration": crawler_security_configuration,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes a specified crawler from the Glue Data Catalog, unless the crawler state is RUNNING.

    Args:
        name(str): The name of the crawler to remove.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.delete(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.delete name=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="glue", operation="delete_crawler", op_kwargs={"Name": name}
    )


async def get(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves metadata for a specified crawler.

    Args:
        name(str): The name of the crawler to retrieve metadata for.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.get(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.get name=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="glue", operation="get_crawler", op_kwargs={"Name": name}
    )


async def get_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves metadata for all crawlers defined in the customer account.

    Args:
        max_results(int, optional): The number of crawlers to return on each call. Defaults to None.
        next_token(str, optional): A continuation token, if this is a continuation request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="get_crawlers",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def list_all(
    hub, ctx, max_results: int = None, next_token: str = None, tags: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the names of all crawler resources in this Amazon Web Services account, or the resources with the specified tag. This operation allows you to see which resources are available in your account, and their names. This operation takes the optional Tags field, which you can use as a filter on the response so that tagged resources can be retrieved as a group. If you choose to use tags filtering, only resources with the tag are retrieved.

    Args:
        max_results(int, optional): The maximum size of a list to return. Defaults to None.
        next_token(str, optional): A continuation token, if this is a continuation request. Defaults to None.
        tags(Dict, optional): Specifies to return only these tagged resources. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="list_crawlers",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token, "Tags": tags},
    )


async def start(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Starts a crawl using the specified crawler, regardless of what is scheduled. If the crawler is already running, returns a CrawlerRunningException.

    Args:
        name(str): Name of the crawler to start.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.start(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.start name=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="glue", operation="start_crawler", op_kwargs={"Name": name}
    )


async def stop(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    If the specified crawler is running, stops the crawl.

    Args:
        name(str): Name of the crawler to stop.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.stop(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.stop name=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="glue", operation="stop_crawler", op_kwargs={"Name": name}
    )


async def update(
    hub,
    ctx,
    name: str,
    role: str = None,
    database_name: str = None,
    description: str = None,
    targets: Dict = None,
    schedule: str = None,
    classifiers: List = None,
    table_prefix: str = None,
    schema_change_policy: Dict = None,
    recrawl_policy: Dict = None,
    lineage_configuration: Dict = None,
    configuration: str = None,
    crawler_security_configuration: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a crawler. If a crawler is running, you must stop it using StopCrawler before updating it.

    Args:
        name(str): Name of the new crawler.
        role(str, optional): The IAM role or Amazon Resource Name (ARN) of an IAM role that is used by the new crawler to access customer resources. Defaults to None.
        database_name(str, optional): The Glue database where results are stored, such as: arn:aws:daylight:us-east-1::database/sometable/*. Defaults to None.
        description(str, optional): A description of the new crawler. Defaults to None.
        targets(Dict, optional): A list of targets to crawl. Defaults to None.
        schedule(str, optional): A cron expression used to specify the schedule (see Time-Based Schedules for Jobs and Crawlers. For example, to run something every day at 12:15 UTC, you would specify: cron(15 12 * * ? *). Defaults to None.
        classifiers(List, optional): A list of custom classifiers that the user has registered. By default, all built-in classifiers are included in a crawl, but these custom classifiers always override the default classifiers for a given classification. Defaults to None.
        table_prefix(str, optional): The table prefix used for catalog tables that are created. Defaults to None.
        schema_change_policy(Dict, optional): The policy for the crawler's update and deletion behavior. Defaults to None.
        recrawl_policy(Dict, optional): A policy that specifies whether to crawl the entire dataset again, or to crawl only folders that were added since the last crawler run. Defaults to None.
        lineage_configuration(Dict, optional): Specifies data lineage configuration settings for the crawler. Defaults to None.
        configuration(str, optional): Crawler configuration information. This versioned JSON string allows users to specify aspects of a crawler's behavior. For more information, see Configuring a Crawler. Defaults to None.
        crawler_security_configuration(str, optional): The name of the SecurityConfiguration structure to be used by this crawler. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.crawler.init.update(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.crawler.init.update name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="update_crawler",
        op_kwargs={
            "Name": name,
            "Role": role,
            "DatabaseName": database_name,
            "Description": description,
            "Targets": targets,
            "Schedule": schedule,
            "Classifiers": classifiers,
            "TablePrefix": table_prefix,
            "SchemaChangePolicy": schema_change_policy,
            "RecrawlPolicy": recrawl_policy,
            "LineageConfiguration": lineage_configuration,
            "Configuration": configuration,
            "CrawlerSecurityConfiguration": crawler_security_configuration,
        },
    )
