"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    grok_classifier: Dict = None,
    xml_classifier: Dict = None,
    json_classifier: Dict = None,
    csv_classifier: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a classifier in the user's account. This can be a GrokClassifier, an XMLClassifier, a JsonClassifier, or a CsvClassifier, depending on which field of the request is present.

    Args:
        grok_classifier(Dict, optional): A GrokClassifier object specifying the classifier to create. Defaults to None.
        xml_classifier(Dict, optional): An XMLClassifier object specifying the classifier to create. Defaults to None.
        json_classifier(Dict, optional): A JsonClassifier object specifying the classifier to create. Defaults to None.
        csv_classifier(Dict, optional): A CsvClassifier object specifying the classifier to create. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.classifier.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.classifier.create
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="create_classifier",
        op_kwargs={
            "GrokClassifier": grok_classifier,
            "XMLClassifier": xml_classifier,
            "JsonClassifier": json_classifier,
            "CsvClassifier": csv_classifier,
        },
    )


async def delete(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes a classifier from the Data Catalog.

    Args:
        name(str): Name of the classifier to remove.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.classifier.delete(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.classifier.delete name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="delete_classifier",
        op_kwargs={"Name": name},
    )


async def get(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieve a classifier by name.

    Args:
        name(str): Name of the classifier to retrieve.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.classifier.get(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.classifier.get name=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="glue", operation="get_classifier", op_kwargs={"Name": name}
    )


async def get_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all classifier objects in the Data Catalog.

    Args:
        max_results(int, optional): The size of the list to return (optional). Defaults to None.
        next_token(str, optional): An optional continuation token. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.classifier.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.classifier.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="get_classifiers",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(
    hub,
    ctx,
    grok_classifier: Dict = None,
    xml_classifier: Dict = None,
    json_classifier: Dict = None,
    csv_classifier: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies an existing classifier (a GrokClassifier, an XMLClassifier, a JsonClassifier, or a CsvClassifier, depending on which field is present).

    Args:
        grok_classifier(Dict, optional): A GrokClassifier object with updated fields. Defaults to None.
        xml_classifier(Dict, optional): An XMLClassifier object with updated fields. Defaults to None.
        json_classifier(Dict, optional): A JsonClassifier object with updated fields. Defaults to None.
        csv_classifier(Dict, optional): A CsvClassifier object with updated fields. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.classifier.update(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.classifier.update
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="update_classifier",
        op_kwargs={
            "GrokClassifier": grok_classifier,
            "XMLClassifier": xml_classifier,
            "JsonClassifier": json_classifier,
            "CsvClassifier": csv_classifier,
        },
    )
