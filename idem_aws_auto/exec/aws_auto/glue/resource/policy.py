"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(
    hub, ctx, policy_hash_condition: str = None, resource_arn: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a specified policy.

    Args:
        policy_hash_condition(str, optional): The hash value returned when this policy was set. Defaults to None.
        resource_arn(str, optional): The ARN of the Glue resource for the resource policy to be deleted. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.resource.policy.delete(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.resource.policy.delete
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="delete_resource_policy",
        op_kwargs={
            "PolicyHashCondition": policy_hash_condition,
            "ResourceArn": resource_arn,
        },
    )


async def get_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the resource policies set on individual resources by Resource Access Manager during cross-account permission grants. Also retrieves the Data Catalog resource policy. If you enabled metadata encryption in Data Catalog settings, and you do not have permission on the KMS key, the operation can't return the Data Catalog resource policy.

    Args:
        next_token(str, optional): A continuation token, if this is a continuation request. Defaults to None.
        max_results(int, optional): The maximum size of a list to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.resource.policy.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.resource.policy.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="get_resource_policies",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )


async def get(hub, ctx, resource_arn: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a specified resource policy.

    Args:
        resource_arn(str, optional): The ARN of the Glue resource for which to retrieve the resource policy. If not supplied, the Data Catalog resource policy is returned. Use GetResourcePolicies to view all existing resource policies. For more information see Specifying Glue Resource ARNs. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.resource.policy.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.resource.policy.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="get_resource_policy",
        op_kwargs={"ResourceArn": resource_arn},
    )


async def put(
    hub,
    ctx,
    policy_in_json: str,
    resource_arn: str = None,
    policy_hash_condition: str = None,
    policy_exists_condition: str = None,
    enable_hybrid: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Sets the Data Catalog resource policy for access control.

    Args:
        policy_in_json(str): Contains the policy document to set, in JSON format.
        resource_arn(str, optional): Do not use. For internal use only. Defaults to None.
        policy_hash_condition(str, optional): The hash value returned when the previous policy was set using PutResourcePolicy. Its purpose is to prevent concurrent modifications of a policy. Do not use this parameter if no previous policy has been set. Defaults to None.
        policy_exists_condition(str, optional): A value of MUST_EXIST is used to update a policy. A value of NOT_EXIST is used to create a new policy. If a value of NONE or a null value is used, the call does not depend on the existence of a policy. Defaults to None.
        enable_hybrid(str, optional): If 'TRUE', indicates that you are using both methods to grant cross-account access to Data Catalog resources:   By directly updating the resource policy with PutResourePolicy    By using the Grant permissions command on the Amazon Web Services Management Console.   Must be set to 'TRUE' if you have already used the Management Console to grant cross-account access, otherwise the call fails. Default is 'FALSE'. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.glue.resource.policy.put(ctx, policy_in_json=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.glue.resource.policy.put policy_in_json=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="glue",
        operation="put_resource_policy",
        op_kwargs={
            "PolicyInJson": policy_in_json,
            "ResourceArn": resource_arn,
            "PolicyHashCondition": policy_hash_condition,
            "PolicyExistsCondition": policy_exists_condition,
            "EnableHybrid": enable_hybrid,
        },
    )
