"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import List


async def tag(hub, ctx, arn: str, tags: List) -> None:
    r"""
    **Autogenerated function**

    Attaches a key-value pair to a resource, as identified by its Amazon Resource Name (ARN). Resources are users, servers, roles, and other entities. There is no response returned from this call.

    Args:
        arn(str): An Amazon Resource Name (ARN) for a specific Amazon Web Services resource, such as a server, user, or role.
        tags(List): Key-value pairs assigned to ARNs that you can use to group and search for resources by type. You can attach this metadata to user accounts for any purpose.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.resource.tag(ctx, arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.resource.tag arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="tag_resource",
        op_kwargs={"Arn": arn, "Tags": tags},
    )


async def untag(hub, ctx, arn: str, tag_keys: List) -> None:
    r"""
    **Autogenerated function**

    Detaches a key-value pair from a resource, as identified by its Amazon Resource Name (ARN). Resources are users, servers, roles, and other entities. No response is returned from this call.

    Args:
        arn(str): The value of the resource that will have the tag removed. An Amazon Resource Name (ARN) is an identifier for a specific Amazon Web Services resource, such as a server, user, or role.
        tag_keys(List): TagKeys are key-value pairs assigned to ARNs that can be used to group and search for resources by type. This metadata can be attached to resources for any purpose.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.resource.untag(ctx, arn=value, tag_keys=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.resource.untag arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="untag_resource",
        op_kwargs={"Arn": arn, "TagKeys": tag_keys},
    )
