"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    certificate: str = None,
    domain: str = None,
    endpoint_details: Dict = None,
    endpoint_type: str = None,
    host_key: str = None,
    identity_provider_details: Dict = None,
    identity_provider_type: str = None,
    logging_role: str = None,
    protocols: List = None,
    security_policy_name: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Instantiates an auto-scaling virtual server based on the selected file transfer protocol in Amazon Web Services. When you make updates to your file transfer protocol-enabled server or when you work with users, use the service-generated ServerId property that is assigned to the newly created server.

    Args:
        certificate(str, optional): The Amazon Resource Name (ARN) of the Amazon Web Services Certificate Manager (ACM) certificate. Required when Protocols is set to FTPS. To request a new public certificate, see Request a public certificate in the  Amazon Web Services Certificate Manager User Guide. To import an existing certificate into ACM, see Importing certificates into ACM in the  Amazon Web Services Certificate Manager User Guide. To request a private certificate to use FTPS through private IP addresses, see Request a private certificate in the  Amazon Web Services Certificate Manager User Guide. Certificates with the following cryptographic algorithms and key sizes are supported:   2048-bit RSA (RSA_2048)   4096-bit RSA (RSA_4096)   Elliptic Prime Curve 256 bit (EC_prime256v1)   Elliptic Prime Curve 384 bit (EC_secp384r1)   Elliptic Prime Curve 521 bit (EC_secp521r1)    The certificate must be a valid SSL/TLS X.509 version 3 certificate with FQDN or IP address specified and information about the issuer. . Defaults to None.
        domain(str, optional): The domain of the storage system that is used for file transfers. There are two domains available: Amazon Simple Storage Service (Amazon S3) and Amazon Elastic File System (Amazon EFS). The default value is S3.  After the server is created, the domain cannot be changed. . Defaults to None.
        endpoint_details(Dict, optional): The virtual private cloud (VPC) endpoint settings that are configured for your server. When you host your endpoint within your VPC, you can make it accessible only to resources within your VPC, or you can attach Elastic IP addresses and make it accessible to clients over the internet. Your VPC's default security groups are automatically assigned to your endpoint. Defaults to None.
        endpoint_type(str, optional): The type of endpoint that you want your server to use. You can choose to make your server's endpoint publicly accessible (PUBLIC) or host it inside your VPC. With an endpoint that is hosted in a VPC, you can restrict access to your server and resources only within your VPC or choose to make it internet facing by attaching Elastic IP addresses directly to it.   After May 19, 2021, you won't be able to create a server using EndpointType=VPC_ENDPOINT in your Amazon Web Services account if your account hasn't already done so before May 19, 2021. If you have already created servers with EndpointType=VPC_ENDPOINT in your Amazon Web Services account on or before May 19, 2021, you will not be affected. After this date, use EndpointType=VPC. For more information, see https://docs.aws.amazon.com/transfer/latest/userguide/create-server-in-vpc.html#deprecate-vpc-endpoint. It is recommended that you use VPC as the EndpointType. With this endpoint type, you have the option to directly associate up to three Elastic IPv4 addresses (BYO IP included) with your server's endpoint and use VPC security groups to restrict traffic by the client's public IP address. This is not possible with EndpointType set to VPC_ENDPOINT. . Defaults to None.
        host_key(str, optional): The RSA private key as generated by the ssh-keygen -N "" -m PEM -f my-new-server-key command.  If you aren't planning to migrate existing users from an existing SFTP-enabled server to a new server, don't update the host key. Accidentally changing a server's host key can be disruptive.  For more information, see Change the host key for your SFTP-enabled server in the Amazon Web Services Transfer Family User Guide. Defaults to None.
        identity_provider_details(Dict, optional): Required when IdentityProviderType is set to AWS_DIRECTORY_SERVICE or API_GATEWAY. Accepts an array containing all of the information required to use a directory in AWS_DIRECTORY_SERVICE or invoke a customer-supplied authentication API, including the API Gateway URL. Not required when IdentityProviderType is set to SERVICE_MANAGED. Defaults to None.
        identity_provider_type(str, optional): Specifies the mode of authentication for a server. The default value is SERVICE_MANAGED, which allows you to store and access user credentials within the Amazon Web Services Transfer Family service. Use AWS_DIRECTORY_SERVICE to provide access to Active Directory groups in Amazon Web Services Managed Active Directory or Microsoft Active Directory in your on-premises environment or in Amazon Web Services using AD Connectors. This option also requires you to provide a Directory ID using the IdentityProviderDetails parameter. Use the API_GATEWAY value to integrate with an identity provider of your choosing. The API_GATEWAY setting requires you to provide an API Gateway endpoint URL to call for authentication using the IdentityProviderDetails parameter. Defaults to None.
        logging_role(str, optional): Specifies the Amazon Resource Name (ARN) of the Amazon Web Services Identity and Access Management (IAM) role that allows a server to turn on Amazon CloudWatch logging for Amazon S3 or Amazon EFS events. When set, user activity can be viewed in your CloudWatch logs. Defaults to None.
        protocols(List, optional): Specifies the file transfer protocol or protocols over which your file transfer protocol client can connect to your server's endpoint. The available protocols are:    SFTP (Secure Shell (SSH) File Transfer Protocol): File transfer over SSH    FTPS (File Transfer Protocol Secure): File transfer with TLS encryption    FTP (File Transfer Protocol): Unencrypted file transfer    If you select FTPS, you must choose a certificate stored in Amazon Web Services Certificate Manager (ACM) which is used to identify your server when clients connect to it over FTPS. If Protocol includes either FTP or FTPS, then the EndpointType must be VPC and the IdentityProviderType must be AWS_DIRECTORY_SERVICE or API_GATEWAY. If Protocol includes FTP, then AddressAllocationIds cannot be associated. If Protocol is set only to SFTP, the EndpointType can be set to PUBLIC and the IdentityProviderType can be set to SERVICE_MANAGED. . Defaults to None.
        security_policy_name(str, optional): Specifies the name of the security policy that is attached to the server. Defaults to None.
        tags(List, optional): Key-value pairs that can be used to group and search for servers. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.create
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="create_server",
        op_kwargs={
            "Certificate": certificate,
            "Domain": domain,
            "EndpointDetails": endpoint_details,
            "EndpointType": endpoint_type,
            "HostKey": host_key,
            "IdentityProviderDetails": identity_provider_details,
            "IdentityProviderType": identity_provider_type,
            "LoggingRole": logging_role,
            "Protocols": protocols,
            "SecurityPolicyName": security_policy_name,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, server_id: str) -> None:
    r"""
    **Autogenerated function**

    Deletes the file transfer protocol-enabled server that you specify. No response returns from this operation.

    Args:
        server_id(str): A unique system-assigned identifier for a server instance.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.delete(ctx, server_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.delete server_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="delete_server",
        op_kwargs={"ServerId": server_id},
    )


async def describe(hub, ctx, server_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes a file transfer protocol-enabled server that you specify by passing the ServerId parameter. The response contains a description of a server's properties. When you set EndpointType to VPC, the response will contain the EndpointDetails.

    Args:
        server_id(str): A system-assigned unique identifier for a server.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.describe(ctx, server_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.describe server_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="describe_server",
        op_kwargs={"ServerId": server_id},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the file transfer protocol-enabled servers that are associated with your Amazon Web Services account.

    Args:
        max_results(int, optional): Specifies the number of servers to return as a response to the ListServers query. Defaults to None.
        next_token(str, optional): When additional results are obtained from the ListServers command, a NextToken parameter is returned in the output. You can then pass the NextToken parameter in a subsequent command to continue listing additional servers. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="list_servers",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def start(hub, ctx, server_id: str) -> None:
    r"""
    **Autogenerated function**

    Changes the state of a file transfer protocol-enabled server from OFFLINE to ONLINE. It has no impact on a server that is already ONLINE. An ONLINE server can accept and process file transfer jobs. The state of STARTING indicates that the server is in an intermediate state, either not fully able to respond, or not fully online. The values of START_FAILED can indicate an error condition. No response is returned from this call.

    Args:
        server_id(str): A system-assigned unique identifier for a server that you start.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.start(ctx, server_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.start server_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="start_server",
        op_kwargs={"ServerId": server_id},
    )


async def stop(hub, ctx, server_id: str) -> None:
    r"""
    **Autogenerated function**

    Changes the state of a file transfer protocol-enabled server from ONLINE to OFFLINE. An OFFLINE server cannot accept and process file transfer jobs. Information tied to your server, such as server and user properties, are not affected by stopping your server.  Stopping the server will not reduce or impact your file transfer protocol endpoint billing; you must delete the server to stop being billed.  The state of STOPPING indicates that the server is in an intermediate state, either not fully able to respond, or not fully offline. The values of STOP_FAILED can indicate an error condition. No response is returned from this call.

    Args:
        server_id(str): A system-assigned unique identifier for a server that you stopped.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.stop(ctx, server_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.stop server_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="stop_server",
        op_kwargs={"ServerId": server_id},
    )


async def update(
    hub,
    ctx,
    server_id: str,
    certificate: str = None,
    protocol_details: Dict = None,
    endpoint_details: Dict = None,
    endpoint_type: str = None,
    host_key: str = None,
    identity_provider_details: Dict = None,
    logging_role: str = None,
    protocols: List = None,
    security_policy_name: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the file transfer protocol-enabled server's properties after that server has been created. The UpdateServer call returns the ServerId of the server you updated.

    Args:
        certificate(str, optional): The Amazon Resource Name (ARN) of the Amazon Web ServicesCertificate Manager (ACM) certificate. Required when Protocols is set to FTPS. To request a new public certificate, see Request a public certificate in the  Amazon Web ServicesCertificate Manager User Guide. To import an existing certificate into ACM, see Importing certificates into ACM in the  Amazon Web ServicesCertificate Manager User Guide. To request a private certificate to use FTPS through private IP addresses, see Request a private certificate in the  Amazon Web ServicesCertificate Manager User Guide. Certificates with the following cryptographic algorithms and key sizes are supported:   2048-bit RSA (RSA_2048)   4096-bit RSA (RSA_4096)   Elliptic Prime Curve 256 bit (EC_prime256v1)   Elliptic Prime Curve 384 bit (EC_secp384r1)   Elliptic Prime Curve 521 bit (EC_secp521r1)    The certificate must be a valid SSL/TLS X.509 version 3 certificate with FQDN or IP address specified and information about the issuer. . Defaults to None.
        protocol_details(Dict, optional):  The protocol settings that are configured for your server.   Use the PassiveIp parameter to indicate passive mode (for FTP and FTPS protocols). Enter a single dotted-quad IPv4 address, such as the external IP address of a firewall, router, or load balancer. . Defaults to None.
        endpoint_details(Dict, optional): The virtual private cloud (VPC) endpoint settings that are configured for your server. When you host your endpoint within your VPC, you can make it accessible only to resources within your VPC, or you can attach Elastic IP addresses and make it accessible to clients over the internet. Your VPC's default security groups are automatically assigned to your endpoint. Defaults to None.
        endpoint_type(str, optional): The type of endpoint that you want your server to use. You can choose to make your server's endpoint publicly accessible (PUBLIC) or host it inside your VPC. With an endpoint that is hosted in a VPC, you can restrict access to your server and resources only within your VPC or choose to make it internet facing by attaching Elastic IP addresses directly to it.   After May 19, 2021, you won't be able to create a server using EndpointType=VPC_ENDPOINT in your Amazon Web Servicesaccount if your account hasn't already done so before May 19, 2021. If you have already created servers with EndpointType=VPC_ENDPOINT in your Amazon Web Servicesaccount on or before May 19, 2021, you will not be affected. After this date, use EndpointType=VPC. For more information, see https://docs.aws.amazon.com/transfer/latest/userguide/create-server-in-vpc.html#deprecate-vpc-endpoint. It is recommended that you use VPC as the EndpointType. With this endpoint type, you have the option to directly associate up to three Elastic IPv4 addresses (BYO IP included) with your server's endpoint and use VPC security groups to restrict traffic by the client's public IP address. This is not possible with EndpointType set to VPC_ENDPOINT. . Defaults to None.
        host_key(str, optional): The RSA private key as generated by ssh-keygen -N "" -m PEM -f my-new-server-key.  If you aren't planning to migrate existing users from an existing server to a new server, don't update the host key. Accidentally changing a server's host key can be disruptive.  For more information, see Change the host key for your SFTP-enabled server in the Amazon Web ServicesTransfer Family User Guide. Defaults to None.
        identity_provider_details(Dict, optional): An array containing all of the information required to call a customer's authentication API method. Defaults to None.
        logging_role(str, optional): Specifies the Amazon Resource Name (ARN) of the Amazon Web Services Identity and Access Management (IAM) role that allows a server to turn on Amazon CloudWatch logging for Amazon S3 or Amazon EFS events. When set, user activity can be viewed in your CloudWatch logs. Defaults to None.
        protocols(List, optional): Specifies the file transfer protocol or protocols over which your file transfer protocol client can connect to your server's endpoint. The available protocols are:   Secure Shell (SSH) File Transfer Protocol (SFTP): File transfer over SSH   File Transfer Protocol Secure (FTPS): File transfer with TLS encryption   File Transfer Protocol (FTP): Unencrypted file transfer    If you select FTPS, you must choose a certificate stored in Amazon Web ServicesCertificate Manager (ACM) which will be used to identify your server when clients connect to it over FTPS. If Protocol includes either FTP or FTPS, then the EndpointType must be VPC and the IdentityProviderType must be AWS_DIRECTORY_SERVICE or API_GATEWAY. If Protocol includes FTP, then AddressAllocationIds cannot be associated. If Protocol is set only to SFTP, the EndpointType can be set to PUBLIC and the IdentityProviderType can be set to SERVICE_MANAGED. . Defaults to None.
        security_policy_name(str, optional): Specifies the name of the security policy that is attached to the server. Defaults to None.
        server_id(str): A system-assigned unique identifier for a server instance that the user account is assigned to.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.transfer.server.update(ctx, server_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.transfer.server.update server_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="transfer",
        operation="update_server",
        op_kwargs={
            "Certificate": certificate,
            "ProtocolDetails": protocol_details,
            "EndpointDetails": endpoint_details,
            "EndpointType": endpoint_type,
            "HostKey": host_key,
            "IdentityProviderDetails": identity_provider_details,
            "LoggingRole": logging_role,
            "Protocols": protocols,
            "SecurityPolicyName": security_policy_name,
            "ServerId": server_id,
        },
    )
