"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_arn: str, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about AWS tags for a specified Amazon Resource Name (ARN) in AWS CodeCommit. For a list of valid resources in AWS CodeCommit, see CodeCommit Resources and Operations in the AWS CodeCommit User Guide.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource for which you want to get information about tags, if any.
        next_token(str, optional): An enumeration token that, when provided in a request, returns the next batch of the results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.tags_for_resource.list(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="list_tags_for_resource",
        op_kwargs={"resourceArn": resource_arn, "nextToken": next_token},
    )
