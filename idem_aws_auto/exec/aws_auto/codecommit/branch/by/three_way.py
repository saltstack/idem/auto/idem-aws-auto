"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def merge(
    hub,
    ctx,
    repository_name: str,
    source_commit_specifier: str,
    destination_commit_specifier: str,
    target_branch: str = None,
    conflict_detail_level: str = None,
    conflict_resolution_strategy: str = None,
    author_name: str = None,
    email: str = None,
    commit_message: str = None,
    keep_empty_folders: bool = None,
    conflict_resolution: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Merges two specified branches using the three-way merge strategy.

    Args:
        repository_name(str): The name of the repository where you want to merge two branches.
        source_commit_specifier(str): The branch, tag, HEAD, or other fully qualified reference used to identify a commit (for example, a branch name or a full commit ID).
        destination_commit_specifier(str): The branch, tag, HEAD, or other fully qualified reference used to identify a commit (for example, a branch name or a full commit ID).
        target_branch(str, optional): The branch where the merge is applied. . Defaults to None.
        conflict_detail_level(str, optional): The level of conflict detail to use. If unspecified, the default FILE_LEVEL is used, which returns a not-mergeable result if the same file has differences in both branches. If LINE_LEVEL is specified, a conflict is considered not mergeable if the same file in both branches has differences on the same line. Defaults to None.
        conflict_resolution_strategy(str, optional): Specifies which branch to use when resolving conflicts, or whether to attempt automatically merging two versions of a file. The default is NONE, which requires any conflicts to be resolved manually before the merge operation is successful. Defaults to None.
        author_name(str, optional): The name of the author who created the commit. This information is used as both the author and committer for the commit. Defaults to None.
        email(str, optional): The email address of the person merging the branches. This information is used in the commit information for the merge. Defaults to None.
        commit_message(str, optional): The commit message to include in the commit information for the merge. Defaults to None.
        keep_empty_folders(bool, optional): If the commit contains deletions, whether to keep a folder or folder structure if the changes leave the folders empty. If true, a .gitkeep file is created for empty folders. The default is false. Defaults to None.
        conflict_resolution(Dict, optional): If AUTOMERGE is the conflict resolution strategy, a list of inputs to use when resolving conflicts during a merge. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.branch.by.three_way.merge(
                ctx,
                repository_name=value,
                source_commit_specifier=value,
                destination_commit_specifier=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.branch.by.three_way.merge repository_name=value, source_commit_specifier=value, destination_commit_specifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="merge_branches_by_three_way",
        op_kwargs={
            "repositoryName": repository_name,
            "sourceCommitSpecifier": source_commit_specifier,
            "destinationCommitSpecifier": destination_commit_specifier,
            "targetBranch": target_branch,
            "conflictDetailLevel": conflict_detail_level,
            "conflictResolutionStrategy": conflict_resolution_strategy,
            "authorName": author_name,
            "email": email,
            "commitMessage": commit_message,
            "keepEmptyFolders": keep_empty_folders,
            "conflictResolution": conflict_resolution,
        },
    )
