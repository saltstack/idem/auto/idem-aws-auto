"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(
    hub,
    ctx,
    approval_rule_template_name: str,
    new_rule_content: str,
    existing_rule_content_sha256: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the content of an approval rule template. You can change the number of required approvals, the membership of the approval rule, and whether an approval pool is defined.

    Args:
        approval_rule_template_name(str): The name of the approval rule template where you want to update the content of the rule. .
        new_rule_content(str): The content that replaces the existing content of the rule. Content statements must be complete. You cannot provide only the changes.
        existing_rule_content_sha256(str, optional): The SHA-256 hash signature for the content of the approval rule. You can retrieve this information by using GetPullRequest. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.approval_rule_template.content.update(
                ctx, approval_rule_template_name=value, new_rule_content=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.approval_rule_template.content.update approval_rule_template_name=value, new_rule_content=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="update_approval_rule_template_content",
        op_kwargs={
            "approvalRuleTemplateName": approval_rule_template_name,
            "newRuleContent": new_rule_content,
            "existingRuleContentSha256": existing_rule_content_sha256,
        },
    )
