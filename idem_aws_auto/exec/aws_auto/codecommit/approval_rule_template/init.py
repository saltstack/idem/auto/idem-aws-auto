"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    approval_rule_template_name: str,
    approval_rule_template_content: str,
    approval_rule_template_description: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a template for approval rules that can then be associated with one or more repositories in your AWS account. When you associate a template with a repository, AWS CodeCommit creates an approval rule that matches the conditions of the template for all pull requests that meet the conditions of the template. For more information, see AssociateApprovalRuleTemplateWithRepository.

    Args:
        approval_rule_template_name(str): The name of the approval rule template. Provide descriptive names, because this name is applied to the approval rules created automatically in associated repositories.
        approval_rule_template_content(str): The content of the approval rule that is created on pull requests in associated repositories. If you specify one or more destination references (branches), approval rules are created in an associated repository only if their destination references (branches) match those specified in the template.  When you create the content of the approval rule template, you can specify approvers in an approval pool in one of two ways:    CodeCommitApprovers: This option only requires an AWS account and a resource. It can be used for both IAM users and federated access users whose name matches the provided resource name. This is a very powerful option that offers a great deal of flexibility. For example, if you specify the AWS account 123456789012 and Mary_Major, all of the following are counted as approvals coming from that user:   An IAM user in the account (arn:aws:iam::123456789012:user/Mary_Major)   A federated user identified in IAM as Mary_Major (arn:aws:sts::123456789012:federated-user/Mary_Major)   This option does not recognize an active session of someone assuming the role of CodeCommitReview with a role session name of Mary_Major (arn:aws:sts::123456789012:assumed-role/CodeCommitReview/Mary_Major) unless you include a wildcard (*Mary_Major).    Fully qualified ARN: This option allows you to specify the fully qualified Amazon Resource Name (ARN) of the IAM user or role.    For more information about IAM ARNs, wildcards, and formats, see IAM Identifiers in the IAM User Guide. .
        approval_rule_template_description(str, optional): The description of the approval rule template. Consider providing a description that explains what this template does and when it might be appropriate to associate it with repositories. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.approval_rule_template.init.create(
                ctx, approval_rule_template_name=value, approval_rule_template_content=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.approval_rule_template.init.create approval_rule_template_name=value, approval_rule_template_content=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="create_approval_rule_template",
        op_kwargs={
            "approvalRuleTemplateName": approval_rule_template_name,
            "approvalRuleTemplateContent": approval_rule_template_content,
            "approvalRuleTemplateDescription": approval_rule_template_description,
        },
    )


async def delete(hub, ctx, approval_rule_template_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a specified approval rule template. Deleting a template does not remove approval rules on pull requests already created with the template.

    Args:
        approval_rule_template_name(str): The name of the approval rule template to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.approval_rule_template.init.delete(
                ctx, approval_rule_template_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.approval_rule_template.init.delete approval_rule_template_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="delete_approval_rule_template",
        op_kwargs={"approvalRuleTemplateName": approval_rule_template_name},
    )


async def get(hub, ctx, approval_rule_template_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a specified approval rule template.

    Args:
        approval_rule_template_name(str): The name of the approval rule template for which you want to get information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.approval_rule_template.init.get(
                ctx, approval_rule_template_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.approval_rule_template.init.get approval_rule_template_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="get_approval_rule_template",
        op_kwargs={"approvalRuleTemplateName": approval_rule_template_name},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all approval rule templates in the specified AWS Region in your AWS account. If an AWS Region is not specified, the AWS Region where you are signed in is used.

    Args:
        next_token(str, optional): An enumeration token that, when provided in a request, returns the next batch of the results. Defaults to None.
        max_results(int, optional): A non-zero, non-negative integer used to limit the number of returned results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.approval_rule_template.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.approval_rule_template.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="list_approval_rule_templates",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )
