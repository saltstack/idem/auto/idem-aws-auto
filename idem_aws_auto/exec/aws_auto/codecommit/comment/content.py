"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, comment_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the content of a comment made on a change, file, or commit in a repository.

    Args:
        comment_id(str): The unique, system-generated ID of the comment. To get this ID, use GetCommentsForComparedCommit or GetCommentsForPullRequest.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.comment.content.delete(ctx, comment_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.comment.content.delete comment_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="delete_comment_content",
        op_kwargs={"commentId": comment_id},
    )
