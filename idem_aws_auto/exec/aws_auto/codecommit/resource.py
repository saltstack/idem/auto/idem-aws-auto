"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: Dict) -> None:
    r"""
    **Autogenerated function**

    Adds or updates tags for a resource in AWS CodeCommit. For a list of valid resources in AWS CodeCommit, see CodeCommit Resources and Operations in the AWS CodeCommit User Guide.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource to which you want to add or update tags.
        tags(Dict): The key-value pair to use when tagging this repository.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="tag_resource",
        op_kwargs={"resourceArn": resource_arn, "tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> None:
    r"""
    **Autogenerated function**

    Removes tags for a resource in AWS CodeCommit. For a list of valid resources in AWS CodeCommit, see CodeCommit Resources and Operations in the AWS CodeCommit User Guide.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource to which you want to remove tags.
        tag_keys(List): The tag key for each tag that you want to remove from the resource.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="untag_resource",
        op_kwargs={"resourceArn": resource_arn, "tagKeys": tag_keys},
    )
