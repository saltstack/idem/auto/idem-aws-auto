"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def merge(
    hub,
    ctx,
    pull_request_id: str,
    repository_name: str,
    source_commit_id: str = None,
    conflict_detail_level: str = None,
    conflict_resolution_strategy: str = None,
    commit_message: str = None,
    author_name: str = None,
    email: str = None,
    keep_empty_folders: bool = None,
    conflict_resolution: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Attempts to merge the source commit of a pull request into the specified destination branch for that pull request at the specified commit using the squash merge strategy. If the merge is successful, it closes the pull request.

    Args:
        pull_request_id(str): The system-generated ID of the pull request. To get this ID, use ListPullRequests.
        repository_name(str): The name of the repository where the pull request was created.
        source_commit_id(str, optional): The full commit ID of the original or updated commit in the pull request source branch. Pass this value if you want an exception thrown if the current commit ID of the tip of the source branch does not match this commit ID. Defaults to None.
        conflict_detail_level(str, optional): The level of conflict detail to use. If unspecified, the default FILE_LEVEL is used, which returns a not-mergeable result if the same file has differences in both branches. If LINE_LEVEL is specified, a conflict is considered not mergeable if the same file in both branches has differences on the same line. Defaults to None.
        conflict_resolution_strategy(str, optional): Specifies which branch to use when resolving conflicts, or whether to attempt automatically merging two versions of a file. The default is NONE, which requires any conflicts to be resolved manually before the merge operation is successful. Defaults to None.
        commit_message(str, optional): The commit message to include in the commit information for the merge. Defaults to None.
        author_name(str, optional): The name of the author who created the commit. This information is used as both the author and committer for the commit. Defaults to None.
        email(str, optional): The email address of the person merging the branches. This information is used in the commit information for the merge. Defaults to None.
        keep_empty_folders(bool, optional): If the commit contains deletions, whether to keep a folder or folder structure if the changes leave the folders empty. If true, a .gitkeep file is created for empty folders. The default is false. Defaults to None.
        conflict_resolution(Dict, optional): If AUTOMERGE is the conflict resolution strategy, a list of inputs to use when resolving conflicts during a merge. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codecommit.pull_request.by.squash.merge(
                ctx, pull_request_id=value, repository_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codecommit.pull_request.by.squash.merge pull_request_id=value, repository_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codecommit",
        operation="merge_pull_request_by_squash",
        op_kwargs={
            "pullRequestId": pull_request_id,
            "repositoryName": repository_name,
            "sourceCommitId": source_commit_id,
            "conflictDetailLevel": conflict_detail_level,
            "conflictResolutionStrategy": conflict_resolution_strategy,
            "commitMessage": commit_message,
            "authorName": author_name,
            "email": email,
            "keepEmptyFolders": keep_empty_folders,
            "conflictResolution": conflict_resolution,
        },
    )
