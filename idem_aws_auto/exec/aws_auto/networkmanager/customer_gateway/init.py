"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(
    hub,
    ctx,
    customer_gateway_arn: str,
    global_network_id: str,
    device_id: str,
    link_id: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Associates a customer gateway with a device and optionally, with a link. If you specify a link, it must be associated with the specified device.  You can only associate customer gateways that are connected to a VPN attachment on a transit gateway. The transit gateway must be registered in your global network. When you register a transit gateway, customer gateways that are connected to the transit gateway are automatically included in the global network. To list customer gateways that are connected to a transit gateway, use the DescribeVpnConnections EC2 API and filter by transit-gateway-id. You cannot associate a customer gateway with more than one device and link.

    Args:
        customer_gateway_arn(str): The Amazon Resource Name (ARN) of the customer gateway. For more information, see Resources Defined by Amazon EC2.
        global_network_id(str): The ID of the global network.
        device_id(str): The ID of the device.
        link_id(str, optional): The ID of the link. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.networkmanager.customer_gateway.init.associate(
                ctx, customer_gateway_arn=value, global_network_id=value, device_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.networkmanager.customer_gateway.init.associate customer_gateway_arn=value, global_network_id=value, device_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="networkmanager",
        operation="associate_customer_gateway",
        op_kwargs={
            "CustomerGatewayArn": customer_gateway_arn,
            "GlobalNetworkId": global_network_id,
            "DeviceId": device_id,
            "LinkId": link_id,
        },
    )


async def disassociate(
    hub, ctx, global_network_id: str, customer_gateway_arn: str
) -> Dict:
    r"""
    **Autogenerated function**

    Disassociates a customer gateway from a device and a link.

    Args:
        global_network_id(str): The ID of the global network.
        customer_gateway_arn(str): The Amazon Resource Name (ARN) of the customer gateway. For more information, see Resources Defined by Amazon EC2.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.networkmanager.customer_gateway.init.disassociate(
                ctx, global_network_id=value, customer_gateway_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.networkmanager.customer_gateway.init.disassociate global_network_id=value, customer_gateway_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="networkmanager",
        operation="disassociate_customer_gateway",
        op_kwargs={
            "GlobalNetworkId": global_network_id,
            "CustomerGatewayArn": customer_gateway_arn,
        },
    )
