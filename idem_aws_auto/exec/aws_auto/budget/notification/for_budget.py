"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub,
    ctx,
    account_id: str,
    budget_name: str,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the notifications that are associated with a budget.

    Args:
        account_id(str): The accountId that is associated with the budget whose notifications you want descriptions of.
        budget_name(str): The name of the budget whose notifications you want descriptions of.
        max_results(int, optional): An optional integer that represents how many entries a paginated response contains. The maximum is 100. Defaults to None.
        next_token(str, optional): The pagination token that you include in your request to indicate the next set of results that you want to retrieve. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.budget.notification.for_budget.describe(
                ctx, account_id=value, budget_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.budget.notification.for_budget.describe account_id=value, budget_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="budgets",
        operation="describe_notifications_for_budget",
        op_kwargs={
            "AccountId": account_id,
            "BudgetName": budget_name,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
