"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def copy(
    hub,
    ctx,
    source_db_cluster_snapshot_identifier: str,
    target_db_cluster_snapshot_identifier: str,
    kms_key_id: str = None,
    pre_signed_url: str = None,
    copy_tags: bool = None,
    tags: List = None,
    source_region: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Copies a snapshot of a DB cluster. To copy a DB cluster snapshot from a shared manual DB cluster snapshot, SourceDBClusterSnapshotIdentifier must be the Amazon Resource Name (ARN) of the shared DB cluster snapshot.

    Args:
        source_db_cluster_snapshot_identifier(str): The identifier of the DB cluster snapshot to copy. This parameter is not case-sensitive. Constraints:   Must specify a valid system snapshot in the "available" state.   Specify a valid DB snapshot identifier.   Example: my-cluster-snapshot1 .
        target_db_cluster_snapshot_identifier(str): The identifier of the new DB cluster snapshot to create from the source DB cluster snapshot. This parameter is not case-sensitive. Constraints:   Must contain from 1 to 63 letters, numbers, or hyphens.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.   Example: my-cluster-snapshot2 .
        kms_key_id(str, optional): The Amazon Amazon KMS key ID for an encrypted DB cluster snapshot. The KMS key ID is the Amazon Resource Name (ARN), KMS key identifier, or the KMS key alias for the KMS encryption key. If you copy an encrypted DB cluster snapshot from your Amazon account, you can specify a value for KmsKeyId to encrypt the copy with a new KMS encryption key. If you don't specify a value for KmsKeyId, then the copy of the DB cluster snapshot is encrypted with the same KMS key as the source DB cluster snapshot. If you copy an encrypted DB cluster snapshot that is shared from another Amazon account, then you must specify a value for KmsKeyId.  KMS encryption keys are specific to the Amazon Region that they are created in, and you can't use encryption keys from one Amazon Region in another Amazon Region. You cannot encrypt an unencrypted DB cluster snapshot when you copy it. If you try to copy an unencrypted DB cluster snapshot and specify a value for the KmsKeyId parameter, an error is returned. Defaults to None.
        pre_signed_url(str, optional): Not currently supported. Defaults to None.
        copy_tags(bool, optional): True to copy all tags from the source DB cluster snapshot to the target DB cluster snapshot, and otherwise false. The default is false. Defaults to None.
        tags(List, optional): The tags to assign to the new DB cluster snapshot copy. Defaults to None.
        source_region(str, optional): The ID of the region that contains the snapshot to be copied. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.neptune.db.cluster.snapshot.init.copy(
                ctx,
                source_db_cluster_snapshot_identifier=value,
                target_db_cluster_snapshot_identifier=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.neptune.db.cluster.snapshot.init.copy source_db_cluster_snapshot_identifier=value, target_db_cluster_snapshot_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="neptune",
        operation="copy_db_cluster_snapshot",
        op_kwargs={
            "SourceDBClusterSnapshotIdentifier": source_db_cluster_snapshot_identifier,
            "TargetDBClusterSnapshotIdentifier": target_db_cluster_snapshot_identifier,
            "KmsKeyId": kms_key_id,
            "PreSignedUrl": pre_signed_url,
            "CopyTags": copy_tags,
            "Tags": tags,
            "SourceRegion": source_region,
        },
    )


async def create(
    hub,
    ctx,
    db_cluster_snapshot_identifier: str,
    db_cluster_identifier: str,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a snapshot of a DB cluster.

    Args:
        db_cluster_snapshot_identifier(str): The identifier of the DB cluster snapshot. This parameter is stored as a lowercase string. Constraints:   Must contain from 1 to 63 letters, numbers, or hyphens.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.   Example: my-cluster1-snapshot1 .
        db_cluster_identifier(str): The identifier of the DB cluster to create a snapshot for. This parameter is not case-sensitive. Constraints:   Must match the identifier of an existing DBCluster.   Example: my-cluster1 .
        tags(List, optional): The tags to be assigned to the DB cluster snapshot. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.neptune.db.cluster.snapshot.init.create(
                ctx, db_cluster_snapshot_identifier=value, db_cluster_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.neptune.db.cluster.snapshot.init.create db_cluster_snapshot_identifier=value, db_cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="neptune",
        operation="create_db_cluster_snapshot",
        op_kwargs={
            "DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier,
            "DBClusterIdentifier": db_cluster_identifier,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, db_cluster_snapshot_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a DB cluster snapshot. If the snapshot is being copied, the copy operation is terminated.  The DB cluster snapshot must be in the available state to be deleted.

    Args:
        db_cluster_snapshot_identifier(str): The identifier of the DB cluster snapshot to delete. Constraints: Must be the name of an existing DB cluster snapshot in the available state.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.neptune.db.cluster.snapshot.init.delete(
                ctx, db_cluster_snapshot_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.neptune.db.cluster.snapshot.init.delete db_cluster_snapshot_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="neptune",
        operation="delete_db_cluster_snapshot",
        op_kwargs={"DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier},
    )


async def describe_all(
    hub,
    ctx,
    db_cluster_identifier: str = None,
    db_cluster_snapshot_identifier: str = None,
    snapshot_type: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
    include_shared: bool = None,
    include_public: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about DB cluster snapshots. This API action supports pagination.

    Args:
        db_cluster_identifier(str, optional): The ID of the DB cluster to retrieve the list of DB cluster snapshots for. This parameter can't be used in conjunction with the DBClusterSnapshotIdentifier parameter. This parameter is not case-sensitive. Constraints:   If supplied, must match the identifier of an existing DBCluster.  . Defaults to None.
        db_cluster_snapshot_identifier(str, optional): A specific DB cluster snapshot identifier to describe. This parameter can't be used in conjunction with the DBClusterIdentifier parameter. This value is stored as a lowercase string. Constraints:   If supplied, must match the identifier of an existing DBClusterSnapshot.   If this identifier is for an automated snapshot, the SnapshotType parameter must also be specified.  . Defaults to None.
        snapshot_type(str, optional): The type of DB cluster snapshots to be returned. You can specify one of the following values:    automated - Return all DB cluster snapshots that have been automatically taken by Amazon Neptune for my Amazon account.    manual - Return all DB cluster snapshots that have been taken by my AWS account.    shared - Return all manual DB cluster snapshots that have been shared to my Amazon account.    public - Return all DB cluster snapshots that have been marked as public.   If you don't specify a SnapshotType value, then both automated and manual DB cluster snapshots are returned. You can include shared DB cluster snapshots with these results by setting the IncludeShared parameter to true. You can include public DB cluster snapshots with these results by setting the IncludePublic parameter to true. The IncludeShared and IncludePublic parameters don't apply for SnapshotType values of manual or automated. The IncludePublic parameter doesn't apply when SnapshotType is set to shared. The IncludeShared parameter doesn't apply when SnapshotType is set to public. Defaults to None.
        filters(List, optional): This parameter is not currently supported. Defaults to None.
        max_records(int, optional): The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional pagination token provided by a previous DescribeDBClusterSnapshots request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. . Defaults to None.
        include_shared(bool, optional): True to include shared manual DB cluster snapshots from other Amazon accounts that this AWS account has been given permission to copy or restore, and otherwise false. The default is false. You can give an Amazon account permission to restore a manual DB cluster snapshot from another Amazon account by the ModifyDBClusterSnapshotAttribute API action. Defaults to None.
        include_public(bool, optional): True to include manual DB cluster snapshots that are public and can be copied or restored by any Amazon account, and otherwise false. The default is false. The default is false. You can share a manual DB cluster snapshot as public by using the ModifyDBClusterSnapshotAttribute API action. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.neptune.db.cluster.snapshot.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.neptune.db.cluster.snapshot.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="neptune",
        operation="describe_db_cluster_snapshots",
        op_kwargs={
            "DBClusterIdentifier": db_cluster_identifier,
            "DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier,
            "SnapshotType": snapshot_type,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
            "IncludeShared": include_shared,
            "IncludePublic": include_public,
        },
    )
