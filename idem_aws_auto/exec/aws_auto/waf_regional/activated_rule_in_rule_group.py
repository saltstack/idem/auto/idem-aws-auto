"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, rule_group_id: str = None, next_marker: str = None, limit: int = None
) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Returns an array of ActivatedRule objects.

    Args:
        rule_group_id(str, optional): The RuleGroupId of the RuleGroup for which you want to get a list of ActivatedRule objects. Defaults to None.
        next_marker(str, optional): If you specify a value for Limit and you have more ActivatedRules than the value of Limit, AWS WAF returns a NextMarker value in the response that allows you to list another group of ActivatedRules. For the second and subsequent ListActivatedRulesInRuleGroup requests, specify the value of NextMarker from the previous response to get information about another batch of ActivatedRules. Defaults to None.
        limit(int, optional): Specifies the number of ActivatedRules that you want AWS WAF to return for this request. If you have more ActivatedRules than the number that you specify for Limit, the response includes a NextMarker value that you can use to get another batch of ActivatedRules. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf_regional.activated_rule_in_rule_group.list(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf_regional.activated_rule_in_rule_group.list
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf-regional",
        operation="list_activated_rules_in_rule_group",
        op_kwargs={
            "RuleGroupId": rule_group_id,
            "NextMarker": next_marker,
            "Limit": limit,
        },
    )
