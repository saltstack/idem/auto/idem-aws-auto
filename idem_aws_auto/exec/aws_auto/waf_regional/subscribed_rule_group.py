"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(hub, ctx, next_marker: str = None, limit: int = None) -> Dict:
    r"""
    **Autogenerated function**

     This is AWS WAF Classic documentation. For more information, see AWS WAF Classic in the developer guide.  For the latest version of AWS WAF, use the AWS WAFV2 API and see the AWS WAF Developer Guide. With the latest version, AWS WAF has a single set of endpoints for regional and global use.   Returns an array of RuleGroup objects that you are subscribed to.

    Args:
        next_marker(str, optional): If you specify a value for Limit and you have more ByteMatchSetssubscribed rule groups than the value of Limit, AWS WAF returns a NextMarker value in the response that allows you to list another group of subscribed rule groups. For the second and subsequent ListSubscribedRuleGroupsRequest requests, specify the value of NextMarker from the previous response to get information about another batch of subscribed rule groups. Defaults to None.
        limit(int, optional): Specifies the number of subscribed rule groups that you want AWS WAF to return for this request. If you have more objects than the number you specify for Limit, the response includes a NextMarker value that you can use to get another batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.waf_regional.subscribed_rule_group.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.waf_regional.subscribed_rule_group.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="waf-regional",
        operation="list_subscribed_rule_groups",
        op_kwargs={"NextMarker": next_marker, "Limit": limit},
    )
