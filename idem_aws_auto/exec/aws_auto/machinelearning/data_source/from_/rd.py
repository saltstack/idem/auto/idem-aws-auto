"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create_multiple(
    hub,
    ctx,
    data_source_id: str,
    rds_data: Dict,
    role_arn: str,
    data_source_name: str = None,
    compute_statistics: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a DataSource object from an  Amazon Relational Database Service (Amazon RDS). A DataSource references data that can be used to perform CreateMLModel, CreateEvaluation, or CreateBatchPrediction operations.  CreateDataSourceFromRDS is an asynchronous operation. In response to CreateDataSourceFromRDS, Amazon Machine Learning (Amazon ML) immediately returns and sets the DataSource status to PENDING. After the DataSource is created and ready for use, Amazon ML sets the Status parameter to COMPLETED. DataSource in the COMPLETED or PENDING state can be used only to perform >CreateMLModel>, CreateEvaluation, or CreateBatchPrediction operations.   If Amazon ML cannot accept the input source, it sets the Status parameter to FAILED and includes an error message in the Message attribute of the GetDataSource operation response.

    Args:
        data_source_id(str): A user-supplied ID that uniquely identifies the DataSource. Typically, an Amazon Resource Number (ARN) becomes the ID for a DataSource.
        data_source_name(str, optional): A user-supplied name or description of the DataSource. Defaults to None.
        rds_data(Dict): The data specification of an Amazon RDS DataSource:   DatabaseInformation -    DatabaseName - The name of the Amazon RDS database.    InstanceIdentifier  - A unique identifier for the Amazon RDS database instance.     DatabaseCredentials - AWS Identity and Access Management (IAM) credentials that are used to connect to the Amazon RDS database.   ResourceRole - A role (DataPipelineDefaultResourceRole) assumed by an EC2 instance to carry out the copy task from Amazon RDS to Amazon Simple Storage Service (Amazon S3). For more information, see Role templates for data pipelines.   ServiceRole - A role (DataPipelineDefaultRole) assumed by the AWS Data Pipeline service to monitor the progress of the copy task from Amazon RDS to Amazon S3. For more information, see Role templates for data pipelines.   SecurityInfo - The security information to use to access an RDS DB instance. You need to set up appropriate ingress rules for the security entity IDs provided to allow access to the Amazon RDS instance. Specify a [SubnetId, SecurityGroupIds] pair for a VPC-based RDS DB instance.   SelectSqlQuery - A query that is used to retrieve the observation data for the Datasource.   S3StagingLocation - The Amazon S3 location for staging Amazon RDS data. The data retrieved from Amazon RDS using SelectSqlQuery is stored in this location.   DataSchemaUri - The Amazon S3 location of the DataSchema.   DataSchema - A JSON string representing the schema. This is not required if DataSchemaUri is specified.    DataRearrangement - A JSON string that represents the splitting and rearrangement requirements for the Datasource.   Sample -  "{\"splitting\":{\"percentBegin\":10,\"percentEnd\":60}}"   .
        role_arn(str): The role that Amazon ML assumes on behalf of the user to create and activate a data pipeline in the user's account and copy data using the SelectSqlQuery query from Amazon RDS to Amazon S3. .
        compute_statistics(bool, optional): The compute statistics for a DataSource. The statistics are generated from the observation data referenced by a DataSource. Amazon ML uses the statistics internally during MLModel training. This parameter must be set to true if the DataSource needs to be used for MLModel training. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.machinelearning.data_source.from_.rd.create_multiple(
                ctx, data_source_id=value, rds_data=value, role_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.machinelearning.data_source.from_.rd.create_multiple data_source_id=value, rds_data=value, role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="machinelearning",
        operation="create_data_source_from_rds",
        op_kwargs={
            "DataSourceId": data_source_id,
            "DataSourceName": data_source_name,
            "RDSData": rds_data,
            "RoleARN": role_arn,
            "ComputeStatistics": compute_statistics,
        },
    )
