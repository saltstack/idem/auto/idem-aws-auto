"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, name: str, recipe_version: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a single version of a DataBrew recipe.

    Args:
        name(str): The name of the recipe.
        recipe_version(str): The version of the recipe to be deleted. You can specify a numeric versions (X.Y) or LATEST_WORKING. LATEST_PUBLISHED is not supported.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.recipe.version.delete(
                ctx, name=value, recipe_version=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.recipe.version.delete name=value, recipe_version=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="delete_recipe_version",
        op_kwargs={"Name": name, "RecipeVersion": recipe_version},
    )


async def list_all(
    hub, ctx, name: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the versions of a particular DataBrew recipe, except for LATEST_WORKING.

    Args:
        max_results(int, optional): The maximum number of results to return in this request. . Defaults to None.
        next_token(str, optional): The token returned by a previous call to retrieve the next set of results. Defaults to None.
        name(str): The name of the recipe for which to return version information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.recipe.version.list_all(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.recipe.version.list_all name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="list_recipe_versions",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token, "Name": name},
    )
