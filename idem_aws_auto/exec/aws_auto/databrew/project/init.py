"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    dataset_name: str,
    name: str,
    recipe_name: str,
    role_arn: str,
    sample: Dict = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new DataBrew project.

    Args:
        dataset_name(str): The name of an existing dataset to associate this project with.
        name(str): A unique name for the new project. Valid characters are alphanumeric (A-Z, a-z, 0-9), hyphen (-), period (.), and space.
        recipe_name(str): The name of an existing recipe to associate with the project.
        sample(Dict, optional): Represents the sample size and sampling type for DataBrew to use for interactive data analysis. Defaults to None.
        role_arn(str): The Amazon Resource Name (ARN) of the Identity and Access Management (IAM) role to be assumed for this request.
        tags(Dict, optional): Metadata tags to apply to this project. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.project.init.create(
                ctx, dataset_name=value, name=value, recipe_name=value, role_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.project.init.create dataset_name=value, name=value, recipe_name=value, role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="create_project",
        op_kwargs={
            "DatasetName": dataset_name,
            "Name": name,
            "RecipeName": recipe_name,
            "Sample": sample,
            "RoleArn": role_arn,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an existing DataBrew project.

    Args:
        name(str): The name of the project to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.project.init.delete(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.project.init.delete name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="delete_project",
        op_kwargs={"Name": name},
    )


async def describe(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the definition of a specific DataBrew project.

    Args:
        name(str): The name of the project to be described.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.project.init.describe(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.project.init.describe name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="describe_project",
        op_kwargs={"Name": name},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all of the DataBrew projects that are defined.

    Args:
        next_token(str, optional): The token returned by a previous call to retrieve the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return in this request. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.project.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.project.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="list_projects",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )


async def update(hub, ctx, role_arn: str, name: str, sample: Dict = None) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the definition of an existing DataBrew project.

    Args:
        sample(Dict, optional): Represents the sample size and sampling type for DataBrew to use for interactive data analysis. Defaults to None.
        role_arn(str): The Amazon Resource Name (ARN) of the IAM role to be assumed for this request.
        name(str): The name of the project to be updated.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.databrew.project.init.update(ctx, role_arn=value, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.databrew.project.init.update role_arn=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="databrew",
        operation="update_project",
        op_kwargs={"Sample": sample, "RoleArn": role_arn, "Name": name},
    )
