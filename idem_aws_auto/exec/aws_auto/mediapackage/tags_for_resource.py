"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Args:
        resource_arn(str): .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediapackage.tags_for_resource.list(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediapackage.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediapackage",
        operation="list_tags_for_resource",
        op_kwargs={"ResourceArn": resource_arn},
    )
