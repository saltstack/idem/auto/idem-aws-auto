"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: Dict) -> None:
    r"""
    **Autogenerated function**

    Args:
        resource_arn(str): .
        tags(Dict): .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediapackage.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediapackage.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediapackage",
        operation="tag_resource",
        op_kwargs={"ResourceArn": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> None:
    r"""
    **Autogenerated function**

    Args:
        resource_arn(str): .
        tag_keys(List): The key(s) of tag to be deleted.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediapackage.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediapackage.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediapackage",
        operation="untag_resource",
        op_kwargs={"ResourceArn": resource_arn, "TagKeys": tag_keys},
    )
