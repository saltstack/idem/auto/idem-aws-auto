"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def modify(
    hub, ctx, cluster_identifier: str, aqua_configuration_status: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies whether a cluster can use AQUA (Advanced Query Accelerator).

    Args:
        cluster_identifier(str): The identifier of the cluster to be modified.
        aqua_configuration_status(str, optional): The new value of AQUA configuration status. Possible values include the following.   enabled - Use AQUA if it is available for the current Amazon Web Services Region and Amazon Redshift node type.   disabled - Don't use AQUA.    auto - Amazon Redshift determines whether to use AQUA.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.aqua_configuration.modify(
                ctx, cluster_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.aqua_configuration.modify cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="modify_aqua_configuration",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "AquaConfigurationStatus": aqua_configuration_status,
        },
    )
