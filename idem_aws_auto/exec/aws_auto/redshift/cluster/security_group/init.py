"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, cluster_security_group_name: str, description: str, tags: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new Amazon Redshift security group. You use security groups to control access to non-VPC clusters.  For information about managing security groups, go to Amazon Redshift Cluster Security Groups in the Amazon Redshift Cluster Management Guide.

    Args:
        cluster_security_group_name(str): The name for the security group. Amazon Redshift stores the value as a lowercase string. Constraints:   Must contain no more than 255 alphanumeric characters or hyphens.   Must not be "Default".   Must be unique for all security groups that are created by your Amazon Web Services account.   Example: examplesecuritygroup .
        description(str): A description for the security group.
        tags(List, optional): A list of tag instances. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.security_group.init.create(
                ctx, cluster_security_group_name=value, description=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.security_group.init.create cluster_security_group_name=value, description=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="create_cluster_security_group",
        op_kwargs={
            "ClusterSecurityGroupName": cluster_security_group_name,
            "Description": description,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, cluster_security_group_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes an Amazon Redshift security group.  You cannot delete a security group that is associated with any clusters. You cannot delete the default security group.   For information about managing security groups, go to Amazon Redshift Cluster Security Groups in the Amazon Redshift Cluster Management Guide.

    Args:
        cluster_security_group_name(str): The name of the cluster security group to be deleted.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.security_group.init.delete(
                ctx, cluster_security_group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.security_group.init.delete cluster_security_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="delete_cluster_security_group",
        op_kwargs={"ClusterSecurityGroupName": cluster_security_group_name},
    )


async def describe_all(
    hub,
    ctx,
    cluster_security_group_name: str = None,
    max_records: int = None,
    marker: str = None,
    tag_keys: List = None,
    tag_values: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about Amazon Redshift security groups. If the name of a security group is specified, the response will contain only information about only that security group.  For information about managing security groups, go to Amazon Redshift Cluster Security Groups in the Amazon Redshift Cluster Management Guide. If you specify both tag keys and tag values in the same request, Amazon Redshift returns all security groups that match any combination of the specified keys and values. For example, if you have owner and environment for tag keys, and admin and test for tag values, all security groups that have any combination of those values are returned. If both tag keys and values are omitted from the request, security groups are returned regardless of whether they have tag keys or values associated with them.

    Args:
        cluster_security_group_name(str, optional): The name of a cluster security group for which you are requesting details. You can specify either the Marker parameter or a ClusterSecurityGroupName parameter, but not both.   Example: securitygroup1 . Defaults to None.
        max_records(int, optional): The maximum number of response records to return in each call. If the number of remaining response records exceeds the specified MaxRecords value, a value is returned in a marker field of the response. You can retrieve the next set of records by retrying the command with the returned marker value.  Default: 100  Constraints: minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional parameter that specifies the starting point to return a set of response records. When the results of a DescribeClusterSecurityGroups request exceed the value specified in MaxRecords, Amazon Web Services returns a value in the Marker field of the response. You can retrieve the next set of response records by providing the returned marker value in the Marker parameter and retrying the request.  Constraints: You can specify either the ClusterSecurityGroupName parameter or the Marker parameter, but not both. . Defaults to None.
        tag_keys(List, optional): A tag key or keys for which you want to return all matching cluster security groups that are associated with the specified key or keys. For example, suppose that you have security groups that are tagged with keys called owner and environment. If you specify both of these tag keys in the request, Amazon Redshift returns a response with the security groups that have either or both of these tag keys associated with them. Defaults to None.
        tag_values(List, optional): A tag value or values for which you want to return all matching cluster security groups that are associated with the specified tag value or values. For example, suppose that you have security groups that are tagged with values called admin and test. If you specify both of these tag values in the request, Amazon Redshift returns a response with the security groups that have either or both of these tag values associated with them. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.security_group.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.security_group.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="describe_cluster_security_groups",
        op_kwargs={
            "ClusterSecurityGroupName": cluster_security_group_name,
            "MaxRecords": max_records,
            "Marker": marker,
            "TagKeys": tag_keys,
            "TagValues": tag_values,
        },
    )
