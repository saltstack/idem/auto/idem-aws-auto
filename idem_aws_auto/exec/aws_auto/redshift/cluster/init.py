"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    cluster_identifier: str,
    node_type: str,
    master_username: str,
    master_user_password: str,
    db_name: str = None,
    cluster_type: str = None,
    cluster_security_groups: List = None,
    vpc_security_group_ids: List = None,
    cluster_subnet_group_name: str = None,
    availability_zone: str = None,
    preferred_maintenance_window: str = None,
    cluster_parameter_group_name: str = None,
    automated_snapshot_retention_period: int = None,
    manual_snapshot_retention_period: int = None,
    port: int = None,
    cluster_version: str = None,
    allow_version_upgrade: bool = None,
    number_of_nodes: int = None,
    publicly_accessible: bool = None,
    encrypted: bool = None,
    hsm_client_certificate_identifier: str = None,
    hsm_configuration_identifier: str = None,
    elastic_ip: str = None,
    tags: List = None,
    kms_key_id: str = None,
    enhanced_vpc_routing: bool = None,
    additional_info: str = None,
    iam_roles: List = None,
    maintenance_track_name: str = None,
    snapshot_schedule_identifier: str = None,
    availability_zone_relocation: bool = None,
    aqua_configuration_status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new cluster with the specified parameters. To create a cluster in Virtual Private Cloud (VPC), you must provide a cluster subnet group name. The cluster subnet group identifies the subnets of your VPC that Amazon Redshift uses when creating the cluster. For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide.

    Args:
        db_name(str, optional): The name of the first database to be created when the cluster is created. To create additional databases after the cluster is created, connect to the cluster with a SQL client and use SQL commands to create a database. For more information, go to Create a Database in the Amazon Redshift Database Developer Guide.  Default: dev  Constraints:   Must contain 1 to 64 alphanumeric characters.   Must contain only lowercase letters.   Cannot be a word that is reserved by the service. A list of reserved words can be found in Reserved Words in the Amazon Redshift Database Developer Guide.   . Defaults to None.
        cluster_identifier(str): A unique identifier for the cluster. You use this identifier to refer to the cluster for any subsequent cluster operations such as deleting or modifying. The identifier also appears in the Amazon Redshift console. Constraints:   Must contain from 1 to 63 alphanumeric characters or hyphens.   Alphabetic characters must be lowercase.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.   Must be unique for all clusters within an Amazon Web Services account.   Example: myexamplecluster .
        cluster_type(str, optional): The type of the cluster. When cluster type is specified as    single-node, the NumberOfNodes parameter is not required.    multi-node, the NumberOfNodes parameter is required.   Valid Values: multi-node | single-node  Default: multi-node . Defaults to None.
        node_type(str): The node type to be provisioned for the cluster. For information about node types, go to  Working with Clusters in the Amazon Redshift Cluster Management Guide.  Valid Values: ds2.xlarge | ds2.8xlarge | dc1.large | dc1.8xlarge | dc2.large | dc2.8xlarge | ra3.xlplus | ra3.4xlarge | ra3.16xlarge .
        master_username(str): The user name associated with the admin user account for the cluster that is being created. Constraints:   Must be 1 - 128 alphanumeric characters. The user name can't be PUBLIC.   First character must be a letter.   Cannot be a reserved word. A list of reserved words can be found in Reserved Words in the Amazon Redshift Database Developer Guide.   .
        master_user_password(str): The password associated with the admin user account for the cluster that is being created. Constraints:   Must be between 8 and 64 characters in length.   Must contain at least one uppercase letter.   Must contain at least one lowercase letter.   Must contain one number.   Can be any printable ASCII character (ASCII code 33 to 126) except ' (single quote), " (double quote), \, /, @, or space.  .
        cluster_security_groups(List, optional): A list of security groups to be associated with this cluster. Default: The default cluster security group for Amazon Redshift. Defaults to None.
        vpc_security_group_ids(List, optional): A list of Virtual Private Cloud (VPC) security groups to be associated with the cluster. Default: The default VPC security group is associated with the cluster. Defaults to None.
        cluster_subnet_group_name(str, optional): The name of a cluster subnet group to be associated with this cluster. If this parameter is not provided the resulting cluster will be deployed outside virtual private cloud (VPC). Defaults to None.
        availability_zone(str, optional): The EC2 Availability Zone (AZ) in which you want Amazon Redshift to provision the cluster. For example, if you have several EC2 instances running in a specific Availability Zone, then you might want the cluster to be provisioned in the same zone in order to decrease network latency. Default: A random, system-chosen Availability Zone in the region that is specified by the endpoint. Example: us-east-2d  Constraint: The specified Availability Zone must be in the same region as the current endpoint. Defaults to None.
        preferred_maintenance_window(str, optional): The weekly time range (in UTC) during which automated cluster maintenance can occur.  Format: ddd:hh24:mi-ddd:hh24:mi   Default: A 30-minute window selected at random from an 8-hour block of time per region, occurring on a random day of the week. For more information about the time blocks for each region, see Maintenance Windows in Amazon Redshift Cluster Management Guide. Valid Days: Mon | Tue | Wed | Thu | Fri | Sat | Sun Constraints: Minimum 30-minute window. Defaults to None.
        cluster_parameter_group_name(str, optional): The name of the parameter group to be associated with this cluster. Default: The default Amazon Redshift cluster parameter group. For information about the default parameter group, go to Working with Amazon Redshift Parameter Groups  Constraints:   Must be 1 to 255 alphanumeric characters or hyphens.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.  . Defaults to None.
        automated_snapshot_retention_period(int, optional): The number of days that automated snapshots are retained. If the value is 0, automated snapshots are disabled. Even if automated snapshots are disabled, you can still create manual snapshots when you want with CreateClusterSnapshot.  You can't disable automated snapshots for RA3 node types. Set the automated retention period from 1-35 days. Default: 1  Constraints: Must be a value from 0 to 35. Defaults to None.
        manual_snapshot_retention_period(int, optional): The default number of days to retain a manual snapshot. If the value is -1, the snapshot is retained indefinitely. This setting doesn't change the retention period of existing snapshots. The value must be either -1 or an integer between 1 and 3,653. Defaults to None.
        port(int, optional): The port number on which the cluster accepts incoming connections. The cluster is accessible only via the JDBC and ODBC connection strings. Part of the connection string requires the port on which the cluster will listen for incoming connections. Default: 5439  Valid Values: 1150-65535 . Defaults to None.
        cluster_version(str, optional): The version of the Amazon Redshift engine software that you want to deploy on the cluster. The version selected runs on all the nodes in the cluster. Constraints: Only version 1.0 is currently available. Example: 1.0 . Defaults to None.
        allow_version_upgrade(bool, optional): If true, major version upgrades can be applied during the maintenance window to the Amazon Redshift engine that is running on the cluster. When a new major version of the Amazon Redshift engine is released, you can request that the service automatically apply upgrades during the maintenance window to the Amazon Redshift engine that is running on your cluster. Default: true . Defaults to None.
        number_of_nodes(int, optional): The number of compute nodes in the cluster. This parameter is required when the ClusterType parameter is specified as multi-node.  For information about determining how many nodes you need, go to  Working with Clusters in the Amazon Redshift Cluster Management Guide.  If you don't specify this parameter, you get a single-node cluster. When requesting a multi-node cluster, you must specify the number of nodes that you want in the cluster. Default: 1  Constraints: Value must be at least 1 and no more than 100. Defaults to None.
        publicly_accessible(bool, optional): If true, the cluster can be accessed from a public network. . Defaults to None.
        encrypted(bool, optional): If true, the data in the cluster is encrypted at rest.  Default: false. Defaults to None.
        hsm_client_certificate_identifier(str, optional): Specifies the name of the HSM client certificate the Amazon Redshift cluster uses to retrieve the data encryption keys stored in an HSM. Defaults to None.
        hsm_configuration_identifier(str, optional): Specifies the name of the HSM configuration that contains the information the Amazon Redshift cluster can use to retrieve and store keys in an HSM. Defaults to None.
        elastic_ip(str, optional): The Elastic IP (EIP) address for the cluster. Constraints: The cluster must be provisioned in EC2-VPC and publicly-accessible through an Internet gateway. For more information about provisioning clusters in EC2-VPC, go to Supported Platforms to Launch Your Cluster in the Amazon Redshift Cluster Management Guide. Defaults to None.
        tags(List, optional): A list of tag instances. Defaults to None.
        kms_key_id(str, optional): The Key Management Service (KMS) key ID of the encryption key that you want to use to encrypt data in the cluster. Defaults to None.
        enhanced_vpc_routing(bool, optional): An option that specifies whether to create the cluster with enhanced VPC routing enabled. To create a cluster that uses enhanced VPC routing, the cluster must be in a VPC. For more information, see Enhanced VPC Routing in the Amazon Redshift Cluster Management Guide. If this option is true, enhanced VPC routing is enabled.  Default: false. Defaults to None.
        additional_info(str, optional): Reserved. Defaults to None.
        iam_roles(List, optional): A list of Identity and Access Management (IAM) roles that can be used by the cluster to access other Amazon Web Services services. You must supply the IAM roles in their Amazon Resource Name (ARN) format. You can supply up to 10 IAM roles in a single request. A cluster can have up to 10 IAM roles associated with it at any time. Defaults to None.
        maintenance_track_name(str, optional): An optional parameter for the name of the maintenance track for the cluster. If you don't provide a maintenance track name, the cluster is assigned to the current track. Defaults to None.
        snapshot_schedule_identifier(str, optional): A unique identifier for the snapshot schedule. Defaults to None.
        availability_zone_relocation(bool, optional): The option to enable relocation for an Amazon Redshift cluster between Availability Zones after the cluster is created. Defaults to None.
        aqua_configuration_status(str, optional): The value represents how the cluster is configured to use AQUA (Advanced Query Accelerator) when it is created. Possible values include the following.   enabled - Use AQUA if it is available for the current Amazon Web Services Region and Amazon Redshift node type.   disabled - Don't use AQUA.    auto - Amazon Redshift determines whether to use AQUA.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.create(
                ctx,
                cluster_identifier=value,
                node_type=value,
                master_username=value,
                master_user_password=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.create cluster_identifier=value, node_type=value, master_username=value, master_user_password=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="create_cluster",
        op_kwargs={
            "DBName": db_name,
            "ClusterIdentifier": cluster_identifier,
            "ClusterType": cluster_type,
            "NodeType": node_type,
            "MasterUsername": master_username,
            "MasterUserPassword": master_user_password,
            "ClusterSecurityGroups": cluster_security_groups,
            "VpcSecurityGroupIds": vpc_security_group_ids,
            "ClusterSubnetGroupName": cluster_subnet_group_name,
            "AvailabilityZone": availability_zone,
            "PreferredMaintenanceWindow": preferred_maintenance_window,
            "ClusterParameterGroupName": cluster_parameter_group_name,
            "AutomatedSnapshotRetentionPeriod": automated_snapshot_retention_period,
            "ManualSnapshotRetentionPeriod": manual_snapshot_retention_period,
            "Port": port,
            "ClusterVersion": cluster_version,
            "AllowVersionUpgrade": allow_version_upgrade,
            "NumberOfNodes": number_of_nodes,
            "PubliclyAccessible": publicly_accessible,
            "Encrypted": encrypted,
            "HsmClientCertificateIdentifier": hsm_client_certificate_identifier,
            "HsmConfigurationIdentifier": hsm_configuration_identifier,
            "ElasticIp": elastic_ip,
            "Tags": tags,
            "KmsKeyId": kms_key_id,
            "EnhancedVpcRouting": enhanced_vpc_routing,
            "AdditionalInfo": additional_info,
            "IamRoles": iam_roles,
            "MaintenanceTrackName": maintenance_track_name,
            "SnapshotScheduleIdentifier": snapshot_schedule_identifier,
            "AvailabilityZoneRelocation": availability_zone_relocation,
            "AquaConfigurationStatus": aqua_configuration_status,
        },
    )


async def delete(
    hub,
    ctx,
    cluster_identifier: str,
    skip_final_cluster_snapshot: bool = None,
    final_cluster_snapshot_identifier: str = None,
    final_cluster_snapshot_retention_period: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a previously provisioned cluster without its final snapshot being created. A successful response from the web service indicates that the request was received correctly. Use DescribeClusters to monitor the status of the deletion. The delete operation cannot be canceled or reverted once submitted. For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide. If you want to shut down the cluster and retain it for future use, set SkipFinalClusterSnapshot to false and specify a name for FinalClusterSnapshotIdentifier. You can later restore this snapshot to resume using the cluster. If a final cluster snapshot is requested, the status of the cluster will be "final-snapshot" while the snapshot is being taken, then it's "deleting" once Amazon Redshift begins deleting the cluster.   For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide.

    Args:
        cluster_identifier(str): The identifier of the cluster to be deleted. Constraints:   Must contain lowercase characters.   Must contain from 1 to 63 alphanumeric characters or hyphens.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.  .
        skip_final_cluster_snapshot(bool, optional): Determines whether a final snapshot of the cluster is created before Amazon Redshift deletes the cluster. If true, a final cluster snapshot is not created. If false, a final cluster snapshot is created before the cluster is deleted.   The FinalClusterSnapshotIdentifier parameter must be specified if SkipFinalClusterSnapshot is false.  Default: false . Defaults to None.
        final_cluster_snapshot_identifier(str, optional): The identifier of the final snapshot that is to be created immediately before deleting the cluster. If this parameter is provided, SkipFinalClusterSnapshot must be false.  Constraints:   Must be 1 to 255 alphanumeric characters.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.  . Defaults to None.
        final_cluster_snapshot_retention_period(int, optional): The number of days that a manual snapshot is retained. If the value is -1, the manual snapshot is retained indefinitely. The value must be either -1 or an integer between 1 and 3,653. The default value is -1. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.delete(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.delete cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="delete_cluster",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "SkipFinalClusterSnapshot": skip_final_cluster_snapshot,
            "FinalClusterSnapshotIdentifier": final_cluster_snapshot_identifier,
            "FinalClusterSnapshotRetentionPeriod": final_cluster_snapshot_retention_period,
        },
    )


async def describe_all(
    hub,
    ctx,
    cluster_identifier: str = None,
    max_records: int = None,
    marker: str = None,
    tag_keys: List = None,
    tag_values: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns properties of provisioned clusters including general cluster properties, cluster database properties, maintenance and backup properties, and security and access properties. This operation supports pagination. For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide. If you specify both tag keys and tag values in the same request, Amazon Redshift returns all clusters that match any combination of the specified keys and values. For example, if you have owner and environment for tag keys, and admin and test for tag values, all clusters that have any combination of those values are returned. If both tag keys and values are omitted from the request, clusters are returned regardless of whether they have tag keys or values associated with them.

    Args:
        cluster_identifier(str, optional): The unique identifier of a cluster whose properties you are requesting. This parameter is case sensitive. The default is that all clusters defined for an account are returned. Defaults to None.
        max_records(int, optional): The maximum number of response records to return in each call. If the number of remaining response records exceeds the specified MaxRecords value, a value is returned in a marker field of the response. You can retrieve the next set of records by retrying the command with the returned marker value.  Default: 100  Constraints: minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional parameter that specifies the starting point to return a set of response records. When the results of a DescribeClusters request exceed the value specified in MaxRecords, Amazon Web Services returns a value in the Marker field of the response. You can retrieve the next set of response records by providing the returned marker value in the Marker parameter and retrying the request.  Constraints: You can specify either the ClusterIdentifier parameter or the Marker parameter, but not both. . Defaults to None.
        tag_keys(List, optional): A tag key or keys for which you want to return all matching clusters that are associated with the specified key or keys. For example, suppose that you have clusters that are tagged with keys called owner and environment. If you specify both of these tag keys in the request, Amazon Redshift returns a response with the clusters that have either or both of these tag keys associated with them. Defaults to None.
        tag_values(List, optional): A tag value or values for which you want to return all matching clusters that are associated with the specified tag value or values. For example, suppose that you have clusters that are tagged with values called admin and test. If you specify both of these tag values in the request, Amazon Redshift returns a response with the clusters that have either or both of these tag values associated with them. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="describe_clusters",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "MaxRecords": max_records,
            "Marker": marker,
            "TagKeys": tag_keys,
            "TagValues": tag_values,
        },
    )


async def modify(
    hub,
    ctx,
    cluster_identifier: str,
    cluster_type: str = None,
    node_type: str = None,
    number_of_nodes: int = None,
    cluster_security_groups: List = None,
    vpc_security_group_ids: List = None,
    master_user_password: str = None,
    cluster_parameter_group_name: str = None,
    automated_snapshot_retention_period: int = None,
    manual_snapshot_retention_period: int = None,
    preferred_maintenance_window: str = None,
    cluster_version: str = None,
    allow_version_upgrade: bool = None,
    hsm_client_certificate_identifier: str = None,
    hsm_configuration_identifier: str = None,
    new_cluster_identifier: str = None,
    publicly_accessible: bool = None,
    elastic_ip: str = None,
    enhanced_vpc_routing: bool = None,
    maintenance_track_name: str = None,
    encrypted: bool = None,
    kms_key_id: str = None,
    availability_zone_relocation: bool = None,
    availability_zone: str = None,
    port: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the settings for a cluster. You can also change node type and the number of nodes to scale up or down the cluster. When resizing a cluster, you must specify both the number of nodes and the node type even if one of the parameters does not change. You can add another security or parameter group, or change the admin user password. Resetting a cluster password or modifying the security groups associated with a cluster do not need a reboot. However, modifying a parameter group requires a reboot for parameters to take effect. For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide.

    Args:
        cluster_identifier(str): The unique identifier of the cluster to be modified. Example: examplecluster .
        cluster_type(str, optional): The new cluster type. When you submit your cluster resize request, your existing cluster goes into a read-only mode. After Amazon Redshift provisions a new cluster based on your resize requirements, there will be outage for a period while the old cluster is deleted and your connection is switched to the new cluster. You can use DescribeResize to track the progress of the resize request.  Valid Values:  multi-node | single-node  . Defaults to None.
        node_type(str, optional): The new node type of the cluster. If you specify a new node type, you must also specify the number of nodes parameter.  For more information about resizing clusters, go to Resizing Clusters in Amazon Redshift in the Amazon Redshift Cluster Management Guide. Valid Values: ds2.xlarge | ds2.8xlarge | dc1.large | dc1.8xlarge | dc2.large | dc2.8xlarge | ra3.xlplus | ra3.4xlarge | ra3.16xlarge . Defaults to None.
        number_of_nodes(int, optional): The new number of nodes of the cluster. If you specify a new number of nodes, you must also specify the node type parameter.  For more information about resizing clusters, go to Resizing Clusters in Amazon Redshift in the Amazon Redshift Cluster Management Guide. Valid Values: Integer greater than 0. Defaults to None.
        cluster_security_groups(List, optional): A list of cluster security groups to be authorized on this cluster. This change is asynchronously applied as soon as possible. Security groups currently associated with the cluster, and not in the list of groups to apply, will be revoked from the cluster. Constraints:   Must be 1 to 255 alphanumeric characters or hyphens   First character must be a letter   Cannot end with a hyphen or contain two consecutive hyphens  . Defaults to None.
        vpc_security_group_ids(List, optional): A list of virtual private cloud (VPC) security groups to be associated with the cluster. This change is asynchronously applied as soon as possible. Defaults to None.
        master_user_password(str, optional): The new password for the cluster admin user. This change is asynchronously applied as soon as possible. Between the time of the request and the completion of the request, the MasterUserPassword element exists in the PendingModifiedValues element of the operation response.   Operations never return the password, so this operation provides a way to regain access to the admin user account for a cluster if the password is lost.  Default: Uses existing setting. Constraints:   Must be between 8 and 64 characters in length.   Must contain at least one uppercase letter.   Must contain at least one lowercase letter.   Must contain one number.   Can be any printable ASCII character (ASCII code 33 to 126) except ' (single quote), " (double quote), \, /, @, or space.  . Defaults to None.
        cluster_parameter_group_name(str, optional): The name of the cluster parameter group to apply to this cluster. This change is applied only after the cluster is rebooted. To reboot a cluster use RebootCluster.  Default: Uses existing setting. Constraints: The cluster parameter group must be in the same parameter group family that matches the cluster version. Defaults to None.
        automated_snapshot_retention_period(int, optional): The number of days that automated snapshots are retained. If the value is 0, automated snapshots are disabled. Even if automated snapshots are disabled, you can still create manual snapshots when you want with CreateClusterSnapshot.  If you decrease the automated snapshot retention period from its current value, existing automated snapshots that fall outside of the new retention period will be immediately deleted. You can't disable automated snapshots for RA3 node types. Set the automated retention period from 1-35 days. Default: Uses existing setting. Constraints: Must be a value from 0 to 35. Defaults to None.
        manual_snapshot_retention_period(int, optional): The default for number of days that a newly created manual snapshot is retained. If the value is -1, the manual snapshot is retained indefinitely. This value doesn't retroactively change the retention periods of existing manual snapshots. The value must be either -1 or an integer between 1 and 3,653. The default value is -1. Defaults to None.
        preferred_maintenance_window(str, optional): The weekly time range (in UTC) during which system maintenance can occur, if necessary. If system maintenance is necessary during the window, it may result in an outage. This maintenance window change is made immediately. If the new maintenance window indicates the current time, there must be at least 120 minutes between the current time and end of the window in order to ensure that pending changes are applied. Default: Uses existing setting. Format: ddd:hh24:mi-ddd:hh24:mi, for example wed:07:30-wed:08:00. Valid Days: Mon | Tue | Wed | Thu | Fri | Sat | Sun Constraints: Must be at least 30 minutes. Defaults to None.
        cluster_version(str, optional): The new version number of the Amazon Redshift engine to upgrade to. For major version upgrades, if a non-default cluster parameter group is currently in use, a new cluster parameter group in the cluster parameter group family for the new version must be specified. The new cluster parameter group can be the default for that cluster parameter group family. For more information about parameters and parameter groups, go to Amazon Redshift Parameter Groups in the Amazon Redshift Cluster Management Guide. Example: 1.0 . Defaults to None.
        allow_version_upgrade(bool, optional): If true, major version upgrades will be applied automatically to the cluster during the maintenance window.  Default: false . Defaults to None.
        hsm_client_certificate_identifier(str, optional): Specifies the name of the HSM client certificate the Amazon Redshift cluster uses to retrieve the data encryption keys stored in an HSM. Defaults to None.
        hsm_configuration_identifier(str, optional): Specifies the name of the HSM configuration that contains the information the Amazon Redshift cluster can use to retrieve and store keys in an HSM. Defaults to None.
        new_cluster_identifier(str, optional): The new identifier for the cluster. Constraints:   Must contain from 1 to 63 alphanumeric characters or hyphens.   Alphabetic characters must be lowercase.   First character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.   Must be unique for all clusters within an Amazon Web Services account.   Example: examplecluster . Defaults to None.
        publicly_accessible(bool, optional): If true, the cluster can be accessed from a public network. Only clusters in VPCs can be set to be publicly available. Defaults to None.
        elastic_ip(str, optional): The Elastic IP (EIP) address for the cluster. Constraints: The cluster must be provisioned in EC2-VPC and publicly-accessible through an Internet gateway. For more information about provisioning clusters in EC2-VPC, go to Supported Platforms to Launch Your Cluster in the Amazon Redshift Cluster Management Guide. Defaults to None.
        enhanced_vpc_routing(bool, optional): An option that specifies whether to create the cluster with enhanced VPC routing enabled. To create a cluster that uses enhanced VPC routing, the cluster must be in a VPC. For more information, see Enhanced VPC Routing in the Amazon Redshift Cluster Management Guide. If this option is true, enhanced VPC routing is enabled.  Default: false. Defaults to None.
        maintenance_track_name(str, optional): The name for the maintenance track that you want to assign for the cluster. This name change is asynchronous. The new track name stays in the PendingModifiedValues for the cluster until the next maintenance window. When the maintenance track changes, the cluster is switched to the latest cluster release available for the maintenance track. At this point, the maintenance track name is applied. Defaults to None.
        encrypted(bool, optional): Indicates whether the cluster is encrypted. If the value is encrypted (true) and you provide a value for the KmsKeyId parameter, we encrypt the cluster with the provided KmsKeyId. If you don't provide a KmsKeyId, we encrypt with the default key.  If the value is not encrypted (false), then the cluster is decrypted. . Defaults to None.
        kms_key_id(str, optional): The Key Management Service (KMS) key ID of the encryption key that you want to use to encrypt data in the cluster. Defaults to None.
        availability_zone_relocation(bool, optional): The option to enable relocation for an Amazon Redshift cluster between Availability Zones after the cluster modification is complete. Defaults to None.
        availability_zone(str, optional): The option to initiate relocation for an Amazon Redshift cluster to the target Availability Zone. Defaults to None.
        port(int, optional): The option to change the port of an Amazon Redshift cluster. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.modify(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.modify cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="modify_cluster",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "ClusterType": cluster_type,
            "NodeType": node_type,
            "NumberOfNodes": number_of_nodes,
            "ClusterSecurityGroups": cluster_security_groups,
            "VpcSecurityGroupIds": vpc_security_group_ids,
            "MasterUserPassword": master_user_password,
            "ClusterParameterGroupName": cluster_parameter_group_name,
            "AutomatedSnapshotRetentionPeriod": automated_snapshot_retention_period,
            "ManualSnapshotRetentionPeriod": manual_snapshot_retention_period,
            "PreferredMaintenanceWindow": preferred_maintenance_window,
            "ClusterVersion": cluster_version,
            "AllowVersionUpgrade": allow_version_upgrade,
            "HsmClientCertificateIdentifier": hsm_client_certificate_identifier,
            "HsmConfigurationIdentifier": hsm_configuration_identifier,
            "NewClusterIdentifier": new_cluster_identifier,
            "PubliclyAccessible": publicly_accessible,
            "ElasticIp": elastic_ip,
            "EnhancedVpcRouting": enhanced_vpc_routing,
            "MaintenanceTrackName": maintenance_track_name,
            "Encrypted": encrypted,
            "KmsKeyId": kms_key_id,
            "AvailabilityZoneRelocation": availability_zone_relocation,
            "AvailabilityZone": availability_zone,
            "Port": port,
        },
    )


async def pause(hub, ctx, cluster_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Pauses a cluster.

    Args:
        cluster_identifier(str): The identifier of the cluster to be paused.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.pause(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.pause cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="pause_cluster",
        op_kwargs={"ClusterIdentifier": cluster_identifier},
    )


async def reboot(hub, ctx, cluster_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Reboots a cluster. This action is taken as soon as possible. It results in a momentary outage to the cluster, during which the cluster status is set to rebooting. A cluster event is created when the reboot is completed. Any pending cluster modifications (see ModifyCluster) are applied at this reboot. For more information about managing clusters, go to Amazon Redshift Clusters in the Amazon Redshift Cluster Management Guide.

    Args:
        cluster_identifier(str): The cluster identifier.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.reboot(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.reboot cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="reboot_cluster",
        op_kwargs={"ClusterIdentifier": cluster_identifier},
    )


async def resize(
    hub,
    ctx,
    cluster_identifier: str,
    cluster_type: str = None,
    node_type: str = None,
    number_of_nodes: int = None,
    classic: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Changes the size of the cluster. You can change the cluster's type, or change the number or type of nodes. The default behavior is to use the elastic resize method. With an elastic resize, your cluster is available for read and write operations more quickly than with the classic resize method.  Elastic resize operations have the following restrictions:   You can only resize clusters of the following types:   dc1.large (if your cluster is in a VPC)   dc1.8xlarge (if your cluster is in a VPC)   dc2.large   dc2.8xlarge   ds2.xlarge   ds2.8xlarge   ra3.xlplus   ra3.4xlarge   ra3.16xlarge     The type of nodes that you add must match the node type for the cluster.

    Args:
        cluster_identifier(str): The unique identifier for the cluster to resize.
        cluster_type(str, optional): The new cluster type for the specified cluster. Defaults to None.
        node_type(str, optional): The new node type for the nodes you are adding. If not specified, the cluster's current node type is used. Defaults to None.
        number_of_nodes(int, optional): The new number of nodes for the cluster. If not specified, the cluster's current number of nodes is used. Defaults to None.
        classic(bool, optional): A boolean value indicating whether the resize operation is using the classic resize process. If you don't provide this parameter or set the value to false, the resize type is elastic. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.resize(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.resize cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="resize_cluster",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "ClusterType": cluster_type,
            "NodeType": node_type,
            "NumberOfNodes": number_of_nodes,
            "Classic": classic,
        },
    )


async def resume(hub, ctx, cluster_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Resumes a paused cluster.

    Args:
        cluster_identifier(str): The identifier of the cluster to be resumed.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.cluster.init.resume(ctx, cluster_identifier=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.cluster.init.resume cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="resume_cluster",
        op_kwargs={"ClusterIdentifier": cluster_identifier},
    )
