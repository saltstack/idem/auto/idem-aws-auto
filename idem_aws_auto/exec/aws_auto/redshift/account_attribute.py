"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe_all(hub, ctx, attribute_names: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of attributes attached to an account

    Args:
        attribute_names(List, optional): A list of attribute names. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift.account_attribute.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift.account_attribute.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift",
        operation="describe_account_attributes",
        op_kwargs={"AttributeNames": attribute_names},
    )
