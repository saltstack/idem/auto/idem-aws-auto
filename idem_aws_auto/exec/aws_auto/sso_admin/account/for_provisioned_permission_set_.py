"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__virtualname__ = "for_provisioned_permission_set"


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    instance_arn: str,
    permission_set_arn: str,
    provisioning_status: str = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the Amazon Web Services accounts where the specified permission set is provisioned.

    Args:
        instance_arn(str): The ARN of the SSO instance under which the operation will be executed. For more information about ARNs, see Amazon Resource Names (ARNs) and Amazon Web Services Service Namespaces in the Amazon Web Services General Reference.
        permission_set_arn(str): The ARN of the PermissionSet from which the associated Amazon Web Services accounts will be listed.
        provisioning_status(str, optional): The permission set provisioning status for an Amazon Web Services account. Defaults to None.
        max_results(int, optional): The maximum number of results to display for the PermissionSet. Defaults to None.
        next_token(str, optional): The pagination token for the list API. Initially the value is null. Use the output of previous API calls to make subsequent calls. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sso_admin.account.for_provisioned_permission_set_.list(
                ctx, instance_arn=value, permission_set_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sso_admin.account.for_provisioned_permission_set.list instance_arn=value, permission_set_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sso-admin",
        operation="list_accounts_for_provisioned_permission_set",
        op_kwargs={
            "InstanceArn": instance_arn,
            "PermissionSetArn": permission_set_arn,
            "ProvisioningStatus": provisioning_status,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
