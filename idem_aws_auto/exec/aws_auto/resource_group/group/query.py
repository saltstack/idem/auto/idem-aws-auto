"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, group_name: str = None, group: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the resource query associated with the specified resource group. For more information about resource queries, see Create a tag-based group in Resource Groups.  Minimum permissions  To run this command, you must have the following permissions:    resource-groups:GetGroupQuery

    Args:
        group_name(str, optional): Don't use this parameter. Use Group instead. Defaults to None.
        group(str, optional): The name or the ARN of the resource group to query. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.resource_group.group.query.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.resource_group.group.query.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="resource-groups",
        operation="get_group_query",
        op_kwargs={"GroupName": group_name, "Group": group},
    )


async def update(
    hub, ctx, resource_query: Dict, group_name: str = None, group: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the resource query of a group. For more information about resource queries, see Create a tag-based group in Resource Groups.  Minimum permissions  To run this command, you must have the following permissions:    resource-groups:UpdateGroupQuery

    Args:
        group_name(str, optional): Don't use this parameter. Use Group instead. Defaults to None.
        group(str, optional): The name or the ARN of the resource group to query. Defaults to None.
        resource_query(Dict): The resource query to determine which AWS resources are members of this resource group.  A resource group can contain either a Configuration or a ResourceQuery, but not both. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.resource_group.group.query.update(ctx, resource_query=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.resource_group.group.query.update resource_query=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="resource-groups",
        operation="update_group_query",
        op_kwargs={
            "GroupName": group_name,
            "Group": group,
            "ResourceQuery": resource_query,
        },
    )
