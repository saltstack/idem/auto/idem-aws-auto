"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified namespace. This action deletes all of the entities in the namespace. Delete the systems and flows that use entities in the namespace before performing this action.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotthingsgraph.namespace.init.delete(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotthingsgraph.namespace.init.delete
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="iotthingsgraph", operation="delete_namespace", op_kwargs={}
    )


async def describe(hub, ctx, namespace_name: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets the latest version of the user's namespace and the public version that it is tracking.

    Args:
        namespace_name(str, optional): The name of the user's namespace. Set this to aws to get the public namespace. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotthingsgraph.namespace.init.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotthingsgraph.namespace.init.describe
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotthingsgraph",
        operation="describe_namespace",
        op_kwargs={"namespaceName": namespace_name},
    )
