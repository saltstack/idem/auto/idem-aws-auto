"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Creates a tag for the specified resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource whose tags are returned.
        tags(List): A list of tags to add to the resource.>.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotthingsgraph.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotthingsgraph.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotthingsgraph",
        operation="tag_resource",
        op_kwargs={"resourceArn": resource_arn, "tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes a tag from the specified resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource whose tags are to be removed.
        tag_keys(List): A list of tag key names to remove from the resource. You don't specify the value. Both the key and its associated value are removed.  This parameter to the API requires a JSON text string argument. For information on how to format a JSON parameter for the various command line tool environments, see Using JSON for Parameters in the AWS CLI User Guide. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotthingsgraph.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotthingsgraph.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotthingsgraph",
        operation="untag_resource",
        op_kwargs={"resourceArn": resource_arn, "tagKeys": tag_keys},
    )
