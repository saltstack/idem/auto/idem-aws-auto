"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def calculate(
    hub,
    ctx,
    calculator_name: str,
    departure_position: List,
    destination_position: List,
    car_mode_options: Dict = None,
    depart_now: bool = None,
    departure_time: str = None,
    distance_unit: str = None,
    include_leg_geometry: bool = None,
    travel_mode: str = None,
    truck_mode_options: Dict = None,
    waypoint_positions: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Calculates a route given the following required parameters: DeparturePostiton and DestinationPosition. Requires that you first create a route calculator resource  By default, a request that doesn't specify a departure time uses the best time of day to travel with the best traffic conditions when calculating the route. Additional options include:    Specifying a departure time using either DepartureTime or DepartureNow. This calculates a route based on predictive traffic data at the given time.   You can't specify both DepartureTime and DepartureNow in a single request. Specifying both parameters returns an error message.     Specifying a travel mode using TravelMode. This lets you specify an additional route preference such as CarModeOptions if traveling by Car, or TruckModeOptions if traveling by Truck.

    Args:
        calculator_name(str): The name of the route calculator resource that you want to use to calculate a route. .
        car_mode_options(Dict, optional): Specifies route preferences when traveling by Car, such as avoiding routes that use ferries or tolls. Requirements: TravelMode must be specified as Car. Defaults to None.
        depart_now(bool, optional): Sets the time of departure as the current time. Uses the current time to calculate a route. Otherwise, the best time of day to travel with the best traffic conditions is used to calculate the route. Default Value: false  Valid Values: false | true . Defaults to None.
        departure_position(List): The start position for the route. Defined in WGS 84 format: [longitude, latitude].   For example, [-123.115, 49.285]     If you specify a departure that's not located on a road, Amazon Location moves the position to the nearest road.  Valid Values: [-180 to 180,-90 to 90] .
        departure_time(str, optional): Specifies the desired time of departure. Uses the given time to calculate a route. Otherwise, the best time of day to travel with the best traffic conditions is used to calculate the route.  Setting a departure time in the past returns a 400 ValidationException error.    In ISO 8601 format: YYYY-MM-DDThh:mm:ss.sssZ. For example, 2020–07-2T12:15:20.000Z+01:00   . Defaults to None.
        destination_position(List): The finish position for the route. Defined in WGS 84 format: [longitude, latitude].    For example, [-122.339, 47.615]     If you specify a destination that's not located on a road, Amazon Location moves the position to the nearest road.   Valid Values: [-180 to 180,-90 to 90] .
        distance_unit(str, optional): Set the unit system to specify the distance. Default Value: Kilometers . Defaults to None.
        include_leg_geometry(bool, optional): Set to include the geometry details in the result for each path between a pair of positions. Default Value: false  Valid Values: false | true . Defaults to None.
        travel_mode(str, optional): Specifies the mode of transport when calculating a route. Used in estimating the speed of travel and road compatibility. The TravelMode you specify determines how you specify route preferences:    If traveling by Car use the CarModeOptions parameter.   If traveling by Truck use the TruckModeOptions parameter.   Default Value: Car . Defaults to None.
        truck_mode_options(Dict, optional): Specifies route preferences when traveling by Truck, such as avoiding routes that use ferries or tolls, and truck specifications to consider when choosing an optimal road. Requirements: TravelMode must be specified as Truck. Defaults to None.
        waypoint_positions(List, optional): Specifies an ordered list of up to 23 intermediate positions to include along a route between the departure position and destination position.    For example, from the DeparturePosition [-123.115, 49.285], the route follows the order that the waypoint positions are given [[-122.757, 49.0021],[-122.349, 47.620]]     If you specify a waypoint position that's not located on a road, Amazon Location moves the position to the nearest road.  Specifying more than 23 waypoints returns a 400 ValidationException error.  Valid Values: [-180 to 180,-90 to 90] . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.location.route.init.calculate(
                ctx, calculator_name=value, departure_position=value, destination_position=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.location.route.init.calculate calculator_name=value, departure_position=value, destination_position=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="location",
        operation="calculate_route",
        op_kwargs={
            "CalculatorName": calculator_name,
            "CarModeOptions": car_mode_options,
            "DepartNow": depart_now,
            "DeparturePosition": departure_position,
            "DepartureTime": departure_time,
            "DestinationPosition": destination_position,
            "DistanceUnit": distance_unit,
            "IncludeLegGeometry": include_leg_geometry,
            "TravelMode": travel_mode,
            "TruckModeOptions": truck_mode_options,
            "WaypointPositions": waypoint_positions,
        },
    )
