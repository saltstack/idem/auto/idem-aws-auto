"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def search(
    hub, ctx, index_name: str, position: List, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Reverse geocodes a given coordinate and returns a legible address. Allows you to search for Places or points of interest near a given position.

    Args:
        index_name(str): The name of the place index resource you want to use for the search.
        max_results(int, optional): An optional paramer. The maximum number of results returned per request.  Default value: 50 . Defaults to None.
        position(List): Specifies a coordinate for the query defined by a longitude, and latitude.   The first position is the X coordinate, or longitude.   The second position is the Y coordinate, or latitude.    For example, position=xLongitude&position=yLatitude .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.location.place_index.for_.position.search(
                ctx, index_name=value, position=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.location.place_index.for_.position.search index_name=value, position=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="location",
        operation="search_place_index_for_position",
        op_kwargs={
            "IndexName": index_name,
            "MaxResults": max_results,
            "Position": position,
        },
    )
