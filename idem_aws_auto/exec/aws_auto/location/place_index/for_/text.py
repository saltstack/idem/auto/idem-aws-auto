"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def search(
    hub,
    ctx,
    index_name: str,
    text: str,
    bias_position: List = None,
    filter_b_box: List = None,
    filter_countries: List = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Geocodes free-form text, such as an address, name, city, or region to allow you to search for Places or points of interest.  Includes the option to apply additional parameters to narrow your list of results.  You can search for places near a given position using BiasPosition, or filter results within a bounding box using FilterBBox. Providing both parameters simultaneously returns an error.

    Args:
        bias_position(List, optional): Searches for results closest to the given position. An optional parameter defined by longitude, and latitude.   The first bias position is the X coordinate, or longitude.   The second bias position is the Y coordinate, or latitude.    For example, bias=xLongitude&bias=yLatitude. Defaults to None.
        filter_b_box(List, optional): Filters the results by returning only Places within the provided bounding box. An optional parameter. The first 2 bbox parameters describe the lower southwest corner:   The first bbox position is the X coordinate or longitude of the lower southwest corner.   The second bbox position is the Y coordinate or latitude of the lower southwest corner.   For example, bbox=xLongitudeSW&bbox=yLatitudeSW. The next bbox parameters describe the upper northeast corner:   The third bbox position is the X coordinate, or longitude of the upper northeast corner.   The fourth bbox position is the Y coordinate, or longitude of the upper northeast corner.   For example, bbox=xLongitudeNE&bbox=yLatitudeNE . Defaults to None.
        filter_countries(List, optional): Limits the search to the given a list of countries/regions. An optional parameter.   Use the ISO 3166 3-digit country code. For example, Australia uses three upper-case characters: AUS.  . Defaults to None.
        index_name(str): The name of the place index resource you want to use for the search.
        max_results(int, optional): An optional parameter. The maximum number of results returned per request.  The default: 50 . Defaults to None.
        text(str): The address, name, city, or region to be used in the search. In free-form text format. For example, 123 Any Street.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.location.place_index.for_.text.search(
                ctx, index_name=value, text=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.location.place_index.for_.text.search index_name=value, text=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="location",
        operation="search_place_index_for_text",
        op_kwargs={
            "BiasPosition": bias_position,
            "FilterBBox": filter_b_box,
            "FilterCountries": filter_countries,
            "IndexName": index_name,
            "MaxResults": max_results,
            "Text": text,
        },
    )
