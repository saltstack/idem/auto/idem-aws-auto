"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(
    hub,
    ctx,
    fleet_id: str,
    start_time: str = None,
    end_time: str = None,
    limit: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves entries from a fleet's event log. Fleet events are initiated by changes in status, such as during fleet creation and termination, changes in capacity, etc. If a fleet has multiple locations, events are also initiated by changes to status and capacity in remote locations.  You can specify a time range to limit the result set. Use the pagination parameters to retrieve results as a set of sequential pages.  If successful, a collection of event log entries matching the request are returned.  Learn more   Setting up GameLift fleets   Related actions   ListFleets | DescribeEC2InstanceLimits | DescribeFleetAttributes | DescribeFleetCapacity | DescribeFleetEvents | DescribeFleetLocationAttributes | DescribeFleetPortSettings | DescribeFleetUtilization | DescribeRuntimeConfiguration | DescribeScalingPolicies | All APIs by task

    Args:
        fleet_id(str): A unique identifier for the fleet to get event logs for. You can use either the fleet ID or ARN value.
        start_time(str, optional): The earliest date to retrieve event logs for. If no start time is specified, this call returns entries starting from when the fleet was created to the specified end time. Format is a number expressed in Unix time as milliseconds (ex: "1469498468.057"). Defaults to None.
        end_time(str, optional): The most recent date to retrieve event logs for. If no end time is specified, this call returns entries from the specified start time up to the present. Format is a number expressed in Unix time as milliseconds (ex: "1469498468.057"). Defaults to None.
        limit(int, optional): The maximum number of results to return. Use this parameter with NextToken to get results as a set of sequential pages. Defaults to None.
        next_token(str, optional): A token that indicates the start of the next sequential page of results. Use the token that is returned with a previous call to this operation. To start at the beginning of the result set, do not specify a value. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.gamelift.fleet.event.describe_all(ctx, fleet_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.gamelift.fleet.event.describe_all fleet_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="gamelift",
        operation="describe_fleet_events",
        op_kwargs={
            "FleetId": fleet_id,
            "StartTime": start_time,
            "EndTime": end_time,
            "Limit": limit,
            "NextToken": next_token,
        },
    )
