"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def get(hub, ctx, catalog_name: str, database_name: str, table_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns table metadata for the specified catalog, database, and table.

    Args:
        catalog_name(str): The name of the data catalog that contains the database and table metadata to return.
        database_name(str): The name of the database that contains the table metadata to return.
        table_name(str): The name of the table for which metadata is returned.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.athena.table_metadata.get(
                ctx, catalog_name=value, database_name=value, table_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.athena.table_metadata.get catalog_name=value, database_name=value, table_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="athena",
        operation="get_table_metadata",
        op_kwargs={
            "CatalogName": catalog_name,
            "DatabaseName": database_name,
            "TableName": table_name,
        },
    )


async def list_(
    hub,
    ctx,
    catalog_name: str,
    database_name: str,
    expression: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the metadata for the tables in the specified data catalog database.

    Args:
        catalog_name(str): The name of the data catalog for which table metadata should be returned.
        database_name(str): The name of the database for which table metadata should be returned.
        expression(str, optional): A regex filter that pattern-matches table names. If no expression is supplied, metadata for all tables are listed. Defaults to None.
        next_token(str, optional): A token generated by the Athena service that specifies where to continue pagination if a previous request was truncated. To obtain the next set of pages, pass in the NextToken from the response object of the previous page call. Defaults to None.
        max_results(int, optional): Specifies the maximum number of results to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.athena.table_metadata.list(
                ctx, catalog_name=value, database_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.athena.table_metadata.list catalog_name=value, database_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="athena",
        operation="list_table_metadata",
        op_kwargs={
            "CatalogName": catalog_name,
            "DatabaseName": database_name,
            "Expression": expression,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
