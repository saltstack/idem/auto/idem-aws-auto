"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Adds one or more tags to an Athena resource. A tag is a label that you assign to a resource. In Athena, a resource can be a workgroup or data catalog. Each tag consists of a key and an optional value, both of which you define. For example, you can use tags to categorize Athena workgroups or data catalogs by purpose, owner, or environment. Use a consistent set of tag keys to make it easier to search and filter workgroups or data catalogs in your account. For best practices, see Tagging Best Practices. Tag keys can be from 1 to 128 UTF-8 Unicode characters, and tag values can be from 0 to 256 UTF-8 Unicode characters. Tags can use letters and numbers representable in UTF-8, and the following characters: + - = . _ : / @. Tag keys and values are case-sensitive. Tag keys must be unique per resource. If you specify more than one tag, separate them by commas.

    Args:
        resource_arn(str): Specifies the ARN of the Athena resource (workgroup or data catalog) to which tags are to be added.
        tags(List): A collection of one or more tags, separated by commas, to be added to an Athena workgroup or data catalog resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.athena.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.athena.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="athena",
        operation="tag_resource",
        op_kwargs={"ResourceARN": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes one or more tags from a data catalog or workgroup resource.

    Args:
        resource_arn(str): Specifies the ARN of the resource from which tags are to be removed.
        tag_keys(List): A comma-separated list of one or more tag keys whose tags are to be removed from the specified resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.athena.resource.untag(ctx, resource_arn=value, tag_keys=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.athena.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="athena",
        operation="untag_resource",
        op_kwargs={"ResourceARN": resource_arn, "TagKeys": tag_keys},
    )
