"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"set_": "set"}


async def get_all(hub, ctx, identity_pool_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets the events and the corresponding Lambda functions associated with an identity pool.This API can only be called with developer credentials. You cannot call this API with the temporary user credentials provided by Cognito Identity.

    Args:
        identity_pool_id(str): The Cognito Identity Pool ID for the request.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_sync.cognito_event.get_all(ctx, identity_pool_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_sync.cognito_event.get_all identity_pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-sync",
        operation="get_cognito_events",
        op_kwargs={"IdentityPoolId": identity_pool_id},
    )


async def set_(hub, ctx, identity_pool_id: str, events: Dict) -> None:
    r"""
    **Autogenerated function**

    Sets the AWS Lambda function for a given event type for an identity pool. This request only updates the key/value pair specified. Other key/values pairs are not updated. To remove a key value pair, pass a empty value for the particular key.This API can only be called with developer credentials. You cannot call this API with the temporary user credentials provided by Cognito Identity.

    Args:
        identity_pool_id(str): The Cognito Identity Pool to use when configuring Cognito Events.
        events(Dict): The events to configure.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_sync.cognito_event.set(
                ctx, identity_pool_id=value, events=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_sync.cognito_event.set identity_pool_id=value, events=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-sync",
        operation="set_cognito_events",
        op_kwargs={"IdentityPoolId": identity_pool_id, "Events": events},
    )
