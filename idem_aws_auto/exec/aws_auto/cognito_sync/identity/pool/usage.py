"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def describe(hub, ctx, identity_pool_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets usage details (for example, data storage) about a particular identity pool. This API can only be called with developer credentials. You cannot call this API with the temporary user credentials provided by Cognito Identity.

    Args:
        identity_pool_id(str): A name-spaced GUID (for example, us-east-1:23EC4050-6AEA-7089-A2DD-08002EXAMPLE) created by Amazon Cognito. GUID generation is unique within a region.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_sync.identity.pool.usage.describe(
                ctx, identity_pool_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_sync.identity.pool.usage.describe identity_pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-sync",
        operation="describe_identity_pool_usage",
        op_kwargs={"IdentityPoolId": identity_pool_id},
    )


async def list_(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets a list of identity pools registered with Cognito. ListIdentityPoolUsage can only be called with developer credentials. You cannot make this API call with the temporary user credentials provided by Cognito Identity.

    Args:
        next_token(str, optional): A pagination token for obtaining the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to be returned. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_sync.identity.pool.usage.list(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_sync.identity.pool.usage.list
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-sync",
        operation="list_identity_pool_usage",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
