"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    asset_model_name: str,
    asset_model_description: str = None,
    asset_model_properties: List = None,
    asset_model_hierarchies: List = None,
    asset_model_composite_models: List = None,
    client_token: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an asset model from specified property and hierarchy definitions. You create assets from asset models. With asset models, you can easily create assets of the same type that have standardized definitions. Each asset created from a model inherits the asset model's property and hierarchy definitions. For more information, see Defining asset models in the IoT SiteWise User Guide.

    Args:
        asset_model_name(str): A unique, friendly name for the asset model.
        asset_model_description(str, optional): A description for the asset model. Defaults to None.
        asset_model_properties(List, optional): The property definitions of the asset model. For more information, see Asset properties in the IoT SiteWise User Guide. You can specify up to 200 properties per asset model. For more information, see Quotas in the IoT SiteWise User Guide. Defaults to None.
        asset_model_hierarchies(List, optional): The hierarchy definitions of the asset model. Each hierarchy specifies an asset model whose assets can be children of any other assets created from this asset model. For more information, see Asset hierarchies in the IoT SiteWise User Guide. You can specify up to 10 hierarchies per asset model. For more information, see Quotas in the IoT SiteWise User Guide. Defaults to None.
        asset_model_composite_models(List, optional): The composite asset models that are part of this asset model. Composite asset models are asset models that contain specific properties. Each composite model has a type that defines the properties that the composite model supports. Use composite asset models to define alarms on this asset model. Defaults to None.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.
        tags(Dict, optional): A list of key-value pairs that contain metadata for the asset model. For more information, see Tagging your IoT SiteWise resources in the IoT SiteWise User Guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.asset.model.create(ctx, asset_model_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.asset.model.create asset_model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="create_asset_model",
        op_kwargs={
            "assetModelName": asset_model_name,
            "assetModelDescription": asset_model_description,
            "assetModelProperties": asset_model_properties,
            "assetModelHierarchies": asset_model_hierarchies,
            "assetModelCompositeModels": asset_model_composite_models,
            "clientToken": client_token,
            "tags": tags,
        },
    )


async def delete(hub, ctx, asset_model_id: str, client_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an asset model. This action can't be undone. You must delete all assets created from an asset model before you can delete the model. Also, you can't delete an asset model if a parent asset model exists that contains a property formula expression that depends on the asset model that you want to delete. For more information, see Deleting assets and models in the IoT SiteWise User Guide.

    Args:
        asset_model_id(str): The ID of the asset model to delete.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.asset.model.delete(ctx, asset_model_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.asset.model.delete asset_model_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="delete_asset_model",
        op_kwargs={"assetModelId": asset_model_id, "clientToken": client_token},
    )


async def describe(hub, ctx, asset_model_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about an asset model.

    Args:
        asset_model_id(str): The ID of the asset model.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.asset.model.describe(ctx, asset_model_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.asset.model.describe asset_model_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="describe_asset_model",
        op_kwargs={"assetModelId": asset_model_id},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a paginated list of summaries of all asset models.

    Args:
        next_token(str, optional): The token to be used for the next set of paginated results. Defaults to None.
        max_results(int, optional): The maximum number of results to return for each paginated request. Default: 50. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.asset.model.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.asset.model.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="list_asset_models",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )


async def update(
    hub,
    ctx,
    asset_model_id: str,
    asset_model_name: str,
    asset_model_description: str = None,
    asset_model_properties: List = None,
    asset_model_hierarchies: List = None,
    asset_model_composite_models: List = None,
    client_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an asset model and all of the assets that were created from the model. Each asset created from the model inherits the updated asset model's property and hierarchy definitions. For more information, see Updating assets and models in the IoT SiteWise User Guide.  This operation overwrites the existing model with the provided model. To avoid deleting your asset model's properties or hierarchies, you must include their IDs and definitions in the updated asset model payload. For more information, see DescribeAssetModel. If you remove a property from an asset model, IoT SiteWise deletes all previous data for that property. If you remove a hierarchy definition from an asset model, IoT SiteWise disassociates every asset associated with that hierarchy. You can't change the type or data type of an existing property.

    Args:
        asset_model_id(str): The ID of the asset model to update.
        asset_model_name(str): A unique, friendly name for the asset model.
        asset_model_description(str, optional): A description for the asset model. Defaults to None.
        asset_model_properties(List, optional): The updated property definitions of the asset model. For more information, see Asset properties in the IoT SiteWise User Guide. You can specify up to 200 properties per asset model. For more information, see Quotas in the IoT SiteWise User Guide. Defaults to None.
        asset_model_hierarchies(List, optional): The updated hierarchy definitions of the asset model. Each hierarchy specifies an asset model whose assets can be children of any other assets created from this asset model. For more information, see Asset hierarchies in the IoT SiteWise User Guide. You can specify up to 10 hierarchies per asset model. For more information, see Quotas in the IoT SiteWise User Guide. Defaults to None.
        asset_model_composite_models(List, optional): The composite asset models that are part of this asset model. Composite asset models are asset models that contain specific properties. Each composite model has a type that defines the properties that the composite model supports. Use composite asset models to define alarms on this asset model. Defaults to None.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.asset.model.update(
                ctx, asset_model_id=value, asset_model_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.asset.model.update asset_model_id=value, asset_model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="update_asset_model",
        op_kwargs={
            "assetModelId": asset_model_id,
            "assetModelName": asset_model_name,
            "assetModelDescription": asset_model_description,
            "assetModelProperties": asset_model_properties,
            "assetModelHierarchies": asset_model_hierarchies,
            "assetModelCompositeModels": asset_model_composite_models,
            "clientToken": client_token,
        },
    )
