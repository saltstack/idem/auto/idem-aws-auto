"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    access_policy_identity: Dict,
    access_policy_resource: Dict,
    access_policy_permission: str,
    client_token: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an access policy that grants the specified identity (Amazon Web Services SSO user, Amazon Web Services SSO group, or IAM user) access to the specified IoT SiteWise Monitor portal or project resource.

    Args:
        access_policy_identity(Dict): The identity for this access policy. Choose an Amazon Web Services SSO user, an Amazon Web Services SSO group, or an IAM user.
        access_policy_resource(Dict): The IoT SiteWise Monitor resource for this access policy. Choose either a portal or a project.
        access_policy_permission(str): The permission level for this access policy. Note that a project ADMINISTRATOR is also known as a project owner.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.
        tags(Dict, optional): A list of key-value pairs that contain metadata for the access policy. For more information, see Tagging your IoT SiteWise resources in the IoT SiteWise User Guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.access_policy.create(
                ctx,
                access_policy_identity=value,
                access_policy_resource=value,
                access_policy_permission=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.access_policy.create access_policy_identity=value, access_policy_resource=value, access_policy_permission=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="create_access_policy",
        op_kwargs={
            "accessPolicyIdentity": access_policy_identity,
            "accessPolicyResource": access_policy_resource,
            "accessPolicyPermission": access_policy_permission,
            "clientToken": client_token,
            "tags": tags,
        },
    )


async def delete(hub, ctx, access_policy_id: str, client_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an access policy that grants the specified identity access to the specified IoT SiteWise Monitor resource. You can use this operation to revoke access to an IoT SiteWise Monitor resource.

    Args:
        access_policy_id(str): The ID of the access policy to be deleted.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.access_policy.delete(ctx, access_policy_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.access_policy.delete access_policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="delete_access_policy",
        op_kwargs={"accessPolicyId": access_policy_id, "clientToken": client_token},
    )


async def describe(hub, ctx, access_policy_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes an access policy, which specifies an identity's access to an IoT SiteWise Monitor portal or project.

    Args:
        access_policy_id(str): The ID of the access policy.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.access_policy.describe(ctx, access_policy_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.access_policy.describe access_policy_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="describe_access_policy",
        op_kwargs={"accessPolicyId": access_policy_id},
    )


async def list_all(
    hub,
    ctx,
    identity_type: str = None,
    identity_id: str = None,
    resource_type: str = None,
    resource_id: str = None,
    iam_arn: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a paginated list of access policies for an identity (an Amazon Web Services SSO user, an Amazon Web Services SSO group, or an IAM user) or an IoT SiteWise Monitor resource (a portal or project).

    Args:
        identity_type(str, optional): The type of identity (Amazon Web Services SSO user, Amazon Web Services SSO group, or IAM user). This parameter is required if you specify identityId. Defaults to None.
        identity_id(str, optional): The ID of the identity. This parameter is required if you specify USER or GROUP for identityType. Defaults to None.
        resource_type(str, optional): The type of resource (portal or project). This parameter is required if you specify resourceId. Defaults to None.
        resource_id(str, optional): The ID of the resource. This parameter is required if you specify resourceType. Defaults to None.
        iam_arn(str, optional): The ARN of the IAM user. For more information, see IAM ARNs in the IAM User Guide. This parameter is required if you specify IAM for identityType. Defaults to None.
        next_token(str, optional): The token to be used for the next set of paginated results. Defaults to None.
        max_results(int, optional): The maximum number of results to return for each paginated request. Default: 50. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.access_policy.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.access_policy.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="list_access_policies",
        op_kwargs={
            "identityType": identity_type,
            "identityId": identity_id,
            "resourceType": resource_type,
            "resourceId": resource_id,
            "iamArn": iam_arn,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    access_policy_id: str,
    access_policy_identity: Dict,
    access_policy_resource: Dict,
    access_policy_permission: str,
    client_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an existing access policy that specifies an identity's access to an IoT SiteWise Monitor portal or project resource.

    Args:
        access_policy_id(str): The ID of the access policy.
        access_policy_identity(Dict): The identity for this access policy. Choose an Amazon Web Services SSO user, an Amazon Web Services SSO group, or an IAM user.
        access_policy_resource(Dict): The IoT SiteWise Monitor resource for this access policy. Choose either a portal or a project.
        access_policy_permission(str): The permission level for this access policy. Note that a project ADMINISTRATOR is also known as a project owner.
        client_token(str, optional): A unique case-sensitive identifier that you can provide to ensure the idempotency of the request. Don't reuse this client token if a new idempotent request is required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.access_policy.update(
                ctx,
                access_policy_id=value,
                access_policy_identity=value,
                access_policy_resource=value,
                access_policy_permission=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.access_policy.update access_policy_id=value, access_policy_identity=value, access_policy_resource=value, access_policy_permission=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="update_access_policy",
        op_kwargs={
            "accessPolicyId": access_policy_id,
            "accessPolicyIdentity": access_policy_identity,
            "accessPolicyResource": access_policy_resource,
            "accessPolicyPermission": access_policy_permission,
            "clientToken": client_token,
        },
    )
