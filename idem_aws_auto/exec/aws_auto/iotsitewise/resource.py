"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Adds tags to an IoT SiteWise resource. If a tag already exists for the resource, this operation updates the tag's value.

    Args:
        resource_arn(str): The ARN of the resource to tag.
        tags(Dict): A list of key-value pairs that contain metadata for the resource. For more information, see Tagging your IoT SiteWise resources in the IoT SiteWise User Guide.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="tag_resource",
        op_kwargs={"resourceArn": resource_arn, "tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes a tag from an IoT SiteWise resource.

    Args:
        resource_arn(str): The ARN of the resource to untag.
        tag_keys(List): A list of keys for tags to remove from the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotsitewise.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotsitewise.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotsitewise",
        operation="untag_resource",
        op_kwargs={"resourceArn": resource_arn, "tagKeys": tag_keys},
    )
