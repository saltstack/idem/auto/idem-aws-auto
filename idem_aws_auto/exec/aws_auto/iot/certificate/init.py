"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, certificate_id: str, force_delete: bool = None) -> None:
    r"""
    **Autogenerated function**

    Deletes the specified certificate. A certificate cannot be deleted if it has a policy or IoT thing attached to it or if its status is set to ACTIVE. To delete a certificate, first use the DetachPrincipalPolicy API to detach all policies. Next, use the UpdateCertificate API to set the certificate to the INACTIVE status.

    Args:
        certificate_id(str): The ID of the certificate. (The last part of the certificate ARN contains the certificate ID.).
        force_delete(bool, optional): Forces the deletion of a certificate if it is inactive and is not attached to an IoT thing. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.delete(ctx, certificate_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.delete certificate_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="delete_certificate",
        op_kwargs={"certificateId": certificate_id, "forceDelete": force_delete},
    )


async def describe(hub, ctx, certificate_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about the specified certificate.

    Args:
        certificate_id(str): The ID of the certificate. (The last part of the certificate ARN contains the certificate ID.).

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.describe(ctx, certificate_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.describe certificate_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="describe_certificate",
        op_kwargs={"certificateId": certificate_id},
    )


async def list_all(
    hub, ctx, page_size: int = None, marker: str = None, ascending_order: bool = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the certificates registered in your AWS account. The results are paginated with a default page size of 25. You can use the returned marker to retrieve additional results.

    Args:
        page_size(int, optional): The result page size. Defaults to None.
        marker(str, optional): The marker for the next set of results. Defaults to None.
        ascending_order(bool, optional): Specifies the order for results. If True, the results are returned in ascending order, based on the creation date. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_certificates",
        op_kwargs={
            "pageSize": page_size,
            "marker": marker,
            "ascendingOrder": ascending_order,
        },
    )


async def register(
    hub,
    ctx,
    certificate_pem: str,
    ca_certificate_pem: str = None,
    set_as_active: bool = None,
    status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Registers a device certificate with AWS IoT. If you have more than one CA certificate that has the same subject field, you must specify the CA certificate that was used to sign the device certificate being registered.

    Args:
        certificate_pem(str): The certificate data, in PEM format.
        ca_certificate_pem(str, optional): The CA certificate used to sign the device certificate being registered. Defaults to None.
        set_as_active(bool, optional): A boolean value that specifies if the certificate is set to active. Defaults to None.
        status(str, optional): The status of the register certificate request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.register(ctx, certificate_pem=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.register certificate_pem=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="register_certificate",
        op_kwargs={
            "certificatePem": certificate_pem,
            "caCertificatePem": ca_certificate_pem,
            "setAsActive": set_as_active,
            "status": status,
        },
    )


async def transfer(
    hub, ctx, certificate_id: str, target_aws_account: str, transfer_message: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Transfers the specified certificate to the specified AWS account. You can cancel the transfer until it is acknowledged by the recipient. No notification is sent to the transfer destination's account. It is up to the caller to notify the transfer target. The certificate being transferred must not be in the ACTIVE state. You can use the UpdateCertificate API to deactivate it. The certificate must not have any policies attached to it. You can use the DetachPrincipalPolicy API to detach them.

    Args:
        certificate_id(str): The ID of the certificate. (The last part of the certificate ARN contains the certificate ID.).
        target_aws_account(str): The AWS account.
        transfer_message(str, optional): The transfer message. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.transfer(
                ctx, certificate_id=value, target_aws_account=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.transfer certificate_id=value, target_aws_account=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="transfer_certificate",
        op_kwargs={
            "certificateId": certificate_id,
            "targetAwsAccount": target_aws_account,
            "transferMessage": transfer_message,
        },
    )


async def update(hub, ctx, certificate_id: str, new_status: str) -> None:
    r"""
    **Autogenerated function**

    Updates the status of the specified certificate. This operation is idempotent. Certificates must be in the ACTIVE state to authenticate devices that use a certificate to connect to AWS IoT. Within a few minutes of updating a certificate from the ACTIVE state to any other state, AWS IoT disconnects all devices that used that certificate to connect. Devices cannot use a certificate that is not in the ACTIVE state to reconnect.

    Args:
        certificate_id(str): The ID of the certificate. (The last part of the certificate ARN contains the certificate ID.).
        new_status(str): The new status.  Note: Setting the status to PENDING_TRANSFER or PENDING_ACTIVATION will result in an exception being thrown. PENDING_TRANSFER and PENDING_ACTIVATION are statuses used internally by AWS IoT. They are not intended for developer use.  Note: The status value REGISTER_INACTIVE is deprecated and should not be used.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.certificate.init.update(
                ctx, certificate_id=value, new_status=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.certificate.init.update certificate_id=value, new_status=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="update_certificate",
        op_kwargs={"certificateId": certificate_id, "newStatus": new_status},
    )
