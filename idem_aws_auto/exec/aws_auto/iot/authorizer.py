"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    authorizer_name: str,
    authorizer_function_arn: str,
    token_key_name: str = None,
    token_signing_public_keys: Dict = None,
    status: str = None,
    tags: List = None,
    signing_disabled: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an authorizer.

    Args:
        authorizer_name(str): The authorizer name.
        authorizer_function_arn(str): The ARN of the authorizer's Lambda function.
        token_key_name(str, optional): The name of the token key used to extract the token from the HTTP headers. Defaults to None.
        token_signing_public_keys(Dict, optional): The public keys used to verify the digital signature returned by your custom authentication service. Defaults to None.
        status(str, optional): The status of the create authorizer request. Defaults to None.
        tags(List, optional): Metadata which can be used to manage the custom authorizer.  For URI Request parameters use format: ...key1=value1&key2=value2... For the CLI command-line parameter use format: &&tags "key1=value1&key2=value2..." For the cli-input-json file use format: "tags": "key1=value1&key2=value2..." . Defaults to None.
        signing_disabled(bool, optional): Specifies whether AWS IoT validates the token signature in an authorization request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.authorizer.create(
                ctx, authorizer_name=value, authorizer_function_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.authorizer.create authorizer_name=value, authorizer_function_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="create_authorizer",
        op_kwargs={
            "authorizerName": authorizer_name,
            "authorizerFunctionArn": authorizer_function_arn,
            "tokenKeyName": token_key_name,
            "tokenSigningPublicKeys": token_signing_public_keys,
            "status": status,
            "tags": tags,
            "signingDisabled": signing_disabled,
        },
    )


async def delete(hub, ctx, authorizer_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an authorizer.

    Args:
        authorizer_name(str): The name of the authorizer to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.authorizer.delete(ctx, authorizer_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.authorizer.delete authorizer_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="delete_authorizer",
        op_kwargs={"authorizerName": authorizer_name},
    )


async def describe(hub, ctx, authorizer_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes an authorizer.

    Args:
        authorizer_name(str): The name of the authorizer to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.authorizer.describe(ctx, authorizer_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.authorizer.describe authorizer_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="describe_authorizer",
        op_kwargs={"authorizerName": authorizer_name},
    )


async def list_all(
    hub,
    ctx,
    page_size: int = None,
    marker: str = None,
    ascending_order: bool = None,
    status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the authorizers registered in your account.

    Args:
        page_size(int, optional): The maximum number of results to return at one time. Defaults to None.
        marker(str, optional): A marker used to get the next set of results. Defaults to None.
        ascending_order(bool, optional): Return the list of authorizers in ascending alphabetical order. Defaults to None.
        status(str, optional): The status of the list authorizers request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.authorizer.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.authorizer.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_authorizers",
        op_kwargs={
            "pageSize": page_size,
            "marker": marker,
            "ascendingOrder": ascending_order,
            "status": status,
        },
    )


async def update(
    hub,
    ctx,
    authorizer_name: str,
    authorizer_function_arn: str = None,
    token_key_name: str = None,
    token_signing_public_keys: Dict = None,
    status: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an authorizer.

    Args:
        authorizer_name(str): The authorizer name.
        authorizer_function_arn(str, optional): The ARN of the authorizer's Lambda function. Defaults to None.
        token_key_name(str, optional): The key used to extract the token from the HTTP headers. . Defaults to None.
        token_signing_public_keys(Dict, optional): The public keys used to verify the token signature. Defaults to None.
        status(str, optional): The status of the update authorizer request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.authorizer.update(ctx, authorizer_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.authorizer.update authorizer_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="update_authorizer",
        op_kwargs={
            "authorizerName": authorizer_name,
            "authorizerFunctionArn": authorizer_function_arn,
            "tokenKeyName": token_key_name,
            "tokenSigningPublicKeys": token_signing_public_keys,
            "status": status,
        },
    )
