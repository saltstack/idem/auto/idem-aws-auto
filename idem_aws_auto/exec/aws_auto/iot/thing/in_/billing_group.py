"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, billing_group_name: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the things you have added to the given billing group.

    Args:
        billing_group_name(str): The name of the billing group.
        next_token(str, optional): To retrieve the next set of results, the nextToken value from a previous response; otherwise null to receive the first set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return per request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.in_.billing_group.list(ctx, billing_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.in_.billing_group.list billing_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_things_in_billing_group",
        op_kwargs={
            "billingGroupName": billing_group_name,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
