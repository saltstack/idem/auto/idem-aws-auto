"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    thing_group_name: str,
    recursive: bool = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the things in the specified group.

    Args:
        thing_group_name(str): The thing group name.
        recursive(bool, optional): When true, list things in this thing group and in all child groups as well. Defaults to None.
        next_token(str, optional): To retrieve the next set of results, the nextToken value from a previous response; otherwise null to receive the first set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return at one time. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.in_.thing_group.list(ctx, thing_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.in_.thing_group.list thing_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_things_in_thing_group",
        op_kwargs={
            "thingGroupName": thing_group_name,
            "recursive": recursive,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
