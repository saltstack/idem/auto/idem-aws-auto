"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def add(
    hub,
    ctx,
    billing_group_name: str = None,
    billing_group_arn: str = None,
    thing_name: str = None,
    thing_arn: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Adds a thing to a billing group.

    Args:
        billing_group_name(str, optional): The name of the billing group. Defaults to None.
        billing_group_arn(str, optional): The ARN of the billing group. Defaults to None.
        thing_name(str, optional): The name of the thing to be added to the billing group. Defaults to None.
        thing_arn(str, optional): The ARN of the thing to be added to the billing group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.to.billing_group.add(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.to.billing_group.add
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="add_thing_to_billing_group",
        op_kwargs={
            "billingGroupName": billing_group_name,
            "billingGroupArn": billing_group_arn,
            "thingName": thing_name,
            "thingArn": thing_arn,
        },
    )
