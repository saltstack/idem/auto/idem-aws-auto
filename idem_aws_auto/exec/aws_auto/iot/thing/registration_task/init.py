"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(hub, ctx, task_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Describes a bulk thing provisioning task.

    Args:
        task_id(str): The task ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.registration_task.init.describe(ctx, task_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.registration_task.init.describe task_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="describe_thing_registration_task",
        op_kwargs={"taskId": task_id},
    )


async def list_all(
    hub, ctx, next_token: str = None, max_results: int = None, status: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    List bulk thing provisioning tasks.

    Args:
        next_token(str, optional): To retrieve the next set of results, the nextToken value from a previous response; otherwise null to receive the first set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return at one time. Defaults to None.
        status(str, optional): The status of the bulk thing provisioning task. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.registration_task.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.registration_task.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_thing_registration_tasks",
        op_kwargs={
            "nextToken": next_token,
            "maxResults": max_results,
            "status": status,
        },
    )


async def start(
    hub,
    ctx,
    template_body: str,
    input_file_bucket: str,
    input_file_key: str,
    role_arn: str,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a bulk thing provisioning task.

    Args:
        template_body(str): The provisioning template.
        input_file_bucket(str): The S3 bucket that contains the input file.
        input_file_key(str): The name of input file within the S3 bucket. This file contains a newline delimited JSON file. Each line contains the parameter values to provision one device (thing).
        role_arn(str): The IAM role ARN that grants permission the input file.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.registration_task.init.start(
                ctx,
                template_body=value,
                input_file_bucket=value,
                input_file_key=value,
                role_arn=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.registration_task.init.start template_body=value, input_file_bucket=value, input_file_key=value, role_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="start_thing_registration_task",
        op_kwargs={
            "templateBody": template_body,
            "inputFileBucket": input_file_bucket,
            "inputFileKey": input_file_key,
            "roleArn": role_arn,
        },
    )


async def stop(hub, ctx, task_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Cancels a bulk thing provisioning task.

    Args:
        task_id(str): The bulk thing provisioning task ID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.registration_task.init.stop(ctx, task_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.registration_task.init.stop task_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="stop_thing_registration_task",
        op_kwargs={"taskId": task_id},
    )
