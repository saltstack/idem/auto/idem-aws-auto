"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    task_id: str,
    report_type: str,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Information about the thing registration tasks.

    Args:
        task_id(str): The id of the task.
        report_type(str): The type of task report.
        next_token(str, optional): To retrieve the next set of results, the nextToken value from a previous response; otherwise null to receive the first set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return per request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.registration_task.report.list_all(
                ctx, task_id=value, report_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.registration_task.report.list_all task_id=value, report_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_thing_registration_task_reports",
        op_kwargs={
            "taskId": task_id,
            "reportType": report_type,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
