"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    thing_group_name: str,
    parent_group_name: str = None,
    thing_group_properties: Dict = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Create a thing group.  This is a control plane operation. See Authorization for information about authorizing control plane actions.

    Args:
        thing_group_name(str): The thing group name to create.
        parent_group_name(str, optional): The name of the parent thing group. Defaults to None.
        thing_group_properties(Dict, optional): The thing group properties. Defaults to None.
        tags(List, optional): Metadata which can be used to manage the thing group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.group.init.create(ctx, thing_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.group.init.create thing_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="create_thing_group",
        op_kwargs={
            "thingGroupName": thing_group_name,
            "parentGroupName": parent_group_name,
            "thingGroupProperties": thing_group_properties,
            "tags": tags,
        },
    )


async def delete(hub, ctx, thing_group_name: str, expected_version: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a thing group.

    Args:
        thing_group_name(str): The name of the thing group to delete.
        expected_version(int, optional): The expected version of the thing group to delete. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.group.init.delete(ctx, thing_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.group.init.delete thing_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="delete_thing_group",
        op_kwargs={
            "thingGroupName": thing_group_name,
            "expectedVersion": expected_version,
        },
    )


async def describe(hub, ctx, thing_group_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Describe a thing group.

    Args:
        thing_group_name(str): The name of the thing group.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.group.init.describe(ctx, thing_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.group.init.describe thing_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="describe_thing_group",
        op_kwargs={"thingGroupName": thing_group_name},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    parent_group: str = None,
    name_prefix_filter: str = None,
    recursive: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    List the thing groups in your account.

    Args:
        next_token(str, optional): To retrieve the next set of results, the nextToken value from a previous response; otherwise null to receive the first set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return at one time. Defaults to None.
        parent_group(str, optional): A filter that limits the results to those with the specified parent group. Defaults to None.
        name_prefix_filter(str, optional): A filter that limits the results to those with the specified name prefix. Defaults to None.
        recursive(bool, optional): If true, return child groups as well. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.group.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.group.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="list_thing_groups",
        op_kwargs={
            "nextToken": next_token,
            "maxResults": max_results,
            "parentGroup": parent_group,
            "namePrefixFilter": name_prefix_filter,
            "recursive": recursive,
        },
    )


async def update(
    hub,
    ctx,
    thing_group_name: str,
    thing_group_properties: Dict,
    expected_version: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Update a thing group.

    Args:
        thing_group_name(str): The thing group to update.
        thing_group_properties(Dict): The thing group properties.
        expected_version(int, optional): The expected version of the thing group. If this does not match the version of the thing group being updated, the update will fail. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.thing.group.init.update(
                ctx, thing_group_name=value, thing_group_properties=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.thing.group.init.update thing_group_name=value, thing_group_properties=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="update_thing_group",
        op_kwargs={
            "thingGroupName": thing_group_name,
            "thingGroupProperties": thing_group_properties,
            "expectedVersion": expected_version,
        },
    )
