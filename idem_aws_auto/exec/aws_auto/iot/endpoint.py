"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(hub, ctx, endpoint_type: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a unique endpoint specific to the AWS account making the call.

    Args:
        endpoint_type(str, optional): The endpoint type. Valid endpoint types include:    iot:Data - Returns a VeriSign signed data endpoint.      iot:Data-ATS - Returns an ATS signed data endpoint.      iot:CredentialProvider - Returns an AWS IoT credentials provider API endpoint.      iot:Jobs - Returns an AWS IoT device management Jobs API endpoint.   We strongly recommend that customers use the newer iot:Data-ATS endpoint type to avoid issues related to the widespread distrust of Symantec certificate authorities. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot.endpoint.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot.endpoint.describe
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot",
        operation="describe_endpoint",
        op_kwargs={"endpointType": endpoint_type},
    )
