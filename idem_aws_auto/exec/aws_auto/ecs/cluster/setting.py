"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def update_multiple(hub, ctx, cluster: str, settings: List) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the settings to use for a cluster.

    Args:
        cluster(str): The name of the cluster to modify the settings for.
        settings(List): The setting to use by default for a cluster. This parameter is used to enable CloudWatch Container Insights for a cluster. If this value is specified, it will override the containerInsights value set with PutAccountSetting or PutAccountSettingDefault.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ecs.cluster.setting.update_multiple(
                ctx, cluster=value, settings=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ecs.cluster.setting.update_multiple cluster=value, settings=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ecs",
        operation="update_cluster_settings",
        op_kwargs={"cluster": cluster, "settings": settings},
    )
