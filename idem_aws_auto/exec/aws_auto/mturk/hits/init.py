"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

     The ListHITs operation returns all of a Requester's HITs. The operation returns HITs of any status, except for HITs that have been deleted of with the DeleteHIT operation or that have been auto-deleted.

    Args:
        next_token(str, optional): Pagination token. Defaults to None.
        max_results(int, optional): . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mturk.hits.init.list(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mturk.hits.init.list
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mturk",
        operation="list_hits",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
