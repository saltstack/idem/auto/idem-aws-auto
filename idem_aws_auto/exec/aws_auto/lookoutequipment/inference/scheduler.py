"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    model_name: str,
    inference_scheduler_name: str,
    data_upload_frequency: str,
    data_input_configuration: Dict,
    data_output_configuration: Dict,
    role_arn: str,
    client_token: str,
    data_delay_offset_in_minutes: int = None,
    server_side_kms_key_id: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a scheduled inference. Scheduling an inference is setting up a continuous real-time inference plan to analyze new measurement data. When setting up the schedule, you provide an S3 bucket location for the input data, assign it a delimiter between separate entries in the data, set an offset delay if desired, and set the frequency of inferencing. You must also provide an S3 bucket location for the output data.

    Args:
        model_name(str): The name of the previously trained ML model being used to create the inference scheduler. .
        inference_scheduler_name(str): The name of the inference scheduler being created. .
        data_delay_offset_in_minutes(int, optional):  A period of time (in minutes) by which inference on the data is delayed after the data starts. For instance, if you select an offset delay time of five minutes, inference will not begin on the data until the first data measurement after the five minute mark. For example, if five minutes is selected, the inference scheduler will wake up at the configured frequency with the additional five minute delay time to check the customer S3 bucket. The customer can upload data at the same frequency and they don't need to stop and restart the scheduler when uploading new data. . Defaults to None.
        data_upload_frequency(str):  How often data is uploaded to the source S3 bucket for the input data. The value chosen is the length of time between data uploads. For instance, if you select 5 minutes, Amazon Lookout for Equipment will upload the real-time data to the source bucket once every 5 minutes. This frequency also determines how often Amazon Lookout for Equipment starts a scheduled inference on your data. In this example, it starts once every 5 minutes. .
        data_input_configuration(Dict): Specifies configuration information for the input data for the inference scheduler, including delimiter, format, and dataset location. .
        data_output_configuration(Dict): Specifies configuration information for the output results for the inference scheduler, including the S3 location for the output. .
        role_arn(str): The Amazon Resource Name (ARN) of a role with permission to access the data source being used for the inference. .
        server_side_kms_key_id(str, optional): Provides the identifier of the AWS KMS customer master key (CMK) used to encrypt inference scheduler data by Amazon Lookout for Equipment. . Defaults to None.
        client_token(str):  A unique identifier for the request. If you do not set the client request token, Amazon Lookout for Equipment generates one. .
        tags(List, optional): Any tags associated with the inference scheduler. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.create(
                ctx,
                model_name=value,
                inference_scheduler_name=value,
                data_upload_frequency=value,
                data_input_configuration=value,
                data_output_configuration=value,
                role_arn=value,
                client_token=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.create model_name=value, inference_scheduler_name=value, data_upload_frequency=value, data_input_configuration=value, data_output_configuration=value, role_arn=value, client_token=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="create_inference_scheduler",
        op_kwargs={
            "ModelName": model_name,
            "InferenceSchedulerName": inference_scheduler_name,
            "DataDelayOffsetInMinutes": data_delay_offset_in_minutes,
            "DataUploadFrequency": data_upload_frequency,
            "DataInputConfiguration": data_input_configuration,
            "DataOutputConfiguration": data_output_configuration,
            "RoleArn": role_arn,
            "ServerSideKmsKeyId": server_side_kms_key_id,
            "ClientToken": client_token,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, inference_scheduler_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes an inference scheduler that has been set up. Already processed output results are not affected.

    Args:
        inference_scheduler_name(str): The name of the inference scheduler to be deleted. .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.delete(
                ctx, inference_scheduler_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.delete inference_scheduler_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="delete_inference_scheduler",
        op_kwargs={"InferenceSchedulerName": inference_scheduler_name},
    )


async def describe(hub, ctx, inference_scheduler_name: str) -> Dict:
    r"""
    **Autogenerated function**

     Specifies information about the inference scheduler being used, including name, model, status, and associated metadata

    Args:
        inference_scheduler_name(str): The name of the inference scheduler being described. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.describe(
                ctx, inference_scheduler_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.describe inference_scheduler_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="describe_inference_scheduler",
        op_kwargs={"InferenceSchedulerName": inference_scheduler_name},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    inference_scheduler_name_begins_with: str = None,
    model_name: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of all inference schedulers currently available for your account.

    Args:
        next_token(str, optional):  An opaque pagination token indicating where to continue the listing of inference schedulers. . Defaults to None.
        max_results(int, optional):  Specifies the maximum number of inference schedulers to list. . Defaults to None.
        inference_scheduler_name_begins_with(str, optional): The beginning of the name of the inference schedulers to be listed. . Defaults to None.
        model_name(str, optional): The name of the ML model used by the inference scheduler to be listed. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="list_inference_schedulers",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "InferenceSchedulerNameBeginsWith": inference_scheduler_name_begins_with,
            "ModelName": model_name,
        },
    )


async def start(hub, ctx, inference_scheduler_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Starts an inference scheduler.

    Args:
        inference_scheduler_name(str): The name of the inference scheduler to be started. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.start(
                ctx, inference_scheduler_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.start inference_scheduler_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="start_inference_scheduler",
        op_kwargs={"InferenceSchedulerName": inference_scheduler_name},
    )


async def stop(hub, ctx, inference_scheduler_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Stops an inference scheduler.

    Args:
        inference_scheduler_name(str): The name of the inference scheduler to be stopped. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.stop(
                ctx, inference_scheduler_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.stop inference_scheduler_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="stop_inference_scheduler",
        op_kwargs={"InferenceSchedulerName": inference_scheduler_name},
    )


async def update(
    hub,
    ctx,
    inference_scheduler_name: str,
    data_delay_offset_in_minutes: int = None,
    data_upload_frequency: str = None,
    data_input_configuration: Dict = None,
    data_output_configuration: Dict = None,
    role_arn: str = None,
) -> None:
    r"""
    **Autogenerated function**

    Updates an inference scheduler.

    Args:
        inference_scheduler_name(str): The name of the inference scheduler to be updated. .
        data_delay_offset_in_minutes(int, optional): > A period of time (in minutes) by which inference on the data is delayed after the data starts. For instance, if you select an offset delay time of five minutes, inference will not begin on the data until the first data measurement after the five minute mark. For example, if five minutes is selected, the inference scheduler will wake up at the configured frequency with the additional five minute delay time to check the customer S3 bucket. The customer can upload data at the same frequency and they don't need to stop and restart the scheduler when uploading new data. Defaults to None.
        data_upload_frequency(str, optional): How often data is uploaded to the source S3 bucket for the input data. The value chosen is the length of time between data uploads. For instance, if you select 5 minutes, Amazon Lookout for Equipment will upload the real-time data to the source bucket once every 5 minutes. This frequency also determines how often Amazon Lookout for Equipment starts a scheduled inference on your data. In this example, it starts once every 5 minutes. . Defaults to None.
        data_input_configuration(Dict, optional):  Specifies information for the input data for the inference scheduler, including delimiter, format, and dataset location. . Defaults to None.
        data_output_configuration(Dict, optional):  Specifies information for the output results from the inference scheduler, including the output S3 location. . Defaults to None.
        role_arn(str, optional):  The Amazon Resource Name (ARN) of a role with permission to access the data source for the inference scheduler. . Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutequipment.inference.scheduler.update(
                ctx, inference_scheduler_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutequipment.inference.scheduler.update inference_scheduler_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutequipment",
        operation="update_inference_scheduler",
        op_kwargs={
            "InferenceSchedulerName": inference_scheduler_name,
            "DataDelayOffsetInMinutes": data_delay_offset_in_minutes,
            "DataUploadFrequency": data_upload_frequency,
            "DataInputConfiguration": data_input_configuration,
            "DataOutputConfiguration": data_output_configuration,
            "RoleArn": role_arn,
        },
    )
