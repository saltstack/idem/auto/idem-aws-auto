"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get_all(
    hub,
    ctx,
    service_code: str = None,
    filters: List = None,
    format_version: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of all products that match the filter criteria.

    Args:
        service_code(str, optional): The code for the service whose products you want to retrieve. . Defaults to None.
        filters(List, optional): The list of filters that limit the returned products. only products that match all filters are returned. Defaults to None.
        format_version(str, optional): The format version that you want the response to be in. Valid values are: aws_v1 . Defaults to None.
        next_token(str, optional): The pagination token that indicates the next set of results that you want to retrieve. Defaults to None.
        max_results(int, optional): The maximum number of results to return in the response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pricing.product.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pricing.product.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pricing",
        operation="get_products",
        op_kwargs={
            "ServiceCode": service_code,
            "Filters": filters,
            "FormatVersion": format_version,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
