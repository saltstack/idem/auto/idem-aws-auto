"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    license_counting_type: str,
    description: str = None,
    license_count: int = None,
    license_count_hard_limit: bool = None,
    license_rules: List = None,
    tags: List = None,
    disassociate_when_not_found: bool = None,
    product_information_list: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a license configuration. A license configuration is an abstraction of a customer license agreement that can be consumed and enforced by License Manager. Components include specifications for the license type (licensing by instance, socket, CPU, or vCPU), allowed tenancy (shared tenancy, Dedicated Instance, Dedicated Host, or all of these), license affinity to host (how long a license must be associated with a host), and the number of licenses purchased and used.

    Args:
        name(str): Name of the license configuration.
        description(str, optional): Description of the license configuration. Defaults to None.
        license_counting_type(str): Dimension used to track the license inventory.
        license_count(int, optional): Number of licenses managed by the license configuration. Defaults to None.
        license_count_hard_limit(bool, optional): Indicates whether hard or soft license enforcement is used. Exceeding a hard limit blocks the launch of new instances. Defaults to None.
        license_rules(List, optional): License rules. The syntax is #name=value (for example, #allowedTenancy=EC2-DedicatedHost). The available rules vary by dimension, as follows.    Cores dimension: allowedTenancy | licenseAffinityToHost | maximumCores | minimumCores     Instances dimension: allowedTenancy | maximumCores | minimumCores | maximumSockets | minimumSockets | maximumVcpus | minimumVcpus     Sockets dimension: allowedTenancy | licenseAffinityToHost | maximumSockets | minimumSockets     vCPUs dimension: allowedTenancy | honorVcpuOptimization | maximumVcpus | minimumVcpus    The unit for licenseAffinityToHost is days and the range is 1 to 180. The possible values for allowedTenancy are EC2-Default, EC2-DedicatedHost, and EC2-DedicatedInstance. The possible values for honorVcpuOptimization are True and False. Defaults to None.
        tags(List, optional): Tags to add to the license configuration. Defaults to None.
        disassociate_when_not_found(bool, optional): When true, disassociates a resource when software is uninstalled. Defaults to None.
        product_information_list(List, optional): Product information. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.license_manager.license_.configuration.create(
                ctx, name=value, license_counting_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.license_manager.license_.configuration.create name=value, license_counting_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="license-manager",
        operation="create_license_configuration",
        op_kwargs={
            "Name": name,
            "Description": description,
            "LicenseCountingType": license_counting_type,
            "LicenseCount": license_count,
            "LicenseCountHardLimit": license_count_hard_limit,
            "LicenseRules": license_rules,
            "Tags": tags,
            "DisassociateWhenNotFound": disassociate_when_not_found,
            "ProductInformationList": product_information_list,
        },
    )


async def delete(hub, ctx, license_configuration_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified license configuration. You cannot delete a license configuration that is in use.

    Args:
        license_configuration_arn(str): ID of the license configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.license_manager.license_.configuration.delete(
                ctx, license_configuration_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.license_manager.license_.configuration.delete license_configuration_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="license-manager",
        operation="delete_license_configuration",
        op_kwargs={"LicenseConfigurationArn": license_configuration_arn},
    )


async def get(hub, ctx, license_configuration_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets detailed information about the specified license configuration.

    Args:
        license_configuration_arn(str): Amazon Resource Name (ARN) of the license configuration.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.license_manager.license_.configuration.get(
                ctx, license_configuration_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.license_manager.license_.configuration.get license_configuration_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="license-manager",
        operation="get_license_configuration",
        op_kwargs={"LicenseConfigurationArn": license_configuration_arn},
    )


async def list_all(
    hub,
    ctx,
    license_configuration_arns: List = None,
    max_results: int = None,
    next_token: str = None,
    filters: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the license configurations for your account.

    Args:
        license_configuration_arns(List, optional): Amazon Resource Names (ARN) of the license configurations. Defaults to None.
        max_results(int, optional): Maximum number of results to return in a single call. Defaults to None.
        next_token(str, optional): Token for the next set of results. Defaults to None.
        filters(List, optional): Filters to scope the results. The following filters and logical operators are supported:    licenseCountingType - The dimension on which licenses are counted. Possible values are vCPU | Instance | Core | Socket. Logical operators are EQUALS | NOT_EQUALS.    enforceLicenseCount - A Boolean value that indicates whether hard license enforcement is used. Logical operators are EQUALS | NOT_EQUALS.    usagelimitExceeded - A Boolean value that indicates whether the available licenses have been exceeded. Logical operators are EQUALS | NOT_EQUALS.  . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.license_manager.license_.configuration.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.license_manager.license_.configuration.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="license-manager",
        operation="list_license_configurations",
        op_kwargs={
            "LicenseConfigurationArns": license_configuration_arns,
            "MaxResults": max_results,
            "NextToken": next_token,
            "Filters": filters,
        },
    )


async def update(
    hub,
    ctx,
    license_configuration_arn: str,
    license_configuration_status: str = None,
    license_rules: List = None,
    license_count: int = None,
    license_count_hard_limit: bool = None,
    name: str = None,
    description: str = None,
    product_information_list: List = None,
    disassociate_when_not_found: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the attributes of an existing license configuration.

    Args:
        license_configuration_arn(str): Amazon Resource Name (ARN) of the license configuration.
        license_configuration_status(str, optional): New status of the license configuration. Defaults to None.
        license_rules(List, optional): New license rule. The only rule that you can add after you create a license configuration is licenseAffinityToHost. Defaults to None.
        license_count(int, optional): New number of licenses managed by the license configuration. Defaults to None.
        license_count_hard_limit(bool, optional): New hard limit of the number of available licenses. Defaults to None.
        name(str, optional): New name of the license configuration. Defaults to None.
        description(str, optional): New description of the license configuration. Defaults to None.
        product_information_list(List, optional): New product information. Defaults to None.
        disassociate_when_not_found(bool, optional): When true, disassociates a resource when software is uninstalled. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.license_manager.license_.configuration.update(
                ctx, license_configuration_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.license_manager.license_.configuration.update license_configuration_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="license-manager",
        operation="update_license_configuration",
        op_kwargs={
            "LicenseConfigurationArn": license_configuration_arn,
            "LicenseConfigurationStatus": license_configuration_status,
            "LicenseRules": license_rules,
            "LicenseCount": license_count,
            "LicenseCountHardLimit": license_count_hard_limit,
            "Name": name,
            "Description": description,
            "ProductInformationList": product_information_list,
            "DisassociateWhenNotFound": disassociate_when_not_found,
        },
    )
