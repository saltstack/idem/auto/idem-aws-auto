"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    format_: str,
    content_range: Dict,
    schedule_name: str = None,
    s3_bucket_name: str = None,
    s3_key_prefix: str = None,
    recurrence: Dict = None,
    client_request_token: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a recurring schedule for usage reports to deliver to the specified S3 location with a specified daily or weekly interval.

    Args:
        schedule_name(str, optional): The name identifier of the schedule. Defaults to None.
        s3_bucket_name(str, optional): The S3 bucket name of the output reports. If this isn't specified, the report can be retrieved from a download link by calling ListBusinessReportSchedule. . Defaults to None.
        s3_key_prefix(str, optional): The S3 key where the report is delivered. Defaults to None.
        format_(str): The format of the generated report (individual CSV files or zipped files of individual files).
        content_range(Dict): The content range of the reports.
        recurrence(Dict, optional): The recurrence of the reports. If this isn't specified, the report will only be delivered one time when the API is called. . Defaults to None.
        client_request_token(str, optional): The client request token. Defaults to None.
        tags(List, optional): The tags for the business report schedule. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.business_report_schedule.create(
                ctx, format_=value, content_range=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.business_report_schedule.create format_=value, content_range=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="create_business_report_schedule",
        op_kwargs={
            "ScheduleName": schedule_name,
            "S3BucketName": s3_bucket_name,
            "S3KeyPrefix": s3_key_prefix,
            "Format": format_,
            "ContentRange": content_range,
            "Recurrence": recurrence,
            "ClientRequestToken": client_request_token,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, schedule_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the recurring report delivery schedule with the specified schedule ARN.

    Args:
        schedule_arn(str): The ARN of the business report schedule.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.business_report_schedule.delete(
                ctx, schedule_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.business_report_schedule.delete schedule_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="delete_business_report_schedule",
        op_kwargs={"ScheduleArn": schedule_arn},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the details of the schedules that a user configured. A download URL of the report associated with each schedule is returned every time this action is called. A new download URL is returned each time, and is valid for 24 hours.

    Args:
        next_token(str, optional): The token used to list the remaining schedules from the previous API call. Defaults to None.
        max_results(int, optional): The maximum number of schedules listed in the call. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.business_report_schedule.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.business_report_schedule.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="list_business_report_schedules",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )


async def update(
    hub,
    ctx,
    schedule_arn: str,
    s3_bucket_name: str = None,
    s3_key_prefix: str = None,
    format_: str = None,
    schedule_name: str = None,
    recurrence: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the configuration of the report delivery schedule with the specified schedule ARN.

    Args:
        schedule_arn(str): The ARN of the business report schedule.
        s3_bucket_name(str, optional): The S3 location of the output reports. Defaults to None.
        s3_key_prefix(str, optional): The S3 key where the report is delivered. Defaults to None.
        format_(str, optional): The format of the generated report (individual CSV files or zipped files of individual files). Defaults to None.
        schedule_name(str, optional): The name identifier of the schedule. Defaults to None.
        recurrence(Dict, optional): The recurrence of the reports. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.business_report_schedule.update(
                ctx, schedule_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.business_report_schedule.update schedule_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="update_business_report_schedule",
        op_kwargs={
            "ScheduleArn": schedule_arn,
            "S3BucketName": s3_bucket_name,
            "S3KeyPrefix": s3_key_prefix,
            "Format": format_,
            "ScheduleName": schedule_name,
            "Recurrence": recurrence,
        },
    )
