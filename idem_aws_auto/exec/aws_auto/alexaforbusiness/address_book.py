"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    name: str,
    description: str = None,
    client_request_token: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an address book with the specified details.

    Args:
        name(str): The name of the address book.
        description(str, optional): The description of the address book. Defaults to None.
        client_request_token(str, optional): A unique, user-specified identifier for the request that ensures idempotency. Defaults to None.
        tags(List, optional): The tags to be added to the specified resource. Do not provide system tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.address_book.create(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.address_book.create name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="create_address_book",
        op_kwargs={
            "Name": name,
            "Description": description,
            "ClientRequestToken": client_request_token,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, address_book_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an address book by the address book ARN.

    Args:
        address_book_arn(str): The ARN of the address book to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.address_book.delete(
                ctx, address_book_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.address_book.delete address_book_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="delete_address_book",
        op_kwargs={"AddressBookArn": address_book_arn},
    )


async def get(hub, ctx, address_book_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets address the book details by the address book ARN.

    Args:
        address_book_arn(str): The ARN of the address book for which to request details.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.address_book.get(ctx, address_book_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.address_book.get address_book_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="get_address_book",
        op_kwargs={"AddressBookArn": address_book_arn},
    )


async def search(
    hub,
    ctx,
    filters: List = None,
    sort_criteria: List = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Searches address books and lists the ones that meet a set of filter and sort criteria.

    Args:
        filters(List, optional): The filters to use to list a specified set of address books. The supported filter key is AddressBookName. Defaults to None.
        sort_criteria(List, optional): The sort order to use in listing the specified set of address books. The supported sort key is AddressBookName. Defaults to None.
        next_token(str, optional): An optional token returned from a prior request. Use this token for pagination of results from this action. If this parameter is specified, the response only includes results beyond the token, up to the value specified by MaxResults. Defaults to None.
        max_results(int, optional): The maximum number of results to include in the response. If more results exist than the specified MaxResults value, a token is included in the response so that the remaining results can be retrieved. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.address_book.search(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.address_book.search
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="search_address_books",
        op_kwargs={
            "Filters": filters,
            "SortCriteria": sort_criteria,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )


async def update(
    hub, ctx, address_book_arn: str, name: str = None, description: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates address book details by the address book ARN.

    Args:
        address_book_arn(str): The ARN of the room to update.
        name(str, optional): The updated name of the room. Defaults to None.
        description(str, optional): The updated description of the room. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.address_book.update(
                ctx, address_book_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.address_book.update address_book_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="update_address_book",
        op_kwargs={
            "AddressBookArn": address_book_arn,
            "Name": name,
            "Description": description,
        },
    )
