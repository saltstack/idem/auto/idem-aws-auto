"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def start(
    hub, ctx, features: List, room_arn: str = None, device_arn: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Resets a device and its account to the known default settings. This clears all information and settings set by previous users in the following ways:   Bluetooth - This unpairs all bluetooth devices paired with your echo device.   Volume - This resets the echo device's volume to the default value.   Notifications - This clears all notifications from your echo device.   Lists - This clears all to-do items from your echo device.   Settings - This internally syncs the room's profile (if the device is assigned to a room), contacts, address books, delegation access for account linking, and communications (if enabled on the room profile).

    Args:
        room_arn(str, optional): The ARN of the room with which the device to sync is associated. Required. Defaults to None.
        device_arn(str, optional): The ARN of the device to sync. Required. Defaults to None.
        features(List): Request structure to start the device sync. Required.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.device.sync.start(ctx, features=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.device.sync.start features=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="start_device_sync",
        op_kwargs={"RoomArn": room_arn, "DeviceArn": device_arn, "Features": features},
    )
