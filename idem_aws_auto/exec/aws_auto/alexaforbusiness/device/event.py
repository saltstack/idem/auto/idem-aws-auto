"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    device_arn: str,
    event_type: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the device event history, including device connection status, for up to 30 days.

    Args:
        device_arn(str): The ARN of a device.
        event_type(str, optional): The event type to filter device events. If EventType isn't specified, this returns a list of all device events in reverse chronological order. If EventType is specified, this returns a list of device events for that EventType in reverse chronological order. . Defaults to None.
        next_token(str, optional): An optional token returned from a prior request. Use this token for pagination of results from this action. If this parameter is specified, the response only includes results beyond the token, up to the value specified by MaxResults. When the end of results is reached, the response has a value of null. Defaults to None.
        max_results(int, optional): The maximum number of results to include in the response. The default value is 50. If more results exist than the specified MaxResults value, a token is included in the response so that the remaining results can be retrieved. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.device.event.list_all(ctx, device_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.device.event.list_all device_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="list_device_events",
        op_kwargs={
            "DeviceArn": device_arn,
            "EventType": event_type,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
