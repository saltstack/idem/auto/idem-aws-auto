"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, arn: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all tags for the specified resource.

    Args:
        arn(str): The ARN of the specified resource for which to list tags.
        next_token(str, optional): An optional token returned from a prior request. Use this token for pagination of results from this action. If this parameter is specified, the response includes only results beyond the token, up to the value specified by MaxResults. . Defaults to None.
        max_results(int, optional): The maximum number of results to include in the response. If more results exist than the specified MaxResults value, a token is included in the response so that the remaining results can be retrieved. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.tags.list(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.tags.list arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="list_tags",
        op_kwargs={"Arn": arn, "NextToken": next_token, "MaxResults": max_results},
    )
