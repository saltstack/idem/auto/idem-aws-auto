"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    skill_group_name: str,
    description: str = None,
    client_request_token: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a skill group with a specified name and description.

    Args:
        skill_group_name(str): The name for the skill group.
        description(str, optional): The description for the skill group. Defaults to None.
        client_request_token(str, optional): A unique, user-specified identifier for this request that ensures idempotency. . Defaults to None.
        tags(List, optional): The tags for the skill group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.skill.group.init.create(
                ctx, skill_group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.skill.group.init.create skill_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="create_skill_group",
        op_kwargs={
            "SkillGroupName": skill_group_name,
            "Description": description,
            "ClientRequestToken": client_request_token,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, skill_group_arn: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a skill group by skill group ARN.

    Args:
        skill_group_arn(str, optional): The ARN of the skill group to delete. Required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.skill.group.init.delete(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.skill.group.init.delete
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="delete_skill_group",
        op_kwargs={"SkillGroupArn": skill_group_arn},
    )


async def get(hub, ctx, skill_group_arn: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets skill group details by skill group ARN.

    Args:
        skill_group_arn(str, optional): The ARN of the skill group for which to get details. Required. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.skill.group.init.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.skill.group.init.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="get_skill_group",
        op_kwargs={"SkillGroupArn": skill_group_arn},
    )


async def search(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    filters: List = None,
    sort_criteria: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Searches skill groups and lists the ones that meet a set of filter and sort criteria.

    Args:
        next_token(str, optional): An optional token returned from a prior request. Use this token for pagination of results from this action. If this parameter is specified, the response includes only results beyond the token, up to the value specified by MaxResults. Required. Defaults to None.
        max_results(int, optional): The maximum number of results to include in the response. If more results exist than the specified MaxResults value, a token is included in the response so that the remaining results can be retrieved. . Defaults to None.
        filters(List, optional): The filters to use to list a specified set of skill groups. The supported filter key is SkillGroupName. . Defaults to None.
        sort_criteria(List, optional): The sort order to use in listing the specified set of skill groups. The supported sort key is SkillGroupName. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.skill.group.init.search(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.skill.group.init.search
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="search_skill_groups",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "Filters": filters,
            "SortCriteria": sort_criteria,
        },
    )


async def update(
    hub,
    ctx,
    skill_group_arn: str = None,
    skill_group_name: str = None,
    description: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates skill group details by skill group ARN.

    Args:
        skill_group_arn(str, optional): The ARN of the skill group to update. . Defaults to None.
        skill_group_name(str, optional): The updated name for the skill group. Defaults to None.
        description(str, optional): The updated description for the skill group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.alexaforbusiness.skill.group.init.update(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.alexaforbusiness.skill.group.init.update
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="alexaforbusiness",
        operation="update_skill_group",
        op_kwargs={
            "SkillGroupArn": skill_group_arn,
            "SkillGroupName": skill_group_name,
            "Description": description,
        },
    )
