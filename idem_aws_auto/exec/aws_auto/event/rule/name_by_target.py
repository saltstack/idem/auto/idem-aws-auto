"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    target_arn: str,
    event_bus_name: str = None,
    next_token: str = None,
    limit: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the rules for the specified target. You can see which of the rules in Amazon EventBridge can invoke a specific target in your account.

    Args:
        target_arn(str): The Amazon Resource Name (ARN) of the target resource.
        event_bus_name(str, optional): The name or ARN of the event bus to list rules for. If you omit this, the default event bus is used. Defaults to None.
        next_token(str, optional): The token returned by a previous call to retrieve the next set of results. Defaults to None.
        limit(int, optional): The maximum number of results to return. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.rule.name_by_target.list(ctx, target_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.rule.name_by_target.list target_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="list_rule_names_by_target",
        op_kwargs={
            "TargetArn": target_arn,
            "EventBusName": event_bus_name,
            "NextToken": next_token,
            "Limit": limit,
        },
    )
