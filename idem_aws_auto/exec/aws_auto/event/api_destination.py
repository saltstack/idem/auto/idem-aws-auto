"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    name: str,
    connection_arn: str,
    invocation_endpoint: str,
    http_method: str,
    description: str = None,
    invocation_rate_limit_per_second: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an API destination, which is an HTTP invocation endpoint configured as a target for events.

    Args:
        name(str): The name for the API destination to create.
        description(str, optional): A description for the API destination to create. Defaults to None.
        connection_arn(str): The ARN of the connection to use for the API destination. The destination endpoint must support the authorization type specified for the connection.
        invocation_endpoint(str): The URL to the HTTP invocation endpoint for the API destination.
        http_method(str): The method to use for the request to the HTTP invocation endpoint.
        invocation_rate_limit_per_second(int, optional): The maximum number of requests per second to send to the HTTP invocation endpoint. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.api_destination.create(
                ctx, name=value, connection_arn=value, invocation_endpoint=value, http_method=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.api_destination.create name=value, connection_arn=value, invocation_endpoint=value, http_method=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="create_api_destination",
        op_kwargs={
            "Name": name,
            "Description": description,
            "ConnectionArn": connection_arn,
            "InvocationEndpoint": invocation_endpoint,
            "HttpMethod": http_method,
            "InvocationRateLimitPerSecond": invocation_rate_limit_per_second,
        },
    )


async def delete(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified API destination.

    Args:
        name(str): The name of the destination to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.api_destination.delete(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.api_destination.delete name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="delete_api_destination",
        op_kwargs={"Name": name},
    )


async def describe(hub, ctx, name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves details about an API destination.

    Args:
        name(str): The name of the API destination to retrieve.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.api_destination.describe(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.api_destination.describe name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="describe_api_destination",
        op_kwargs={"Name": name},
    )


async def list_all(
    hub,
    ctx,
    name_prefix: str = None,
    connection_arn: str = None,
    next_token: str = None,
    limit: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of API destination in the account in the current Region.

    Args:
        name_prefix(str, optional): A name prefix to filter results returned. Only API destinations with a name that starts with the prefix are returned. Defaults to None.
        connection_arn(str, optional): The ARN of the connection specified for the API destination. Defaults to None.
        next_token(str, optional): The token returned by a previous call to retrieve the next set of results. Defaults to None.
        limit(int, optional): The maximum number of API destinations to include in the response. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.api_destination.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.api_destination.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="list_api_destinations",
        op_kwargs={
            "NamePrefix": name_prefix,
            "ConnectionArn": connection_arn,
            "NextToken": next_token,
            "Limit": limit,
        },
    )


async def update(
    hub,
    ctx,
    name: str,
    description: str = None,
    connection_arn: str = None,
    invocation_endpoint: str = None,
    http_method: str = None,
    invocation_rate_limit_per_second: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates an API destination.

    Args:
        name(str): The name of the API destination to update.
        description(str, optional): The name of the API destination to update. Defaults to None.
        connection_arn(str, optional): The ARN of the connection to use for the API destination. Defaults to None.
        invocation_endpoint(str, optional): The URL to the endpoint to use for the API destination. Defaults to None.
        http_method(str, optional): The method to use for the API destination. Defaults to None.
        invocation_rate_limit_per_second(int, optional): The maximum number of invocations per second to send to the API destination. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.api_destination.update(ctx, name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.api_destination.update name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="update_api_destination",
        op_kwargs={
            "Name": name,
            "Description": description,
            "ConnectionArn": connection_arn,
            "InvocationEndpoint": invocation_endpoint,
            "HttpMethod": http_method,
            "InvocationRateLimitPerSecond": invocation_rate_limit_per_second,
        },
    )
