"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Assigns one or more tags (key-value pairs) to the specified EventBridge resource. Tags can help you organize and categorize your resources. You can also use them to scope user permissions by granting a user permission to access or change only resources with certain tag values. In EventBridge, rules and event buses can be tagged. Tags don't have any semantic meaning to AWS and are interpreted strictly as strings of characters. You can use the TagResource action with a resource that already has tags. If you specify a new tag key, this tag is appended to the list of tags associated with the resource. If you specify a tag key that is already associated with the resource, the new tag value that you specify replaces the previous value for that tag. You can associate as many as 50 tags with a resource.

    Args:
        resource_arn(str): The ARN of the EventBridge resource that you're adding tags to.
        tags(List): The list of key-value pairs to associate with the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="tag_resource",
        op_kwargs={"ResourceARN": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes one or more tags from the specified EventBridge resource. In Amazon EventBridge (CloudWatch Events), rules and event buses can be tagged.

    Args:
        resource_arn(str): The ARN of the EventBridge resource from which you are removing tags.
        tag_keys(List): The list of tag keys to remove from the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.event.resource.untag(ctx, resource_arn=value, tag_keys=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.event.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="events",
        operation="untag_resource",
        op_kwargs={"ResourceARN": resource_arn, "TagKeys": tag_keys},
    )
