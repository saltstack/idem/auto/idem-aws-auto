"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub, ctx, suite_definition_configuration: Dict = None, tags: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Device Advisor test suite.

    Args:
        suite_definition_configuration(Dict, optional): Creates a Device Advisor test suite with suite definition configuration. Defaults to None.
        tags(Dict, optional): The tags to be attached to the suite definition. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotdeviceadvisor.suite.definition.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotdeviceadvisor.suite.definition.create
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotdeviceadvisor",
        operation="create_suite_definition",
        op_kwargs={
            "suiteDefinitionConfiguration": suite_definition_configuration,
            "tags": tags,
        },
    )


async def delete(hub, ctx, suite_definition_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a Device Advisor test suite.

    Args:
        suite_definition_id(str): Suite definition Id of the test suite to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotdeviceadvisor.suite.definition.delete(
                ctx, suite_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotdeviceadvisor.suite.definition.delete suite_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotdeviceadvisor",
        operation="delete_suite_definition",
        op_kwargs={"suiteDefinitionId": suite_definition_id},
    )


async def get(
    hub, ctx, suite_definition_id: str, suite_definition_version: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a Device Advisor test suite.

    Args:
        suite_definition_id(str): Suite definition Id of the test suite to get.
        suite_definition_version(str, optional): Suite definition version of the test suite to get. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotdeviceadvisor.suite.definition.get(
                ctx, suite_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotdeviceadvisor.suite.definition.get suite_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotdeviceadvisor",
        operation="get_suite_definition",
        op_kwargs={
            "suiteDefinitionId": suite_definition_id,
            "suiteDefinitionVersion": suite_definition_version,
        },
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the Device Advisor test suites you have created.

    Args:
        max_results(int, optional): The maximum number of results to return at once. Defaults to None.
        next_token(str, optional): A token used to get the next set of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotdeviceadvisor.suite.definition.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotdeviceadvisor.suite.definition.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotdeviceadvisor",
        operation="list_suite_definitions",
        op_kwargs={"maxResults": max_results, "nextToken": next_token},
    )


async def update(
    hub, ctx, suite_definition_id: str, suite_definition_configuration: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates a Device Advisor test suite.

    Args:
        suite_definition_id(str): Suite definition Id of the test suite to be updated.
        suite_definition_configuration(Dict, optional): Updates a Device Advisor test suite with suite definition configuration. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotdeviceadvisor.suite.definition.update(
                ctx, suite_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotdeviceadvisor.suite.definition.update suite_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotdeviceadvisor",
        operation="update_suite_definition",
        op_kwargs={
            "suiteDefinitionId": suite_definition_id,
            "suiteDefinitionConfiguration": suite_definition_configuration,
        },
    )
