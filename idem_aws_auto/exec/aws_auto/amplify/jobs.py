"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    app_id: str,
    branch_name: str,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Lists the jobs for a branch of an Amplify app.

    Args:
        app_id(str):  The unique ID for an Amplify app. .
        branch_name(str):  The name for a branch. .
        next_token(str, optional):  A pagination token. Set to null to start listing steps from the start. If a non-null pagination token is returned in a result, pass its value in here to list more steps. . Defaults to None.
        max_results(int, optional):  The maximum number of records to list in a single response. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplify.jobs.list(ctx, app_id=value, branch_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplify.jobs.list app_id=value, branch_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplify",
        operation="list_jobs",
        op_kwargs={
            "appId": app_id,
            "branchName": branch_name,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
