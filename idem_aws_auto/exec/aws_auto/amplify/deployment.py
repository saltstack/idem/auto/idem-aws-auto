"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub, ctx, app_id: str, branch_name: str, file_map: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a deployment for a manually deployed Amplify app. Manually deployed apps are not connected to a repository.

    Args:
        app_id(str):  The unique ID for an Amplify app. .
        branch_name(str):  The name for the branch, for the job. .
        file_map(Dict, optional):  An optional file map that contains the file name as the key and the file content md5 hash as the value. If this argument is provided, the service will generate a unique upload URL per file. Otherwise, the service will only generate a single upload URL for the zipped files. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplify.deployment.create(ctx, app_id=value, branch_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplify.deployment.create app_id=value, branch_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplify",
        operation="create_deployment",
        op_kwargs={"appId": app_id, "branchName": branch_name, "fileMap": file_map},
    )


async def start(
    hub, ctx, app_id: str, branch_name: str, job_id: str = None, source_url: str = None
) -> Dict:
    r"""
    **Autogenerated function**

     Starts a deployment for a manually deployed app. Manually deployed apps are not connected to a repository.

    Args:
        app_id(str):  The unique ID for an Amplify app. .
        branch_name(str):  The name for the branch, for the job. .
        job_id(str, optional):  The job ID for this deployment, generated by the create deployment request. . Defaults to None.
        source_url(str, optional):  The source URL for this deployment, used when calling start deployment without create deployment. The source URL can be any HTTP GET URL that is publicly accessible and downloads a single .zip file. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amplify.deployment.start(ctx, app_id=value, branch_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amplify.deployment.start app_id=value, branch_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amplify",
        operation="start_deployment",
        op_kwargs={
            "appId": app_id,
            "branchName": branch_name,
            "jobId": job_id,
            "sourceUrl": source_url,
        },
    )
