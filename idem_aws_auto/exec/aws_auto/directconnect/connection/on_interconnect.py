"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def allocate(
    hub,
    ctx,
    bandwidth: str,
    connection_name: str,
    owner_account: str,
    interconnect_id: str,
    vlan: int,
) -> Dict:
    r"""
    **Autogenerated function**

    Deprecated. Use AllocateHostedConnection instead. Creates a hosted connection on an interconnect. Allocates a VLAN number and a specified amount of bandwidth for use by a hosted connection on the specified interconnect.  Intended for use by Direct Connect Partners only.

    Args:
        bandwidth(str): The bandwidth of the connection. The possible values are 50Mbps, 100Mbps, 200Mbps, 300Mbps, 400Mbps, 500Mbps, 1Gbps, 2Gbps, 5Gbps, and 10Gbps. Note that only those Direct Connect Partners who have met specific requirements are allowed to create a 1Gbps, 2Gbps, 5Gbps or 10Gbps hosted connection.
        connection_name(str): The name of the provisioned connection.
        owner_account(str): The ID of the account of the customer for whom the connection will be provisioned.
        interconnect_id(str): The ID of the interconnect on which the connection will be provisioned.
        vlan(int): The dedicated VLAN provisioned to the connection.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.directconnect.connection.on_interconnect.allocate(
                ctx,
                bandwidth=value,
                connection_name=value,
                owner_account=value,
                interconnect_id=value,
                vlan=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.directconnect.connection.on_interconnect.allocate bandwidth=value, connection_name=value, owner_account=value, interconnect_id=value, vlan=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="directconnect",
        operation="allocate_connection_on_interconnect",
        op_kwargs={
            "bandwidth": bandwidth,
            "connectionName": connection_name,
            "ownerAccount": owner_account,
            "interconnectId": interconnect_id,
            "vlan": vlan,
        },
    )


async def describe(hub, ctx, interconnect_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deprecated. Use DescribeHostedConnections instead. Lists the connections that have been provisioned on the specified interconnect.  Intended for use by Direct Connect Partners only.

    Args:
        interconnect_id(str): The ID of the interconnect.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.directconnect.connection.on_interconnect.describe(
                ctx, interconnect_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.directconnect.connection.on_interconnect.describe interconnect_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="directconnect",
        operation="describe_connections_on_interconnect",
        op_kwargs={"interconnectId": interconnect_id},
    )
