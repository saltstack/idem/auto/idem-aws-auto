"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Lists the virtual private gateways owned by the account. You can create one or more Direct Connect private virtual interfaces linked to a virtual private gateway.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.directconnect.virtual.gateway.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.directconnect.virtual.gateway.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="directconnect",
        operation="describe_virtual_gateways",
        op_kwargs={},
    )
