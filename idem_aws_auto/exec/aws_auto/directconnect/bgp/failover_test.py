"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def start(
    hub,
    ctx,
    virtual_interface_id: str,
    bgp_peers: List = None,
    test_duration_in_minutes: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Starts the virtual interface failover test that verifies your configuration meets your resiliency requirements by placing the BGP peering session in the DOWN state. You can then send traffic to verify that there are no outages. You can run the test on public, private, transit, and hosted virtual interfaces. You can use ListVirtualInterfaceTestHistory to view the virtual interface test history. If you need to stop the test before the test interval completes, use StopBgpFailoverTest.

    Args:
        virtual_interface_id(str): The ID of the virtual interface you want to test.
        bgp_peers(List, optional): The BGP peers to place in the DOWN state. Defaults to None.
        test_duration_in_minutes(int, optional): The time in minutes that the virtual interface failover test will last. Maximum value: 180 minutes (3 hours). Default: 180 minutes (3 hours). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.directconnect.bgp.failover_test.start(
                ctx, virtual_interface_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.directconnect.bgp.failover_test.start virtual_interface_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="directconnect",
        operation="start_bgp_failover_test",
        op_kwargs={
            "virtualInterfaceId": virtual_interface_id,
            "bgpPeers": bgp_peers,
            "testDurationInMinutes": test_duration_in_minutes,
        },
    )


async def stop(hub, ctx, virtual_interface_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Stops the virtual interface failover test.

    Args:
        virtual_interface_id(str): The ID of the virtual interface you no longer want to test.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.directconnect.bgp.failover_test.stop(
                ctx, virtual_interface_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.directconnect.bgp.failover_test.stop virtual_interface_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="directconnect",
        operation="stop_bgp_failover_test",
        op_kwargs={"virtualInterfaceId": virtual_interface_id},
    )
