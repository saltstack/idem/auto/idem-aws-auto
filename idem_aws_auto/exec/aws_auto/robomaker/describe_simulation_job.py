"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, jobs: List) -> Dict:
    r"""
    **Autogenerated function**

    Describes one or more simulation jobs.

    Args:
        jobs(List): A list of Amazon Resource Names (ARNs) of simulation jobs to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.robomaker.describe_simulation_job.batch(ctx, jobs=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.robomaker.describe_simulation_job.batch jobs=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="robomaker",
        operation="batch_describe_simulation_job",
        op_kwargs={"jobs": jobs},
    )
