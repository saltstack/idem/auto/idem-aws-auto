"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update(
    hub,
    ctx,
    deployment_type: str,
    service_name: str,
    spec: str,
    template_major_version: str = None,
    template_minor_version: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Update the service pipeline. There are four modes for updating a service pipeline as described in the following. The deploymentType field defines the mode.     NONE  In this mode, a deployment doesn't occur. Only the requested metadata parameters are updated.     CURRENT_VERSION  In this mode, the service pipeline is deployed and updated with the new spec that you provide. Only requested parameters are updated. Don’t include minor or major version parameters when you use this deployment-type.     MINOR_VERSION  In this mode, the service pipeline is deployed and updated with the published, recommended (latest) minor version of the current major version in use, by default. You can also specify a different minor version of the current major version in use.     MAJOR_VERSION  In this mode, the service pipeline is deployed and updated with the published, recommended (latest) major and minor version of the current template by default. You can also specify a different major version that is higher than the major version in use and a minor version (optional).

    Args:
        deployment_type(str): The deployment type. There are four modes for updating a service pipeline as described in the following. The deploymentType field defines the mode.     NONE  In this mode, a deployment doesn't occur. Only the requested metadata parameters are updated.     CURRENT_VERSION  In this mode, the service pipeline is deployed and updated with the new spec that you provide. Only requested parameters are updated. Don’t include minor or major version parameters when you use this deployment-type.     MINOR_VERSION  In this mode, the service pipeline is deployed and updated with the published, recommended (latest) minor version of the current major version in use, by default. You can also specify a different minor version of the current major version in use.     MAJOR_VERSION  In this mode, the service pipeline is deployed and updated with the published, recommended (latest) major and minor version of the current template, by default. You can also specify a different major version that is higher than the major version in use and a minor version (optional).  .
        service_name(str): The name of the service to that the pipeline is associated with.
        spec(str): The spec for the service pipeline to update.
        template_major_version(str, optional): The major version of the service template that was used to create the service that the pipeline is associated with. Defaults to None.
        template_minor_version(str, optional): The minor version of the service template that was used to create the service that the pipeline is associated with. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.proton.service.pipeline.init.update(
                ctx, deployment_type=value, service_name=value, spec=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.proton.service.pipeline.init.update deployment_type=value, service_name=value, spec=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="proton",
        operation="update_service_pipeline",
        op_kwargs={
            "deploymentType": deployment_type,
            "serviceName": service_name,
            "spec": spec,
            "templateMajorVersion": template_major_version,
            "templateMinorVersion": template_minor_version,
        },
    )
