"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, arn: str, tags: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Add tags to a MediaConvert queue, preset, or job template. For information about tagging, see the User Guide at https://docs.aws.amazon.com/mediaconvert/latest/ug/tagging-resources.html

    Args:
        arn(str): The Amazon Resource Name (ARN) of the resource that you want to tag. To get the ARN, send a GET request with the resource name.
        tags(Dict): The tags that you want to add to the resource. You can tag resources with a key-value pair or with only a key.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediaconvert.resource.tag(ctx, arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediaconvert.resource.tag arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediaconvert",
        operation="tag_resource",
        op_kwargs={"Arn": arn, "Tags": tags},
    )


async def untag(hub, ctx, arn: str, tag_keys: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Remove tags from a MediaConvert queue, preset, or job template. For information about tagging, see the User Guide at https://docs.aws.amazon.com/mediaconvert/latest/ug/tagging-resources.html

    Args:
        arn(str): The Amazon Resource Name (ARN) of the resource that you want to remove tags from. To get the ARN, send a GET request with the resource name.
        tag_keys(List, optional): The keys of the tags that you want to remove from the resource. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediaconvert.resource.untag(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediaconvert.resource.untag arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediaconvert",
        operation="untag_resource",
        op_kwargs={"Arn": arn, "TagKeys": tag_keys},
    )
