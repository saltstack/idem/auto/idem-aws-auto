"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__func_alias__ = {"set_": "set"}


async def describe(hub, ctx, instance_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Describes time-based auto scaling configurations for specified instances.  You must specify at least one of the parameters.   Required Permissions: To use this action, an IAM user must have a Show, Deploy, or Manage permissions level for the stack, or an attached policy that explicitly grants permissions. For more information about user permissions, see Managing User Permissions.

    Args:
        instance_ids(List): An array of instance IDs.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.opswork.time_based_auto_scaling.describe(
                ctx, instance_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.opswork.time_based_auto_scaling.describe instance_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="opsworks",
        operation="describe_time_based_auto_scaling",
        op_kwargs={"InstanceIds": instance_ids},
    )


async def set_(hub, ctx, instance_id: str, auto_scaling_schedule: Dict = None) -> None:
    r"""
    **Autogenerated function**

    Specify the time-based auto scaling configuration for a specified instance. For more information, see Managing Load with Time-based and Load-based Instances.  Required Permissions: To use this action, an IAM user must have a Manage permissions level for the stack, or an attached policy that explicitly grants permissions. For more information on user permissions, see Managing User Permissions.

    Args:
        instance_id(str): The instance ID.
        auto_scaling_schedule(Dict, optional): An AutoScalingSchedule with the instance schedule. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.opswork.time_based_auto_scaling.set(ctx, instance_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.opswork.time_based_auto_scaling.set instance_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="opsworks",
        operation="set_time_based_auto_scaling",
        op_kwargs={
            "InstanceId": instance_id,
            "AutoScalingSchedule": auto_scaling_schedule,
        },
    )
