"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"set_": "set"}


async def describe_all(
    hub, ctx, iam_user_arn: str = None, stack_id: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the permissions for a specified stack.  Required Permissions: To use this action, an IAM user must have a Manage permissions level for the stack, or an attached policy that explicitly grants permissions. For more information on user permissions, see Managing User Permissions.

    Args:
        iam_user_arn(str, optional): The user's IAM ARN. This can also be a federated user's ARN. For more information about IAM ARNs, see Using Identifiers. Defaults to None.
        stack_id(str, optional): The stack ID. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.opswork.permission.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.opswork.permission.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="opsworks",
        operation="describe_permissions",
        op_kwargs={"IamUserArn": iam_user_arn, "StackId": stack_id},
    )


async def set_(
    hub,
    ctx,
    stack_id: str,
    iam_user_arn: str,
    allow_ssh: bool = None,
    allow_sudo: bool = None,
    level: str = None,
) -> None:
    r"""
    **Autogenerated function**

    Specifies a user's permissions. For more information, see Security and Permissions.  Required Permissions: To use this action, an IAM user must have a Manage permissions level for the stack, or an attached policy that explicitly grants permissions. For more information on user permissions, see Managing User Permissions.

    Args:
        stack_id(str): The stack ID.
        iam_user_arn(str): The user's IAM ARN. This can also be a federated user's ARN.
        allow_ssh(bool, optional): The user is allowed to use SSH to communicate with the instance. Defaults to None.
        allow_sudo(bool, optional): The user is allowed to use sudo to elevate privileges. Defaults to None.
        level(str, optional): The user's permission level, which must be set to one of the following strings. You cannot set your own permissions level.    deny     show     deploy     manage     iam_only    For more information about the permissions associated with these levels, see Managing User Permissions. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.opswork.permission.set(ctx, stack_id=value, iam_user_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.opswork.permission.set stack_id=value, iam_user_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="opsworks",
        operation="set_permission",
        op_kwargs={
            "StackId": stack_id,
            "IamUserArn": iam_user_arn,
            "AllowSsh": allow_ssh,
            "AllowSudo": allow_sudo,
            "Level": level,
        },
    )
