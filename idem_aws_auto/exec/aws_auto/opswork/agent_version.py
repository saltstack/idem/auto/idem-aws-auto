"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(
    hub, ctx, stack_id: str = None, configuration_manager: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the available AWS OpsWorks Stacks agent versions. You must specify a stack ID or a configuration manager. DescribeAgentVersions returns a list of available agent versions for the specified stack or configuration manager.

    Args:
        stack_id(str, optional): The stack ID. Defaults to None.
        configuration_manager(Dict, optional): The configuration manager. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.opswork.agent_version.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.opswork.agent_version.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="opsworks",
        operation="describe_agent_versions",
        op_kwargs={"StackId": stack_id, "ConfigurationManager": configuration_manager},
    )
