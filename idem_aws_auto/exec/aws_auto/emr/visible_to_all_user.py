"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import List


__func_alias__ = {"set_": "set"}


async def set_(hub, ctx, job_flow_ids: List, visible_to_all_users: bool) -> None:
    r"""
    **Autogenerated function**

    Sets the Cluster$VisibleToAllUsers value for an EMR cluster. When true, IAM principals in the account can perform EMR cluster actions that their IAM policies allow. When false, only the IAM principal that created the cluster and the account root user can perform EMR actions on the cluster, regardless of IAM permissions policies attached to other IAM principals. This action works on running clusters. When you create a cluster, use the RunJobFlowInput$VisibleToAllUsers parameter. For more information, see Understanding the EMR Cluster VisibleToAllUsers Setting in the Amazon EMR Management Guide.

    Args:
        job_flow_ids(List): The unique identifier of the job flow (cluster).
        visible_to_all_users(bool): A value of true indicates that an IAM principal in the account can perform EMR actions on the cluster that the IAM policies attached to the principal allow. A value of false indicates that only the IAM principal that created the cluster and the Amazon Web Services root user can perform EMR actions on the cluster.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.emr.visible_to_all_user.set(
                ctx, job_flow_ids=value, visible_to_all_users=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.emr.visible_to_all_user.set job_flow_ids=value, visible_to_all_users=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="emr",
        operation="set_visible_to_all_users",
        op_kwargs={
            "JobFlowIds": job_flow_ids,
            "VisibleToAllUsers": visible_to_all_users,
        },
    )
