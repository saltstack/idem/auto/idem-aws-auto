"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Returns the Amazon EMR block public access configuration for your account in the current Region. For more information see Configure Block Public Access for Amazon EMR in the Amazon EMR Management Guide.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.emr.block_public_access_configuration.get(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.emr.block_public_access_configuration.get
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="emr",
        operation="get_block_public_access_configuration",
        op_kwargs={},
    )


async def put(hub, ctx, block_public_access_configuration: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Creates or updates an Amazon EMR block public access configuration for your account in the current Region. For more information see Configure Block Public Access for Amazon EMR in the Amazon EMR Management Guide.

    Args:
        block_public_access_configuration(Dict): A configuration for Amazon EMR block public access. The configuration applies to all clusters created in your account for the current Region. The configuration specifies whether block public access is enabled. If block public access is enabled, security groups associated with the cluster cannot have rules that allow inbound traffic from 0.0.0.0/0 or ::/0 on a port, unless the port is specified as an exception using PermittedPublicSecurityGroupRuleRanges in the BlockPublicAccessConfiguration. By default, Port 22 (SSH) is an exception, and public access is allowed on this port. You can change this by updating BlockPublicSecurityGroupRules to remove the exception.  For accounts that created clusters in a Region before November 25, 2019, block public access is disabled by default in that Region. To use this feature, you must manually enable and configure it. For accounts that did not create an EMR cluster in a Region before this date, block public access is enabled by default in that Region. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.emr.block_public_access_configuration.put(
                ctx, block_public_access_configuration=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.emr.block_public_access_configuration.put block_public_access_configuration=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="emr",
        operation="put_block_public_access_configuration",
        op_kwargs={"BlockPublicAccessConfiguration": block_public_access_configuration},
    )
