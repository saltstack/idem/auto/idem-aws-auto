"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(
    hub,
    ctx,
    job_id: str,
    max_results: int = None,
    next_token: str = None,
    sort_by: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Gets the unsafe content analysis results for a Amazon Rekognition Video analysis started by StartContentModeration. Unsafe content analysis of a video is an asynchronous operation. You start analysis by calling StartContentModeration which returns a job identifier (JobId). When analysis finishes, Amazon Rekognition Video publishes a completion status to the Amazon Simple Notification Service topic registered in the initial call to StartContentModeration. To get the results of the unsafe content analysis, first check that the status value published to the Amazon SNS topic is SUCCEEDED. If so, call GetContentModeration and pass the job identifier (JobId) from the initial call to StartContentModeration.  For more information, see Working with Stored Videos in the Amazon Rekognition Devlopers Guide.  GetContentModeration returns detected unsafe content labels, and the time they are detected, in an array, ModerationLabels, of ContentModerationDetection objects.  By default, the moderated labels are returned sorted by time, in milliseconds from the start of the video. You can also sort them by moderated label by specifying NAME for the SortBy input parameter.  Since video analysis can return a large number of results, use the MaxResults parameter to limit the number of labels returned in a single call to GetContentModeration. If there are more results than specified in MaxResults, the value of NextToken in the operation response contains a pagination token for getting the next set of results. To get the next page of results, call GetContentModeration and populate the NextToken request parameter with the value of NextToken returned from the previous call to GetContentModeration. For more information, see Detecting Unsafe Content in the Amazon Rekognition Developer Guide.

    Args:
        job_id(str): The identifier for the unsafe content job. Use JobId to identify the job in a subsequent call to GetContentModeration.
        max_results(int, optional): Maximum number of results to return per paginated call. The largest value you can specify is 1000. If you specify a value greater than 1000, a maximum of 1000 results is returned. The default value is 1000. Defaults to None.
        next_token(str, optional): If the previous response was incomplete (because there is more data to retrieve), Amazon Rekognition returns a pagination token in the response. You can use this pagination token to retrieve the next set of unsafe content labels. Defaults to None.
        sort_by(str, optional): Sort to use for elements in the ModerationLabelDetections array. Use TIMESTAMP to sort array elements by the time labels are detected. Use NAME to alphabetically group elements for a label together. Within each label group, the array element are sorted by detection confidence. The default sort is by TIMESTAMP. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rekognition.content_moderation.get(ctx, job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rekognition.content_moderation.get job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rekognition",
        operation="get_content_moderation",
        op_kwargs={
            "JobId": job_id,
            "MaxResults": max_results,
            "NextToken": next_token,
            "SortBy": sort_by,
        },
    )


async def start(
    hub,
    ctx,
    video: Dict,
    min_confidence: float = None,
    client_request_token: str = None,
    notification_channel: Dict = None,
    job_tag: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Starts asynchronous detection of unsafe content in a stored video. Amazon Rekognition Video can moderate content in a video stored in an Amazon S3 bucket. Use Video to specify the bucket name and the filename of the video. StartContentModeration returns a job identifier (JobId) which you use to get the results of the analysis. When unsafe content analysis is finished, Amazon Rekognition Video publishes a completion status to the Amazon Simple Notification Service topic that you specify in NotificationChannel. To get the results of the unsafe content analysis, first check that the status value published to the Amazon SNS topic is SUCCEEDED. If so, call GetContentModeration and pass the job identifier (JobId) from the initial call to StartContentModeration.  For more information, see Detecting Unsafe Content in the Amazon Rekognition Developer Guide.

    Args:
        video(Dict): The video in which you want to detect unsafe content. The video must be stored in an Amazon S3 bucket.
        min_confidence(float, optional): Specifies the minimum confidence that Amazon Rekognition must have in order to return a moderated content label. Confidence represents how certain Amazon Rekognition is that the moderated content is correctly identified. 0 is the lowest confidence. 100 is the highest confidence. Amazon Rekognition doesn't return any moderated content labels with a confidence level lower than this specified value. If you don't specify MinConfidence, GetContentModeration returns labels with confidence values greater than or equal to 50 percent. Defaults to None.
        client_request_token(str, optional): Idempotent token used to identify the start request. If you use the same token with multiple StartContentModeration requests, the same JobId is returned. Use ClientRequestToken to prevent the same job from being accidently started more than once. . Defaults to None.
        notification_channel(Dict, optional): The Amazon SNS topic ARN that you want Amazon Rekognition Video to publish the completion status of the unsafe content analysis to. Defaults to None.
        job_tag(str, optional): An identifier you specify that's returned in the completion notification that's published to your Amazon Simple Notification Service topic. For example, you can use JobTag to group related jobs and identify them in the completion notification. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.rekognition.content_moderation.start(ctx, video=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.rekognition.content_moderation.start video=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="rekognition",
        operation="start_content_moderation",
        op_kwargs={
            "Video": video,
            "MinConfidence": min_confidence,
            "ClientRequestToken": client_request_token,
            "NotificationChannel": notification_channel,
            "JobTag": job_tag,
        },
    )
