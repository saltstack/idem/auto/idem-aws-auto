"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe(
    hub, ctx, endpoint_arn: str, max_records: int = None, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the schema for the specified endpoint.

    Args:
        endpoint_arn(str): The Amazon Resource Name (ARN) string that uniquely identifies the endpoint.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that the remaining results can be retrieved.  Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional):  An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.schemas.describe(ctx, endpoint_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.schemas.describe endpoint_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="describe_schemas",
        op_kwargs={
            "EndpointArn": endpoint_arn,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def refresh(hub, ctx, endpoint_arn: str, replication_instance_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Populates the schema for the specified endpoint. This is an asynchronous operation and can take several minutes. You can check the status of this operation by calling the DescribeRefreshSchemasStatus operation.

    Args:
        endpoint_arn(str): The Amazon Resource Name (ARN) string that uniquely identifies the endpoint.
        replication_instance_arn(str): The Amazon Resource Name (ARN) of the replication instance.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.schemas.refresh(
                ctx, endpoint_arn=value, replication_instance_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.schemas.refresh endpoint_arn=value, replication_instance_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="refresh_schemas",
        op_kwargs={
            "EndpointArn": endpoint_arn,
            "ReplicationInstanceArn": replication_instance_arn,
        },
    )
