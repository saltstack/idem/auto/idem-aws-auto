"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__virtualname__ = "type"


async def describe_all(
    hub, ctx, filters: List = None, max_records: int = None, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about the type of endpoints available.

    Args:
        filters(List, optional): Filters applied to the endpoint types. Valid filter names: engine-name | endpoint-type. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that the remaining results can be retrieved.  Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional):  An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.endpoint.type_.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.endpoint.type.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="describe_endpoint_types",
        op_kwargs={"Filters": filters, "MaxRecords": max_records, "Marker": marker},
    )
