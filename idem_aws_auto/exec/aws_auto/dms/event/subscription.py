"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str,
    source_type: str = None,
    event_categories: List = None,
    source_ids: List = None,
    enabled: bool = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Creates an DMS event notification subscription.  You can specify the type of source (SourceType) you want to be notified of, provide a list of DMS source IDs (SourceIds) that triggers the events, and provide a list of event categories (EventCategories) for events you want to be notified of. If you specify both the SourceType and SourceIds, such as SourceType = replication-instance and SourceIdentifier = my-replinstance, you will be notified of all the replication instance events for the specified source. If you specify a SourceType but don't specify a SourceIdentifier, you receive notice of the events for that source type for all your DMS sources. If you don't specify either SourceType nor SourceIdentifier, you will be notified of events generated from all DMS sources belonging to your customer account. For more information about DMS events, see Working with Events and Notifications in the Database Migration Service User Guide.

    Args:
        subscription_name(str): The name of the DMS event notification subscription. This name must be less than 255 characters.
        sns_topic_arn(str):  The Amazon Resource Name (ARN) of the Amazon SNS topic created for event notification. The ARN is created by Amazon SNS when you create a topic and subscribe to it. .
        source_type(str, optional):  The type of DMS resource that generates the events. For example, if you want to be notified of events generated by a replication instance, you set this parameter to replication-instance. If this value isn't specified, all events are returned.  Valid values: replication-instance | replication-task . Defaults to None.
        event_categories(List, optional): A list of event categories for a source type that you want to subscribe to. For more information, see Working with Events and Notifications in the Database Migration Service User Guide. . Defaults to None.
        source_ids(List, optional): A list of identifiers for which DMS provides notification events. If you don't specify a value, notifications are provided for all sources. If you specify multiple values, they must be of the same type. For example, if you specify a database instance ID, then all of the other values must be database instance IDs. Defaults to None.
        enabled(bool, optional):  A Boolean value; set to true to activate the subscription, or set to false to create the subscription but not activate it. . Defaults to None.
        tags(List, optional): One or more tags to be assigned to the event subscription. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.event.subscription.create(
                ctx, subscription_name=value, sns_topic_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.event.subscription.create subscription_name=value, sns_topic_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="create_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "SourceIds": source_ids,
            "Enabled": enabled,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, subscription_name: str) -> Dict:
    r"""
    **Autogenerated function**

     Deletes an DMS event subscription.

    Args:
        subscription_name(str): The name of the DMS event notification subscription to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.event.subscription.delete(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.event.subscription.delete subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="delete_event_subscription",
        op_kwargs={"SubscriptionName": subscription_name},
    )


async def describe_all(
    hub,
    ctx,
    subscription_name: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the event subscriptions for a customer account. The description of a subscription includes SubscriptionName, SNSTopicARN, CustomerID, SourceType, SourceID, CreationTime, and Status.  If you specify SubscriptionName, this action lists the description for that subscription.

    Args:
        subscription_name(str, optional): The name of the DMS event subscription to be described. Defaults to None.
        filters(List, optional): Filters applied to event subscriptions. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that the remaining results can be retrieved.  Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional):  An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.event.subscription.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.event.subscription.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="describe_event_subscriptions",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str = None,
    source_type: str = None,
    event_categories: List = None,
    enabled: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies an existing DMS event notification subscription.

    Args:
        subscription_name(str): The name of the DMS event notification subscription to be modified.
        sns_topic_arn(str, optional):  The Amazon Resource Name (ARN) of the Amazon SNS topic created for event notification. The ARN is created by Amazon SNS when you create a topic and subscribe to it. Defaults to None.
        source_type(str, optional):  The type of DMS resource that generates the events you want to subscribe to.  Valid values: replication-instance | replication-task. Defaults to None.
        event_categories(List, optional):  A list of event categories for a source type that you want to subscribe to. Use the DescribeEventCategories action to see a list of event categories. . Defaults to None.
        enabled(bool, optional):  A Boolean value; set to true to activate the subscription. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.event.subscription.modify(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.event.subscription.modify subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="modify_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "Enabled": enabled,
        },
    )
