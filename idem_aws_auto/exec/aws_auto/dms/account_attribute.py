"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(hub, ctx) -> Dict:
    r"""
    **Autogenerated function**

    Lists all of the DMS attributes for a customer account. These attributes include DMS quotas for the account and a unique account identifier in a particular DMS region. DMS quotas include a list of resource quotas supported by the account, such as the number of replication instances allowed. The description for each resource quota, includes the quota name, current usage toward that quota, and the quota's maximum value. DMS uses the unique account identifier to name each artifact used by DMS in the given region. This command does not take any parameters.


    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.account_attribute.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.account_attribute.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="dms", operation="describe_account_attributes", op_kwargs={}
    )
