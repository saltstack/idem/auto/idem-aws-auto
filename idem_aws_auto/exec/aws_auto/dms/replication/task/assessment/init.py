"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def start(hub, ctx, replication_task_arn: str) -> Dict:
    r"""
    **Autogenerated function**

     Starts the replication task assessment for unsupported data types in the source database.

    Args:
        replication_task_arn(str):  The Amazon Resource Name (ARN) of the replication task. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.replication.task.assessment.init.start(
                ctx, replication_task_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.replication.task.assessment.init.start replication_task_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="start_replication_task_assessment",
        op_kwargs={"ReplicationTaskArn": replication_task_arn},
    )
