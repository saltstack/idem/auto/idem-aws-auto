"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def cancel(hub, ctx, replication_task_assessment_run_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Cancels a single premigration assessment run. This operation prevents any individual assessments from running if they haven't started running. It also attempts to cancel any individual assessments that are currently running.

    Args:
        replication_task_assessment_run_arn(str): Amazon Resource Name (ARN) of the premigration assessment run to be canceled.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.replication.task.assessment.run.cancel(
                ctx, replication_task_assessment_run_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.replication.task.assessment.run.cancel replication_task_assessment_run_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="cancel_replication_task_assessment_run",
        op_kwargs={
            "ReplicationTaskAssessmentRunArn": replication_task_assessment_run_arn
        },
    )


async def delete(hub, ctx, replication_task_assessment_run_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the record of a single premigration assessment run. This operation removes all metadata that DMS maintains about this assessment run. However, the operation leaves untouched all information about this assessment run that is stored in your Amazon S3 bucket.

    Args:
        replication_task_assessment_run_arn(str): Amazon Resource Name (ARN) of the premigration assessment run to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.replication.task.assessment.run.delete(
                ctx, replication_task_assessment_run_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.replication.task.assessment.run.delete replication_task_assessment_run_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="delete_replication_task_assessment_run",
        op_kwargs={
            "ReplicationTaskAssessmentRunArn": replication_task_assessment_run_arn
        },
    )


async def describe_all(
    hub, ctx, filters: List = None, max_records: int = None, marker: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a paginated list of premigration assessment runs based on filter settings. These filter settings can specify a combination of premigration assessment runs, migration tasks, replication instances, and assessment run status values.  This operation doesn't return information about individual assessments. For this information, see the DescribeReplicationTaskIndividualAssessments operation.

    Args:
        filters(List, optional): Filters applied to the premigration assessment runs described in the form of key-value pairs. Valid filter names: replication-task-assessment-run-arn, replication-task-arn, replication-instance-arn, status . Defaults to None.
        max_records(int, optional): The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token called a marker is included in the response so that the remaining results can be retrieved. Defaults to None.
        marker(str, optional): An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.replication.task.assessment.run.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.replication.task.assessment.run.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="describe_replication_task_assessment_runs",
        op_kwargs={"Filters": filters, "MaxRecords": max_records, "Marker": marker},
    )


async def start(
    hub,
    ctx,
    replication_task_arn: str,
    service_access_role_arn: str,
    result_location_bucket: str,
    assessment_run_name: str,
    result_location_folder: str = None,
    result_encryption_mode: str = None,
    result_kms_key_arn: str = None,
    include_only: List = None,
    exclude: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Starts a new premigration assessment run for one or more individual assessments of a migration task. The assessments that you can specify depend on the source and target database engine and the migration type defined for the given task. To run this operation, your migration task must already be created. After you run this operation, you can review the status of each individual assessment. You can also run the migration task manually after the assessment run and its individual assessments complete.

    Args:
        replication_task_arn(str): Amazon Resource Name (ARN) of the migration task associated with the premigration assessment run that you want to start.
        service_access_role_arn(str): ARN of the service role needed to start the assessment run. The role must allow the iam:PassRole action.
        result_location_bucket(str): Amazon S3 bucket where you want DMS to store the results of this assessment run.
        result_location_folder(str, optional): Folder within an Amazon S3 bucket where you want DMS to store the results of this assessment run. Defaults to None.
        result_encryption_mode(str, optional): Encryption mode that you can specify to encrypt the results of this assessment run. If you don't specify this request parameter, DMS stores the assessment run results without encryption. You can specify one of the options following:    "SSE_S3" – The server-side encryption provided as a default by Amazon S3.    "SSE_KMS" – Key Management Service (KMS) encryption. This encryption can use either a custom KMS encryption key that you specify or the default KMS encryption key that DMS provides.  . Defaults to None.
        result_kms_key_arn(str, optional): ARN of a custom KMS encryption key that you specify when you set ResultEncryptionMode to "SSE_KMS". Defaults to None.
        assessment_run_name(str): Unique name to identify the assessment run.
        include_only(List, optional): Space-separated list of names for specific individual assessments that you want to include. These names come from the default list of individual assessments that DMS supports for the associated migration task. This task is specified by ReplicationTaskArn.  You can't set a value for IncludeOnly if you also set a value for Exclude in the API operation.  To identify the names of the default individual assessments that DMS supports for the associated migration task, run the DescribeApplicableIndividualAssessments operation using its own ReplicationTaskArn request parameter. . Defaults to None.
        exclude(List, optional): Space-separated list of names for specific individual assessments that you want to exclude. These names come from the default list of individual assessments that DMS supports for the associated migration task. This task is specified by ReplicationTaskArn.  You can't set a value for Exclude if you also set a value for IncludeOnly in the API operation. To identify the names of the default individual assessments that DMS supports for the associated migration task, run the DescribeApplicableIndividualAssessments operation using its own ReplicationTaskArn request parameter. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dms.replication.task.assessment.run.start(
                ctx,
                replication_task_arn=value,
                service_access_role_arn=value,
                result_location_bucket=value,
                assessment_run_name=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dms.replication.task.assessment.run.start replication_task_arn=value, service_access_role_arn=value, result_location_bucket=value, assessment_run_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dms",
        operation="start_replication_task_assessment_run",
        op_kwargs={
            "ReplicationTaskArn": replication_task_arn,
            "ServiceAccessRoleArn": service_access_role_arn,
            "ResultLocationBucket": result_location_bucket,
            "ResultLocationFolder": result_location_folder,
            "ResultEncryptionMode": result_encryption_mode,
            "ResultKmsKeyArn": result_kms_key_arn,
            "AssessmentRunName": assessment_run_name,
            "IncludeOnly": include_only,
            "Exclude": exclude,
        },
    )
