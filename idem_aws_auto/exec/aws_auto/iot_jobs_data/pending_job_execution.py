"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, thing_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets the list of all jobs for a thing that are not in a terminal status.

    Args:
        thing_name(str): The name of the thing that is executing the job.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iot_jobs_data.pending_job_execution.get_all(
                ctx, thing_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iot_jobs_data.pending_job_execution.get_all thing_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iot-jobs-data",
        operation="get_pending_job_executions",
        op_kwargs={"thingName": thing_name},
    )
