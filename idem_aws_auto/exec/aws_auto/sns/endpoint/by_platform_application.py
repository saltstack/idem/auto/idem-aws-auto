"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, platform_application_arn: str, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the endpoints and endpoint attributes for devices in a supported push notification service, such as GCM (Firebase Cloud Messaging) and APNS. The results for ListEndpointsByPlatformApplication are paginated and return a limited list of endpoints, up to 100. If additional records are available after the first page results, then a NextToken string will be returned. To receive the next page, you call ListEndpointsByPlatformApplication again using the NextToken string received from the previous call. When there are no more records to return, NextToken will be null. For more information, see Using Amazon SNS Mobile Push Notifications.  This action is throttled at 30 transactions per second (TPS).

    Args:
        platform_application_arn(str): PlatformApplicationArn for ListEndpointsByPlatformApplicationInput action.
        next_token(str, optional): NextToken string is used when calling ListEndpointsByPlatformApplication action to retrieve additional records that are available after the first page results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sns.endpoint.by_platform_application.list(
                ctx, platform_application_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sns.endpoint.by_platform_application.list platform_application_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sns",
        operation="list_endpoints_by_platform_application",
        op_kwargs={
            "PlatformApplicationArn": platform_application_arn,
            "NextToken": next_token,
        },
    )
