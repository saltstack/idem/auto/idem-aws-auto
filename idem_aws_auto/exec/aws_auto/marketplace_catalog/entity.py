"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def describe(hub, ctx, catalog: str, entity_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the metadata and content of the entity.

    Args:
        catalog(str): Required. The catalog related to the request. Fixed value: AWSMarketplace .
        entity_id(str): Required. The unique ID of the entity to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.marketplace_catalog.entity.describe(
                ctx, catalog=value, entity_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.marketplace_catalog.entity.describe catalog=value, entity_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="marketplace-catalog",
        operation="describe_entity",
        op_kwargs={"Catalog": catalog, "EntityId": entity_id},
    )


async def list_all(
    hub,
    ctx,
    catalog: str,
    entity_type: str,
    filter_list: List = None,
    sort: Dict = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Provides the list of entities of a given type.

    Args:
        catalog(str): The catalog related to the request. Fixed value: AWSMarketplace .
        entity_type(str): The type of entities to retrieve.
        filter_list(List, optional): An array of filter objects. Each filter object contains two attributes, filterName and filterValues. Defaults to None.
        sort(Dict, optional): An object that contains two attributes, SortBy and SortOrder. Defaults to None.
        next_token(str, optional): The value of the next token, if it exists. Null if there are no more results. Defaults to None.
        max_results(int, optional): Specifies the upper limit of the elements on a single page. If a value isn't provided, the default value is 20. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.marketplace_catalog.entity.list_all(
                ctx, catalog=value, entity_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.marketplace_catalog.entity.list_all catalog=value, entity_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="marketplace-catalog",
        operation="list_entities",
        op_kwargs={
            "Catalog": catalog,
            "EntityType": entity_type,
            "FilterList": filter_list,
            "Sort": sort,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
