"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, container_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the access policy that is associated with the specified container.

    Args:
        container_name(str): The name of the container that holds the policy.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediastore.container.policy.delete(ctx, container_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediastore.container.policy.delete container_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediastore",
        operation="delete_container_policy",
        op_kwargs={"ContainerName": container_name},
    )


async def get(hub, ctx, container_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the access policy for the specified container. For information about the data that is included in an access policy, see the AWS Identity and Access Management User Guide.

    Args:
        container_name(str): The name of the container. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediastore.container.policy.get(ctx, container_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediastore.container.policy.get container_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediastore",
        operation="get_container_policy",
        op_kwargs={"ContainerName": container_name},
    )


async def put(hub, ctx, container_name: str, policy: str) -> Dict:
    r"""
    **Autogenerated function**

    Creates an access policy for the specified container to restrict the users and clients that can access it. For information about the data that is included in an access policy, see the AWS Identity and Access Management User Guide. For this release of the REST API, you can create only one policy for a container. If you enter PutContainerPolicy twice, the second command modifies the existing policy.

    Args:
        container_name(str): The name of the container.
        policy(str): The contents of the policy, which includes the following:    One Version tag   One Statement tag that contains the standard tags for the policy.  .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.mediastore.container.policy.put(
                ctx, container_name=value, policy=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.mediastore.container.policy.put container_name=value, policy=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="mediastore",
        operation="put_container_policy",
        op_kwargs={"ContainerName": container_name, "Policy": policy},
    )
