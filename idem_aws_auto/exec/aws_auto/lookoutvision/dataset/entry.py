"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    project_name: str,
    dataset_type: str,
    labeled: bool = None,
    anomaly_class: str = None,
    before_creation_date: str = None,
    after_creation_date: str = None,
    next_token: str = None,
    max_results: int = None,
    source_ref_contains: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the JSON Lines within a dataset. An Amazon Lookout for Vision JSON Line contains the anomaly information for a single image, including the image location and the assigned label. This operation requires permissions to perform the lookoutvision:ListDatasetEntries operation.

    Args:
        project_name(str): The name of the project that contains the dataset that you want to list.
        dataset_type(str): The type of the dataset that you want to list. Specify train to list the training dataset. Specify test to list the test dataset. If you have a single dataset project, specify train.
        labeled(bool, optional): Specify true to include labeled entries, otherwise specify false. If you don't specify a value, Lookout for Vision returns all entries. Defaults to None.
        anomaly_class(str, optional): Specify normal to include only normal images. Specify anomaly to only include anomalous entries. If you don't specify a value, Amazon Lookout for Vision returns normal and anomalous images. Defaults to None.
        before_creation_date(str, optional): Only includes entries before the specified date in the response. For example, 2020-06-23T00:00:00. Defaults to None.
        after_creation_date(str, optional): Only includes entries after the specified date in the response. For example, 2020-06-23T00:00:00. Defaults to None.
        next_token(str, optional): If the previous response was incomplete (because there is more data to retrieve), Amazon Lookout for Vision returns a pagination token in the response. You can use this pagination token to retrieve the next set of dataset entries. Defaults to None.
        max_results(int, optional): The maximum number of results to return per paginated call. The largest value you can specify is 100. If you specify a value greater than 100, a ValidationException error occurs. The default value is 100. Defaults to None.
        source_ref_contains(str, optional): Perform a "contains" search on the values of the source-ref key within the dataset. For example a value of "IMG_17" returns all JSON Lines where the source-ref key value matches *IMG_17*. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutvision.dataset.entry.list_all(
                ctx, project_name=value, dataset_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutvision.dataset.entry.list_all project_name=value, dataset_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutvision",
        operation="list_dataset_entries",
        op_kwargs={
            "ProjectName": project_name,
            "DatasetType": dataset_type,
            "Labeled": labeled,
            "AnomalyClass": anomaly_class,
            "BeforeCreationDate": before_creation_date,
            "AfterCreationDate": after_creation_date,
            "NextToken": next_token,
            "MaxResults": max_results,
            "SourceRefContains": source_ref_contains,
        },
    )


async def update_multiple(
    hub,
    ctx,
    project_name: str,
    dataset_type: str,
    changes: bytes,
    client_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Adds one or more JSON Line entries to a dataset. A JSON Line includes information about an image used for training or testing an Amazon Lookout for Vision model. The following is an example JSON Line. Updating a dataset might take a while to complete. To check the current status, call DescribeDataset and check the Status field in the response. This operation requires permissions to perform the lookoutvision:UpdateDatasetEntries operation.

    Args:
        project_name(str): The name of the project that contains the dataset that you want to update.
        dataset_type(str): The type of the dataset that you want to update. Specify train to update the training dataset. Specify test to update the test dataset. If you have a single dataset project, specify train.
        changes(bytes): The entries to add to the dataset.
        client_token(str, optional): ClientToken is an idempotency token that ensures a call to UpdateDatasetEntries completes only once. You choose the value to pass. For example, An issue, such as an network outage, might prevent you from getting a response from UpdateDatasetEntries. In this case, safely retry your call to UpdateDatasetEntries by using the same ClientToken parameter value. An error occurs if the other input parameters are not the same as in the first request. Using a different value for ClientToken is considered a new call to UpdateDatasetEntries. An idempotency token is active for 8 hours. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lookoutvision.dataset.entry.update_multiple(
                ctx, project_name=value, dataset_type=value, changes=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lookoutvision.dataset.entry.update_multiple project_name=value, dataset_type=value, changes=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lookoutvision",
        operation="update_dataset_entries",
        op_kwargs={
            "ProjectName": project_name,
            "DatasetType": dataset_type,
            "Changes": changes,
            "ClientToken": client_token,
        },
    )
