"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    server_id: str,
    seed_replication_time: str,
    frequency: int = None,
    run_once: bool = None,
    license_type: str = None,
    role_name: str = None,
    description: str = None,
    number_of_recent_amis_to_keep: int = None,
    encrypted: bool = None,
    kms_key_id: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a replication job. The replication job schedules periodic replication runs to replicate your server to AWS. Each replication run creates an Amazon Machine Image (AMI).

    Args:
        server_id(str): The ID of the server.
        seed_replication_time(str): The seed replication time.
        frequency(int, optional): The time between consecutive replication runs, in hours. Defaults to None.
        run_once(bool, optional): Indicates whether to run the replication job one time. Defaults to None.
        license_type(str, optional): The license type to be used for the AMI created by a successful replication run. Defaults to None.
        role_name(str, optional): The name of the IAM role to be used by the AWS SMS. Defaults to None.
        description(str, optional): The description of the replication job. Defaults to None.
        number_of_recent_amis_to_keep(int, optional): The maximum number of SMS-created AMIs to retain. The oldest is deleted after the maximum number is reached and a new AMI is created. Defaults to None.
        encrypted(bool, optional): Indicates whether the replication job produces encrypted AMIs. Defaults to None.
        kms_key_id(str, optional): The ID of the KMS key for replication jobs that produce encrypted AMIs. This value can be any of the following:   KMS key ID   KMS key alias   ARN referring to the KMS key ID   ARN referring to the KMS key alias    If encrypted is true but a KMS key ID is not specified, the customer's default KMS key for Amazon EBS is used. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sms.replication.job.create(
                ctx, server_id=value, seed_replication_time=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sms.replication.job.create server_id=value, seed_replication_time=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sms",
        operation="create_replication_job",
        op_kwargs={
            "serverId": server_id,
            "seedReplicationTime": seed_replication_time,
            "frequency": frequency,
            "runOnce": run_once,
            "licenseType": license_type,
            "roleName": role_name,
            "description": description,
            "numberOfRecentAmisToKeep": number_of_recent_amis_to_keep,
            "encrypted": encrypted,
            "kmsKeyId": kms_key_id,
        },
    )


async def delete(hub, ctx, replication_job_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified replication job. After you delete a replication job, there are no further replication runs. AWS deletes the contents of the Amazon S3 bucket used to store AWS SMS artifacts. The AMIs created by the replication runs are not deleted.

    Args:
        replication_job_id(str): The ID of the replication job.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sms.replication.job.delete(ctx, replication_job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sms.replication.job.delete replication_job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sms",
        operation="delete_replication_job",
        op_kwargs={"replicationJobId": replication_job_id},
    )


async def get_all(
    hub,
    ctx,
    replication_job_id: str = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the specified replication job or all of your replication jobs.

    Args:
        replication_job_id(str, optional): The ID of the replication job. Defaults to None.
        next_token(str, optional): The token for the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return in a single call. The default value is 50. To retrieve the remaining results, make another call with the returned NextToken value. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sms.replication.job.get_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sms.replication.job.get_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sms",
        operation="get_replication_jobs",
        op_kwargs={
            "replicationJobId": replication_job_id,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )


async def update(
    hub,
    ctx,
    replication_job_id: str,
    frequency: int = None,
    next_replication_run_start_time: str = None,
    license_type: str = None,
    role_name: str = None,
    description: str = None,
    number_of_recent_amis_to_keep: int = None,
    encrypted: bool = None,
    kms_key_id: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the specified settings for the specified replication job.

    Args:
        replication_job_id(str): The ID of the replication job.
        frequency(int, optional): The time between consecutive replication runs, in hours. Defaults to None.
        next_replication_run_start_time(str, optional): The start time of the next replication run. Defaults to None.
        license_type(str, optional): The license type to be used for the AMI created by a successful replication run. Defaults to None.
        role_name(str, optional): The name of the IAM role to be used by AWS SMS. Defaults to None.
        description(str, optional): The description of the replication job. Defaults to None.
        number_of_recent_amis_to_keep(int, optional): The maximum number of SMS-created AMIs to retain. The oldest is deleted after the maximum number is reached and a new AMI is created. Defaults to None.
        encrypted(bool, optional): When true, the replication job produces encrypted AMIs. For more information, KmsKeyId. Defaults to None.
        kms_key_id(str, optional): The ID of the KMS key for replication jobs that produce encrypted AMIs. This value can be any of the following:   KMS key ID   KMS key alias   ARN referring to the KMS key ID   ARN referring to the KMS key alias   If encrypted is enabled but a KMS key ID is not specified, the customer's default KMS key for Amazon EBS is used. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sms.replication.job.update(ctx, replication_job_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sms.replication.job.update replication_job_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sms",
        operation="update_replication_job",
        op_kwargs={
            "replicationJobId": replication_job_id,
            "frequency": frequency,
            "nextReplicationRunStartTime": next_replication_run_start_time,
            "licenseType": license_type,
            "roleName": role_name,
            "description": description,
            "numberOfRecentAmisToKeep": number_of_recent_amis_to_keep,
            "encrypted": encrypted,
            "kmsKeyId": kms_key_id,
        },
    )
