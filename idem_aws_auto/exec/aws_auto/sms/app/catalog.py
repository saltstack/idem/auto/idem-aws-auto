"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"import_": "import"}


async def import_(hub, ctx, role_name: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Allows application import from AWS Migration Hub.

    Args:
        role_name(str, optional): The name of the service role. If you omit this parameter, we create a service-linked role for AWS Migration Hub in your account. Otherwise, the role that you provide must have the policy and trust policy described in the AWS Migration Hub User Guide. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.sms.app.catalog.import(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.sms.app.catalog.import
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="sms",
        operation="import_app_catalog",
        op_kwargs={"roleName": role_name},
    )
