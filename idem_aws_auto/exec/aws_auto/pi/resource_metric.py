"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get_all(
    hub,
    ctx,
    service_type: str,
    identifier: str,
    metric_queries: List,
    start_time: str,
    end_time: str,
    period_in_seconds: int = None,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieve Performance Insights metrics for a set of data sources, over a time period. You can provide specific dimension groups and dimensions, and provide aggregation and filtering criteria for each group.  Each response element returns a maximum of 500 bytes. For larger elements, such as SQL statements, only the first 500 bytes are returned.

    Args:
        service_type(str): The AWS service for which Performance Insights returns metrics. The only valid value for ServiceType is RDS.
        identifier(str): An immutable, AWS Region-unique identifier for a data source. Performance Insights gathers metrics from this data source. To use a DB instance as a data source, specify its DbiResourceId value. For example, specify db-FAIHNTYBKTGAUSUZQYPDS2GW4A.
        metric_queries(List): An array of one or more queries to perform. Each query must specify a Performance Insights metric, and can optionally specify aggregation and filtering criteria.
        start_time(str): The date and time specifying the beginning of the requested time series data. You can't specify a StartTime that's earlier than 7 days ago. The value specified is inclusive - data points equal to or greater than StartTime will be returned. The value for StartTime must be earlier than the value for EndTime.
        end_time(str): The date and time specifying the end of the requested time series data. The value specified is exclusive - data points less than (but not equal to) EndTime will be returned. The value for EndTime must be later than the value for StartTime.
        period_in_seconds(int, optional): The granularity, in seconds, of the data points returned from Performance Insights. A period can be as short as one second, or as long as one day (86400 seconds). Valid values are:    1 (one second)    60 (one minute)    300 (five minutes)    3600 (one hour)    86400 (twenty-four hours)   If you don't specify PeriodInSeconds, then Performance Insights will choose a value for you, with a goal of returning roughly 100-200 data points in the response. Defaults to None.
        max_results(int, optional): The maximum number of items to return in the response. If more items exist than the specified MaxRecords value, a pagination token is included in the response so that the remaining results can be retrieved. . Defaults to None.
        next_token(str, optional): An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the token, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.pi.resource_metric.get_all(
                ctx,
                service_type=value,
                identifier=value,
                metric_queries=value,
                start_time=value,
                end_time=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.pi.resource_metric.get_all service_type=value, identifier=value, metric_queries=value, start_time=value, end_time=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="pi",
        operation="get_resource_metrics",
        op_kwargs={
            "ServiceType": service_type,
            "Identifier": identifier,
            "MetricQueries": metric_queries,
            "StartTime": start_time,
            "EndTime": end_time,
            "PeriodInSeconds": period_in_seconds,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
