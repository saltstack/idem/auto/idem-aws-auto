"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"set_": "set"}


async def get_all(hub, ctx, identity_pool_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets the roles for an identity pool. You must use AWS Developer credentials to call this API.

    Args:
        identity_pool_id(str): An identity pool ID in the format REGION:GUID.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_identity.identity.pool.role.get_all(
                ctx, identity_pool_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_identity.identity.pool.role.get_all identity_pool_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-identity",
        operation="get_identity_pool_roles",
        op_kwargs={"IdentityPoolId": identity_pool_id},
    )


async def set_(
    hub, ctx, identity_pool_id: str, roles: Dict, role_mappings: Dict = None
) -> None:
    r"""
    **Autogenerated function**

    Sets the roles for an identity pool. These roles are used when making calls to GetCredentialsForIdentity action. You must use AWS Developer credentials to call this API.

    Args:
        identity_pool_id(str): An identity pool ID in the format REGION:GUID.
        roles(Dict): The map of roles associated with this pool. For a given role, the key will be either "authenticated" or "unauthenticated" and the value will be the Role ARN.
        role_mappings(Dict, optional): How users for a specific identity provider are to mapped to roles. This is a string to RoleMapping object map. The string identifies the identity provider, for example, "graph.facebook.com" or "cognito-idp.us-east-1.amazonaws.com/us-east-1_abcdefghi:app_client_id". Up to 25 rules can be specified per identity provider. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.cognito_identity.identity.pool.role.set(
                ctx, identity_pool_id=value, roles=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.cognito_identity.identity.pool.role.set identity_pool_id=value, roles=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="cognito-identity",
        operation="set_identity_pool_roles",
        op_kwargs={
            "IdentityPoolId": identity_pool_id,
            "Roles": roles,
            "RoleMappings": role_mappings,
        },
    )
