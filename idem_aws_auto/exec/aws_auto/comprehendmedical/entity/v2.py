"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def detect(hub, ctx, text: str) -> Dict:
    r"""
    **Autogenerated function**

    Inspects the clinical text for a variety of medical entities and returns specific information about them such as entity category, location, and confidence score on that information. Amazon Comprehend Medical only detects medical entities in English language texts. The DetectEntitiesV2 operation replaces the DetectEntities operation. This new action uses a different model for determining the entities in your medical text and changes the way that some entities are returned in the output. You should use the DetectEntitiesV2 operation in all new applications. The DetectEntitiesV2 operation returns the Acuity and Direction entities as attributes instead of types.

    Args:
        text(str): A UTF-8 string containing the clinical content being examined for entities. Each string must contain fewer than 20,000 bytes of characters.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.comprehendmedical.entity.v2.detect(ctx, text=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.comprehendmedical.entity.v2.detect text=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="comprehendmedical",
        operation="detect_entities_v2",
        op_kwargs={"Text": text},
    )
