"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub, ctx, cell_name: str, cells: List = None, tags: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new Cell.

    Args:
        cell_name(str): The name of the Cell to create.
        cells(List, optional): A list of Cell arns contained within this Cell (for use in nested Cells, e.g. regions within which AZs). Defaults to None.
        tags(Dict, optional): A collection of tags associated with a resource. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53_recovery_readiness.cell.init.create(
                ctx, cell_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53_recovery_readiness.cell.init.create cell_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53-recovery-readiness",
        operation="create_cell",
        op_kwargs={"CellName": cell_name, "Cells": cells, "Tags": tags},
    )


async def delete(hub, ctx, cell_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes an existing Cell.

    Args:
        cell_name(str): The Cell to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53_recovery_readiness.cell.init.delete(
                ctx, cell_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53_recovery_readiness.cell.init.delete cell_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53-recovery-readiness",
        operation="delete_cell",
        op_kwargs={"CellName": cell_name},
    )


async def get(hub, ctx, cell_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a Cell.

    Args:
        cell_name(str): The Cell to get.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53_recovery_readiness.cell.init.get(ctx, cell_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53_recovery_readiness.cell.init.get cell_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53-recovery-readiness",
        operation="get_cell",
        op_kwargs={"CellName": cell_name},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Returns a collection of Cells.

    Args:
        max_results(int, optional): Upper bound on number of records to return. Defaults to None.
        next_token(str, optional): A token used to resume pagination from the end of a previous request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53_recovery_readiness.cell.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53_recovery_readiness.cell.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53-recovery-readiness",
        operation="list_cells",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(hub, ctx, cell_name: str, cells: List) -> Dict:
    r"""
    **Autogenerated function**

    Updates an existing Cell.

    Args:
        cell_name(str): The Cell to update.
        cells(List): A list of Cell arns, completely replaces previous list.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53_recovery_readiness.cell.init.update(
                ctx, cell_name=value, cells=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53_recovery_readiness.cell.init.update cell_name=value, cells=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53-recovery-readiness",
        operation="update_cell",
        op_kwargs={"CellName": cell_name, "Cells": cells},
    )
