"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"import_": "import"}


async def delete(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a specified authorization key pair. This invalidates future viewer tokens generated using the key pair’s privateKey. For more information, see Setting Up Private Channels in the Amazon IVS User Guide.

    Args:
        arn(str): ARN of the key pair to be deleted.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ivs.playback_key_pair.delete(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ivs.playback_key_pair.delete arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ivs",
        operation="delete_playback_key_pair",
        op_kwargs={"arn": arn},
    )


async def get(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets a specified playback authorization key pair and returns the arn and fingerprint. The privateKey held by the caller can be used to generate viewer authorization tokens, to grant viewers access to private channels. For more information, see Setting Up Private Channels in the Amazon IVS User Guide.

    Args:
        arn(str): ARN of the key pair to be returned.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ivs.playback_key_pair.get(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ivs.playback_key_pair.get arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ivs",
        operation="get_playback_key_pair",
        op_kwargs={"arn": arn},
    )


async def import_(
    hub, ctx, public_key_material: str, name: str = None, tags: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Imports the public portion of a new key pair and returns its arn and fingerprint. The privateKey can then be used to generate viewer authorization tokens, to grant viewers access to private channels. For more information, see Setting Up Private Channels in the Amazon IVS User Guide.

    Args:
        public_key_material(str): The public portion of a customer-generated key pair.
        name(str, optional): An arbitrary string (a nickname) assigned to a playback key pair that helps the customer identify that resource. The value does not need to be unique. Defaults to None.
        tags(Dict, optional): Any tags provided with the request are added to the playback key pair tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ivs.playback_key_pair.import(ctx, public_key_material=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ivs.playback_key_pair.import public_key_material=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ivs",
        operation="import_playback_key_pair",
        op_kwargs={
            "publicKeyMaterial": public_key_material,
            "name": name,
            "tags": tags,
        },
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets summary information about playback key pairs. For more information, see Setting Up Private Channels in the Amazon IVS User Guide.

    Args:
        next_token(str, optional): Maximum number of key pairs to return. Defaults to None.
        max_results(int, optional): The first key pair to retrieve. This is used for pagination; see the nextToken response field. Default: 50. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ivs.playback_key_pair.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ivs.playback_key_pair.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="ivs",
        operation="list_playback_key_pairs",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )
