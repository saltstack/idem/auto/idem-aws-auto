"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, arns: List) -> Dict:
    r"""
    **Autogenerated function**

    Performs GetChannel on multiple ARNs simultaneously.

    Args:
        arns(List): Array of ARNs, one per channel.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.ivs.get.channel.batch(ctx, arns=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.ivs.get.channel.batch arns=value
    """
    return await hub.tool.aws.client.exec(
        ctx, service_name="ivs", operation="batch_get_channel", op_kwargs={"arns": arns}
    )
