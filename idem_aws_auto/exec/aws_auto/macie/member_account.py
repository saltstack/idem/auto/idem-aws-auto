"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(hub, ctx, member_account_id: str) -> None:
    r"""
    **Autogenerated function**

    Associates a specified AWS account with Amazon Macie Classic as a member account.

    Args:
        member_account_id(str): The ID of the AWS account that you want to associate with Amazon Macie Classic as a member account.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.macie.member_account.associate(ctx, member_account_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.macie.member_account.associate member_account_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="macie",
        operation="associate_member_account",
        op_kwargs={"memberAccountId": member_account_id},
    )


async def disassociate(hub, ctx, member_account_id: str) -> None:
    r"""
    **Autogenerated function**

    Removes the specified member account from Amazon Macie Classic.

    Args:
        member_account_id(str): The ID of the member account that you want to remove from Amazon Macie Classic.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.macie.member_account.disassociate(ctx, member_account_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.macie.member_account.disassociate member_account_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="macie",
        operation="disassociate_member_account",
        op_kwargs={"memberAccountId": member_account_id},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all Amazon Macie Classic member accounts for the current Macie Classic administrator account.

    Args:
        next_token(str, optional): Use this parameter when paginating results. Set the value of this parameter to null on your first call to the ListMemberAccounts action. Subsequent calls to the action fill nextToken in the request with the value of nextToken from the previous response to continue listing data. . Defaults to None.
        max_results(int, optional): Use this parameter to indicate the maximum number of items that you want in the response. The default value is 250. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.macie.member_account.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.macie.member_account.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="macie",
        operation="list_member_accounts",
        op_kwargs={"nextToken": next_token, "maxResults": max_results},
    )
