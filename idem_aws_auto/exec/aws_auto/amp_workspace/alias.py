"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""


async def update(
    hub, ctx, workspace_id: str, alias: str = None, client_token: str = None
) -> None:
    r"""
    **Autogenerated function**

    Updates an AMP workspace alias.

    Args:
        alias(str, optional): The new alias of the workspace. Defaults to None.
        client_token(str, optional): Optional, unique, case-sensitive, user-provided identifier to ensure the idempotency of the request. Defaults to None.
        workspace_id(str): The ID of the workspace being updated.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.amp_workspace.alias.update(ctx, workspace_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.amp_workspace.alias.update workspace_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="amp",
        operation="update_workspace_alias",
        op_kwargs={
            "alias": alias,
            "clientToken": client_token,
            "workspaceId": workspace_id,
        },
    )
