"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, resource_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets a collection of Tag resources.

    Args:
        resource_arn(str): The resource ARN for the tag.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigatewayv2.tags.get(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigatewayv2.tags.get resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigatewayv2",
        operation="get_tags",
        op_kwargs={"ResourceArn": resource_arn},
    )
