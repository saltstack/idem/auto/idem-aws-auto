"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: Dict = None) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new Tag resource to represent a tag.

    Args:
        resource_arn(str): The resource ARN for the tag.
        tags(Dict, optional): The collection of tags. Each tag element is associated with a given resource. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigatewayv2.resource.tag(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigatewayv2.resource.tag resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigatewayv2",
        operation="tag_resource",
        op_kwargs={"ResourceArn": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> None:
    r"""
    **Autogenerated function**

    Deletes a Tag.

    Args:
        resource_arn(str): The resource ARN for the tag.
        tag_keys(List): The Tag keys to delete.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.apigatewayv2.resource.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.apigatewayv2.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="apigatewayv2",
        operation="untag_resource",
        op_kwargs={"ResourceArn": resource_arn, "TagKeys": tag_keys},
    )
