"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, arn: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a version of a component from IoT Greengrass.  This operation deletes the component's recipe and artifacts. As a result, deployments that refer to this component version will fail. If you have deployments that use this component version, you can remove the component from the deployment or update the deployment to use a valid version.

    Args:
        arn(str): The ARN of the component version.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrassv2.component.init.delete(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrassv2.component.init.delete arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrassv2",
        operation="delete_component",
        op_kwargs={"arn": arn},
    )


async def describe(hub, ctx, arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves metadata for a version of a component.

    Args:
        arn(str): The ARN of the component version.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrassv2.component.init.describe(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrassv2.component.init.describe arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrassv2",
        operation="describe_component",
        op_kwargs={"arn": arn},
    )


async def get(hub, ctx, arn: str, recipe_output_format: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Gets the recipe for a version of a component. Core devices can call this operation to identify the artifacts and requirements to install a component.

    Args:
        recipe_output_format(str, optional): The format of the recipe. Defaults to None.
        arn(str): The ARN of the component version.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrassv2.component.init.get(ctx, arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrassv2.component.init.get arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrassv2",
        operation="get_component",
        op_kwargs={"recipeOutputFormat": recipe_output_format, "arn": arn},
    )


async def list_all(
    hub, ctx, scope: str = None, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a paginated list of component summaries. This list includes components that you have permission to view.

    Args:
        scope(str, optional): The scope of the components to list. Default: PRIVATE . Defaults to None.
        max_results(int, optional): The maximum number of results to be returned per paginated request. Defaults to None.
        next_token(str, optional): The token to be used for the next set of paginated results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrassv2.component.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrassv2.component.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrassv2",
        operation="list_components",
        op_kwargs={"scope": scope, "maxResults": max_results, "nextToken": next_token},
    )
