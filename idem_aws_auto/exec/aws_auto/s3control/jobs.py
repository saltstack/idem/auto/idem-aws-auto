"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    account_id: str,
    job_statuses: List = None,
    next_token: str = None,
    max_results: int = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists current S3 Batch Operations jobs and jobs that have ended within the last 30 days for the account making the request. For more information, see S3 Batch Operations in the Amazon S3 User Guide. Related actions include:     CreateJob     DescribeJob     UpdateJobPriority     UpdateJobStatus

    Args:
        account_id(str): The account ID associated with the S3 Batch Operations job.
        job_statuses(List, optional): The List Jobs request returns jobs that match the statuses listed in this element. Defaults to None.
        next_token(str, optional): A pagination token to request the next page of results. Use the token that Amazon S3 returned in the NextToken element of the ListJobsResult from the previous List Jobs request. Defaults to None.
        max_results(int, optional): The maximum number of jobs that Amazon S3 will include in the List Jobs response. If there are more jobs than this number, the response will include a pagination token in the NextToken field to enable you to retrieve the next page of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.s3control.jobs.list(ctx, account_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.s3control.jobs.list account_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="s3control",
        operation="list_jobs",
        op_kwargs={
            "AccountId": account_id,
            "JobStatuses": job_statuses,
            "NextToken": next_token,
            "MaxResults": max_results,
        },
    )
