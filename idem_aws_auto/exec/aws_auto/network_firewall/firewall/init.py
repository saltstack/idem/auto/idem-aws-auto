"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    firewall_name: str,
    firewall_policy_arn: str,
    vpc_id: str,
    subnet_mappings: List,
    delete_protection: bool = None,
    subnet_change_protection: bool = None,
    firewall_policy_change_protection: bool = None,
    description: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an AWS Network Firewall Firewall and accompanying FirewallStatus for a VPC.  The firewall defines the configuration settings for an AWS Network Firewall firewall. The settings that you can define at creation include the firewall policy, the subnets in your VPC to use for the firewall endpoints, and any tags that are attached to the firewall AWS resource.  After you create a firewall, you can provide additional settings, like the logging configuration.  To update the settings for a firewall, you use the operations that apply to the settings themselves, for example UpdateLoggingConfiguration, AssociateSubnets, and UpdateFirewallDeleteProtection.  To manage a firewall's tags, use the standard AWS resource tagging operations, ListTagsForResource, TagResource, and UntagResource. To retrieve information about firewalls, use ListFirewalls and DescribeFirewall.

    Args:
        firewall_name(str): The descriptive name of the firewall. You can't change the name of a firewall after you create it.
        firewall_policy_arn(str): The Amazon Resource Name (ARN) of the FirewallPolicy that you want to use for the firewall.
        vpc_id(str): The unique identifier of the VPC where Network Firewall should create the firewall.  You can't change this setting after you create the firewall. .
        subnet_mappings(List): The public subnets to use for your Network Firewall firewalls. Each subnet must belong to a different Availability Zone in the VPC. Network Firewall creates a firewall endpoint in each subnet. .
        delete_protection(bool, optional): A flag indicating whether it is possible to delete the firewall. A setting of TRUE indicates that the firewall is protected against deletion. Use this setting to protect against accidentally deleting a firewall that is in use. When you create a firewall, the operation initializes this flag to TRUE. Defaults to None.
        subnet_change_protection(bool, optional): A setting indicating whether the firewall is protected against changes to the subnet associations. Use this setting to protect against accidentally modifying the subnet associations for a firewall that is in use. When you create a firewall, the operation initializes this setting to TRUE. Defaults to None.
        firewall_policy_change_protection(bool, optional): A setting indicating whether the firewall is protected against a change to the firewall policy association. Use this setting to protect against accidentally modifying the firewall policy for a firewall that is in use. When you create a firewall, the operation initializes this setting to TRUE. Defaults to None.
        description(str, optional): A description of the firewall. Defaults to None.
        tags(List, optional): The key:value pairs to associate with the resource. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.network_firewall.firewall.init.create(
                ctx,
                firewall_name=value,
                firewall_policy_arn=value,
                vpc_id=value,
                subnet_mappings=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.network_firewall.firewall.init.create firewall_name=value, firewall_policy_arn=value, vpc_id=value, subnet_mappings=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="network-firewall",
        operation="create_firewall",
        op_kwargs={
            "FirewallName": firewall_name,
            "FirewallPolicyArn": firewall_policy_arn,
            "VpcId": vpc_id,
            "SubnetMappings": subnet_mappings,
            "DeleteProtection": delete_protection,
            "SubnetChangeProtection": subnet_change_protection,
            "FirewallPolicyChangeProtection": firewall_policy_change_protection,
            "Description": description,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, firewall_name: str = None, firewall_arn: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified Firewall and its FirewallStatus. This operation requires the firewall's DeleteProtection flag to be FALSE. You can't revert this operation.  You can check whether a firewall is in use by reviewing the route tables for the Availability Zones where you have firewall subnet mappings. Retrieve the subnet mappings by calling DescribeFirewall. You define and update the route tables through Amazon VPC. As needed, update the route tables for the zones to remove the firewall endpoints. When the route tables no longer use the firewall endpoints, you can remove the firewall safely. To delete a firewall, remove the delete protection if you need to using UpdateFirewallDeleteProtection, then delete the firewall by calling DeleteFirewall.

    Args:
        firewall_name(str, optional): The descriptive name of the firewall. You can't change the name of a firewall after you create it. You must specify the ARN or the name, and you can specify both. . Defaults to None.
        firewall_arn(str, optional): The Amazon Resource Name (ARN) of the firewall. You must specify the ARN or the name, and you can specify both. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.network_firewall.firewall.init.delete(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.network_firewall.firewall.init.delete
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="network-firewall",
        operation="delete_firewall",
        op_kwargs={"FirewallName": firewall_name, "FirewallArn": firewall_arn},
    )


async def describe(
    hub, ctx, firewall_name: str = None, firewall_arn: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the data objects for the specified firewall.

    Args:
        firewall_name(str, optional): The descriptive name of the firewall. You can't change the name of a firewall after you create it. You must specify the ARN or the name, and you can specify both. . Defaults to None.
        firewall_arn(str, optional): The Amazon Resource Name (ARN) of the firewall. You must specify the ARN or the name, and you can specify both. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.network_firewall.firewall.init.describe(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.network_firewall.firewall.init.describe
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="network-firewall",
        operation="describe_firewall",
        op_kwargs={"FirewallName": firewall_name, "FirewallArn": firewall_arn},
    )


async def list_all(
    hub, ctx, next_token: str = None, vpc_ids: List = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the metadata for the firewalls that you have defined. If you provide VPC identifiers in your request, this returns only the firewalls for those VPCs. Depending on your setting for max results and the number of firewalls, a single call might not return the full list.

    Args:
        next_token(str, optional): When you request a list of objects with a MaxResults setting, if the number of objects that are still available for retrieval exceeds the maximum you requested, Network Firewall returns a NextToken value in the response. To retrieve the next batch of objects, use the token returned from the prior request in your next request. Defaults to None.
        vpc_ids(List, optional): The unique identifiers of the VPCs that you want Network Firewall to retrieve the firewalls for. Leave this blank to retrieve all firewalls that you have defined. Defaults to None.
        max_results(int, optional): The maximum number of objects that you want Network Firewall to return for this request. If more objects are available, in the response, Network Firewall provides a NextToken value that you can use in a subsequent call to get the next batch of objects. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.network_firewall.firewall.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.network_firewall.firewall.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="network-firewall",
        operation="list_firewalls",
        op_kwargs={
            "NextToken": next_token,
            "VpcIds": vpc_ids,
            "MaxResults": max_results,
        },
    )
