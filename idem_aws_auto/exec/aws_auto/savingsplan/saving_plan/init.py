"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    savings_plan_offering_id: str,
    commitment: str,
    upfront_payment_amount: str = None,
    purchase_time: str = None,
    client_token: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Savings Plan.

    Args:
        savings_plan_offering_id(str): The ID of the offering.
        commitment(str): The hourly commitment, in USD. This is a value between 0.001 and 1 million. You cannot specify more than three digits after the decimal point.
        upfront_payment_amount(str, optional): The up-front payment amount. This is a whole number between 50 and 99 percent of the total value of the Savings Plan. This parameter is supported only if the payment option is Partial Upfront. Defaults to None.
        purchase_time(str, optional): The time at which to purchase the Savings Plan, in UTC format (YYYY-MM-DDTHH:MM:SSZ). Defaults to None.
        client_token(str, optional): Unique, case-sensitive identifier that you provide to ensure the idempotency of the request. Defaults to None.
        tags(Dict, optional): One or more tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.savingsplan.saving_plan.init.create(
                ctx, savings_plan_offering_id=value, commitment=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.savingsplan.saving_plan.init.create savings_plan_offering_id=value, commitment=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="savingsplans",
        operation="create_savings_plan",
        op_kwargs={
            "savingsPlanOfferingId": savings_plan_offering_id,
            "commitment": commitment,
            "upfrontPaymentAmount": upfront_payment_amount,
            "purchaseTime": purchase_time,
            "clientToken": client_token,
            "tags": tags,
        },
    )


async def describe_all(
    hub,
    ctx,
    savings_plan_arns: List = None,
    savings_plan_ids: List = None,
    next_token: str = None,
    max_results: int = None,
    states: List = None,
    filters: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Describes the specified Savings Plans.

    Args:
        savings_plan_arns(List, optional): The Amazon Resource Names (ARN) of the Savings Plans. Defaults to None.
        savings_plan_ids(List, optional): The IDs of the Savings Plans. Defaults to None.
        next_token(str, optional): The token for the next page of results. Defaults to None.
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve additional results, make another call with the returned token value. Defaults to None.
        states(List, optional): The states. Defaults to None.
        filters(List, optional): The filters. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.savingsplan.saving_plan.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.savingsplan.saving_plan.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="savingsplans",
        operation="describe_savings_plans",
        op_kwargs={
            "savingsPlanArns": savings_plan_arns,
            "savingsPlanIds": savings_plan_ids,
            "nextToken": next_token,
            "maxResults": max_results,
            "states": states,
            "filters": filters,
        },
    )
