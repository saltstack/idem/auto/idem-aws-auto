"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def delete(hub, ctx, savings_plan_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the queued purchase for the specified Savings Plan.

    Args:
        savings_plan_id(str): The ID of the Savings Plan.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.savingsplan.queued_saving_plan.delete(
                ctx, savings_plan_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.savingsplan.queued_saving_plan.delete savings_plan_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="savingsplans",
        operation="delete_queued_savings_plan",
        op_kwargs={"savingsPlanId": savings_plan_id},
    )
