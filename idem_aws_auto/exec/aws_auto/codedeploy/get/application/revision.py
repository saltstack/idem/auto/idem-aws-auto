"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, application_name: str, revisions: List) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about one or more application revisions. The maximum number of application revisions that can be returned is 25.

    Args:
        application_name(str): The name of an AWS CodeDeploy application about which to get revision information.
        revisions(List): An array of RevisionLocation objects that specify information to get about the application revisions, including type and location. The maximum number of RevisionLocation objects you can specify is 25.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.get.application.revision.batch(
                ctx, application_name=value, revisions=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.get.application.revision.batch application_name=value, revisions=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="batch_get_application_revisions",
        op_kwargs={"applicationName": application_name, "revisions": revisions},
    )
