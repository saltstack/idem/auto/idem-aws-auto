"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, deployment_ids: List) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about one or more deployments. The maximum number of deployments that can be returned is 25.

    Args:
        deployment_ids(List):  A list of deployment IDs, separated by spaces. The maximum number of deployment IDs you can specify is 25.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.get.deployment.init.batch(ctx, deployment_ids=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.get.deployment.init.batch deployment_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="batch_get_deployments",
        op_kwargs={"deploymentIds": deployment_ids},
    )
