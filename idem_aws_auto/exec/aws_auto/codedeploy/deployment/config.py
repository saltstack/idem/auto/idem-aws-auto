"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    deployment_config_name: str,
    minimum_healthy_hosts: Dict = None,
    traffic_routing_config: Dict = None,
    compute_platform: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

     Creates a deployment configuration.

    Args:
        deployment_config_name(str): The name of the deployment configuration to create.
        minimum_healthy_hosts(Dict, optional): The minimum number of healthy instances that should be available at any time during the deployment. There are two parameters expected in the input: type and value. The type parameter takes either of the following values:   HOST_COUNT: The value parameter represents the minimum number of healthy instances as an absolute value.   FLEET_PERCENT: The value parameter represents the minimum number of healthy instances as a percentage of the total number of instances in the deployment. If you specify FLEET_PERCENT, at the start of the deployment, AWS CodeDeploy converts the percentage to the equivalent number of instances and rounds up fractional instances.   The value parameter takes an integer. For example, to set a minimum of 95% healthy instance, specify a type of FLEET_PERCENT and a value of 95. Defaults to None.
        traffic_routing_config(Dict, optional): The configuration that specifies how the deployment traffic is routed. Defaults to None.
        compute_platform(str, optional): The destination platform type for the deployment (Lambda, Server, or ECS). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.deployment.config.create(
                ctx, deployment_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.deployment.config.create deployment_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="create_deployment_config",
        op_kwargs={
            "deploymentConfigName": deployment_config_name,
            "minimumHealthyHosts": minimum_healthy_hosts,
            "trafficRoutingConfig": traffic_routing_config,
            "computePlatform": compute_platform,
        },
    )


async def delete(hub, ctx, deployment_config_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a deployment configuration.  A deployment configuration cannot be deleted if it is currently in use. Predefined configurations cannot be deleted.

    Args:
        deployment_config_name(str): The name of a deployment configuration associated with the IAM user or AWS account.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.deployment.config.delete(
                ctx, deployment_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.deployment.config.delete deployment_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="delete_deployment_config",
        op_kwargs={"deploymentConfigName": deployment_config_name},
    )


async def get(hub, ctx, deployment_config_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a deployment configuration.

    Args:
        deployment_config_name(str): The name of a deployment configuration associated with the IAM user or AWS account.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.deployment.config.get(
                ctx, deployment_config_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.deployment.config.get deployment_config_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="get_deployment_config",
        op_kwargs={"deploymentConfigName": deployment_config_name},
    )


async def list_all(hub, ctx, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the deployment configurations with the IAM user or AWS account.

    Args:
        next_token(str, optional): An identifier returned from the previous ListDeploymentConfigs call. It can be used to return the next set of deployment configurations in the list. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.deployment.config.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.deployment.config.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="list_deployment_configs",
        op_kwargs={"nextToken": next_token},
    )
