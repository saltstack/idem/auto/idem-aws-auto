"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

     Associates the list of tags in the input Tags parameter with the resource identified by the ResourceArn input parameter.

    Args:
        resource_arn(str):  The ARN of a resource, such as a CodeDeploy application or deployment group. .
        tags(List):  A list of tags that TagResource associates with a resource. The resource is identified by the ResourceArn input parameter. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.resource.init.tag(
                ctx, resource_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.resource.init.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="tag_resource",
        op_kwargs={"ResourceArn": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

     Disassociates a resource from a list of tags. The resource is identified by the ResourceArn input parameter. The tags are identified by the list of keys in the TagKeys input parameter.

    Args:
        resource_arn(str):  The Amazon Resource Name (ARN) that specifies from which resource to disassociate the tags with the keys in the TagKeys input parameter. .
        tag_keys(List):  A list of keys of Tag objects. The Tag objects identified by the keys are disassociated from the resource specified by the ResourceArn input parameter. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codedeploy.resource.init.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codedeploy.resource.init.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codedeploy",
        operation="untag_resource",
        op_kwargs={"ResourceArn": resource_arn, "TagKeys": tag_keys},
    )
