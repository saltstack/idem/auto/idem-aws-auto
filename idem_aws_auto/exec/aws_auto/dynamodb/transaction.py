"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def execute(
    hub, ctx, transact_statements: List, client_request_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

     This operation allows you to perform transactional reads or writes on data stored in DynamoDB, using PartiQL.

    Args:
        transact_statements(List):  The list of PartiQL statements representing the transaction to run. .
        client_request_token(str, optional):  Set this value to get remaining results, if NextToken was returned in the statement response. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.dynamodb.transaction.execute(ctx, transact_statements=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.dynamodb.transaction.execute transact_statements=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="dynamodb",
        operation="execute_transaction",
        op_kwargs={
            "TransactStatements": transact_statements,
            "ClientRequestToken": client_request_token,
        },
    )
