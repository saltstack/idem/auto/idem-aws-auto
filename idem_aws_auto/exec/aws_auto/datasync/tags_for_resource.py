"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub, ctx, resource_arn: str, max_results: int = None, next_token: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns all the tags associated with a specified resource.

    Args:
        resource_arn(str): The Amazon Resource Name (ARN) of the resource whose tags to list.
        max_results(int, optional): The maximum number of locations to return. Defaults to None.
        next_token(str, optional): An opaque string that indicates the position at which to begin the next list of locations. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.datasync.tags_for_resource.list(ctx, resource_arn=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.datasync.tags_for_resource.list resource_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="datasync",
        operation="list_tags_for_resource",
        op_kwargs={
            "ResourceArn": resource_arn,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
