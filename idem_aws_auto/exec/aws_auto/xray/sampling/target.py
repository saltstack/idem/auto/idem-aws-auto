"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get_all(hub, ctx, sampling_statistics_documents: List) -> Dict:
    r"""
    **Autogenerated function**

    Requests a sampling quota for rules that the service is using to sample requests.

    Args:
        sampling_statistics_documents(List): Information about rules that the service is using to sample requests.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.xray.sampling.target.get_all(
                ctx, sampling_statistics_documents=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.xray.sampling.target.get_all sampling_statistics_documents=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="xray",
        operation="get_sampling_targets",
        op_kwargs={"SamplingStatisticsDocuments": sampling_statistics_documents},
    )
