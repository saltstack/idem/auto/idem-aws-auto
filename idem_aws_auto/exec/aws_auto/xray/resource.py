"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: List) -> Dict:
    r"""
    **Autogenerated function**

    Applies tags to an existing AWS X-Ray group or sampling rule.

    Args:
        resource_arn(str): The Amazon Resource Number (ARN) of an X-Ray group or sampling rule.
        tags(List): A map that contains one or more tag keys and tag values to attach to an X-Ray group or sampling rule. For more information about ways to use tags, see Tagging AWS resources in the AWS General Reference. The following restrictions apply to tags:   Maximum number of user-applied tags per resource: 50   Maximum tag key length: 128 Unicode characters   Maximum tag value length: 256 Unicode characters   Valid values for key and value: a-z, A-Z, 0-9, space, and the following characters: _ . : / = + - and @   Tag keys and values are case sensitive.   Don't use aws: as a prefix for keys; it's reserved for AWS use. You cannot edit or delete system tags.  .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.xray.resource.tag(ctx, resource_arn=value, tags=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.xray.resource.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="xray",
        operation="tag_resource",
        op_kwargs={"ResourceARN": resource_arn, "Tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes tags from an AWS X-Ray group or sampling rule. You cannot edit or delete system tags (those with an aws: prefix).

    Args:
        resource_arn(str): The Amazon Resource Number (ARN) of an X-Ray group or sampling rule.
        tag_keys(List): Keys for one or more tags that you want to remove from an X-Ray group or sampling rule.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.xray.resource.untag(ctx, resource_arn=value, tag_keys=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.xray.resource.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="xray",
        operation="untag_resource",
        op_kwargs={"ResourceARN": resource_arn, "TagKeys": tag_keys},
    )
