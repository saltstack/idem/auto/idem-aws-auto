"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, deployment_id: str, group_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the status of a deployment.

    Args:
        deployment_id(str): The ID of the deployment.
        group_id(str): The ID of the Greengrass group.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.deployment.status.get(
                ctx, deployment_id=value, group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.deployment.status.get deployment_id=value, group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="get_deployment_status",
        op_kwargs={"DeploymentId": deployment_id, "GroupId": group_id},
    )
