"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, thing_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Get the runtime configuration of a thing.

    Args:
        thing_name(str): The thing name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.thing_runtime_configuration.get(
                ctx, thing_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.thing_runtime_configuration.get thing_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="get_thing_runtime_configuration",
        op_kwargs={"ThingName": thing_name},
    )


async def update(
    hub, ctx, thing_name: str, telemetry_configuration: Dict = None
) -> Dict:
    r"""
    **Autogenerated function**

    Updates the runtime configuration of a thing.

    Args:
        telemetry_configuration(Dict, optional): Configuration for telemetry service. Defaults to None.
        thing_name(str): The thing name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.thing_runtime_configuration.update(
                ctx, thing_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.thing_runtime_configuration.update thing_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="update_thing_runtime_configuration",
        op_kwargs={
            "TelemetryConfiguration": telemetry_configuration,
            "ThingName": thing_name,
        },
    )
