"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def create(
    hub,
    ctx,
    amzn_client_token: str = None,
    initial_version: Dict = None,
    name: str = None,
    tags: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a connector definition. You may provide the initial version of the connector definition now or use ''CreateConnectorDefinitionVersion'' at a later time.

    Args:
        amzn_client_token(str, optional): A client token used to correlate requests and responses. Defaults to None.
        initial_version(Dict, optional): Information about the initial version of the connector definition. Defaults to None.
        name(str, optional): The name of the connector definition. Defaults to None.
        tags(Dict, optional): Tag(s) to add to the new resource. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connector_definition.init.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connector_definition.init.create
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="create_connector_definition",
        op_kwargs={
            "AmznClientToken": amzn_client_token,
            "InitialVersion": initial_version,
            "Name": name,
            "tags": tags,
        },
    )


async def delete(hub, ctx, connector_definition_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a connector definition.

    Args:
        connector_definition_id(str): The ID of the connector definition.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connector_definition.init.delete(
                ctx, connector_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connector_definition.init.delete connector_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="delete_connector_definition",
        op_kwargs={"ConnectorDefinitionId": connector_definition_id},
    )


async def get(hub, ctx, connector_definition_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves information about a connector definition.

    Args:
        connector_definition_id(str): The ID of the connector definition.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connector_definition.init.get(
                ctx, connector_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connector_definition.init.get connector_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="get_connector_definition",
        op_kwargs={"ConnectorDefinitionId": connector_definition_id},
    )


async def list_all(hub, ctx, max_results: str = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a list of connector definitions.

    Args:
        max_results(str, optional): The maximum number of results to be returned per request. Defaults to None.
        next_token(str, optional): The token for the next set of results, or ''null'' if there are no additional results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connector_definition.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connector_definition.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="list_connector_definitions",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(hub, ctx, connector_definition_id: str, name: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates a connector definition.

    Args:
        connector_definition_id(str): The ID of the connector definition.
        name(str, optional): The name of the definition. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connector_definition.init.update(
                ctx, connector_definition_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connector_definition.init.update connector_definition_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="update_connector_definition",
        op_kwargs={"ConnectorDefinitionId": connector_definition_id, "Name": name},
    )
