"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(hub, ctx, thing_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the connectivity information for a core.

    Args:
        thing_name(str): The thing name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connectivity_info.get(ctx, thing_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connectivity_info.get thing_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="get_connectivity_info",
        op_kwargs={"ThingName": thing_name},
    )


async def update(hub, ctx, thing_name: str, connectivity_info: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates the connectivity information for the core. Any devices that belong to the group which has this core will receive this information in order to find the location of the core and connect to it.

    Args:
        connectivity_info(List, optional): A list of connectivity info. Defaults to None.
        thing_name(str): The thing name.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.greengrass.connectivity_info.update(ctx, thing_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.greengrass.connectivity_info.update thing_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="greengrass",
        operation="update_connectivity_info",
        op_kwargs={"ConnectivityInfo": connectivity_info, "ThingName": thing_name},
    )
