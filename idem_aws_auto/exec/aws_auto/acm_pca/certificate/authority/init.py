"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    certificate_authority_configuration: Dict,
    certificate_authority_type: str,
    revocation_configuration: Dict = None,
    idempotency_token: str = None,
    key_storage_security_standard: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a root or subordinate private certificate authority (CA). You must specify the CA configuration, the certificate revocation list (CRL) configuration, the CA type, and an optional idempotency token to avoid accidental creation of multiple CAs. The CA configuration specifies the name of the algorithm and key size to be used to create the CA private key, the type of signing algorithm that the CA uses, and X.500 subject information. The CRL configuration specifies the CRL expiration period in days (the validity period of the CRL), the Amazon S3 bucket that will contain the CRL, and a CNAME alias for the S3 bucket that is included in certificates issued by the CA. If successful, this action returns the Amazon Resource Name (ARN) of the CA. ACM Private CA assets that are stored in Amazon S3 can be protected with encryption. For more information, see Encrypting Your CRLs.  Both PCA and the IAM principal must have permission to write to the S3 bucket that you specify. If the IAM principal making the call does not have permission to write to the bucket, then an exception is thrown. For more information, see Configure Access to ACM Private CA.

    Args:
        certificate_authority_configuration(Dict): Name and bit size of the private key algorithm, the name of the signing algorithm, and X.500 certificate subject information.
        revocation_configuration(Dict, optional): Contains a Boolean value that you can use to enable a certification revocation list (CRL) for the CA, the name of the S3 bucket to which ACM Private CA will write the CRL, and an optional CNAME alias that you can use to hide the name of your bucket in the CRL Distribution Points extension of your CA certificate. For more information, see the CrlConfiguration structure. . Defaults to None.
        certificate_authority_type(str): The type of the certificate authority.
        idempotency_token(str, optional): Custom string that can be used to distinguish between calls to the CreateCertificateAuthority action. Idempotency tokens for CreateCertificateAuthority time out after five minutes. Therefore, if you call CreateCertificateAuthority multiple times with the same idempotency token within five minutes, ACM Private CA recognizes that you are requesting only certificate authority and will issue only one. If you change the idempotency token for each call, PCA recognizes that you are requesting multiple certificate authorities. Defaults to None.
        key_storage_security_standard(str, optional): Specifies a cryptographic key management compliance standard used for handling CA keys. Default: FIPS_140_2_LEVEL_3_OR_HIGHER Note: FIPS_140_2_LEVEL_3_OR_HIGHER is not supported in Region ap-northeast-3. When creating a CA in the ap-northeast-3, you must provide FIPS_140_2_LEVEL_2_OR_HIGHER as the argument for KeyStorageSecurityStandard. Failure to do this results in an InvalidArgsException with the message, "A certificate authority cannot be created in this region with the specified security standard.". Defaults to None.
        tags(List, optional): Key-value pairs that will be attached to the new private CA. You can associate up to 50 tags with a private CA. For information using tags with IAM to manage permissions, see Controlling Access Using IAM Tags. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.create(
                ctx, certificate_authority_configuration=value, certificate_authority_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.create certificate_authority_configuration=value, certificate_authority_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="create_certificate_authority",
        op_kwargs={
            "CertificateAuthorityConfiguration": certificate_authority_configuration,
            "RevocationConfiguration": revocation_configuration,
            "CertificateAuthorityType": certificate_authority_type,
            "IdempotencyToken": idempotency_token,
            "KeyStorageSecurityStandard": key_storage_security_standard,
            "Tags": tags,
        },
    )


async def delete(
    hub,
    ctx,
    certificate_authority_arn: str,
    permanent_deletion_time_in_days: int = None,
) -> None:
    r"""
    **Autogenerated function**

    Deletes a private certificate authority (CA). You must provide the Amazon Resource Name (ARN) of the private CA that you want to delete. You can find the ARN by calling the ListCertificateAuthorities action.   Deleting a CA will invalidate other CAs and certificates below it in your CA hierarchy.  Before you can delete a CA that you have created and activated, you must disable it. To do this, call the UpdateCertificateAuthority action and set the CertificateAuthorityStatus parameter to DISABLED.  Additionally, you can delete a CA if you are waiting for it to be created (that is, the status of the CA is CREATING). You can also delete it if the CA has been created but you haven't yet imported the signed certificate into ACM Private CA (that is, the status of the CA is PENDING_CERTIFICATE).  When you successfully call DeleteCertificateAuthority, the CA's status changes to DELETED. However, the CA won't be permanently deleted until the restoration period has passed. By default, if you do not set the PermanentDeletionTimeInDays parameter, the CA remains restorable for 30 days. You can set the parameter from 7 to 30 days. The DescribeCertificateAuthority action returns the time remaining in the restoration window of a private CA in the DELETED state. To restore an eligible CA, call the RestoreCertificateAuthority action.

    Args:
        certificate_authority_arn(str): The Amazon Resource Name (ARN) that was returned when you called CreateCertificateAuthority. This must have the following form:   arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012 . .
        permanent_deletion_time_in_days(int, optional): The number of days to make a CA restorable after it has been deleted. This can be anywhere from 7 to 30 days, with 30 being the default. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.delete(
                ctx, certificate_authority_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.delete certificate_authority_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="delete_certificate_authority",
        op_kwargs={
            "CertificateAuthorityArn": certificate_authority_arn,
            "PermanentDeletionTimeInDays": permanent_deletion_time_in_days,
        },
    )


async def describe(hub, ctx, certificate_authority_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Lists information about your private certificate authority (CA) or one that has been shared with you. You specify the private CA on input by its ARN (Amazon Resource Name). The output contains the status of your CA. This can be any of the following:     CREATING - ACM Private CA is creating your private certificate authority.    PENDING_CERTIFICATE - The certificate is pending. You must use your ACM Private CA-hosted or on-premises root or subordinate CA to sign your private CA CSR and then import it into PCA.     ACTIVE - Your private CA is active.    DISABLED - Your private CA has been disabled.    EXPIRED - Your private CA certificate has expired.    FAILED - Your private CA has failed. Your CA can fail because of problems such a network outage or back-end AWS failure or other errors. A failed CA can never return to the pending state. You must create a new CA.     DELETED - Your private CA is within the restoration period, after which it is permanently deleted. The length of time remaining in the CA's restoration period is also included in this action's output.

    Args:
        certificate_authority_arn(str): The Amazon Resource Name (ARN) that was returned when you called CreateCertificateAuthority. This must be of the form:   arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012 . .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.describe(
                ctx, certificate_authority_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.describe certificate_authority_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="describe_certificate_authority",
        op_kwargs={"CertificateAuthorityArn": certificate_authority_arn},
    )


async def list_all(
    hub,
    ctx,
    next_token: str = None,
    max_results: int = None,
    resource_owner: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the private certificate authorities that you created by using the CreateCertificateAuthority action.

    Args:
        next_token(str, optional): Use this parameter when paginating results in a subsequent request after you receive a response with truncated results. Set it to the value of the NextToken parameter from the response you just received. Defaults to None.
        max_results(int, optional): Use this parameter when paginating results to specify the maximum number of items to return in the response on each page. If additional items exist beyond the number you specify, the NextToken element is sent in the response. Use this NextToken value in a subsequent request to retrieve additional items. Defaults to None.
        resource_owner(str, optional): Use this parameter to filter the returned set of certificate authorities based on their owner. The default is SELF. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="list_certificate_authorities",
        op_kwargs={
            "NextToken": next_token,
            "MaxResults": max_results,
            "ResourceOwner": resource_owner,
        },
    )


async def restore(hub, ctx, certificate_authority_arn: str) -> None:
    r"""
    **Autogenerated function**

    Restores a certificate authority (CA) that is in the DELETED state. You can restore a CA during the period that you defined in the PermanentDeletionTimeInDays parameter of the DeleteCertificateAuthority action. Currently, you can specify 7 to 30 days. If you did not specify a PermanentDeletionTimeInDays value, by default you can restore the CA at any time in a 30 day period. You can check the time remaining in the restoration period of a private CA in the DELETED state by calling the DescribeCertificateAuthority or ListCertificateAuthorities actions. The status of a restored CA is set to its pre-deletion status when the RestoreCertificateAuthority action returns. To change its status to ACTIVE, call the UpdateCertificateAuthority action. If the private CA was in the PENDING_CERTIFICATE state at deletion, you must use the ImportCertificateAuthorityCertificate action to import a certificate authority into the private CA before it can be activated. You cannot restore a CA after the restoration period has ended.

    Args:
        certificate_authority_arn(str): The Amazon Resource Name (ARN) that was returned when you called the CreateCertificateAuthority action. This must be of the form:   arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012  .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.restore(
                ctx, certificate_authority_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.restore certificate_authority_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="restore_certificate_authority",
        op_kwargs={"CertificateAuthorityArn": certificate_authority_arn},
    )


async def tag(hub, ctx, certificate_authority_arn: str, tags: List) -> None:
    r"""
    **Autogenerated function**

    Adds one or more tags to your private CA. Tags are labels that you can use to identify and organize your AWS resources. Each tag consists of a key and an optional value. You specify the private CA on input by its Amazon Resource Name (ARN). You specify the tag by using a key-value pair. You can apply a tag to just one private CA if you want to identify a specific characteristic of that CA, or you can apply the same tag to multiple private CAs if you want to filter for a common relationship among those CAs. To remove one or more tags, use the UntagCertificateAuthority action. Call the ListTags action to see what tags are associated with your CA.

    Args:
        certificate_authority_arn(str): The Amazon Resource Name (ARN) that was returned when you called CreateCertificateAuthority. This must be of the form:   arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012  .
        tags(List): List of tags to be associated with the CA.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.tag(
                ctx, certificate_authority_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.tag certificate_authority_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="tag_certificate_authority",
        op_kwargs={"CertificateAuthorityArn": certificate_authority_arn, "Tags": tags},
    )


async def untag(hub, ctx, certificate_authority_arn: str, tags: List) -> None:
    r"""
    **Autogenerated function**

    Remove one or more tags from your private CA. A tag consists of a key-value pair. If you do not specify the value portion of the tag when calling this action, the tag will be removed regardless of value. If you specify a value, the tag is removed only if it is associated with the specified value. To add tags to a private CA, use the TagCertificateAuthority. Call the ListTags action to see what tags are associated with your CA.

    Args:
        certificate_authority_arn(str): The Amazon Resource Name (ARN) that was returned when you called CreateCertificateAuthority. This must be of the form:   arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012  .
        tags(List): List of tags to be removed from the CA.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.untag(
                ctx, certificate_authority_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.untag certificate_authority_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="untag_certificate_authority",
        op_kwargs={"CertificateAuthorityArn": certificate_authority_arn, "Tags": tags},
    )


async def update(
    hub,
    ctx,
    certificate_authority_arn: str,
    revocation_configuration: Dict = None,
    status: str = None,
) -> None:
    r"""
    **Autogenerated function**

    Updates the status or configuration of a private certificate authority (CA). Your private CA must be in the ACTIVE or DISABLED state before you can update it. You can disable a private CA that is in the ACTIVE state or make a CA that is in the DISABLED state active again.  Both PCA and the IAM principal must have permission to write to the S3 bucket that you specify. If the IAM principal making the call does not have permission to write to the bucket, then an exception is thrown. For more information, see Configure Access to ACM Private CA.

    Args:
        certificate_authority_arn(str): Amazon Resource Name (ARN) of the private CA that issued the certificate to be revoked. This must be of the form:  arn:aws:acm-pca:region:account:certificate-authority/12345678-1234-1234-1234-123456789012  .
        revocation_configuration(Dict, optional): Revocation information for your private CA. Defaults to None.
        status(str, optional): Status of your private CA. Defaults to None.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm_pca.certificate.authority.init.update(
                ctx, certificate_authority_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm_pca.certificate.authority.init.update certificate_authority_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm-pca",
        operation="update_certificate_authority",
        op_kwargs={
            "CertificateAuthorityArn": certificate_authority_arn,
            "RevocationConfiguration": revocation_configuration,
            "Status": status,
        },
    )
