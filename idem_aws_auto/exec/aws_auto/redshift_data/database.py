"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub,
    ctx,
    cluster_identifier: str,
    database: str,
    db_user: str = None,
    max_results: int = None,
    next_token: str = None,
    secret_arn: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    List the databases in a cluster. A token is returned to page through the database list. Depending on the authorization method, use one of the following combinations of request parameters:    Secrets Manager - specify the Amazon Resource Name (ARN) of the secret, the database name, and the cluster identifier that matches the cluster in the secret.    Temporary credentials - specify the cluster identifier, the database name, and the database user name. Permission to call the redshift:GetClusterCredentials operation is required to use this method.

    Args:
        cluster_identifier(str): The cluster identifier. This parameter is required when authenticating using either Secrets Manager or temporary credentials. .
        database(str): The name of the database. This parameter is required when authenticating using either Secrets Manager or temporary credentials. .
        db_user(str, optional): The database user name. This parameter is required when authenticating using temporary credentials. . Defaults to None.
        max_results(int, optional): The maximum number of databases to return in the response. If more databases exist than fit in one response, then NextToken is returned to page through the results. . Defaults to None.
        next_token(str, optional): A value that indicates the starting point for the next set of response records in a subsequent request. If a value is returned in a response, you can retrieve the next set of records by providing this returned NextToken value in the next NextToken parameter and retrying the command. If the NextToken field is empty, all response records have been retrieved for the request. . Defaults to None.
        secret_arn(str, optional): The name or ARN of the secret that enables access to the database. This parameter is required when authenticating using Secrets Manager. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.redshift_data.database.list_all(
                ctx, cluster_identifier=value, database=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.redshift_data.database.list_all cluster_identifier=value, database=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="redshift-data",
        operation="list_databases",
        op_kwargs={
            "ClusterIdentifier": cluster_identifier,
            "Database": database,
            "DbUser": db_user,
            "MaxResults": max_results,
            "NextToken": next_token,
            "SecretArn": secret_arn,
        },
    )
