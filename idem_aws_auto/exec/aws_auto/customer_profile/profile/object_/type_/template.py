"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, template_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Returns the template information for a specific object type. A template is a predefined ProfileObjectType, such as “Salesforce-Account” or “Salesforce-Contact.” When a user sends a ProfileObject, using the PutProfileObject API, with an ObjectTypeName that matches one of the TemplateIds, it uses the mappings from the template.

    Args:
        template_id(str): A unique identifier for the object template.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.customer_profile.profile.object_.type_.template.get(
                ctx, template_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.customer_profile.profile.object_.type_.template.get template_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="customer-profiles",
        operation="get_profile_object_type_template",
        op_kwargs={"TemplateId": template_id},
    )


async def list_all(hub, ctx, next_token: str = None, max_results: int = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists all of the template information for object types.

    Args:
        next_token(str, optional): The pagination token from the previous ListObjectTypeTemplates API call. Defaults to None.
        max_results(int, optional): The maximum number of objects returned per page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.customer_profile.profile.object_.type_.template.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.customer_profile.profile.object_.type_.template.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="customer-profiles",
        operation="list_profile_object_type_templates",
        op_kwargs={"NextToken": next_token, "MaxResults": max_results},
    )
