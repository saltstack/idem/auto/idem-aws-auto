"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, uri: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all of the integrations associated to a specific URI in the AWS account.

    Args:
        uri(str): The URI of the S3 bucket or any other type of data source.
        next_token(str, optional): The pagination token from the previous ListAccountIntegrations API call. Defaults to None.
        max_results(int, optional): The maximum number of objects returned per page. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.customer_profile.account_integration.list_all(ctx, uri=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.customer_profile.account_integration.list_all uri=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="customer-profiles",
        operation="list_account_integrations",
        op_kwargs={"Uri": uri, "NextToken": next_token, "MaxResults": max_results},
    )
