"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    replication_group_id: str,
    replication_group_description: str,
    global_replication_group_id: str = None,
    primary_cluster_id: str = None,
    automatic_failover_enabled: bool = None,
    multi_az_enabled: bool = None,
    num_cache_clusters: int = None,
    preferred_cache_cluster_a_zs: List = None,
    num_node_groups: int = None,
    replicas_per_node_group: int = None,
    node_group_configuration: List = None,
    cache_node_type: str = None,
    engine: str = None,
    engine_version: str = None,
    cache_parameter_group_name: str = None,
    cache_subnet_group_name: str = None,
    cache_security_group_names: List = None,
    security_group_ids: List = None,
    tags: List = None,
    snapshot_arns: List = None,
    snapshot_name: str = None,
    preferred_maintenance_window: str = None,
    port: int = None,
    notification_topic_arn: str = None,
    auto_minor_version_upgrade: bool = None,
    snapshot_retention_limit: int = None,
    snapshot_window: str = None,
    auth_token: str = None,
    transit_encryption_enabled: bool = None,
    at_rest_encryption_enabled: bool = None,
    kms_key_id: str = None,
    user_group_ids: List = None,
    log_delivery_configurations: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Redis (cluster mode disabled) or a Redis (cluster mode enabled) replication group. This API can be used to create a standalone regional replication group or a secondary replication group associated with a Global datastore. A Redis (cluster mode disabled) replication group is a collection of clusters, where one of the clusters is a read/write primary and the others are read-only replicas. Writes to the primary are asynchronously propagated to the replicas. A Redis cluster-mode enabled cluster is comprised of from 1 to 90 shards (API/CLI: node groups). Each shard has a primary node and up to 5 read-only replica nodes. The configuration can range from 90 shards and 0 replicas to 15 shards and 5 replicas, which is the maximum number or replicas allowed.  The node or shard limit can be increased to a maximum of 500 per cluster if the Redis engine version is 5.0.6 or higher. For example, you can choose to configure a 500 node cluster that ranges between 83 shards (one primary and 5 replicas per shard) and 500 shards (single primary and no replicas). Make sure there are enough available IP addresses to accommodate the increase. Common pitfalls include the subnets in the subnet group have too small a CIDR range or the subnets are shared and heavily used by other clusters. For more information, see Creating a Subnet Group. For versions below 5.0.6, the limit is 250 per cluster. To request a limit increase, see AWS Service Limits and choose the limit type Nodes per cluster per instance type.  When a Redis (cluster mode disabled) replication group has been successfully created, you can add one or more read replicas to it, up to a total of 5 read replicas. If you need to increase or decrease the number of node groups (console: shards), you can avail yourself of ElastiCache for Redis' scaling. For more information, see Scaling ElastiCache for Redis Clusters in the ElastiCache User Guide.  This operation is valid for Redis only.

    Args:
        replication_group_id(str): The replication group identifier. This parameter is stored as a lowercase string. Constraints:   A name must contain from 1 to 40 alphanumeric characters or hyphens.   The first character must be a letter.   A name cannot end with a hyphen or contain two consecutive hyphens.  .
        replication_group_description(str): A user-created description for the replication group.
        global_replication_group_id(str, optional): The name of the Global datastore. Defaults to None.
        primary_cluster_id(str, optional): The identifier of the cluster that serves as the primary for this replication group. This cluster must already exist and have a status of available. This parameter is not required if NumCacheClusters, NumNodeGroups, or ReplicasPerNodeGroup is specified. Defaults to None.
        automatic_failover_enabled(bool, optional): Specifies whether a read-only replica is automatically promoted to read/write primary if the existing primary fails.  AutomaticFailoverEnabled must be enabled for Redis (cluster mode enabled) replication groups. Default: false. Defaults to None.
        multi_az_enabled(bool, optional): A flag indicating if you have Multi-AZ enabled to enhance fault tolerance. For more information, see Minimizing Downtime: Multi-AZ. Defaults to None.
        num_cache_clusters(int, optional): The number of clusters this replication group initially has. This parameter is not used if there is more than one node group (shard). You should use ReplicasPerNodeGroup instead. If AutomaticFailoverEnabled is true, the value of this parameter must be at least 2. If AutomaticFailoverEnabled is false you can omit this parameter (it will default to 1), or you can explicitly set it to a value between 2 and 6. The maximum permitted value for NumCacheClusters is 6 (1 primary plus 5 replicas). Defaults to None.
        preferred_cache_cluster_a_zs(List, optional): A list of EC2 Availability Zones in which the replication group's clusters are created. The order of the Availability Zones in the list is the order in which clusters are allocated. The primary cluster is created in the first AZ in the list. This parameter is not used if there is more than one node group (shard). You should use NodeGroupConfiguration instead.  If you are creating your replication group in an Amazon VPC (recommended), you can only locate clusters in Availability Zones associated with the subnets in the selected subnet group. The number of Availability Zones listed must equal the value of NumCacheClusters.  Default: system chosen Availability Zones. Defaults to None.
        num_node_groups(int, optional): An optional parameter that specifies the number of node groups (shards) for this Redis (cluster mode enabled) replication group. For Redis (cluster mode disabled) either omit this parameter or set it to 1. Default: 1. Defaults to None.
        replicas_per_node_group(int, optional): An optional parameter that specifies the number of replica nodes in each node group (shard). Valid values are 0 to 5. Defaults to None.
        node_group_configuration(List, optional): A list of node group (shard) configuration options. Each node group (shard) configuration has the following members: PrimaryAvailabilityZone, ReplicaAvailabilityZones, ReplicaCount, and Slots. If you're creating a Redis (cluster mode disabled) or a Redis (cluster mode enabled) replication group, you can use this parameter to individually configure each node group (shard), or you can omit this parameter. However, it is required when seeding a Redis (cluster mode enabled) cluster from a S3 rdb file. You must configure each node group (shard) using this parameter because you must specify the slots for each node group. Defaults to None.
        cache_node_type(str, optional): The compute and memory capacity of the nodes in the node group (shard). The following node types are supported by ElastiCache. Generally speaking, the current generation types provide more memory and computational power at lower cost when compared to their equivalent previous generation counterparts.   General purpose:   Current generation:   M6g node types (available only for Redis engine version 5.0.6 onward and for Memcached engine version 1.5.16 onward).  cache.m6g.large, cache.m6g.xlarge, cache.m6g.2xlarge, cache.m6g.4xlarge, cache.m6g.8xlarge, cache.m6g.12xlarge, cache.m6g.16xlarge   For region availability, see Supported Node Types    M5 node types: cache.m5.large, cache.m5.xlarge, cache.m5.2xlarge, cache.m5.4xlarge, cache.m5.12xlarge, cache.m5.24xlarge   M4 node types: cache.m4.large, cache.m4.xlarge, cache.m4.2xlarge, cache.m4.4xlarge, cache.m4.10xlarge   T3 node types: cache.t3.micro, cache.t3.small, cache.t3.medium   T2 node types: cache.t2.micro, cache.t2.small, cache.t2.medium    Previous generation: (not recommended)  T1 node types: cache.t1.micro   M1 node types: cache.m1.small, cache.m1.medium, cache.m1.large, cache.m1.xlarge   M3 node types: cache.m3.medium, cache.m3.large, cache.m3.xlarge, cache.m3.2xlarge      Compute optimized:   Previous generation: (not recommended)  C1 node types: cache.c1.xlarge      Memory optimized:   Current generation:   R6g node types (available only for Redis engine version 5.0.6 onward and for Memcached engine version 1.5.16 onward).  cache.r6g.large, cache.r6g.xlarge, cache.r6g.2xlarge, cache.r6g.4xlarge, cache.r6g.8xlarge, cache.r6g.12xlarge, cache.r6g.16xlarge   For region availability, see Supported Node Types    R5 node types: cache.r5.large, cache.r5.xlarge, cache.r5.2xlarge, cache.r5.4xlarge, cache.r5.12xlarge, cache.r5.24xlarge   R4 node types: cache.r4.large, cache.r4.xlarge, cache.r4.2xlarge, cache.r4.4xlarge, cache.r4.8xlarge, cache.r4.16xlarge    Previous generation: (not recommended)  M2 node types: cache.m2.xlarge, cache.m2.2xlarge, cache.m2.4xlarge   R3 node types: cache.r3.large, cache.r3.xlarge, cache.r3.2xlarge, cache.r3.4xlarge, cache.r3.8xlarge       Additional node type info    All current generation instance types are created in Amazon VPC by default.   Redis append-only files (AOF) are not supported for T1 or T2 instances.   Redis Multi-AZ with automatic failover is not supported on T1 instances.   Redis configuration variables appendonly and appendfsync are not supported on Redis version 2.8.22 and later.  . Defaults to None.
        engine(str, optional): The name of the cache engine to be used for the clusters in this replication group. Must be Redis. Defaults to None.
        engine_version(str, optional): The version number of the cache engine to be used for the clusters in this replication group. To view the supported cache engine versions, use the DescribeCacheEngineVersions operation.  Important: You can upgrade to a newer engine version (see Selecting a Cache Engine and Version) in the ElastiCache User Guide, but you cannot downgrade to an earlier engine version. If you want to use an earlier engine version, you must delete the existing cluster or replication group and create it anew with the earlier engine version. . Defaults to None.
        cache_parameter_group_name(str, optional): The name of the parameter group to associate with this replication group. If this argument is omitted, the default cache parameter group for the specified engine is used. If you are running Redis version 3.2.4 or later, only one node group (shard), and want to use a default parameter group, we recommend that you specify the parameter group by name.    To create a Redis (cluster mode disabled) replication group, use CacheParameterGroupName=default.redis3.2.   To create a Redis (cluster mode enabled) replication group, use CacheParameterGroupName=default.redis3.2.cluster.on.  . Defaults to None.
        cache_subnet_group_name(str, optional): The name of the cache subnet group to be used for the replication group.  If you're going to launch your cluster in an Amazon VPC, you need to create a subnet group before you start creating a cluster. For more information, see Subnets and Subnet Groups. . Defaults to None.
        cache_security_group_names(List, optional): A list of cache security group names to associate with this replication group. Defaults to None.
        security_group_ids(List, optional): One or more Amazon VPC security groups associated with this replication group. Use this parameter only when you are creating a replication group in an Amazon Virtual Private Cloud (Amazon VPC). Defaults to None.
        tags(List, optional): A list of tags to be added to this resource. Tags are comma-separated key,value pairs (e.g. Key=myKey, Value=myKeyValue. You can include multiple tags as shown following: Key=myKey, Value=myKeyValue Key=mySecondKey, Value=mySecondKeyValue. Tags on replication groups will be replicated to all nodes. Defaults to None.
        snapshot_arns(List, optional): A list of Amazon Resource Names (ARN) that uniquely identify the Redis RDB snapshot files stored in Amazon S3. The snapshot files are used to populate the new replication group. The Amazon S3 object name in the ARN cannot contain any commas. The new replication group will have the number of node groups (console: shards) specified by the parameter NumNodeGroups or the number of node groups configured by NodeGroupConfiguration regardless of the number of ARNs specified here. Example of an Amazon S3 ARN: arn:aws:s3:::my_bucket/snapshot1.rdb . Defaults to None.
        snapshot_name(str, optional): The name of a snapshot from which to restore data into the new replication group. The snapshot status changes to restoring while the new replication group is being created. Defaults to None.
        preferred_maintenance_window(str, optional): Specifies the weekly time range during which maintenance on the cluster is performed. It is specified as a range in the format ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period. Valid values for ddd are: Specifies the weekly time range during which maintenance on the cluster is performed. It is specified as a range in the format ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period. Valid values for ddd are:    sun     mon     tue     wed     thu     fri     sat    Example: sun:23:00-mon:01:30 . Defaults to None.
        port(int, optional): The port number on which each member of the replication group accepts connections. Defaults to None.
        notification_topic_arn(str, optional): The Amazon Resource Name (ARN) of the Amazon Simple Notification Service (SNS) topic to which notifications are sent.  The Amazon SNS topic owner must be the same as the cluster owner. . Defaults to None.
        auto_minor_version_upgrade(bool, optional): This parameter is currently disabled. Defaults to None.
        snapshot_retention_limit(int, optional): The number of days for which ElastiCache retains automatic snapshots before deleting them. For example, if you set SnapshotRetentionLimit to 5, a snapshot that was taken today is retained for 5 days before being deleted. Default: 0 (i.e., automatic backups are disabled for this cluster). Defaults to None.
        snapshot_window(str, optional): The daily time range (in UTC) during which ElastiCache begins taking a daily snapshot of your node group (shard). Example: 05:00-09:00  If you do not specify this parameter, ElastiCache automatically chooses an appropriate time range. Defaults to None.
        auth_token(str, optional):  Reserved parameter. The password used to access a password protected server.  AuthToken can be specified only on replication groups where TransitEncryptionEnabled is true.  For HIPAA compliance, you must specify TransitEncryptionEnabled as true, an AuthToken, and a CacheSubnetGroup.  Password constraints:   Must be only printable ASCII characters.   Must be at least 16 characters and no more than 128 characters in length.   The only permitted printable special characters are !, &, #, $, ^, <, >, and -. Other printable special characters cannot be used in the AUTH token.   For more information, see AUTH password at http://redis.io/commands/AUTH. Defaults to None.
        transit_encryption_enabled(bool, optional): A flag that enables in-transit encryption when set to true. You cannot modify the value of TransitEncryptionEnabled after the cluster is created. To enable in-transit encryption on a cluster you must set TransitEncryptionEnabled to true when you create a cluster. This parameter is valid only if the Engine parameter is redis, the EngineVersion parameter is 3.2.6, 4.x or later, and the cluster is being created in an Amazon VPC. If you enable in-transit encryption, you must also specify a value for CacheSubnetGroup.  Required: Only available when creating a replication group in an Amazon VPC using redis version 3.2.6, 4.x or later. Default: false   For HIPAA compliance, you must specify TransitEncryptionEnabled as true, an AuthToken, and a CacheSubnetGroup. . Defaults to None.
        at_rest_encryption_enabled(bool, optional): A flag that enables encryption at rest when set to true. You cannot modify the value of AtRestEncryptionEnabled after the replication group is created. To enable encryption at rest on a replication group you must set AtRestEncryptionEnabled to true when you create the replication group.   Required: Only available when creating a replication group in an Amazon VPC using redis version 3.2.6, 4.x or later. Default: false . Defaults to None.
        kms_key_id(str, optional): The ID of the KMS key used to encrypt the disk in the cluster. Defaults to None.
        user_group_ids(List, optional): The user group to associate with the replication group. Defaults to None.
        log_delivery_configurations(List, optional): Specifies the destination, format and type of the logs. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.replication_group.init.create(
                ctx, replication_group_id=value, replication_group_description=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.replication_group.init.create replication_group_id=value, replication_group_description=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="create_replication_group",
        op_kwargs={
            "ReplicationGroupId": replication_group_id,
            "ReplicationGroupDescription": replication_group_description,
            "GlobalReplicationGroupId": global_replication_group_id,
            "PrimaryClusterId": primary_cluster_id,
            "AutomaticFailoverEnabled": automatic_failover_enabled,
            "MultiAZEnabled": multi_az_enabled,
            "NumCacheClusters": num_cache_clusters,
            "PreferredCacheClusterAZs": preferred_cache_cluster_a_zs,
            "NumNodeGroups": num_node_groups,
            "ReplicasPerNodeGroup": replicas_per_node_group,
            "NodeGroupConfiguration": node_group_configuration,
            "CacheNodeType": cache_node_type,
            "Engine": engine,
            "EngineVersion": engine_version,
            "CacheParameterGroupName": cache_parameter_group_name,
            "CacheSubnetGroupName": cache_subnet_group_name,
            "CacheSecurityGroupNames": cache_security_group_names,
            "SecurityGroupIds": security_group_ids,
            "Tags": tags,
            "SnapshotArns": snapshot_arns,
            "SnapshotName": snapshot_name,
            "PreferredMaintenanceWindow": preferred_maintenance_window,
            "Port": port,
            "NotificationTopicArn": notification_topic_arn,
            "AutoMinorVersionUpgrade": auto_minor_version_upgrade,
            "SnapshotRetentionLimit": snapshot_retention_limit,
            "SnapshotWindow": snapshot_window,
            "AuthToken": auth_token,
            "TransitEncryptionEnabled": transit_encryption_enabled,
            "AtRestEncryptionEnabled": at_rest_encryption_enabled,
            "KmsKeyId": kms_key_id,
            "UserGroupIds": user_group_ids,
            "LogDeliveryConfigurations": log_delivery_configurations,
        },
    )


async def delete(
    hub,
    ctx,
    replication_group_id: str,
    retain_primary_cluster: bool = None,
    final_snapshot_identifier: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an existing replication group. By default, this operation deletes the entire replication group, including the primary/primaries and all of the read replicas. If the replication group has only one primary, you can optionally delete only the read replicas, while retaining the primary by setting RetainPrimaryCluster=true. When you receive a successful response from this operation, Amazon ElastiCache immediately begins deleting the selected resources; you cannot cancel or revert this operation.  This operation is valid for Redis only.

    Args:
        replication_group_id(str): The identifier for the cluster to be deleted. This parameter is not case sensitive.
        retain_primary_cluster(bool, optional): If set to true, all of the read replicas are deleted, but the primary node is retained. Defaults to None.
        final_snapshot_identifier(str, optional): The name of a final node group (shard) snapshot. ElastiCache creates the snapshot from the primary node in the cluster, rather than one of the replicas; this is to ensure that it captures the freshest data. After the final snapshot is taken, the replication group is immediately deleted. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.replication_group.init.delete(
                ctx, replication_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.replication_group.init.delete replication_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="delete_replication_group",
        op_kwargs={
            "ReplicationGroupId": replication_group_id,
            "RetainPrimaryCluster": retain_primary_cluster,
            "FinalSnapshotIdentifier": final_snapshot_identifier,
        },
    )


async def describe_all(
    hub,
    ctx,
    replication_group_id: str = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about a particular replication group. If no identifier is specified, DescribeReplicationGroups returns information about all replication groups.  This operation is valid for Redis only.

    Args:
        replication_group_id(str, optional): The identifier for the replication group to be described. This parameter is not case sensitive. If you do not specify this parameter, information about all replication groups is returned. Defaults to None.
        max_records(int, optional): The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a marker is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: minimum 20; maximum 100. Defaults to None.
        marker(str, optional): An optional marker returned from a prior request. Use this marker for pagination of results from this operation. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.replication_group.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.replication_group.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="describe_replication_groups",
        op_kwargs={
            "ReplicationGroupId": replication_group_id,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub,
    ctx,
    replication_group_id: str,
    replication_group_description: str = None,
    primary_cluster_id: str = None,
    snapshotting_cluster_id: str = None,
    automatic_failover_enabled: bool = None,
    multi_az_enabled: bool = None,
    node_group_id: str = None,
    cache_security_group_names: List = None,
    security_group_ids: List = None,
    preferred_maintenance_window: str = None,
    notification_topic_arn: str = None,
    cache_parameter_group_name: str = None,
    notification_topic_status: str = None,
    apply_immediately: bool = None,
    engine_version: str = None,
    auto_minor_version_upgrade: bool = None,
    snapshot_retention_limit: int = None,
    snapshot_window: str = None,
    cache_node_type: str = None,
    auth_token: str = None,
    auth_token_update_strategy: str = None,
    user_group_ids_to_add: List = None,
    user_group_ids_to_remove: List = None,
    remove_user_groups: bool = None,
    log_delivery_configurations: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies the settings for a replication group.    Scaling for Amazon ElastiCache for Redis (cluster mode enabled) in the ElastiCache User Guide    ModifyReplicationGroupShardConfiguration in the ElastiCache API Reference    This operation is valid for Redis only.

    Args:
        replication_group_id(str): The identifier of the replication group to modify.
        replication_group_description(str, optional): A description for the replication group. Maximum length is 255 characters. Defaults to None.
        primary_cluster_id(str, optional): For replication groups with a single primary, if this parameter is specified, ElastiCache promotes the specified cluster in the specified replication group to the primary role. The nodes of all other clusters in the replication group are read replicas. Defaults to None.
        snapshotting_cluster_id(str, optional): The cluster ID that is used as the daily snapshot source for the replication group. This parameter cannot be set for Redis (cluster mode enabled) replication groups. Defaults to None.
        automatic_failover_enabled(bool, optional): Determines whether a read replica is automatically promoted to read/write primary if the existing primary encounters a failure. Valid values: true | false . Defaults to None.
        multi_az_enabled(bool, optional): A list of tags to be added to this resource. A tag is a key-value pair. A tag key must be accompanied by a tag value, although null is accepted. Defaults to None.
        node_group_id(str, optional): Deprecated. This parameter is not used. Defaults to None.
        cache_security_group_names(List, optional): A list of cache security group names to authorize for the clusters in this replication group. This change is asynchronously applied as soon as possible. This parameter can be used only with replication group containing clusters running outside of an Amazon Virtual Private Cloud (Amazon VPC). Constraints: Must contain no more than 255 alphanumeric characters. Must not be Default. Defaults to None.
        security_group_ids(List, optional): Specifies the VPC Security Groups associated with the clusters in the replication group. This parameter can be used only with replication group containing clusters running in an Amazon Virtual Private Cloud (Amazon VPC). Defaults to None.
        preferred_maintenance_window(str, optional): Specifies the weekly time range during which maintenance on the cluster is performed. It is specified as a range in the format ddd:hh24:mi-ddd:hh24:mi (24H Clock UTC). The minimum maintenance window is a 60 minute period. Valid values for ddd are:    sun     mon     tue     wed     thu     fri     sat    Example: sun:23:00-mon:01:30 . Defaults to None.
        notification_topic_arn(str, optional): The Amazon Resource Name (ARN) of the Amazon SNS topic to which notifications are sent.  The Amazon SNS topic owner must be same as the replication group owner.  . Defaults to None.
        cache_parameter_group_name(str, optional): The name of the cache parameter group to apply to all of the clusters in this replication group. This change is asynchronously applied as soon as possible for parameters when the ApplyImmediately parameter is specified as true for this request. Defaults to None.
        notification_topic_status(str, optional): The status of the Amazon SNS notification topic for the replication group. Notifications are sent only if the status is active. Valid values: active | inactive . Defaults to None.
        apply_immediately(bool, optional): If true, this parameter causes the modifications in this request and any pending modifications to be applied, asynchronously and as soon as possible, regardless of the PreferredMaintenanceWindow setting for the replication group. If false, changes to the nodes in the replication group are applied on the next maintenance reboot, or the next failure reboot, whichever occurs first. Valid values: true | false  Default: false . Defaults to None.
        engine_version(str, optional): The upgraded version of the cache engine to be run on the clusters in the replication group.  Important: You can upgrade to a newer engine version (see Selecting a Cache Engine and Version), but you cannot downgrade to an earlier engine version. If you want to use an earlier engine version, you must delete the existing replication group and create it anew with the earlier engine version. . Defaults to None.
        auto_minor_version_upgrade(bool, optional): This parameter is currently disabled. Defaults to None.
        snapshot_retention_limit(int, optional): The number of days for which ElastiCache retains automatic node group (shard) snapshots before deleting them. For example, if you set SnapshotRetentionLimit to 5, a snapshot that was taken today is retained for 5 days before being deleted.  Important If the value of SnapshotRetentionLimit is set to zero (0), backups are turned off. Defaults to None.
        snapshot_window(str, optional): The daily time range (in UTC) during which ElastiCache begins taking a daily snapshot of the node group (shard) specified by SnapshottingClusterId. Example: 05:00-09:00  If you do not specify this parameter, ElastiCache automatically chooses an appropriate time range. Defaults to None.
        cache_node_type(str, optional): A valid cache node type that you want to scale this replication group to. Defaults to None.
        auth_token(str, optional): Reserved parameter. The password used to access a password protected server. This parameter must be specified with the auth-token-update-strategy  parameter. Password constraints:   Must be only printable ASCII characters   Must be at least 16 characters and no more than 128 characters in length   Cannot contain any of the following characters: '/', '"', or '@', '%'    For more information, see AUTH password at AUTH. Defaults to None.
        auth_token_update_strategy(str, optional): Specifies the strategy to use to update the AUTH token. This parameter must be specified with the auth-token parameter. Possible values:   Rotate   Set    For more information, see Authenticating Users with Redis AUTH . Defaults to None.
        user_group_ids_to_add(List, optional): The user group you are associating with the replication group. Defaults to None.
        user_group_ids_to_remove(List, optional): The user group to remove, meaning the users in the group no longer can access the replication group. Defaults to None.
        remove_user_groups(bool, optional): Removes the user groups that can access this replication group. Defaults to None.
        log_delivery_configurations(List, optional): Specifies the destination, format and type of the logs. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.replication_group.init.modify(
                ctx, replication_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.replication_group.init.modify replication_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="modify_replication_group",
        op_kwargs={
            "ReplicationGroupId": replication_group_id,
            "ReplicationGroupDescription": replication_group_description,
            "PrimaryClusterId": primary_cluster_id,
            "SnapshottingClusterId": snapshotting_cluster_id,
            "AutomaticFailoverEnabled": automatic_failover_enabled,
            "MultiAZEnabled": multi_az_enabled,
            "NodeGroupId": node_group_id,
            "CacheSecurityGroupNames": cache_security_group_names,
            "SecurityGroupIds": security_group_ids,
            "PreferredMaintenanceWindow": preferred_maintenance_window,
            "NotificationTopicArn": notification_topic_arn,
            "CacheParameterGroupName": cache_parameter_group_name,
            "NotificationTopicStatus": notification_topic_status,
            "ApplyImmediately": apply_immediately,
            "EngineVersion": engine_version,
            "AutoMinorVersionUpgrade": auto_minor_version_upgrade,
            "SnapshotRetentionLimit": snapshot_retention_limit,
            "SnapshotWindow": snapshot_window,
            "CacheNodeType": cache_node_type,
            "AuthToken": auth_token,
            "AuthTokenUpdateStrategy": auth_token_update_strategy,
            "UserGroupIdsToAdd": user_group_ids_to_add,
            "UserGroupIdsToRemove": user_group_ids_to_remove,
            "RemoveUserGroups": remove_user_groups,
            "LogDeliveryConfigurations": log_delivery_configurations,
        },
    )
