"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(
    hub,
    ctx,
    cache_parameter_group_name: str,
    source: str = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns the detailed parameter list for a particular cache parameter group.

    Args:
        cache_parameter_group_name(str): The name of a specific cache parameter group to return details for.
        source(str, optional): The parameter types to return. Valid values: user | system | engine-default . Defaults to None.
        max_records(int, optional): The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a marker is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: minimum 20; maximum 100. Defaults to None.
        marker(str, optional): An optional marker returned from a prior request. Use this marker for pagination of results from this operation. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.cache.parameter.init.describe_all(
                ctx, cache_parameter_group_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.cache.parameter.init.describe_all cache_parameter_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="describe_cache_parameters",
        op_kwargs={
            "CacheParameterGroupName": cache_parameter_group_name,
            "Source": source,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )
