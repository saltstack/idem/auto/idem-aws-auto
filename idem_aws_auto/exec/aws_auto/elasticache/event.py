"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def describe_all(
    hub,
    ctx,
    source_identifier: str = None,
    source_type: str = None,
    start_time: str = None,
    end_time: str = None,
    duration: int = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns events related to clusters, cache security groups, and cache parameter groups. You can obtain events specific to a particular cluster, cache security group, or cache parameter group by providing the name as a parameter. By default, only the events occurring within the last hour are returned; however, you can retrieve up to 14 days' worth of events if necessary.

    Args:
        source_identifier(str, optional): The identifier of the event source for which events are returned. If not specified, all sources are included in the response. Defaults to None.
        source_type(str, optional): The event source to retrieve events for. If no value is specified, all events are returned. Defaults to None.
        start_time(str, optional): The beginning of the time interval to retrieve events for, specified in ISO 8601 format.  Example: 2017-03-30T07:03:49.555Z. Defaults to None.
        end_time(str, optional): The end of the time interval for which to retrieve events, specified in ISO 8601 format.  Example: 2017-03-30T07:03:49.555Z. Defaults to None.
        duration(int, optional): The number of minutes worth of events to retrieve. Defaults to None.
        max_records(int, optional): The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a marker is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: minimum 20; maximum 100. Defaults to None.
        marker(str, optional): An optional marker returned from a prior request. Use this marker for pagination of results from this operation. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elasticache.event.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elasticache.event.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elasticache",
        operation="describe_events",
        op_kwargs={
            "SourceIdentifier": source_identifier,
            "SourceType": source_type,
            "StartTime": start_time,
            "EndTime": end_time,
            "Duration": duration,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )
