"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def start(
    hub,
    ctx,
    delivery_stream_name: str,
    delivery_stream_encryption_configuration_input: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Enables server-side encryption (SSE) for the delivery stream.  This operation is asynchronous. It returns immediately. When you invoke it, Kinesis Data Firehose first sets the encryption status of the stream to ENABLING, and then to ENABLED. The encryption status of a delivery stream is the Status property in DeliveryStreamEncryptionConfiguration. If the operation fails, the encryption status changes to ENABLING_FAILED. You can continue to read and write data to your delivery stream while the encryption status is ENABLING, but the data is not encrypted. It can take up to 5 seconds after the encryption status changes to ENABLED before all records written to the delivery stream are encrypted. To find out whether a record or a batch of records was encrypted, check the response elements PutRecordOutput$Encrypted and PutRecordBatchOutput$Encrypted, respectively. To check the encryption status of a delivery stream, use DescribeDeliveryStream. Even if encryption is currently enabled for a delivery stream, you can still invoke this operation on it to change the ARN of the CMK or both its type and ARN. If you invoke this method to change the CMK, and the old CMK is of type CUSTOMER_MANAGED_CMK, Kinesis Data Firehose schedules the grant it had on the old CMK for retirement. If the new CMK is of type CUSTOMER_MANAGED_CMK, Kinesis Data Firehose creates a grant that enables it to use the new CMK to encrypt and decrypt data and to manage the grant. If a delivery stream already has encryption enabled and then you invoke this operation to change the ARN of the CMK or both its type and ARN and you get ENABLING_FAILED, this only means that the attempt to change the CMK failed. In this case, encryption remains enabled with the old CMK. If the encryption status of your delivery stream is ENABLING_FAILED, you can invoke this operation again with a valid CMK. The CMK must be enabled and the key policy mustn't explicitly deny the permission for Kinesis Data Firehose to invoke KMS encrypt and decrypt operations. You can enable SSE for a delivery stream only if it's a delivery stream that uses DirectPut as its source.  The StartDeliveryStreamEncryption and StopDeliveryStreamEncryption operations have a combined limit of 25 calls per delivery stream per 24 hours. For example, you reach the limit if you call StartDeliveryStreamEncryption 13 times and StopDeliveryStreamEncryption 12 times for the same delivery stream in a 24-hour period.

    Args:
        delivery_stream_name(str): The name of the delivery stream for which you want to enable server-side encryption (SSE).
        delivery_stream_encryption_configuration_input(Dict, optional): Used to specify the type and Amazon Resource Name (ARN) of the KMS key needed for Server-Side Encryption (SSE). Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.firehose.delivery_stream.encryption.start(
                ctx, delivery_stream_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.firehose.delivery_stream.encryption.start delivery_stream_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="firehose",
        operation="start_delivery_stream_encryption",
        op_kwargs={
            "DeliveryStreamName": delivery_stream_name,
            "DeliveryStreamEncryptionConfigurationInput": delivery_stream_encryption_configuration_input,
        },
    )


async def stop(hub, ctx, delivery_stream_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Disables server-side encryption (SSE) for the delivery stream.  This operation is asynchronous. It returns immediately. When you invoke it, Kinesis Data Firehose first sets the encryption status of the stream to DISABLING, and then to DISABLED. You can continue to read and write data to your stream while its status is DISABLING. It can take up to 5 seconds after the encryption status changes to DISABLED before all records written to the delivery stream are no longer subject to encryption. To find out whether a record or a batch of records was encrypted, check the response elements PutRecordOutput$Encrypted and PutRecordBatchOutput$Encrypted, respectively. To check the encryption state of a delivery stream, use DescribeDeliveryStream.  If SSE is enabled using a customer managed CMK and then you invoke StopDeliveryStreamEncryption, Kinesis Data Firehose schedules the related KMS grant for retirement and then retires it after it ensures that it is finished delivering records to the destination. The StartDeliveryStreamEncryption and StopDeliveryStreamEncryption operations have a combined limit of 25 calls per delivery stream per 24 hours. For example, you reach the limit if you call StartDeliveryStreamEncryption 13 times and StopDeliveryStreamEncryption 12 times for the same delivery stream in a 24-hour period.

    Args:
        delivery_stream_name(str): The name of the delivery stream for which you want to disable server-side encryption (SSE).

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.firehose.delivery_stream.encryption.stop(
                ctx, delivery_stream_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.firehose.delivery_stream.encryption.stop delivery_stream_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="firehose",
        operation="stop_delivery_stream_encryption",
        op_kwargs={"DeliveryStreamName": delivery_stream_name},
    )
