"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def list_all(
    hub, ctx, alarm_model_name: str, next_token: str = None, max_results: int = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the versions of an alarm model. The operation returns only the metadata associated with each alarm model version.

    Args:
        alarm_model_name(str): The name of the alarm model.
        next_token(str, optional): The token that you can use to return the next set of results. Defaults to None.
        max_results(int, optional): The maximum number of results to be returned per request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotevent.alarm_model.version.list_all(
                ctx, alarm_model_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotevent.alarm_model.version.list_all alarm_model_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotevents",
        operation="list_alarm_model_versions",
        op_kwargs={
            "alarmModelName": alarm_model_name,
            "nextToken": next_token,
            "maxResults": max_results,
        },
    )
