"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


__func_alias__ = {"list_": "list"}


async def list_(
    hub,
    ctx,
    endpoint_id: str,
    destination_address: str,
    max_results: int = None,
    next_token: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    List the port mappings for a specific EC2 instance (destination) in a VPC subnet endpoint. The response is the mappings for one destination IP address. This is useful when your subnet endpoint has mappings that span multiple custom routing accelerators in your account, or for scenarios where you only want to list the port mappings for a specific destination instance.

    Args:
        endpoint_id(str): The ID for the virtual private cloud (VPC) subnet.
        destination_address(str): The endpoint IP address in a virtual private cloud (VPC) subnet for which you want to receive back port mappings.
        max_results(int, optional): The number of destination port mappings that you want to return with this call. The default value is 10. Defaults to None.
        next_token(str, optional): The token for the next set of results. You receive this token from a previous call. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.custom_routing.port_mapping.by_destination.list(
                ctx, endpoint_id=value, destination_address=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.custom_routing.port_mapping.by_destination.list endpoint_id=value, destination_address=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="list_custom_routing_port_mappings_by_destination",
        op_kwargs={
            "EndpointId": endpoint_id,
            "DestinationAddress": destination_address,
            "MaxResults": max_results,
            "NextToken": next_token,
        },
    )
