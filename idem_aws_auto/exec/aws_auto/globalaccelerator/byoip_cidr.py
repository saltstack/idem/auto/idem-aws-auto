"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def advertise(hub, ctx, cidr: str) -> Dict:
    r"""
    **Autogenerated function**

    Advertises an IPv4 address range that is provisioned for use with your AWS resources through bring your own IP addresses (BYOIP). It can take a few minutes before traffic to the specified addresses starts routing to AWS because of propagation delays.  To stop advertising the BYOIP address range, use  WithdrawByoipCidr. For more information, see Bring Your Own IP Addresses (BYOIP) in the AWS Global Accelerator Developer Guide.

    Args:
        cidr(str): The address range, in CIDR notation. This must be the exact range that you provisioned. You can't advertise only a portion of the provisioned range.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.byoip_cidr.advertise(ctx, cidr=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.byoip_cidr.advertise cidr=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="advertise_byoip_cidr",
        op_kwargs={"Cidr": cidr},
    )


async def deprovision(hub, ctx, cidr: str) -> Dict:
    r"""
    **Autogenerated function**

    Releases the specified address range that you provisioned to use with your AWS resources through bring your own IP addresses (BYOIP) and deletes the corresponding address pool.  Before you can release an address range, you must stop advertising it by using WithdrawByoipCidr and you must not have any accelerators that are using static IP addresses allocated from its address range.  For more information, see Bring Your Own IP Addresses (BYOIP) in the AWS Global Accelerator Developer Guide.

    Args:
        cidr(str): The address range, in CIDR notation. The prefix must be the same prefix that you specified when you provisioned the address range.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.byoip_cidr.deprovision(ctx, cidr=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.byoip_cidr.deprovision cidr=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="deprovision_byoip_cidr",
        op_kwargs={"Cidr": cidr},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Lists the IP address ranges that were specified in calls to ProvisionByoipCidr, including the current state and a history of state changes.

    Args:
        max_results(int, optional): The maximum number of results to return with a single call. To retrieve the remaining results, make another call with the returned nextToken value. Defaults to None.
        next_token(str, optional): The token for the next page of results. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.byoip_cidr.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.byoip_cidr.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="list_byoip_cidrs",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def provision(hub, ctx, cidr: str, cidr_authorization_context: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Provisions an IP address range to use with your AWS resources through bring your own IP addresses (BYOIP) and creates a corresponding address pool. After the address range is provisioned, it is ready to be advertised using  AdvertiseByoipCidr. For more information, see Bring Your Own IP Addresses (BYOIP) in the AWS Global Accelerator Developer Guide.

    Args:
        cidr(str): The public IPv4 address range, in CIDR notation. The most specific IP prefix that you can specify is /24. The address range cannot overlap with another address range that you've brought to this or another Region.
        cidr_authorization_context(Dict): A signed document that proves that you are authorized to bring the specified IP address range to Amazon using BYOIP. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.byoip_cidr.provision(
                ctx, cidr=value, cidr_authorization_context=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.byoip_cidr.provision cidr=value, cidr_authorization_context=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="provision_byoip_cidr",
        op_kwargs={
            "Cidr": cidr,
            "CidrAuthorizationContext": cidr_authorization_context,
        },
    )


async def withdraw(hub, ctx, cidr: str) -> Dict:
    r"""
    **Autogenerated function**

    Stops advertising an address range that is provisioned as an address pool. You can perform this operation at most once every 10 seconds, even if you specify different address ranges each time. It can take a few minutes before traffic to the specified addresses stops routing to AWS because of propagation delays. For more information, see Bring Your Own IP Addresses (BYOIP) in the AWS Global Accelerator Developer Guide.

    Args:
        cidr(str): The address range, in CIDR notation.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.globalaccelerator.byoip_cidr.withdraw(ctx, cidr=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.globalaccelerator.byoip_cidr.withdraw cidr=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="globalaccelerator",
        operation="withdraw_byoip_cidr",
        op_kwargs={"Cidr": cidr},
    )
