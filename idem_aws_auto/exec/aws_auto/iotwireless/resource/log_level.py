"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get(hub, ctx, resource_identifier: str, resource_type: str) -> Dict:
    r"""
    **Autogenerated function**

    Fetches the log-level override, if any, for a given resource-ID and resource-type. It can be used for a wireless device or a wireless gateway.

    Args:
        resource_identifier(str): The identifier of the resource. For a Wireless Device, it is the wireless device ID. For a wireless gateway, it is the wireless gateway ID.
        resource_type(str): The type of the resource, which can be WirelessDevice or WirelessGateway.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.resource.log_level.get(
                ctx, resource_identifier=value, resource_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.resource.log_level.get resource_identifier=value, resource_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="get_resource_log_level",
        op_kwargs={
            "ResourceIdentifier": resource_identifier,
            "ResourceType": resource_type,
        },
    )


async def put(
    hub, ctx, resource_identifier: str, resource_type: str, log_level: str
) -> Dict:
    r"""
    **Autogenerated function**

    Sets the log-level override for a resource-ID and resource-type. This option can be specified for a wireless gateway or a wireless device. A limit of 200 log level override can be set per account.

    Args:
        resource_identifier(str): The identifier of the resource. For a Wireless Device, it is the wireless device ID. For a wireless gateway, it is the wireless gateway ID.
        resource_type(str): The type of the resource, which can be WirelessDevice or WirelessGateway.
        log_level(str): The log level for a log message.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.resource.log_level.put(
                ctx, resource_identifier=value, resource_type=value, log_level=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.resource.log_level.put resource_identifier=value, resource_type=value, log_level=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="put_resource_log_level",
        op_kwargs={
            "ResourceIdentifier": resource_identifier,
            "ResourceType": resource_type,
            "LogLevel": log_level,
        },
    )


async def reset(hub, ctx, resource_identifier: str, resource_type: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes the log-level override, if any, for a specific resource-ID and resource-type. It can be used for a wireless device or a wireless gateway.

    Args:
        resource_identifier(str): The identifier of the resource. For a Wireless Device, it is the wireless device ID. For a wireless gateway, it is the wireless gateway ID.
        resource_type(str): The type of the resource, which can be WirelessDevice or WirelessGateway.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.resource.log_level.reset(
                ctx, resource_identifier=value, resource_type=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.resource.log_level.reset resource_identifier=value, resource_type=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="reset_resource_log_level",
        op_kwargs={
            "ResourceIdentifier": resource_identifier,
            "ResourceType": resource_type,
        },
    )
