"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(hub, ctx, id_: str, thing_arn: str) -> Dict:
    r"""
    **Autogenerated function**

    Associates a wireless gateway with a thing.

    Args:
        id_(str): The ID of the resource to update.
        thing_arn(str): The ARN of the thing to associate with the wireless gateway.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.wireless.gateway.with_.thing.associate(
                ctx, id_=value, thing_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.wireless.gateway.with_.thing.associate id_=value, thing_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="associate_wireless_gateway_with_thing",
        op_kwargs={"Id": id_, "ThingArn": thing_arn},
    )
