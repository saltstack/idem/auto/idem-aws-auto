"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def associate(hub, ctx, id_: str, iot_certificate_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Associates a wireless gateway with a certificate.

    Args:
        id_(str): The ID of the resource to update.
        iot_certificate_id(str): The ID of the certificate to associate with the wireless gateway.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.wireless.gateway.with_.certificate.associate(
                ctx, id_=value, iot_certificate_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.wireless.gateway.with_.certificate.associate id_=value, iot_certificate_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="associate_wireless_gateway_with_certificate",
        op_kwargs={"Id": id_, "IotCertificateId": iot_certificate_id},
    )
