"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def get_all(hub, ctx, wireless_device_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets operating information about a wireless device.

    Args:
        wireless_device_id(str): The ID of the wireless device for which to get the data.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.iotwireless.wireless.device.statistic.get_all(
                ctx, wireless_device_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.iotwireless.wireless.device.statistic.get_all wireless_device_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="iotwireless",
        operation="get_wireless_device_statistics",
        op_kwargs={"WirelessDeviceId": wireless_device_id},
    )
