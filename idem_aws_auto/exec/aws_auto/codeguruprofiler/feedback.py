"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def submit(
    hub,
    ctx,
    anomaly_instance_id: str,
    profiling_group_name: str,
    type_: str,
    comment: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Sends feedback to CodeGuru Profiler about whether the anomaly detected by the analysis is useful or not.

    Args:
        anomaly_instance_id(str): The universally unique identifier (UUID) of the  AnomalyInstance  object that is included in the analysis data.
        comment(str, optional): Optional feedback about this anomaly. Defaults to None.
        profiling_group_name(str): The name of the profiling group that is associated with the analysis data.
        type_(str):  The feedback tpye. Thee are two valid values, Positive and Negative. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.codeguruprofiler.feedback.submit(
                ctx, anomaly_instance_id=value, profiling_group_name=value, type_=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.codeguruprofiler.feedback.submit anomaly_instance_id=value, profiling_group_name=value, type_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="codeguruprofiler",
        operation="submit_feedback",
        op_kwargs={
            "anomalyInstanceId": anomaly_instance_id,
            "comment": comment,
            "profilingGroupName": profiling_group_name,
            "type": type_,
        },
    )
