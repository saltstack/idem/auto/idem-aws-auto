"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def tag(hub, ctx, resource_arn: str, tags: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Adds a tag to the specified resource.

    Args:
        resource_arn(str): The ARN of the resource to add the tag to.
        tags(Dict): The tags to add to the resource.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.accessanalyzer.resource.init.tag(
                ctx, resource_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.accessanalyzer.resource.init.tag resource_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="accessanalyzer",
        operation="tag_resource",
        op_kwargs={"resourceArn": resource_arn, "tags": tags},
    )


async def untag(hub, ctx, resource_arn: str, tag_keys: List) -> Dict:
    r"""
    **Autogenerated function**

    Removes a tag from the specified resource.

    Args:
        resource_arn(str): The ARN of the resource to remove the tag from.
        tag_keys(List): The key for the tag to add.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.accessanalyzer.resource.init.untag(
                ctx, resource_arn=value, tag_keys=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.accessanalyzer.resource.init.untag resource_arn=value, tag_keys=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="accessanalyzer",
        operation="untag_resource",
        op_kwargs={"resourceArn": resource_arn, "tagKeys": tag_keys},
    )
