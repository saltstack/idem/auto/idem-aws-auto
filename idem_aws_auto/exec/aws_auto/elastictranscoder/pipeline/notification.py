"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update_multiple(hub, ctx, id_: str, notifications: Dict) -> Dict:
    r"""
    **Autogenerated function**

    With the UpdatePipelineNotifications operation, you can update Amazon Simple Notification Service (Amazon SNS) notifications for a pipeline. When you update notifications for a pipeline, Elastic Transcoder returns the values that you specified in the request.

    Args:
        id_(str): The identifier of the pipeline for which you want to change notification settings.
        notifications(Dict): The topic ARN for the Amazon Simple Notification Service (Amazon SNS) topic that you want to notify to report job status.  To receive notifications, you must also subscribe to the new topic in the Amazon SNS console.     Progressing: The topic ARN for the Amazon Simple Notification Service (Amazon SNS) topic that you want to notify when Elastic Transcoder has started to process jobs that are added to this pipeline. This is the ARN that Amazon SNS returned when you created the topic.    Complete: The topic ARN for the Amazon SNS topic that you want to notify when Elastic Transcoder has finished processing a job. This is the ARN that Amazon SNS returned when you created the topic.    Warning: The topic ARN for the Amazon SNS topic that you want to notify when Elastic Transcoder encounters a warning condition. This is the ARN that Amazon SNS returned when you created the topic.    Error: The topic ARN for the Amazon SNS topic that you want to notify when Elastic Transcoder encounters an error condition. This is the ARN that Amazon SNS returned when you created the topic.  .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elastictranscoder.pipeline.notification.update_multiple(
                ctx, id_=value, notifications=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elastictranscoder.pipeline.notification.update_multiple id_=value, notifications=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elastictranscoder",
        operation="update_pipeline_notifications",
        op_kwargs={"Id": id_, "Notifications": notifications},
    )
