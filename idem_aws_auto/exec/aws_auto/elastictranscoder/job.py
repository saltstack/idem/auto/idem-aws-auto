"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def cancel(hub, ctx, id_: str) -> Dict:
    r"""
    **Autogenerated function**

    The CancelJob operation cancels an unfinished job.  You can only cancel a job that has a status of Submitted. To prevent a pipeline from starting to process a job while you're getting the job identifier, use UpdatePipelineStatus to temporarily pause the pipeline.

    Args:
        id_(str): The identifier of the job that you want to cancel. To get a list of the jobs (including their jobId) that have a status of Submitted, use the ListJobsByStatus API action.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elastictranscoder.job.cancel(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elastictranscoder.job.cancel id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elastictranscoder",
        operation="cancel_job",
        op_kwargs={"Id": id_},
    )


async def create(
    hub,
    ctx,
    pipeline_id: str,
    input_: Dict = None,
    inputs: List = None,
    output: Dict = None,
    outputs: List = None,
    output_key_prefix: str = None,
    playlists: List = None,
    user_metadata: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    When you create a job, Elastic Transcoder returns JSON data that includes the values that you specified plus information about the job that is created. If you have specified more than one output for your jobs (for example, one output for the Kindle Fire and another output for the Apple iPhone 4s), you currently must use the Elastic Transcoder API to list the jobs (as opposed to the AWS Console).

    Args:
        pipeline_id(str): The Id of the pipeline that you want Elastic Transcoder to use for transcoding. The pipeline determines several settings, including the Amazon S3 bucket from which Elastic Transcoder gets the files to transcode and the bucket into which Elastic Transcoder puts the transcoded files.
        input_(Dict, optional): A section of the request body that provides information about the file that is being transcoded. Defaults to None.
        inputs(List, optional): A section of the request body that provides information about the files that are being transcoded. Defaults to None.
        output(Dict, optional):  A section of the request body that provides information about the transcoded (target) file. We strongly recommend that you use the Outputs syntax instead of the Output syntax. . Defaults to None.
        outputs(List, optional):  A section of the request body that provides information about the transcoded (target) files. We recommend that you use the Outputs syntax instead of the Output syntax. . Defaults to None.
        output_key_prefix(str, optional): The value, if any, that you want Elastic Transcoder to prepend to the names of all files that this job creates, including output files, thumbnails, and playlists. Defaults to None.
        playlists(List, optional): If you specify a preset in PresetId for which the value of Container is fmp4 (Fragmented MP4) or ts (MPEG-TS), Playlists contains information about the master playlists that you want Elastic Transcoder to create. The maximum number of master playlists in a job is 30. Defaults to None.
        user_metadata(Dict, optional): User-defined metadata that you want to associate with an Elastic Transcoder job. You specify metadata in key/value pairs, and you can add up to 10 key/value pairs per job. Elastic Transcoder does not guarantee that key/value pairs are returned in the same order in which you specify them. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elastictranscoder.job.create(ctx, pipeline_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elastictranscoder.job.create pipeline_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elastictranscoder",
        operation="create_job",
        op_kwargs={
            "PipelineId": pipeline_id,
            "Input": input_,
            "Inputs": inputs,
            "Output": output,
            "Outputs": outputs,
            "OutputKeyPrefix": output_key_prefix,
            "Playlists": playlists,
            "UserMetadata": user_metadata,
        },
    )


async def read(hub, ctx, id_: str) -> Dict:
    r"""
    **Autogenerated function**

    The ReadJob operation returns detailed information about a job.

    Args:
        id_(str): The identifier of the job for which you want to get detailed information.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.elastictranscoder.job.read(ctx, id_=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.elastictranscoder.job.read id_=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="elastictranscoder",
        operation="read_job",
        op_kwargs={"Id": id_},
    )
