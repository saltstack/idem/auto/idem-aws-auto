"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def get(hub, ctx, task_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves a specific SpeechSynthesisTask object based on its TaskID. This object contains information about the given speech synthesis task, including the status of the task, and a link to the S3 bucket containing the output of the task.

    Args:
        task_id(str): The Amazon Polly generated identifier for a speech synthesis task.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.polly.speech.synthesis_task.get(ctx, task_id=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.polly.speech.synthesis_task.get task_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="polly",
        operation="get_speech_synthesis_task",
        op_kwargs={"TaskId": task_id},
    )


async def list_all(
    hub, ctx, max_results: int = None, next_token: str = None, status: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of SpeechSynthesisTask objects ordered by their creation date. This operation can filter the tasks by their status, for example, allowing users to list only tasks that are completed.

    Args:
        max_results(int, optional): Maximum number of speech synthesis tasks returned in a List operation. Defaults to None.
        next_token(str, optional): The pagination token to use in the next request to continue the listing of speech synthesis tasks. . Defaults to None.
        status(str, optional): Status of the speech synthesis tasks returned in a List operation. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.polly.speech.synthesis_task.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.polly.speech.synthesis_task.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="polly",
        operation="list_speech_synthesis_tasks",
        op_kwargs={
            "MaxResults": max_results,
            "NextToken": next_token,
            "Status": status,
        },
    )


async def start(
    hub,
    ctx,
    output_format: str,
    output_s3_bucket_name: str,
    text: str,
    voice_id: str,
    engine: str = None,
    language_code: str = None,
    lexicon_names: List = None,
    output_s3_key_prefix: str = None,
    sample_rate: str = None,
    sns_topic_arn: str = None,
    speech_mark_types: List = None,
    text_type: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Allows the creation of an asynchronous synthesis task, by starting a new SpeechSynthesisTask. This operation requires all the standard information needed for speech synthesis, plus the name of an Amazon S3 bucket for the service to store the output of the synthesis task and two optional parameters (OutputS3KeyPrefix and SnsTopicArn). Once the synthesis task is created, this operation will return a SpeechSynthesisTask object, which will include an identifier of this task as well as the current status.

    Args:
        engine(str, optional): Specifies the engine (standard or neural) for Amazon Polly to use when processing input text for speech synthesis. Using a voice that is not supported for the engine selected will result in an error. Defaults to None.
        language_code(str, optional): Optional language code for the Speech Synthesis request. This is only necessary if using a bilingual voice, such as Aditi, which can be used for either Indian English (en-IN) or Hindi (hi-IN).  If a bilingual voice is used and no language code is specified, Amazon Polly will use the default language of the bilingual voice. The default language for any voice is the one returned by the DescribeVoices operation for the LanguageCode parameter. For example, if no language code is specified, Aditi will use Indian English rather than Hindi. Defaults to None.
        lexicon_names(List, optional): List of one or more pronunciation lexicon names you want the service to apply during synthesis. Lexicons are applied only if the language of the lexicon is the same as the language of the voice. . Defaults to None.
        output_format(str): The format in which the returned output will be encoded. For audio stream, this will be mp3, ogg_vorbis, or pcm. For speech marks, this will be json. .
        output_s3_bucket_name(str): Amazon S3 bucket name to which the output file will be saved.
        output_s3_key_prefix(str, optional): The Amazon S3 key prefix for the output speech file. Defaults to None.
        sample_rate(str, optional): The audio frequency specified in Hz. The valid values for mp3 and ogg_vorbis are "8000", "16000", "22050", and "24000". The default value for standard voices is "22050". The default value for neural voices is "24000". Valid values for pcm are "8000" and "16000" The default value is "16000". . Defaults to None.
        sns_topic_arn(str, optional): ARN for the SNS topic optionally used for providing status notification for a speech synthesis task. Defaults to None.
        speech_mark_types(List, optional): The type of speech marks returned for the input text. Defaults to None.
        text(str): The input text to synthesize. If you specify ssml as the TextType, follow the SSML format for the input text. .
        text_type(str, optional): Specifies whether the input text is plain text or SSML. The default value is plain text. . Defaults to None.
        voice_id(str): Voice ID to use for the synthesis. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.polly.speech.synthesis_task.start(
                ctx, output_format=value, output_s3_bucket_name=value, text=value, voice_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.polly.speech.synthesis_task.start output_format=value, output_s3_bucket_name=value, text=value, voice_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="polly",
        operation="start_speech_synthesis_task",
        op_kwargs={
            "Engine": engine,
            "LanguageCode": language_code,
            "LexiconNames": lexicon_names,
            "OutputFormat": output_format,
            "OutputS3BucketName": output_s3_bucket_name,
            "OutputS3KeyPrefix": output_s3_key_prefix,
            "SampleRate": sample_rate,
            "SnsTopicArn": sns_topic_arn,
            "SpeechMarkTypes": speech_mark_types,
            "Text": text,
            "TextType": text_type,
            "VoiceId": voice_id,
        },
    )
