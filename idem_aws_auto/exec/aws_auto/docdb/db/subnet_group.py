"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    db_subnet_group_name: str,
    db_subnet_group_description: str,
    subnet_ids: List,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a new subnet group. subnet groups must contain at least one subnet in at least two Availability Zones in the Region.

    Args:
        db_subnet_group_name(str): The name for the subnet group. This value is stored as a lowercase string. Constraints: Must contain no more than 255 letters, numbers, periods, underscores, spaces, or hyphens. Must not be default. Example: mySubnetgroup .
        db_subnet_group_description(str): The description for the subnet group.
        subnet_ids(List): The Amazon EC2 subnet IDs for the subnet group.
        tags(List, optional): The tags to be assigned to the subnet group. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.subnet_group.create(
                ctx, db_subnet_group_name=value, db_subnet_group_description=value, subnet_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.subnet_group.create db_subnet_group_name=value, db_subnet_group_description=value, subnet_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="create_db_subnet_group",
        op_kwargs={
            "DBSubnetGroupName": db_subnet_group_name,
            "DBSubnetGroupDescription": db_subnet_group_description,
            "SubnetIds": subnet_ids,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, db_subnet_group_name: str) -> None:
    r"""
    **Autogenerated function**

    Deletes a subnet group.  The specified database subnet group must not be associated with any DB instances.

    Args:
        db_subnet_group_name(str): The name of the database subnet group to delete.  You can't delete the default subnet group.  Constraints: Must match the name of an existing DBSubnetGroup. Must not be default. Example: mySubnetgroup .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.subnet_group.delete(ctx, db_subnet_group_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.subnet_group.delete db_subnet_group_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="delete_db_subnet_group",
        op_kwargs={"DBSubnetGroupName": db_subnet_group_name},
    )


async def describe_all(
    hub,
    ctx,
    db_subnet_group_name: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns a list of DBSubnetGroup descriptions. If a DBSubnetGroupName is specified, the list will contain only the descriptions of the specified DBSubnetGroup.

    Args:
        db_subnet_group_name(str, optional): The name of the subnet group to return details for. Defaults to None.
        filters(List, optional): This parameter is not currently supported. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token (marker) is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.subnet_group.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.subnet_group.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="describe_db_subnet_groups",
        op_kwargs={
            "DBSubnetGroupName": db_subnet_group_name,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub,
    ctx,
    db_subnet_group_name: str,
    subnet_ids: List,
    db_subnet_group_description: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies an existing subnet group. subnet groups must contain at least one subnet in at least two Availability Zones in the Region.

    Args:
        db_subnet_group_name(str): The name for the subnet group. This value is stored as a lowercase string. You can't modify the default subnet group.  Constraints: Must match the name of an existing DBSubnetGroup. Must not be default. Example: mySubnetgroup .
        db_subnet_group_description(str, optional): The description for the subnet group. Defaults to None.
        subnet_ids(List): The Amazon EC2 subnet IDs for the subnet group.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.subnet_group.modify(
                ctx, db_subnet_group_name=value, subnet_ids=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.subnet_group.modify db_subnet_group_name=value, subnet_ids=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="modify_db_subnet_group",
        op_kwargs={
            "DBSubnetGroupName": db_subnet_group_name,
            "DBSubnetGroupDescription": db_subnet_group_description,
            "SubnetIds": subnet_ids,
        },
    )
