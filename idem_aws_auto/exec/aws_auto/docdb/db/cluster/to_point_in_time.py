"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def restore(
    hub,
    ctx,
    db_cluster_identifier: str,
    source_db_cluster_identifier: str,
    restore_to_time: str = None,
    use_latest_restorable_time: bool = None,
    port: int = None,
    db_subnet_group_name: str = None,
    vpc_security_group_ids: List = None,
    tags: List = None,
    kms_key_id: str = None,
    enable_cloudwatch_logs_exports: List = None,
    deletion_protection: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Restores a cluster to an arbitrary point in time. Users can restore to any point in time before LatestRestorableTime for up to BackupRetentionPeriod days. The target cluster is created from the source cluster with the same configuration as the original cluster, except that the new cluster is created with the default security group.

    Args:
        db_cluster_identifier(str): The name of the new cluster to be created. Constraints:   Must contain from 1 to 63 letters, numbers, or hyphens.   The first character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.  .
        source_db_cluster_identifier(str): The identifier of the source cluster from which to restore. Constraints:   Must match the identifier of an existing DBCluster.  .
        restore_to_time(str, optional): The date and time to restore the cluster to. Valid values: A time in Universal Coordinated Time (UTC) format. Constraints:   Must be before the latest restorable time for the instance.   Must be specified if the UseLatestRestorableTime parameter is not provided.   Cannot be specified if the UseLatestRestorableTime parameter is true.   Cannot be specified if the RestoreType parameter is copy-on-write.   Example: 2015-03-07T23:45:00Z . Defaults to None.
        use_latest_restorable_time(bool, optional): A value that is set to true to restore the cluster to the latest restorable backup time, and false otherwise.  Default: false  Constraints: Cannot be specified if the RestoreToTime parameter is provided. Defaults to None.
        port(int, optional): The port number on which the new cluster accepts connections. Constraints: Must be a value from 1150 to 65535.  Default: The default port for the engine. Defaults to None.
        db_subnet_group_name(str, optional): The subnet group name to use for the new cluster. Constraints: If provided, must match the name of an existing DBSubnetGroup. Example: mySubnetgroup . Defaults to None.
        vpc_security_group_ids(List, optional): A list of VPC security groups that the new cluster belongs to. Defaults to None.
        tags(List, optional): The tags to be assigned to the restored cluster. Defaults to None.
        kms_key_id(str, optional): The KMS key identifier to use when restoring an encrypted cluster from an encrypted cluster. The KMS key identifier is the Amazon Resource Name (ARN) for the KMS encryption key. If you are restoring a cluster with the same account that owns the KMS encryption key used to encrypt the new cluster, then you can use the KMS key alias instead of the ARN for the KMS encryption key. You can restore to a new cluster and encrypt the new cluster with an KMS key that is different from the KMS key used to encrypt the source cluster. The new DB cluster is encrypted with the KMS key identified by the KmsKeyId parameter. If you do not specify a value for the KmsKeyId parameter, then the following occurs:   If the cluster is encrypted, then the restored cluster is encrypted using the KMS key that was used to encrypt the source cluster.   If the cluster is not encrypted, then the restored cluster is not encrypted.   If DBClusterIdentifier refers to a cluster that is not encrypted, then the restore request is rejected. Defaults to None.
        enable_cloudwatch_logs_exports(List, optional): A list of log types that must be enabled for exporting to Amazon CloudWatch Logs. Defaults to None.
        deletion_protection(bool, optional): Specifies whether this cluster can be deleted. If DeletionProtection is enabled, the cluster cannot be deleted unless it is modified and DeletionProtection is disabled. DeletionProtection protects clusters from being accidentally deleted. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.cluster.to_point_in_time.restore(
                ctx, db_cluster_identifier=value, source_db_cluster_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.cluster.to_point_in_time.restore db_cluster_identifier=value, source_db_cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="restore_db_cluster_to_point_in_time",
        op_kwargs={
            "DBClusterIdentifier": db_cluster_identifier,
            "SourceDBClusterIdentifier": source_db_cluster_identifier,
            "RestoreToTime": restore_to_time,
            "UseLatestRestorableTime": use_latest_restorable_time,
            "Port": port,
            "DBSubnetGroupName": db_subnet_group_name,
            "VpcSecurityGroupIds": vpc_security_group_ids,
            "Tags": tags,
            "KmsKeyId": kms_key_id,
            "EnableCloudwatchLogsExports": enable_cloudwatch_logs_exports,
            "DeletionProtection": deletion_protection,
        },
    )
