"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def copy(
    hub,
    ctx,
    source_db_cluster_snapshot_identifier: str,
    target_db_cluster_snapshot_identifier: str,
    kms_key_id: str = None,
    pre_signed_url: str = None,
    copy_tags: bool = None,
    tags: List = None,
    source_region: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Copies a snapshot of a cluster. To copy a cluster snapshot from a shared manual cluster snapshot, SourceDBClusterSnapshotIdentifier must be the Amazon Resource Name (ARN) of the shared cluster snapshot. You can only copy a shared DB cluster snapshot, whether encrypted or not, in the same Region. To cancel the copy operation after it is in progress, delete the target cluster snapshot identified by TargetDBClusterSnapshotIdentifier while that cluster snapshot is in the copying status.

    Args:
        source_db_cluster_snapshot_identifier(str): The identifier of the cluster snapshot to copy. This parameter is not case sensitive. Constraints:   Must specify a valid system snapshot in the available state.   If the source snapshot is in the same Region as the copy, specify a valid snapshot identifier.   If the source snapshot is in a different Region than the copy, specify a valid cluster snapshot ARN.   Example: my-cluster-snapshot1 .
        target_db_cluster_snapshot_identifier(str): The identifier of the new cluster snapshot to create from the source cluster snapshot. This parameter is not case sensitive. Constraints:   Must contain from 1 to 63 letters, numbers, or hyphens.    The first character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.    Example: my-cluster-snapshot2 .
        kms_key_id(str, optional): The KMS key ID for an encrypted cluster snapshot. The KMS key ID is the Amazon Resource Name (ARN), KMS key identifier, or the KMS key alias for the KMS encryption key.  If you copy an encrypted cluster snapshot from your account, you can specify a value for KmsKeyId to encrypt the copy with a new KMS encryption key. If you don't specify a value for KmsKeyId, then the copy of the cluster snapshot is encrypted with the same KMS key as the source cluster snapshot. If you copy an encrypted cluster snapshot that is shared from another account, then you must specify a value for KmsKeyId. To copy an encrypted cluster snapshot to another Region, set KmsKeyId to the KMS key ID that you want to use to encrypt the copy of the cluster snapshot in the destination Region. KMS encryption keys are specific to the Region that they are created in, and you can't use encryption keys from one Region in another Region. If you copy an unencrypted cluster snapshot and specify a value for the KmsKeyId parameter, an error is returned. Defaults to None.
        pre_signed_url(str, optional): The URL that contains a Signature Version 4 signed request for theCopyDBClusterSnapshot API action in the Region that contains the source cluster snapshot to copy. You must use the PreSignedUrl parameter when copying a cluster snapshot from another Region. If you are using an Amazon Web Services SDK tool or the CLI, you can specify SourceRegion (or --source-region for the CLI) instead of specifying PreSignedUrl manually. Specifying SourceRegion autogenerates a pre-signed URL that is a valid request for the operation that can be executed in the source Region. The presigned URL must be a valid request for the CopyDBClusterSnapshot API action that can be executed in the source Region that contains the cluster snapshot to be copied. The presigned URL request must contain the following parameter values:    SourceRegion - The ID of the region that contains the snapshot to be copied.    SourceDBClusterSnapshotIdentifier - The identifier for the the encrypted cluster snapshot to be copied. This identifier must be in the Amazon Resource Name (ARN) format for the source Region. For example, if you are copying an encrypted cluster snapshot from the us-east-1 Region, then your SourceDBClusterSnapshotIdentifier looks something like the following: arn:aws:rds:us-east-1:12345678012:sample-cluster:sample-cluster-snapshot.    TargetDBClusterSnapshotIdentifier - The identifier for the new cluster snapshot to be created. This parameter isn't case sensitive.  . Defaults to None.
        copy_tags(bool, optional): Set to true to copy all tags from the source cluster snapshot to the target cluster snapshot, and otherwise false. The default is false. Defaults to None.
        tags(List, optional): The tags to be assigned to the cluster snapshot. Defaults to None.
        source_region(str, optional): The ID of the region that contains the snapshot to be copied. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.cluster.snapshot.init.copy(
                ctx,
                source_db_cluster_snapshot_identifier=value,
                target_db_cluster_snapshot_identifier=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.cluster.snapshot.init.copy source_db_cluster_snapshot_identifier=value, target_db_cluster_snapshot_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="copy_db_cluster_snapshot",
        op_kwargs={
            "SourceDBClusterSnapshotIdentifier": source_db_cluster_snapshot_identifier,
            "TargetDBClusterSnapshotIdentifier": target_db_cluster_snapshot_identifier,
            "KmsKeyId": kms_key_id,
            "PreSignedUrl": pre_signed_url,
            "CopyTags": copy_tags,
            "Tags": tags,
            "SourceRegion": source_region,
        },
    )


async def create(
    hub,
    ctx,
    db_cluster_snapshot_identifier: str,
    db_cluster_identifier: str,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a snapshot of a cluster.

    Args:
        db_cluster_snapshot_identifier(str): The identifier of the cluster snapshot. This parameter is stored as a lowercase string. Constraints:   Must contain from 1 to 63 letters, numbers, or hyphens.   The first character must be a letter.   Cannot end with a hyphen or contain two consecutive hyphens.    Example: my-cluster-snapshot1 .
        db_cluster_identifier(str): The identifier of the cluster to create a snapshot for. This parameter is not case sensitive. Constraints:   Must match the identifier of an existing DBCluster.   Example: my-cluster .
        tags(List, optional): The tags to be assigned to the cluster snapshot. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.cluster.snapshot.init.create(
                ctx, db_cluster_snapshot_identifier=value, db_cluster_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.cluster.snapshot.init.create db_cluster_snapshot_identifier=value, db_cluster_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="create_db_cluster_snapshot",
        op_kwargs={
            "DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier,
            "DBClusterIdentifier": db_cluster_identifier,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, db_cluster_snapshot_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a cluster snapshot. If the snapshot is being copied, the copy operation is terminated.  The cluster snapshot must be in the available state to be deleted.

    Args:
        db_cluster_snapshot_identifier(str): The identifier of the cluster snapshot to delete. Constraints: Must be the name of an existing cluster snapshot in the available state.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.cluster.snapshot.init.delete(
                ctx, db_cluster_snapshot_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.cluster.snapshot.init.delete db_cluster_snapshot_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="delete_db_cluster_snapshot",
        op_kwargs={"DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier},
    )


async def describe_all(
    hub,
    ctx,
    db_cluster_identifier: str = None,
    db_cluster_snapshot_identifier: str = None,
    snapshot_type: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
    include_shared: bool = None,
    include_public: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Returns information about cluster snapshots. This API operation supports pagination.

    Args:
        db_cluster_identifier(str, optional): The ID of the cluster to retrieve the list of cluster snapshots for. This parameter can't be used with the DBClusterSnapshotIdentifier parameter. This parameter is not case sensitive.  Constraints:   If provided, must match the identifier of an existing DBCluster.  . Defaults to None.
        db_cluster_snapshot_identifier(str, optional): A specific cluster snapshot identifier to describe. This parameter can't be used with the DBClusterIdentifier parameter. This value is stored as a lowercase string.  Constraints:   If provided, must match the identifier of an existing DBClusterSnapshot.   If this identifier is for an automated snapshot, the SnapshotType parameter must also be specified.  . Defaults to None.
        snapshot_type(str, optional): The type of cluster snapshots to be returned. You can specify one of the following values:    automated - Return all cluster snapshots that Amazon DocumentDB has automatically created for your account.    manual - Return all cluster snapshots that you have manually created for your account.    shared - Return all manual cluster snapshots that have been shared to your account.    public - Return all cluster snapshots that have been marked as public.   If you don't specify a SnapshotType value, then both automated and manual cluster snapshots are returned. You can include shared cluster snapshots with these results by setting the IncludeShared parameter to true. You can include public cluster snapshots with these results by setting theIncludePublic parameter to true. The IncludeShared and IncludePublic parameters don't apply for SnapshotType values of manual or automated. The IncludePublic parameter doesn't apply when SnapshotType is set to shared. The IncludeShared parameter doesn't apply when SnapshotType is set to public. Defaults to None.
        filters(List, optional): This parameter is not currently supported. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token (marker) is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.
        include_shared(bool, optional): Set to true to include shared manual cluster snapshots from other accounts that this account has been given permission to copy or restore, and otherwise false. The default is false. Defaults to None.
        include_public(bool, optional): Set to true to include manual cluster snapshots that are public and can be copied or restored by any account, and otherwise false. The default is false. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.db.cluster.snapshot.init.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.db.cluster.snapshot.init.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="describe_db_cluster_snapshots",
        op_kwargs={
            "DBClusterIdentifier": db_cluster_identifier,
            "DBClusterSnapshotIdentifier": db_cluster_snapshot_identifier,
            "SnapshotType": snapshot_type,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
            "IncludeShared": include_shared,
            "IncludePublic": include_public,
        },
    )
