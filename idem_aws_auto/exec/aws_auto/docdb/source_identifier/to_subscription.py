"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def add(hub, ctx, subscription_name: str, source_identifier: str) -> Dict:
    r"""
    **Autogenerated function**

    Adds a source identifier to an existing event notification subscription.

    Args:
        subscription_name(str): The name of the Amazon DocumentDB event notification subscription that you want to add a source identifier to.
        source_identifier(str): The identifier of the event source to be added:   If the source type is an instance, a DBInstanceIdentifier must be provided.   If the source type is a security group, a DBSecurityGroupName must be provided.   If the source type is a parameter group, a DBParameterGroupName must be provided.   If the source type is a snapshot, a DBSnapshotIdentifier must be provided.  .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.source_identifier.to_subscription.add(
                ctx, subscription_name=value, source_identifier=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.source_identifier.to_subscription.add subscription_name=value, source_identifier=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="add_source_identifier_to_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SourceIdentifier": source_identifier,
        },
    )
