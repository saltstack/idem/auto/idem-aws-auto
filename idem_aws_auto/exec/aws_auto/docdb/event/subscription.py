"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str,
    source_type: str = None,
    event_categories: List = None,
    source_ids: List = None,
    enabled: bool = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an Amazon DocumentDB event notification subscription. This action requires a topic Amazon Resource Name (ARN) created by using the Amazon DocumentDB console, the Amazon SNS console, or the Amazon SNS API. To obtain an ARN with Amazon SNS, you must create a topic in Amazon SNS and subscribe to the topic. The ARN is displayed in the Amazon SNS console. You can specify the type of source (SourceType) that you want to be notified of. You can also provide a list of Amazon DocumentDB sources (SourceIds) that trigger the events, and you can provide a list of event categories (EventCategories) for events that you want to be notified of. For example, you can specify SourceType = db-instance, SourceIds = mydbinstance1, mydbinstance2 and EventCategories = Availability, Backup. If you specify both the SourceType and SourceIds (such as SourceType = db-instance and SourceIdentifier = myDBInstance1), you are notified of all the db-instance events for the specified source. If you specify a SourceType but do not specify a SourceIdentifier, you receive notice of the events for that source type for all your Amazon DocumentDB sources. If you do not specify either the SourceType or the SourceIdentifier, you are notified of events generated from all Amazon DocumentDB sources belonging to your customer account.

    Args:
        subscription_name(str): The name of the subscription. Constraints: The name must be fewer than 255 characters.
        sns_topic_arn(str): The Amazon Resource Name (ARN) of the SNS topic created for event notification. Amazon SNS creates the ARN when you create a topic and subscribe to it.
        source_type(str, optional): The type of source that is generating the events. For example, if you want to be notified of events generated by an instance, you would set this parameter to db-instance. If this value is not specified, all events are returned. Valid values: db-instance, db-cluster, db-parameter-group, db-security-group, db-cluster-snapshot . Defaults to None.
        event_categories(List, optional):  A list of event categories for a SourceType that you want to subscribe to. . Defaults to None.
        source_ids(List, optional): The list of identifiers of the event sources for which events are returned. If not specified, then all sources are included in the response. An identifier must begin with a letter and must contain only ASCII letters, digits, and hyphens; it can't end with a hyphen or contain two consecutive hyphens. Constraints:   If SourceIds are provided, SourceType must also be provided.   If the source type is an instance, a DBInstanceIdentifier must be provided.   If the source type is a security group, a DBSecurityGroupName must be provided.   If the source type is a parameter group, a DBParameterGroupName must be provided.   If the source type is a snapshot, a DBSnapshotIdentifier must be provided.  . Defaults to None.
        enabled(bool, optional):  A Boolean value; set to true to activate the subscription, set to false to create the subscription but not active it. . Defaults to None.
        tags(List, optional): The tags to be assigned to the event subscription. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.event.subscription.create(
                ctx, subscription_name=value, sns_topic_arn=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.event.subscription.create subscription_name=value, sns_topic_arn=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="create_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "SourceIds": source_ids,
            "Enabled": enabled,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, subscription_name: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an Amazon DocumentDB event notification subscription.

    Args:
        subscription_name(str): The name of the Amazon DocumentDB event notification subscription that you want to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.event.subscription.delete(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.event.subscription.delete subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="delete_event_subscription",
        op_kwargs={"SubscriptionName": subscription_name},
    )


async def describe_all(
    hub,
    ctx,
    subscription_name: str = None,
    filters: List = None,
    max_records: int = None,
    marker: str = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the subscription descriptions for a customer account. The description for a subscription includes SubscriptionName, SNSTopicARN, CustomerID, SourceType, SourceID, CreationTime, and Status. If you specify a SubscriptionName, lists the description for that subscription.

    Args:
        subscription_name(str, optional): The name of the Amazon DocumentDB event notification subscription that you want to describe. Defaults to None.
        filters(List, optional): This parameter is not currently supported. Defaults to None.
        max_records(int, optional):  The maximum number of records to include in the response. If more records exist than the specified MaxRecords value, a pagination token (marker) is included in the response so that the remaining results can be retrieved. Default: 100 Constraints: Minimum 20, maximum 100. Defaults to None.
        marker(str, optional): An optional pagination token provided by a previous request. If this parameter is specified, the response includes only records beyond the marker, up to the value specified by MaxRecords. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.event.subscription.describe_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.event.subscription.describe_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="describe_event_subscriptions",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "Filters": filters,
            "MaxRecords": max_records,
            "Marker": marker,
        },
    )


async def modify(
    hub,
    ctx,
    subscription_name: str,
    sns_topic_arn: str = None,
    source_type: str = None,
    event_categories: List = None,
    enabled: bool = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Modifies an existing Amazon DocumentDB event notification subscription.

    Args:
        subscription_name(str): The name of the Amazon DocumentDB event notification subscription.
        sns_topic_arn(str, optional): The Amazon Resource Name (ARN) of the SNS topic created for event notification. The ARN is created by Amazon SNS when you create a topic and subscribe to it. Defaults to None.
        source_type(str, optional): The type of source that is generating the events. For example, if you want to be notified of events generated by an instance, set this parameter to db-instance. If this value is not specified, all events are returned. Valid values: db-instance, db-parameter-group, db-security-group . Defaults to None.
        event_categories(List, optional):  A list of event categories for a SourceType that you want to subscribe to. Defaults to None.
        enabled(bool, optional):  A Boolean value; set to true to activate the subscription. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.docdb.event.subscription.modify(ctx, subscription_name=value)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.docdb.event.subscription.modify subscription_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="docdb",
        operation="modify_event_subscription",
        op_kwargs={
            "SubscriptionName": subscription_name,
            "SnsTopicArn": sns_topic_arn,
            "SourceType": source_type,
            "EventCategories": event_categories,
            "Enabled": enabled,
        },
    )
