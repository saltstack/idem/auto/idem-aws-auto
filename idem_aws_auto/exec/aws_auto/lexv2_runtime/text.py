"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def recognize(
    hub,
    ctx,
    bot_id: str,
    bot_alias_id: str,
    locale_id: str,
    session_id: str,
    text: str,
    session_state: Dict = None,
    request_attributes: Dict = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Sends user input to Amazon Lex V2. Client applications use this API to send requests to Amazon Lex V2 at runtime. Amazon Lex V2 then interprets the user input using the machine learning model that it build for the bot. In response, Amazon Lex V2 returns the next message to convey to the user and an optional response card to display.

    Args:
        bot_id(str): The identifier of the bot that processes the request.
        bot_alias_id(str): The alias identifier in use for the bot that processes the request.
        locale_id(str): The locale where the session is in use.
        session_id(str): The identifier of the user session that is having the conversation.
        text(str): The text that the user entered. Amazon Lex V2 interprets this text.
        session_state(Dict, optional): The current state of the dialog between the user and the bot. Defaults to None.
        request_attributes(Dict, optional): Request-specific information passed between the client application and Amazon Lex V2  The namespace x-amz-lex: is reserved for special attributes. Don't create any request attributes with the prefix x-amz-lex:. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.lexv2_runtime.text.recognize(
                ctx, bot_id=value, bot_alias_id=value, locale_id=value, session_id=value, text=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.lexv2_runtime.text.recognize bot_id=value, bot_alias_id=value, locale_id=value, session_id=value, text=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="lexv2-runtime",
        operation="recognize_text",
        op_kwargs={
            "botId": bot_id,
            "botAliasId": bot_alias_id,
            "localeId": locale_id,
            "sessionId": session_id,
            "text": text,
            "sessionState": session_state,
            "requestAttributes": request_attributes,
        },
    )
