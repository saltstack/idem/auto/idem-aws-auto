"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


__virtualname__ = "list"


async def create(
    hub, ctx, creator_request_id: str, name: str, tags: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Creates an empty firewall domain list for use in DNS Firewall rules. You can populate the domains for the new list with a file, using ImportFirewallDomains, or with domain strings, using UpdateFirewallDomains.

    Args:
        creator_request_id(str): A unique string that identifies the request and that allows you to retry failed requests without the risk of running the operation twice. CreatorRequestId can be any unique string, for example, a date/time stamp. .
        name(str): A name that lets you identify the domain list to manage and use it.
        tags(List, optional): A list of the tag keys and values that you want to associate with the domain list. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.firewall.domain.list_.create(
                ctx, creator_request_id=value, name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.firewall.domain.list.create creator_request_id=value, name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="create_firewall_domain_list",
        op_kwargs={"CreatorRequestId": creator_request_id, "Name": name, "Tags": tags},
    )


async def delete(hub, ctx, firewall_domain_list_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes the specified domain list.

    Args:
        firewall_domain_list_id(str): The ID of the domain list that you want to delete. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.firewall.domain.list_.delete(
                ctx, firewall_domain_list_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.firewall.domain.list.delete firewall_domain_list_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="delete_firewall_domain_list",
        op_kwargs={"FirewallDomainListId": firewall_domain_list_id},
    )


async def get(hub, ctx, firewall_domain_list_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the specified firewall domain list.

    Args:
        firewall_domain_list_id(str): The ID of the domain list. .

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.firewall.domain.list_.get(
                ctx, firewall_domain_list_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.firewall.domain.list.get firewall_domain_list_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="get_firewall_domain_list",
        op_kwargs={"FirewallDomainListId": firewall_domain_list_id},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Retrieves the firewall domain lists that you have defined. For each firewall domain list, you can retrieve the domains that are defined for a list by calling ListFirewallDomains.  A single call to this list operation might return only a partial list of the domain lists. For information, see MaxResults.

    Args:
        max_results(int, optional): The maximum number of objects that you want Resolver to return for this request. If more objects are available, in the response, Resolver provides a NextToken value that you can use in a subsequent call to get the next batch of objects. If you don't specify a value for MaxResults, Resolver returns up to 100 objects. . Defaults to None.
        next_token(str, optional): For the first call to this list request, omit this value. When you request a list of objects, Resolver returns at most the number of objects specified in MaxResults. If more objects are available for retrieval, Resolver returns a NextToken value in the response. To retrieve the next batch of objects, use the token that was returned for the prior request in your next request. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.firewall.domain.list_.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.firewall.domain.list.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="list_firewall_domain_lists",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )
