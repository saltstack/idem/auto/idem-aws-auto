"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def associate(
    hub, ctx, resolver_rule_id: str, vpc_id: str, name: str = None
) -> Dict:
    r"""
    **Autogenerated function**

    Associates a Resolver rule with a VPC. When you associate a rule with a VPC, Resolver forwards all DNS queries for the domain name that is specified in the rule and that originate in the VPC. The queries are forwarded to the IP addresses for the DNS resolvers that are specified in the rule. For more information about rules, see CreateResolverRule.

    Args:
        resolver_rule_id(str): The ID of the Resolver rule that you want to associate with the VPC. To list the existing Resolver rules, use ListResolverRules.
        name(str, optional): A name for the association that you're creating between a Resolver rule and a VPC. Defaults to None.
        vpc_id(str): The ID of the VPC that you want to associate the Resolver rule with.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.associate(
                ctx, resolver_rule_id=value, vpc_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.associate resolver_rule_id=value, vpc_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="associate_resolver_rule",
        op_kwargs={"ResolverRuleId": resolver_rule_id, "Name": name, "VPCId": vpc_id},
    )


async def create(
    hub,
    ctx,
    creator_request_id: str,
    rule_type: str,
    domain_name: str,
    name: str = None,
    target_ips: List = None,
    resolver_endpoint_id: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    For DNS queries that originate in your VPCs, specifies which Resolver endpoint the queries pass through, one domain name that you want to forward to your network, and the IP addresses of the DNS resolvers in your network.

    Args:
        creator_request_id(str): A unique string that identifies the request and that allows failed requests to be retried without the risk of running the operation twice. CreatorRequestId can be any unique string, for example, a date/time stamp. .
        name(str, optional): A friendly name that lets you easily find a rule in the Resolver dashboard in the Route 53 console. Defaults to None.
        rule_type(str): When you want to forward DNS queries for specified domain name to resolvers on your network, specify FORWARD. When you have a forwarding rule to forward DNS queries for a domain to your network and you want Resolver to process queries for a subdomain of that domain, specify SYSTEM. For example, to forward DNS queries for example.com to resolvers on your network, you create a rule and specify FORWARD for RuleType. To then have Resolver process queries for apex.example.com, you create a rule and specify SYSTEM for RuleType. Currently, only Resolver can create rules that have a value of RECURSIVE for RuleType.
        domain_name(str): DNS queries for this domain name are forwarded to the IP addresses that you specify in TargetIps. If a query matches multiple Resolver rules (example.com and www.example.com), outbound DNS queries are routed using the Resolver rule that contains the most specific domain name (www.example.com).
        target_ips(List, optional): The IPs that you want Resolver to forward DNS queries to. You can specify only IPv4 addresses. Separate IP addresses with a space.  TargetIps is available only when the value of Rule type is FORWARD. Defaults to None.
        resolver_endpoint_id(str, optional): The ID of the outbound Resolver endpoint that you want to use to route DNS queries to the IP addresses that you specify in TargetIps. Defaults to None.
        tags(List, optional): A list of the tag keys and values that you want to associate with the endpoint. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.create(
                ctx, creator_request_id=value, rule_type=value, domain_name=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.create creator_request_id=value, rule_type=value, domain_name=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="create_resolver_rule",
        op_kwargs={
            "CreatorRequestId": creator_request_id,
            "Name": name,
            "RuleType": rule_type,
            "DomainName": domain_name,
            "TargetIps": target_ips,
            "ResolverEndpointId": resolver_endpoint_id,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, resolver_rule_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a Resolver rule. Before you can delete a Resolver rule, you must disassociate it from all the VPCs that you associated the Resolver rule with. For more information, see DisassociateResolverRule.

    Args:
        resolver_rule_id(str): The ID of the Resolver rule that you want to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.delete(
                ctx, resolver_rule_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.delete resolver_rule_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="delete_resolver_rule",
        op_kwargs={"ResolverRuleId": resolver_rule_id},
    )


async def disassociate(hub, ctx, vpc_id: str, resolver_rule_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Removes the association between a specified Resolver rule and a specified VPC.  If you disassociate a Resolver rule from a VPC, Resolver stops forwarding DNS queries for the domain name that you specified in the Resolver rule.

    Args:
        vpc_id(str): The ID of the VPC that you want to disassociate the Resolver rule from.
        resolver_rule_id(str): The ID of the Resolver rule that you want to disassociate from the specified VPC.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.disassociate(
                ctx, vpc_id=value, resolver_rule_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.disassociate vpc_id=value, resolver_rule_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="disassociate_resolver_rule",
        op_kwargs={"VPCId": vpc_id, "ResolverRuleId": resolver_rule_id},
    )


async def get(hub, ctx, resolver_rule_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a specified Resolver rule, such as the domain name that the rule forwards DNS queries for and the ID of the outbound Resolver endpoint that the rule is associated with.

    Args:
        resolver_rule_id(str): The ID of the Resolver rule that you want to get information about.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.get(
                ctx, resolver_rule_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.get resolver_rule_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="get_resolver_rule",
        op_kwargs={"ResolverRuleId": resolver_rule_id},
    )


async def list_all(
    hub, ctx, max_results: int = None, next_token: str = None, filters: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists the Resolver rules that were created using the current AWS account.

    Args:
        max_results(int, optional): The maximum number of Resolver rules that you want to return in the response to a ListResolverRules request. If you don't specify a value for MaxResults, Resolver returns up to 100 Resolver rules. Defaults to None.
        next_token(str, optional): For the first ListResolverRules request, omit this value. If you have more than MaxResults Resolver rules, you can submit another ListResolverRules request to get the next group of Resolver rules. In the next request, specify the value of NextToken from the previous response. . Defaults to None.
        filters(List, optional): An optional specification to return a subset of Resolver rules, such as all Resolver rules that are associated with the same Resolver endpoint.  If you submit a second or subsequent ListResolverRules request and specify the NextToken parameter, you must use the same values for Filters, if any, as in the previous request. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="list_resolver_rules",
        op_kwargs={
            "MaxResults": max_results,
            "NextToken": next_token,
            "Filters": filters,
        },
    )


async def update(hub, ctx, resolver_rule_id: str, config: Dict) -> Dict:
    r"""
    **Autogenerated function**

    Updates settings for a specified Resolver rule. ResolverRuleId is required, and all other parameters are optional. If you don't specify a parameter, it retains its current value.

    Args:
        resolver_rule_id(str): The ID of the Resolver rule that you want to update.
        config(Dict): The new settings for the Resolver rule.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.rule.init.update(
                ctx, resolver_rule_id=value, config=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.rule.init.update resolver_rule_id=value, config=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="update_resolver_rule",
        op_kwargs={"ResolverRuleId": resolver_rule_id, "Config": config},
    )
