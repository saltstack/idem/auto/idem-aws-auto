"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(
    hub,
    ctx,
    creator_request_id: str,
    security_group_ids: List,
    direction: str,
    ip_addresses: List,
    name: str = None,
    tags: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Resolver endpoint. There are two types of Resolver endpoints, inbound and outbound:   An inbound Resolver endpoint forwards DNS queries to the DNS service for a VPC from your network.   An outbound Resolver endpoint forwards DNS queries from the DNS service for a VPC to your network.

    Args:
        creator_request_id(str): A unique string that identifies the request and that allows failed requests to be retried without the risk of running the operation twice. CreatorRequestId can be any unique string, for example, a date/time stamp. .
        name(str, optional): A friendly name that lets you easily find a configuration in the Resolver dashboard in the Route 53 console. Defaults to None.
        security_group_ids(List): The ID of one or more security groups that you want to use to control access to this VPC. The security group that you specify must include one or more inbound rules (for inbound Resolver endpoints) or outbound rules (for outbound Resolver endpoints). Inbound and outbound rules must allow TCP and UDP access. For inbound access, open port 53. For outbound access, open the port that you're using for DNS queries on your network.
        direction(str): Specify the applicable value:    INBOUND: Resolver forwards DNS queries to the DNS service for a VPC from your network    OUTBOUND: Resolver forwards DNS queries from the DNS service for a VPC to your network  .
        ip_addresses(List): The subnets and IP addresses in your VPC that DNS queries originate from (for outbound endpoints) or that you forward DNS queries to (for inbound endpoints). The subnet ID uniquely identifies a VPC. .
        tags(List, optional): A list of the tag keys and values that you want to associate with the endpoint. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.endpoint.init.create(
                ctx,
                creator_request_id=value,
                security_group_ids=value,
                direction=value,
                ip_addresses=value,
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.endpoint.init.create creator_request_id=value, security_group_ids=value, direction=value, ip_addresses=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="create_resolver_endpoint",
        op_kwargs={
            "CreatorRequestId": creator_request_id,
            "Name": name,
            "SecurityGroupIds": security_group_ids,
            "Direction": direction,
            "IpAddresses": ip_addresses,
            "Tags": tags,
        },
    )


async def delete(hub, ctx, resolver_endpoint_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes a Resolver endpoint. The effect of deleting a Resolver endpoint depends on whether it's an inbound or an outbound Resolver endpoint:    Inbound: DNS queries from your network are no longer routed to the DNS service for the specified VPC.    Outbound: DNS queries from a VPC are no longer routed to your network.

    Args:
        resolver_endpoint_id(str): The ID of the Resolver endpoint that you want to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.endpoint.init.delete(
                ctx, resolver_endpoint_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.endpoint.init.delete resolver_endpoint_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="delete_resolver_endpoint",
        op_kwargs={"ResolverEndpointId": resolver_endpoint_id},
    )


async def get(hub, ctx, resolver_endpoint_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Gets information about a specified Resolver endpoint, such as whether it's an inbound or an outbound Resolver endpoint, and the current status of the endpoint.

    Args:
        resolver_endpoint_id(str): The ID of the Resolver endpoint that you want to get information about.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.endpoint.init.get(
                ctx, resolver_endpoint_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.endpoint.init.get resolver_endpoint_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="get_resolver_endpoint",
        op_kwargs={"ResolverEndpointId": resolver_endpoint_id},
    )


async def list_all(
    hub, ctx, max_results: int = None, next_token: str = None, filters: List = None
) -> Dict:
    r"""
    **Autogenerated function**

    Lists all the Resolver endpoints that were created using the current AWS account.

    Args:
        max_results(int, optional): The maximum number of Resolver endpoints that you want to return in the response to a ListResolverEndpoints request. If you don't specify a value for MaxResults, Resolver returns up to 100 Resolver endpoints. . Defaults to None.
        next_token(str, optional): For the first ListResolverEndpoints request, omit this value. If you have more than MaxResults Resolver endpoints, you can submit another ListResolverEndpoints request to get the next group of Resolver endpoints. In the next request, specify the value of NextToken from the previous response. . Defaults to None.
        filters(List, optional): An optional specification to return a subset of Resolver endpoints, such as all inbound Resolver endpoints.  If you submit a second or subsequent ListResolverEndpoints request and specify the NextToken parameter, you must use the same values for Filters, if any, as in the previous request. . Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.endpoint.init.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.endpoint.init.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="list_resolver_endpoints",
        op_kwargs={
            "MaxResults": max_results,
            "NextToken": next_token,
            "Filters": filters,
        },
    )


async def update(hub, ctx, resolver_endpoint_id: str, name: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Updates the name of an inbound or an outbound Resolver endpoint.

    Args:
        resolver_endpoint_id(str): The ID of the Resolver endpoint that you want to update.
        name(str, optional): The name of the Resolver endpoint that you want to update. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.route53resolver.resolver.endpoint.init.update(
                ctx, resolver_endpoint_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.route53resolver.resolver.endpoint.init.update resolver_endpoint_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="route53resolver",
        operation="update_resolver_endpoint",
        op_kwargs={"ResolverEndpointId": resolver_endpoint_id, "Name": name},
    )
