"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict


async def update_multiple(hub, ctx, certificate_arn: str, options: Dict) -> None:
    r"""
    **Autogenerated function**

    Updates a certificate. Currently, you can use this function to specify whether to opt in to or out of recording your certificate in a certificate transparency log. For more information, see  Opting Out of Certificate Transparency Logging.

    Args:
        certificate_arn(str): ARN of the requested certificate to update. This must be of the form:  arn:aws:acm:us-east-1:account:certificate/12345678-1234-1234-1234-123456789012  .
        options(Dict): Use to update the options for your certificate. Currently, you can specify whether to add your certificate to a transparency log. Certificate transparency makes it possible to detect SSL/TLS certificates that have been mistakenly or maliciously issued. Certificates that have not been logged typically produce an error message in a browser. .
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm.certificate.option.update_multiple(
                ctx, certificate_arn=value, options=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm.certificate.option.update_multiple certificate_arn=value, options=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm",
        operation="update_certificate_options",
        op_kwargs={"CertificateArn": certificate_arn, "Options": options},
    )
