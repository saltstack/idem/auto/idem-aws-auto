"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import List


async def add(hub, ctx, certificate_arn: str, tags: List) -> None:
    r"""
    **Autogenerated function**

    Adds one or more tags to an ACM certificate. Tags are labels that you can use to identify and organize your Amazon Web Services resources. Each tag consists of a key and an optional value. You specify the certificate on input by its Amazon Resource Name (ARN). You specify the tag by using a key-value pair.  You can apply a tag to just one certificate if you want to identify a specific characteristic of that certificate, or you can apply the same tag to multiple certificates if you want to filter for a common relationship among those certificates. Similarly, you can apply the same tag to multiple resources if you want to specify a relationship among those resources. For example, you can add the same tag to an ACM certificate and an Elastic Load Balancing load balancer to indicate that they are both used by the same website. For more information, see Tagging ACM certificates.  To remove one or more tags, use the RemoveTagsFromCertificate action. To view all of the tags that have been applied to the certificate, use the ListTagsForCertificate action.

    Args:
        certificate_arn(str): String that contains the ARN of the ACM certificate to which the tag is to be applied. This must be of the form:  arn:aws:acm:region:123456789012:certificate/12345678-1234-1234-1234-123456789012  For more information about ARNs, see Amazon Resource Names (ARNs).
        tags(List): The key-value pair that defines the tag. The tag value is optional.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm.tags.to_certificate.add(
                ctx, certificate_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm.tags.to_certificate.add certificate_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm",
        operation="add_tags_to_certificate",
        op_kwargs={"CertificateArn": certificate_arn, "Tags": tags},
    )
