"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import List


async def remove(hub, ctx, certificate_arn: str, tags: List) -> None:
    r"""
    **Autogenerated function**

    Remove one or more tags from an ACM certificate. A tag consists of a key-value pair. If you do not specify the value portion of the tag when calling this function, the tag will be removed regardless of value. If you specify a value, the tag is removed only if it is associated with the specified value.  To add tags to a certificate, use the AddTagsToCertificate action. To view all of the tags that have been applied to a specific ACM certificate, use the ListTagsForCertificate action.

    Args:
        certificate_arn(str): String that contains the ARN of the ACM Certificate with one or more tags that you want to remove. This must be of the form:  arn:aws:acm:region:123456789012:certificate/12345678-1234-1234-1234-123456789012  For more information about ARNs, see Amazon Resource Names (ARNs).
        tags(List): The key-value pair that defines the tag to remove.
    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.acm.tags.from_certificate.remove(
                ctx, certificate_arn=value, tags=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.acm.tags.from_certificate.remove certificate_arn=value, tags=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="acm",
        operation="remove_tags_from_certificate",
        op_kwargs={"CertificateArn": certificate_arn, "Tags": tags},
    )
