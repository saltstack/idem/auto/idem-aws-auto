"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def create(hub, ctx, tags: Dict = None, whitelist_rules: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Creates a Input Security Group

    Args:
        tags(Dict, optional): A collection of key-value pairs. Defaults to None.
        whitelist_rules(List, optional): List of IPv4 CIDR addresses to whitelist. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.input_.security_group.create(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.input_.security_group.create
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="create_input_security_group",
        op_kwargs={"Tags": tags, "WhitelistRules": whitelist_rules},
    )


async def delete(hub, ctx, input_security_group_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Deletes an Input Security Group

    Args:
        input_security_group_id(str): The Input Security Group to delete.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.input_.security_group.delete(
                ctx, input_security_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.input_.security_group.delete input_security_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="delete_input_security_group",
        op_kwargs={"InputSecurityGroupId": input_security_group_id},
    )


async def describe(hub, ctx, input_security_group_id: str) -> Dict:
    r"""
    **Autogenerated function**

    Produces a summary of an Input Security Group

    Args:
        input_security_group_id(str): The id of the Input Security Group to describe.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.input_.security_group.describe(
                ctx, input_security_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.input_.security_group.describe input_security_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="describe_input_security_group",
        op_kwargs={"InputSecurityGroupId": input_security_group_id},
    )


async def list_all(hub, ctx, max_results: int = None, next_token: str = None) -> Dict:
    r"""
    **Autogenerated function**

    Produces a list of Input Security Groups for an account

    Args:
        max_results(int, optional): Placeholder documentation for MaxResults. Defaults to None.
        next_token(str, optional): Placeholder documentation for __string. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.input_.security_group.list_all(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.input_.security_group.list_all
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="list_input_security_groups",
        op_kwargs={"MaxResults": max_results, "NextToken": next_token},
    )


async def update(
    hub,
    ctx,
    input_security_group_id: str,
    tags: Dict = None,
    whitelist_rules: List = None,
) -> Dict:
    r"""
    **Autogenerated function**

    Update an Input Security Group's Whilelists.

    Args:
        input_security_group_id(str): The id of the Input Security Group to update.
        tags(Dict, optional): A collection of key-value pairs. Defaults to None.
        whitelist_rules(List, optional): List of IPv4 CIDR addresses to whitelist. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.input_.security_group.update(
                ctx, input_security_group_id=value
            )

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.input_.security_group.update input_security_group_id=value
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="update_input_security_group",
        op_kwargs={
            "InputSecurityGroupId": input_security_group_id,
            "Tags": tags,
            "WhitelistRules": whitelist_rules,
        },
    )
