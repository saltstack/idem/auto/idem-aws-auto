"""
Autogenerated module using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__
"""
from typing import Dict
from typing import List


async def batch(hub, ctx, channel_ids: List = None, multiplex_ids: List = None) -> Dict:
    r"""
    **Autogenerated function**

    Stops running resources

    Args:
        channel_ids(List, optional): List of channel IDs. Defaults to None.
        multiplex_ids(List, optional): List of multiplex IDs. Defaults to None.

    Returns:
        Dict

    Examples:
        Call from code:

        .. code-block:: python

            await hub.exec.aws_auto.medialive.stop.batch(ctx)

        Call from CLI:

        .. code-block:: bash

            $ idem exec aws_auto.medialive.stop.batch
    """
    return await hub.tool.aws.client.exec(
        ctx,
        service_name="medialive",
        operation="batch_stop",
        op_kwargs={"ChannelIds": channel_ids, "MultiplexIds": multiplex_ids},
    )
